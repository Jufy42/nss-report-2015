//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NSS_2015_Report
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReportGeneration
    {
        public System.Guid GenerationID { get; set; }
        public System.Guid UserID { get; set; }
        public System.DateTime DateDrawn { get; set; }
        public System.Guid ReportHistoryID { get; set; }
        public string ReportType { get; set; }
        public string OutputType { get; set; }
        public Nullable<bool> IsVisible { get; set; }
    }
}
