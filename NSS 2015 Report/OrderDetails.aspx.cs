﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NSS_2015_Report
{
    public partial class OrderDetails : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (GetCurrentUser() == null) Response.Redirect("~/login");
                else
                {
                    User user = GetCurrentUser();
                    lblUser.Text = user.Name;

                    Guid HistoryID = new Guid(RouteData.Values["ReportHistoryID"].ToString());

                    ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == HistoryID).SingleOrDefault();

                    string reportname = history.ReportName;
                    lblCluster.Text = history.Cluster;
                    lblYear1Division1.Text = history.Y1Division1;
                    lblYear1Division2.Text = history.Y1Division2;
                    lblYear1Division3.Text = history.Y1Division3;
                    lblYear1Division4.Text = history.Y1Division4;
                    lblYear1Division5.Text = history.Y1Division5;
                    lblYear1Division6.Text = history.Y1Division6;
                    lblYear1Branch.Text = history.Y1Branch;
                    lblYear2Division1.Text = history.Y2Division1;
                    lblYear2Division2.Text = history.Y2Division2;
                    lblYear2Division3.Text = history.Y2Division3;
                    lblYear2Division4.Text = history.Y2Division4;
                    lblYear2Division5.Text = history.Y2Division5;
                    lblYear2Division6.Text = history.Y2Division6;
                    lblYear2Branch.Text = history.Y2Branch;

                    string sample = lblCluster.Text == "All" ? "Post Sample" : "";
                    string year2 = history.Year2;

                    lblLocation.Text = history.Location;
                    lblGeneralClassification.Text = history.Classification;
                    lblJobFamily.Text = history.JobFamily;
                    lblLOS.Text = history.LOS;
                    lblRegion.Text = history.Region;
                    lblMngPassage.Text = history.ManagementPassage;
                    lblRace.Text = history.Race;
                    lblOccLevel.Text = history.OccupationalLevel;
                    lblGender.Text = history.Gender;

                    lblYear.Text = "2015" + (year2 == "" ? "" : " vs " + year2);

                    lblReportName.Text = reportname + " " + sample;
                }
            }
        }

        protected void cmdBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }

        protected void cmdOrder_Click(object sender, EventArgs e)
        {
            if (txtBranchCode.Text == "")
                AlertShow("Please supply a branch number.");
            else if (txtPerson.Text == "")
                AlertShow("Please supply a contact person.");
            else if (txtEmail.Text == "")
                AlertShow("Please supply an email address.");
            else if (txtEmailConfirm.Text != txtEmail.Text)
                AlertShow("The email address is not the same as the confirmed email address.");
            else if (txtFOName.Text == "")
                AlertShow("Please supply the financial officer`s name.");
            else if (txtFOEmail.Text == "")
                AlertShow("Please supply the financial officer`s email address.");
            else if (txtFOEmailConfirm.Text != txtFOEmail.Text)
                AlertShow("The financial officer`s email address is not the same as the confirmed financial officer`s email address.");
            else
            {
                if (GetCurrentUser() == null) Response.Redirect("~/login");
                else
                {
                    OrderDetail od = new OrderDetail();
                    od.OrderID = Guid.NewGuid();
                    od.ManualInvoiceNumber = "";
                    od.userSeq = UserID;
                    od.OrderSeq = db.OrderStatus.Where(p => p.Description == "Ordered").SingleOrDefault().OrderSeq;
                    od.InvoiceSeq = db.InvoiceStatus.Where(p => p.Description == "Processing").SingleOrDefault().InvoiceSeq;
                    od.FilterSelection = new Guid(RouteData.Values["ReportHistoryID"].ToString());
                    od.BranchCode = txtBranchCode.Text;
                    od.ContactPerson = txtPerson.Text;
                    od.Email = txtEmail.Text;
                    od.FOName = txtFOName.Text;
                    od.FOEmail = txtFOEmail.Text;
                    od.DateRequested = DateTime.Now;
                    od.DateLastUpdated = DateTime.Now;
                    db.OrderDetails.Add(od);
                    db.SaveChanges();

                    SendMail(od.Seq, txtFOEmail.Text, UserID);

                    AlertShow("Thank you for submitting your order. An email will be sent to you shortly.");
                }
            }
        }

        private void SendMail(int eInvoiceNumber, string FOEmail, Guid userid)
        {
            try
            {
                HttpCookie cookie = Request.Cookies.Get("NedbankReportCookie");

                string message = "Hi " + db.Users.Where(p => p.UserID == userid).Select(p => p.Name).SingleOrDefault() + ",<br/><br/>" + "The report has been successfully ordered. Your electronic invoice number is " + eInvoiceNumber + "." +
                    "<br/>" + "Please click on the following link to view your report status." +
                    "<br/>" + GetSiteRoot() + "/orders ." +
                    "<br/><br/>Kind Regards," +
                    "<br/>The Pure Survey Team" +
                    "<br/>Support: 021 788 6856" +
                    "<br/><a href='mailto:support@puresurvey.co.za'>support@puresurvey.co.za</a>" +
                    "<br/>Web:      http://www.puresurvey.co.za";

                MailMessage mail = new MailMessage();
                mail.Subject = "Nedbank Staff Survey Report Order";

                mail.Body = message;
                mail.BodyEncoding = Encoding.Default;
                mail.IsBodyHtml = true;

                mail.To.Add(db.Users.Where(p => p.UserID == userid).SingleOrDefault().Email);

                if (lblCluster.Text == "Nedbank Retail")
                    mail.CC.Add(ConfigurationManager.AppSettings["EmailTo"].ToString());

                mail.CC.Add(FOEmail);

                mail.From = new MailAddress("noreply@puresurvey.co.za", "Nedbank Survey");

                SmtpClient mailClient = new SmtpClient();
                mailClient.Send(mail);

                mail = null;
                mailClient = null;
            }
            catch (SmtpException ex)
            {
                EventLog.WriteEntry("Nedbank Report Error:SendMail", ex.Message);
            }
        }
    }
}