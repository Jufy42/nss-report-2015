﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="History.aspx.cs" Inherits="NSS_2015_Report.History" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $(function () {
            $("#<%=fromdate.ClientID%>").datepicker();
            $("#<%=todate.ClientID%>").datepicker();
        });

        function showConfirmRequest(callBackFunction, title, content) {
            $("#divConfirm").html(content).dialog({
                autoOpen: true,
                modal: true,
                title: title,
                draggable: true,
                resizable: false,
                close: function (event, ui) { $(this).dialog("destroy"); },
                buttons: {
                    'Ok': function () { callBackFunction(); },
                    'Cancel': function () {
                        $(this).dialog("destroy");
                    }
                },
                overlay: {
                    opacity: 0.45,
                    background: "black"
                }
            });
        }
    </script>
    <center>  
        <div style="width:100%;">
            <asp:Panel ID="pnlCriteria" runat="server" BackColor="#BFCF89" HorizontalAlign="Center" Width="100%">
                <table style="width:100%" cellpadding="5" cellspacing="0">
                    <tr>
                        <td align="center"><h2>Report History</h2></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td style="text-align:left;">From :</td>
                                    <td style="text-align:left;"><asp:TextBox ID="fromdate" runat="server"></asp:TextBox></td>
                                    <td style="text-align:left;">To :</td>
                                    <td style="text-align:left;"><asp:TextBox ID="todate" runat="server"></asp:TextBox></td>
                                    <td style="text-align:left;">Type :</td>
                                    <td style="text-align:left;">
                                        <asp:DropDownList ID="ddlType" runat="server">
                                            <asp:ListItem>All</asp:ListItem>
                                            <asp:ListItem>Comprehensive</asp:ListItem>
                                            <asp:ListItem>EE Barrier</asp:ListItem>
                                            <asp:ListItem>Executive Summary</asp:ListItem>
                                            <asp:ListItem>Infograph 1</asp:ListItem>
                                            <asp:ListItem>Infograph 2</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td><asp:Button ID="cmdSearch" runat="server" Text="Search" onclick="cmdSearch_Click" CssClass="btn" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="width:100%;Height:300px;margin: 0 auto;overflow-y:scroll;text-align:center;">
                            <table cellpadding="2" cellspacing="0" style="border:1px solid #6F6F6F;width:100%;">
                                <tr style="font-weight:bold;color:White;background-color:#53b606;">
                                    <td style="border:1px solid #6F6F6F;width:75px;">Date</td>
                                    <td id="tdNameHeader" runat="server" style="border:1px solid #6F6F6F;width:150px;">Name</td>
                                    <td style="border:1px solid #6F6F6F;width:300px;">Report Name</td>
                                    <td style="border:1px solid #6F6F6F;width:100px;">Report Type</td>
                                    <td style="border:1px solid #6F6F6F;width:100px;">Output Type</td>
                                    <td style="border:1px solid #6F6F6F;width:100px;">Sample Size</td>
                                    <td style="border:1px solid #6F6F6F;width:300px;">Actions</td>
                                </tr>
                                <asp:Repeater ID="rptHistory" runat="server" OnItemDataBound="rptHistory_ItemDataBound">
                                    <ItemTemplate>
                                        <tr style="background-color:#fff;">
                                            <td style="border:1px solid #6F6F6F;width:75px;"><%#Eval("Date")%></td>
                                            <td id="tdName" runat="server" style="border:1px solid #6F6F6F;width:150px;"><%#Eval("Name") %></td>
                                            <td style="border:1px solid #6F6F6F;width:300px;"><%#Eval("ReportName")%></td>
                                            <td style="border:1px solid #6F6F6F;width:100px;"><%#Eval("ReportType")%></td>
                                            <td style="border:1px solid #6F6F6F;width:100px;"><%#Eval("OutputType")%></td>
                                            <td style="border:1px solid #6F6F6F;width:100px;"><%#Eval("SampleSize")%></td>
                                            <td style="border:1px solid #6F6F6F;width:300px;"><asp:HiddenField ID="hdReportGenerationID" runat="server" Value='<%#Eval("ReportGenerationID") %>' /><asp:LinkButton ID="cmdView" runat="server" OnClick="cmdView_Click" Text="View"></asp:LinkButton>&nbsp;<asp:LinkButton ID="cmdOnline" runat="server" Text="Online" OnClick="cmdOnline_Click"></asp:LinkButton>&nbsp;<asp:LinkButton ID="cmdPDF" runat="server" Text="PDF" OnClick="cmdPDF_Click"></asp:LinkButton>&nbsp;<asp:LinkButton ID="cmdPPT" runat="server" Text="PPT" OnClick="cmdPPT_Click"></asp:LinkButton>&nbsp;<asp:LinkButton ID="cmdExcel" runat="server" Text="Excel" OnClick="cmdExcel_Click"></asp:LinkButton>&nbsp;<asp:LinkButton ID="cmdRemove" runat="server" Text="Remove" OnClick="cmdRemove_Click"></asp:LinkButton></td>
                                        </tr>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tr style="background-color:#ececd8;">
                                            <td style="border:1px solid #6F6F6F;width:75px;"><%#Eval("Date")%></td>
                                            <td id="tdName" runat="server" style="border:1px solid #6F6F6F;width:150px;"><%#Eval("Name") %></td>
                                            <td style="border:1px solid #6F6F6F;width:300px;"><%#Eval("ReportName")%></td>
                                            <td style="border:1px solid #6F6F6F;width:100px;"><%#Eval("ReportType")%></td>
                                            <td style="border:1px solid #6F6F6F;width:100px;"><%#Eval("OutputType")%></td>
                                            <td style="border:1px solid #6F6F6F;width:100px;"><%#Eval("SampleSize")%></td>
                                            <td style="border:1px solid #6F6F6F;width:300px;"><asp:HiddenField ID="hdReportGenerationID" runat="server" Value='<%#Eval("ReportGenerationID") %>' /><asp:LinkButton ID="cmdView" runat="server" OnClick="cmdView_Click" Text="View"></asp:LinkButton>&nbsp;<asp:LinkButton ID="cmdOnline" runat="server" Text="Online" OnClick="cmdOnline_Click"></asp:LinkButton>&nbsp;<asp:LinkButton ID="cmdPDF" runat="server" Text="PDF" OnClick="cmdPDF_Click"></asp:LinkButton>&nbsp;<asp:LinkButton ID="cmdPPT" runat="server" Text="PPT" OnClick="cmdPPT_Click"></asp:LinkButton>&nbsp;<asp:LinkButton ID="cmdExcel" runat="server" Text="Excel" OnClick="cmdExcel_Click"></asp:LinkButton>&nbsp;<asp:LinkButton ID="cmdRemove" runat="server" Text="Remove" OnClick="cmdRemove_Click"></asp:LinkButton></td>
                                        </tr>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                            </table> 
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center" colspan="2"><asp:Button ID="cmdBack" runat="server" Text="Return to filter selection" CssClass="btn" onclick="cmdBack_Click" /></td>
                    </tr>
                </table>
                <iframe id="iframe" src="" runat="server" width="0px" height="0px"></iframe>
            </asp:Panel>
        </div>
    </center>
    <div id="divConfirm" class="ModalWindow"></div>
</asp:Content>
