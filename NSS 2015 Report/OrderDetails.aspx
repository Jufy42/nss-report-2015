﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OrderDetails.aspx.cs" Inherits="NSS_2015_Report.OrderDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <center>                      
        <div style="width:100%;">
            <asp:Panel ID="pnlCriteria" runat="server" style="background:#BFCF89;text-align:center;">
                <table style="width:100%;" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="text-align:center" colspan="2"><h2>Order Details</h2></td>
                    </tr>
                    <tr>
                        <td style="text-align:center" colspan="2">Please select the "Report Type" you wish to order and click on the "Order Report" button to submit your request.</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 15px;text-align:left;vertical-align:top;">
                            <fieldset style="height: 250px;border:1px solid black;-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;">
                                <legend><b>Report Type</b></legend>
                                <asp:RadioButtonList ID="lstReportType" runat="server" RepeatDirection="Vertical" CssClass="RadioButton" AutoPostBack="True">
                                        <asp:ListItem Value="1" Selected="True">Demographic Splits Report</asp:ListItem>
                                        <asp:ListItem Value="2">EE Barrier Excel Report</asp:ListItem>
                                </asp:RadioButtonList>
                            </fieldset>
                        </td>
                        <td style="padding-right: 15px;vertical-align:top;">
                            <fieldset style="height: 250px;border:1px solid black;-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;">
                                <legend><b>Pricing</b></legend>
                                <table style="width:100%;" cellpadding="2" cellspacing="0">
                                    <tr>
                                        <td style="text-align:left;">Price:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblPrice" runat="server" Text="R3,400"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Branch No:</td>
                                        <td style="text-align:left;"><asp:TextBox ID="txtBranchCode" runat="server" MaxLength="20" Width="300px"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Contact Person:</td>
                                        <td style="text-align:left;"><asp:TextBox ID="txtPerson" runat="server" MaxLength="20" Width="300px"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Contact e-mail:</td>
                                        <td style="text-align:left;"><asp:TextBox ID="txtEmail" runat="server" Width="300px"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Confirm e-mail address:</td>
                                        <td style="text-align:left;"><asp:TextBox ID="txtEmailConfirm" runat="server" Width="300px"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Financial Officer’s Name:</td>
                                        <td style="text-align:left;"><asp:TextBox ID="txtFOName" runat="server" MaxLength="20" Width="300px"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Financial Officer’s e-mail:</td>
                                        <td style="text-align:left;"><asp:TextBox ID="txtFOEmail" runat="server" Width="300px"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Confirm Financial Officer’s e-mail:</td>
                                        <td style="text-align:left;"><asp:TextBox ID="txtFOEmailConfirm" runat="server" Width="300px"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 15px;padding-right: 15px;text-align:left;vertical-align:top;">
                            <fieldset style="border:1px solid black;-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;">
                                <legend><b>Applied Filters</b></legend>
                                <table style="width:100%" cellpadding="2" cellspacing="0">
                                    <tr>
                                        <td style="text-align:left;">Generated by:</td>
                                        <td style="text-align:left;vertical-align:top;" colspan="3"><asp:Label ID="lblUser" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Report Name:</td>
                                        <td style="text-align:left;vertical-align:top;" colspan="3"><asp:Label ID="lblReportName" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Cluster:</td>
                                        <td style="text-align:left;vertical-align:top;" colspan="3"><asp:Label ID="lblCluster" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr>                                                                                                    
                                        <td style="text-align:left;vertical-align:top;" colspan="2"><b>Applied Division Filters for <asp:Label ID="lblYear1" runat="server"></asp:Label> :</b></td>
                                        <td style="text-align:left;vertical-align:top;" colspan="2"><b>Applied Division Filters for <asp:Label ID="lblYear2" runat="server"></asp:Label> :</b></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Division 1:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Division1" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Division 1:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Division1" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Division 2:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Division2" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Division 2:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Division2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Division 3:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Division3" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Division 3:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Division3" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Division 4:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Division4" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Division 4:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Division4" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Division 5:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Division5" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Division 5:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Division5" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Division 6:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Division6" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Division 6:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Division6" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Branch:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Branch" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Branch:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Branch" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Location:</td>
                                        <td style="text-align:left;vertical-align:top;" colspan="3"><asp:Label ID="lblLocation" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Generational Classification:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblGeneralClassification" runat="server"></asp:Label></td>
                                        <td style="text-align:left;">Job Family:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblJobFamily" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Length of service:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblLOS" runat="server"></asp:Label></td>
                                        <td style="text-align:left;">Region:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblRegion" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Management Passage:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblMngPassage" runat="server"></asp:Label></td>
                                        <td style="text-align:left;">Race:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblRace" runat="server"></asp:Label></td>           
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">EE Occupational Level:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblOccLevel" runat="server"></asp:Label></td>
                                        <td style="text-align:left;">Gender:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblGender" runat="server"></asp:Label></td> 
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Compare Year:</td>
                                        <td style="text-align:left;" colspan="3"><asp:Label ID="lblYear" runat="server"></asp:Label></td>    
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" colspan="2"><b><i>Please note it will take 3 – 4 working days to complete the report.<br />No orders will be processed until proof of payment is provided. Once proof of payment is received we will process the report order.</i></b></td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" colspan="2"><asp:Button ID="cmdOrder" runat="server" Text="Order Report" CssClass="btn" OnClick="cmdOrder_Click" /><asp:Button ID="cmdBack" runat="server" Text="Return to filter selection" CssClass="btn" onclick="cmdBack_Click" /></td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </center>
</asp:Content>
