﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NSS_2015_Report
{
    public class Generic
    {
        public static List<string> newDimensions()
        {
            return new List<string>() { "engagement", "employment equity" };
        }

        public static List<string> AfricaChangeDimensions()
        {
            return new List<string>() { "change & transformation", "diversity", "relationships & trust" };
        }

        public static List<string> ExclQuestions(string year2, List<Dimension> dimensions)
        {
            NSS2015Report db = new NSS2015Report();

            if (Convert.ToDouble(year2.ToLower() == "none" ? "0" : year2) <= 2011)
            {
                List<string> returnlist = new List<string>();
                foreach (string dimension in Generic.newDimensions())
                    returnlist.AddRange(dimensions.Where(p => p.DimensionText.ToLower() == dimension.ToLower()).SingleOrDefault().DimensionQuestions2014.Split('|').ToList());

                return returnlist;
            }
            else
                return new List<string>();
        }
    }

    public static class StringExtension
    {
        public static string NullCheck(this object source)
        {
            if (source == null) return "Not Specified"; else return source.ToString();
        }

        public static string NewDimension(this object source, string year2)
        {
            if (Convert.ToDouble(year2.ToLower() == "none" ? "0" : year2) <= 2011 && (Generic.newDimensions().Contains(source.ToString().ToLower()))) return source.ToString() + " (New Dimension in 2012)"; else return source.ToString();
        }

        public static string AfricaNewDimension(this object source, string year2, bool africa)
        {
            if (!africa) return source.ToString();
            else if (Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) <= 2012 && (Generic.AfricaChangeDimensions().Contains(source.ToString().ToLower()))) return source.ToString() + "**"; else return source.ToString();
        }
    }
}