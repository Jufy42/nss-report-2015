﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NSS_2015_Report
{
    public static class NPS
    {
        public static string getNPSHeaderRows(string label, List<Year> Year1Dataset, string npstype)
        {
            double detractor, passive, promoter, count;
            detractor = passive = promoter = count = 0;

            detractor = Year1Dataset.Where(p => Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) <= 6).Count();
            passive = Year1Dataset.Where(p => Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) == 7 || Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) == 8).Count();
            promoter = Year1Dataset.Where(p => Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) == 9 || Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) == 10).Count();
            count = Year1Dataset.Count();

            if (count >= 10)
                return "<tr style='font-weight:bold;'><td align='left'>" + label + "</td><td width='50px' align='center' style='padding-right:10px'>" + count + "</td>" + getNPSCell(Math.Round(promoter / count * 100, 1, MidpointRounding.AwayFromZero) - Math.Round(detractor / count * 100, 1, MidpointRounding.AwayFromZero), true, count) + getNPSCell(Math.Round(detractor / count * 100, 1, MidpointRounding.AwayFromZero), false, count) + getNPSCell(Math.Round(passive / count * 100, 1, MidpointRounding.AwayFromZero), false, count) + getNPSCell(Math.Round(promoter / count * 100, 1, MidpointRounding.AwayFromZero), false, count) + "</tr>";
            else
                return "<tr style='font-weight:bold;'><td align='left'>" + label + "</td><td width='50px' align='center' style='padding-right:10px'>" + count + "</td>" + getNPSCell(0, false, 0) + getNPSCell(0, false, 0) + getNPSCell(0, false, 0) + getNPSCell(0, false, 0) + "</tr>";
        }

        public static string getNPSCell(double Value, bool color, double count)
        {
            string colour = "#FFFFFF";

            if (Value < 0) colour = "#FF0000";

            return "<td style='height:20px;text-align: center;font-family: Verdana; font-size: 10px;background-color:" + (color ? colour : "#fff") + ";color:" + (color && colour == "#FF0000" ? "#fff" : "#000") + ";border:1px solid black;width:35px'><b>" + (count == 0 ? "&nbsp;" : Math.Round(Value, 1, MidpointRounding.AwayFromZero).ToString("0.0")) + "</b></td>";
        }

        public static string GenerateNPSChart(string reportname, List<Year> year1Dataset, string property, string propertyname, string headerrows, int headercount, string npstype, string year2, ref int pageno, bool execsum, bool pdf, bool previous)
        {
            List<string> items = year1Dataset.Select(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck()).Distinct().ToList().OrderBy(p => p).ToList();

            double detractor, passive, promoter, count;
            detractor = passive = promoter = count = 0;

            detractor = year1Dataset.Where(p => Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) <= 6).ToList().Where(p => (property.ToLower() == "cluster" ? p.IsSample == true : true)).Count();
            passive = year1Dataset.Where(p => (Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) == 7 || Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) == 8) && (property.ToLower() == "cluster" ? p.IsSample == true : true)).Count();
            promoter = year1Dataset.Where(p => (Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) == 9 || Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) == 10) && (property.ToLower() == "cluster" ? p.IsSample == true : true)).Count();
            count = year1Dataset.Where(p => property.ToLower() == "cluster" ? p.IsSample == true : true).Count();

            double nps = Math.Round(promoter / count * 100, 1, MidpointRounding.AwayFromZero) - Math.Round(detractor / count * 100, 1, MidpointRounding.AwayFromZero);
            double totalcount = count;

            string returnstring = "";

            for (int i = 0; i < items.Count / (15 - headercount) + (items.Count % (15 - headercount) > 0 ? 1 : 0); i++)
            {
                string body = "<table cellpadding='2' cellspacing='0' style=\"width:600px;vertical-align:top;\"><tr><td>&nbsp;</td><td width='50px' align='center' style='padding-right:10px'>n</td><td width='35px' align='center'><b>NPS</b></td><td width='115px' colspan='3'><img src='" + GetSiteRoot() + "/Images/NPSHeader.png'/></td></tr>";

                body += headerrows;

                foreach (string item in items.Skip((i) * (15 - headercount)).Take((15 - headercount)))
                {
                    detractor = passive = promoter = count = 0;

                    detractor = year1Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item && Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) <= 6).Count();
                    passive = year1Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item && (Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) == 7 || Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) == 8)).Count();
                    promoter = year1Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item && (Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) == 9 || Convert.ToInt32(p.GetType().GetProperty("QNPS" + npstype).GetValue(p, null).ToString()) == 10)).Count();
                    count = year1Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item).Count();

                    if (count >= 10)
                        body += "<tr><td align='left'>" + item + "</td><td width='50px' align='center' style='padding-right:10px'>" + count + "</td>" + getNPSCell(Math.Round(promoter / count * 100, 1, MidpointRounding.AwayFromZero) - Math.Round(detractor / count * 100, 1, MidpointRounding.AwayFromZero), true, count) + getNPSCell(Math.Round(detractor / count * 100, 1, MidpointRounding.AwayFromZero), false, count) + getNPSCell(Math.Round(passive / count * 100, 1, MidpointRounding.AwayFromZero), false, count) + getNPSCell(Math.Round(promoter / count * 100, 1, MidpointRounding.AwayFromZero), false, count) + "</tr>";
                    else
                        body += "<tr><td align='left'>" + item + "</td><td width='50px' align='center' style='padding-right:10px'>" + count + "</td>" + getNPSCell(0, false, 0) + getNPSCell(0, false, 0) + getNPSCell(0, false, 0) + getNPSCell(0, false, 0) + "</tr>";
                }

                body += "</table></td><td><table cellpadding='2' cellspacing='0' width='300px'><tr><td><img src='" + GetSiteRoot() + "/Images/NPSLegend1.jpg' width='350px' /></td></tr><tr><td><img src='" + GetSiteRoot() + "/Images/NPSLegend2.jpg' width='350px' /></td></tr><tr><td align='left' style='font-style: italic'><fieldset><legend><b>Definitions</b></legend><table cellpadding='0' cellspacing='0' style='font-size: 9px;'><tr><td>No Value : scores not shown due to insufficient sample size</td></tr></tr><tr><td>n = total number of participants that completed the NPS question.</td></tr></table></fieldset></td></tr><tr><td>&nbsp;</td></tr></table>";

                returnstring += PageTemplate.Template("Likelihood that you would recommend Nedbank as a “great place to " + (npstype == "1" ? "bank" : "work") + "”? " + (previous ? "2014 Scores " : "") + (items.Count + headercount < 15 ? "" : i > 0 ? "(Continued)" : ""), reportname, "<tr><td align='center' colspan='2'><h3><u>NPS - Great Place to " + (npstype == "1" ? "bank" : "work") + " (" + reportname + " n=" + totalcount + "): " + Math.Round(nps, 1, MidpointRounding.AwayFromZero).ToString() + "%</u></h3></td></tr><tr><td>&nbsp;</td></tr><tr><td align='center' style=\"height:375px;vertical-align:top;\">" + body + "</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td colspan='2'><b>Note</b><table><tr><td style=\"vertical-align:bottom;text-align:left;\">• Group NPS score excludes African and International countries</td></tr><tr><td style=\"vertical-align:bottom;text-align:left;\">• Cluster sample sizes and scores are based on total cluster response rates (and where applicable including African and international countries).</td></tr>" + (!execsum ? "<tr><td style=\"text-align:left;\">• Refer to Appendix for " + year2 + " comparative</td></tr>" : "") + "</table></td></tr>", pdf, pageno++, false);
            }

            return returnstring;
        }

        public static string GetSiteRoot()
        {
            string port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == null || port == "80" || port == "443")
                port = "";
            else
                port = ":" + port;

            string protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
                protocol = "http://";
            else
                protocol = "https://";

            string sOut = protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + System.Web.HttpContext.Current.Request.ApplicationPath;

            if (sOut.EndsWith("/"))
            {
                sOut = sOut.Substring(0, sOut.Length - 1);
            }

            return sOut;
        }
    }
}