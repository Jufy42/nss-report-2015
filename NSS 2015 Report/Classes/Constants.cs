﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NSS_2015_Report
{
    public class Constants
    {
        public ComprehensiveDimension Nedbank2015 { get; set; }
        public ComprehensiveDimension Nedbank2014 { get; set; }
        public ComprehensiveDimension Nedbank2013 { get; set; }
        public ComprehensiveDimension Nedbank2012 { get; set; }
        public ComprehensiveDimension Nedbank2011 { get; set; }
        public ComprehensiveDimension Nedbank2010 { get; set; }
        public ComprehensiveDimension Nedbank2009 { get; set; }
        public ComprehensiveDimension Nedbank2008 { get; set; }
        public ComprehensiveDimension Nedbank2007 { get; set; }

        public Constants() { }

        public Constants(string cluster, List<string> location)
        {
            Nedbank2015 = new ComprehensiveDimension();
            Nedbank2014 = new ComprehensiveDimension();
            Nedbank2013 = new ComprehensiveDimension();
            Nedbank2012 = new ComprehensiveDimension();
            Nedbank2011 = new ComprehensiveDimension();
            Nedbank2010 = new ComprehensiveDimension();
            Nedbank2009 = new ComprehensiveDimension();
            Nedbank2008 = new ComprehensiveDimension();
            Nedbank2007 = new ComprehensiveDimension();

            Nedbank2015.CompDimensions = new List<ReportDimension>();
            Nedbank2014.CompDimensions = new List<ReportDimension>();
            Nedbank2013.CompDimensions = new List<ReportDimension>();
            Nedbank2012.CompDimensions = new List<ReportDimension>();
            Nedbank2011.CompDimensions = new List<ReportDimension>();
            Nedbank2010.CompDimensions = new List<ReportDimension>();
            Nedbank2009.CompDimensions = new List<ReportDimension>();
            Nedbank2008.CompDimensions = new List<ReportDimension>();
            Nedbank2007.CompDimensions = new List<ReportDimension>();

            switch (cluster)
            {
                case "NEDBANK CAPITAL":
                    {
                        if (location.Join(new List<string>() { "South Africa", "Not Specified" }, loc => loc.Trim().ToLower(), list => list.Trim().ToLower(), (loc, list) => new { loc }).Distinct().Count() > 0)
                        {
                            Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 70.6);
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 74.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 78.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 75.8));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 73.7));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 73.6));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 80.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 71.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 59.4));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 64.5));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.7));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 69.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 59.7));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 68.0));

                            Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 70.2);
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 75.2));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 77.9));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 76.6));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 74.1));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.7));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 71.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 77.6));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 71.9));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 57.9));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 66.6));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 66.8));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 68.1));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 60.3));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 66.5));

                            Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 67.9);
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 72.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 74.6));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 72.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 72.0));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 73.3));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 71.1));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 78.9));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 67.6));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 53.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 63.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 64.4));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 66.3));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 58.0));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 62.7));

                            Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 67.2);
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 70));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 71.7));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 73.2));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 72.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 70.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 69.6));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 76.6));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 67.3));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 56.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 63.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 65.8));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 64.9));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 59.6));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 61.5));

                            Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 69.7);
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 84.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 67.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 70.2));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 71.3));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 66.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 75.0));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 77.2));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 71.4));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 60.3));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 61.4));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.5));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 59.8));

                            Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 71.1);
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 85.8));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 73.7));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 72.3));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 75.5));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.3));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 74.0));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 76.8));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 72.2));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 63.8));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 61.7));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.2));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 59.4));

                            Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 67.9);
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 84.4));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 71.5));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 70.2));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 71.5));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 67.7));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 70.1));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 74.4));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 68.9));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 59.3));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 59.6));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 67.6));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 55.0));
                        }
                        else
                        {
                            Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 69.4);
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 73.4));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 76.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 74.6));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 72.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 69.6));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 72.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 78.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 70.6));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 58.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 63.5));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.8));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 68.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 58.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 66.8));

                            Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 69.6);
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 74.7));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 77.3));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 75.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 73.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 70.3));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 70.7));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 77.3));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 70.9));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 57.6));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 65.8));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 67.1));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.7));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 59.7));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 66.2));

                            Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 67.6);
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 71.9));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 73.8));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 71.9));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 71.8));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 71.9));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 70.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 79.0));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 67.1));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 53.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 63.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 64.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 66.3));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 57.6));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 63.4));

                            Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 66.7);
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 69.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 71));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 72.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 72.2));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 69));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 69.1));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 76.3));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 66.8));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 56.2));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 62.8));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 66.1));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 64.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 58.7));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 61.4));

                            Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 68.7);
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 83.2));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 66.4));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 68.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 70.1));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 64.9));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 74.0));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 76.4));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 70.2));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 59.1));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 60.5));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.5));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 59.4));

                            Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 69.5);
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 83.6));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 71.5));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 70.2));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 74.0));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 69.8));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 72.5));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 75.8));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 70.5));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 62.0));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 60.4));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 68.6));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 58.6));

                            Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 67.5);
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 83.0));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 70.0));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 69.5));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 70.7));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 66.3));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 69.8));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 74.5));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 68.3));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 58.6));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 59.1));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 68.2));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 54.9));

                            Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 67.8);
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 79.2));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 64.7));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 68.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 72.5));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 66.8));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 69.5));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 76.9));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 69.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 57.8));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 61.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.1));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 55.1));

                            Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 64.5);
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 75.9));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 64.3));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 65.8));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 66.9));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 63.8));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 66.4));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 74.9));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 64.6));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 53.6));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 54.7));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 65.1));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 57.0));
                        }
                        break;
                    }
                case "TOTAL BALANCE SHEET MANAGEMENT":
                    {
                        Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.8);
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 82.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 83.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 85.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 84.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 82.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 80.6));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 82.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 84.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 75.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 74.6));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 77.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 78.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 73.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 76.5));

                        Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 81.8);
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 83.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 88.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 87.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 84.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 85.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 80.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 89.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 88.8));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 74.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 75.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 79.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 76.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 69.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 85.3));

                        Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.2);
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 80.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 86.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 80.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 83.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 77.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 84.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 85.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 73.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 74.0));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 76.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 67.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 82.9));

                        Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.3);
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 80.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 90.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 85.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 83.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 81.5));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 77.1));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 79.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 86.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 73.1));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 76));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 77));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 74.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 71.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 82.3));

                        Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.5);
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 86.6));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 81.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 81.5));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 76.5));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 75.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 73.5));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 77.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 80.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 65.5));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 65.0));

                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.0);
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 89.3));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 82.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 81.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 78.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 76.4));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 76.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 80.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 81.4));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.4));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 59.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 77.4));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 68.3));
                        break;
                    }
                case "Bancassurance & Wealth":
                case "NEDBANK WEALTH":
                    {
                        if (location.Join(new List<string>() { "South Africa", "Not Specified" }, loc => loc.Trim().ToLower(), list => list.Trim().ToLower(), (loc, list) => new { loc }).Distinct().Count() > 0)
                        {
                            Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 70.1);
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 75.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 77.6));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 76.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 74.5));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 73.4));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 71.4));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 76.6));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 69.7));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 59.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 66.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 67.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 66.7));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 60.6));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 68.0));

                            Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 70.5);
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 75.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 78.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 77.1));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 74.7));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 75.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 70.9));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 76.6));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 70.6));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 60.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 66.6));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 66.6));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 66.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 60.1));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 69.5));

                            Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 70.2);
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 76.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 78.9));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 75.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 73.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 76.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 71.1));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 76.3));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 69.9));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 59.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 67.1));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 66.3));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 65.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 60.9));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 68.3));

                            Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 71.7);
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 75.6));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 81.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 77.9));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 76.2));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 75.1));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 72.2));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 77.2));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 71));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 63));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 68));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 68.9));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.1));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 63.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 70.0));

                            Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 72.5);
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 86.3));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 76.9));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 75.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 73.1));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.6));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 74.9));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 75.2));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 73.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 66.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 66.8));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.8));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 62.7));

                            Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 73.8);
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 88.3));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 81.1));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 77.2));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 76.8));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 75.7));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 74.3));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 75.5));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.3));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 68.6));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 65.4));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.0));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.8));

                            Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 72.8);
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 87.5));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 80.2));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 76.6));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 76.3));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.9));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 72.1));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 76.3));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 72.8));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 67.2));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 65.7));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.3));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.5));

                            Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 67.8);
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 79.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 69.7));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 70.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 71.1));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 66.2));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 70.3));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 74.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 67.3));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 60.8));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 59.8));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 67.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 59.0));

                            Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 69.8);
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 83.9));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 73.6));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 72.5));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 72.7));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 70.9));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 71.7));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 76.8));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 69.0));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 59.8));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 60.9));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 68.3));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.0));
                        }
                        else
                        {
                            Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 71.4);
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 76.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 77.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 76.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 73.8));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 72.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 78.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 72.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 61.8));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 65.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 68.8));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 60.4));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 70.0));

                            Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 71.5);
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 76.2));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 78.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 75.8));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 75.3));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 71.7));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 77.8));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 72.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 62.6));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 66.9));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.0));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 68.1));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 60.2));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 70.7));

                            Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 71.2);
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 76.3));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 76.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 74.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 75.9));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 71.3));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 78.0));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 71.6));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 61.9));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 66.1));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 68.8));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 60.3));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 70.4));

                            Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 71.5);
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 74.9));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 80.6));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 78));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 76.2));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 73.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 71.7));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 77.8));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 71.7));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 64.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 66.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.9));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.9));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 61.6));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 70.9));

                            Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 72.8);
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 84.6));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 75.6));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 75.3));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 73.4));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.0));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 75.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 76.3));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.0));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 66.8));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 66.2));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.3));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 64.3));

                            Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 73.0);
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 86.7));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.4));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 75.8));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 75.9));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 74.2));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 74.2));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 75.5));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.3));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 67.8));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 64.4));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.2));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.6));

                            Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 72.8);
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 87.5));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 80.2));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 76.6));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 76.3));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.9));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 72.1));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 76.3));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 72.8));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 67.2));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 65.7));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.3));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.5));

                            Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 67.8);
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 79.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 69.7));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 70.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 71.1));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 66.2));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 70.3));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 74.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 67.3));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 60.8));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 59.8));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 67.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 59.0));

                            Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 69.8);
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 83.9));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 73.6));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 72.5));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 72.7));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 70.9));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 71.7));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 76.8));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 69.0));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 59.8));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 60.9));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 68.3));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.0));
                        } break;
                    }
                case "NEDBANK BUSINESS BANKING":
                    {
                        Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.0);
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 79.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 79.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 80.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 76.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 76.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 82.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 65.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 72.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 72.5));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 66.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 70.8));

                        Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.3);
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 80.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 87.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 82.0));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 81.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 78.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 77.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 83.0));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.8));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 66.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 73.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 73.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 67.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 72.9));

                        Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.8);
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 81.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 89.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 82.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 83.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 77.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 82.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 77.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 66.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 73.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 72.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 68.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 72.0));

                        Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.1);
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 79.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 88.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 82.4));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 82.1));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 76.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 77.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 82.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 76.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 68.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 73.4));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 72.1));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 68.9));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 72.2));

                        Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.2);
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 88.7));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.6));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 80.9));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 76.7));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 74.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 77.5));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 79.6));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 66.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 69.7));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 65.6));

                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 78.1);
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 91.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 90.5));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 83.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 81.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 79.5));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 77.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 81.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 71.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.2));

                        Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.2);
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 91.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 89.1));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 82.4));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 79.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 75.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 74.9));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 80.9));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.8));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 68.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 71.4));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.2));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 66.3));

                        Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 74.7);
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 89.9));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 85.3));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 81.1));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 79.8));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 73.2));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 73.7));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 80.1));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 72.7));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 66.3));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 69.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.4));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.6));

                        Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 72.0);
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 87.2));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 83.4));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 77.6));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 76.8));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 71.1));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 71.9));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 79.4));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 69.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 61.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 65.2));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 62.6));
                        break;
                    }
                case "CE's Office/Cosec":
                case "CE OFFICE / COSEC":
                    {
                        Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 92.2);
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 91.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 95.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 96.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 89.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 95.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 90.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 94.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 91.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 92.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 88.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 90.0));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 91.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 91.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 91.6));

                        Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.2);
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 80.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 83.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 81.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 80));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 82));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 83.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 77.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 77.1));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 73.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 82.4));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 72.9));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 81.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 81.6));

                        Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 85.8);
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 90.5));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 89.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 88.9));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 85.7));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 88.8));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 85.7));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 87.6));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 83.7));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 80.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 83.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 79.6));

                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 85.1);
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 94.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 91.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 91.6));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 89.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 89.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 81.5));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 82.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 85.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 85.3));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 77.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 77.5));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 79.9));

                        Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 87.4);
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 94.8));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 90.9));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 89.4));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 89.8));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 92.2));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 84.4));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 90.1));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 88.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 87.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 79.8));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 80.3));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 81.1));
                        break;
                    }
                case "ENTRPRSE GOV & COMPLIANCE":
                    {
                        Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 80.8);
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 81.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 90.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 90.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 86.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 81.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 86.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 90.5));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 84.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 65.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 75.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 76.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 79.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 68.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 82.6));

                        Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.3);
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 80.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 81.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 83.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 81.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 75.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 79.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 79.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 58.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 70.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 66.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 68.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 65.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 81.9));

                        Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.3);
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 83.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 85.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 83.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 78.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.0));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 80.0));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 87.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 82.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 73.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 75.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 75.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 71.0));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 82.2));

                        Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.9);
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 80.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 91.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 87.5));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 81.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 78));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 80));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 84.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 85));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 63.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 66.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 70));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 60.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 82.5));

                        Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 72.6);
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 92.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 77.9));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 77.5));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 69.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 73.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 76.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 78.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.5));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 58.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 60.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.9));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 59.7));

                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 78.3);
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 94.5));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 82.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 83.3));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 81.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 83.5));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 87.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 82.4));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 84.6));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 63.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 64.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 78.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 59.0));

                        Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 82.4);
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 98.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 92.9));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 85.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 84.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 89.8));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 84.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 84.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 86.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 78.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 68.2));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 76.2));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.2));

                        Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 60.2);
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 76.9));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 75.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 73.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 66.3));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 60.4));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 71.4));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 61.5));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 62.2));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 51.6));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 43.6));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 57.7));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 39.1));

                        Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 74.5);
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 84.8));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 93.8));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 84.9));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 78.1));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 78.6));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 83.9));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 84.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.7));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 61.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 59.0));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 66.7));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 55.7));

                        break;
                    }
                case "GROUP HUMAN RESOURCES":
                    {
                        Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 74.8);
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 76.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 88.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 83.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 76.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 74.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 77.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 80.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 66.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 67.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 65.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 73.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 68.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 78.4));

                        Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 72.9);
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 74.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 82.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 76.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 77.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 75.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 72.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.0));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 58.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 68.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 64.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 69.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 70.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 70.0));

                        Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.4);
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 77.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 83.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 75.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 81.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 80.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 80.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 62.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 71.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 63.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 73.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 72.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 75.6));

                        Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.3);
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 79.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 88.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 88));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 81.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 82.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 82.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 83.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 83.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 67.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 76.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 74.9));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 72.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 74.4));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 80.0));

                        Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 78.5);
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 91.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 82.7));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 79.6));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 79.6));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 79.6));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 81.0));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 82.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 70.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 71.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.0));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.9));

                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.0);
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 91.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 87.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 82.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 80.3));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 79.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 82.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 82.6));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 72.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 67.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 74.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 69.7));

                        Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.9);
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 88.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 82.5));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 81.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 77.6));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 78.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 71.4));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 80.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.4));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.5));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 69.8));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.1));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 62.6));

                        Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.4);
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 89.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 85.9));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 82.3));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 80.2));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 77.1));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 74.6));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 77.6));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 79.2));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 73.7));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 68.3));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 61.6));

                        Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.9);
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 88.1));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.6));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 83.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 81.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.2));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 81.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 82.0));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.9));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 71.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 67.6));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 68.8));
                        break;
                    }
                case "GMCCA TOTAL":
                    {
                        Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 69.8);
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 69.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 81.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 74.5));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 81.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 74.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 71.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 82.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 69.6));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 54.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 64.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 60.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 69.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 64.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 65.3));

                        Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 70.0);
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 72.8));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 80.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 78.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 73.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 73.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 79.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 70.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 54.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 63.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 65.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 61.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 70.3));

                        Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 69.3);
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 73.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 76.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 78.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 71.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 71.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 78.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 72.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 52.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 63.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 60.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.0));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 64.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 68.3));

                        Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 69.6);
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 72.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.9));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 78.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 74.1));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 70.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 76.4));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 50.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 63.4));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 62.9));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 65.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 63.0));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 71.5));

                        Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 69.3);
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 86.0));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 73.8));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 71.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 68.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 66.9));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 74.9));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 77.7));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 73.9));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 53.6));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 55.0));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 59.1));

                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 64.6);
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 81.3));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 74.3));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 66.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 69.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 65.6));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 66.3));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 69.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 68.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 53.3));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 51.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 62.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 53.7));

                        Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 71.5);
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 86.2));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 82.2));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 77.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 76.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 68.1));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 70.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 75.5));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 73.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 62.4));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 61.6));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.2));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 59.8));

                        Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 73.2);
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 85.3));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 81.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 79.2));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 79.8));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 71.7));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 73.8));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 78.4));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 73.8));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 66.9));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 62.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 67.3));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 62.1));

                        Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 70.8);
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 83.4));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 81.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 78.1));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 74.1));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.2));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 74.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 74.2));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 73.1));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 61.1));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 57.8));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 67.0));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 60.9));
                        break;
                    }
                case "GROUP TECHNOLOGY":
                    {
                        Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 73.5);
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 75.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 83.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 80.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 77.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 79.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 72.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 79.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 64.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 69.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 66.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 66.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 72.3));

                        Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 73.9);
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 76.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 81.0));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 77.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 74.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 80.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 64.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 69.8));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 67.0));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 71.4));

                        Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 73.3);
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 75.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 82.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 80.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 77.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 79.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 73.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 80.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 63.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 69.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 66.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 66.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 70.6));

                        Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 68.6);
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 69.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 80.1));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 76.4));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 73.5));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 71.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 67.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 76.4));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 71));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 60.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 64.5));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 66.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 61));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 62.9));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 66.1));

                        Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 69.9);
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 84.5));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 74.9));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 73.8));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 71.8));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 69.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 66.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 74.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 72.5));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 60.8));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 65.0));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 58.2));

                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.0);
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 89.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 85.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 81.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 80.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 79.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 74.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 78.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 77.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 68.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 69.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.4));

                        Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.4);
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 91.3));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 88.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 84.4));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 84.4));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 81.6));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 77.1));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 83.1));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 79.9));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 71.9));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 75.3));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 77.4));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.4));

                        Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 74.3);
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 84.5));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 80.9));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 78.1));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 80.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 76.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 73.9));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 79.8));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.6));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 66.2));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 69.3));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.1));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 61.5));

                        Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 74.1);
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 84.6));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.6));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 77.9));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 78.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 76.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 75.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 80.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 72.8));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 65.8));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 68.4));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 64.6));
                        break;
                    }
                case "Imperial Bank":
                    {
                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 61.4);
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 66.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 66.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 67.5));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 66.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 63.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 64.4));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 65.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 61.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 55.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 56.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 60.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 49.7));
                        break;
                    }
                case "NEDBANK CORPORATE":
                    {
                        if (location.Join(new List<string>() { "South Africa", "Not Specified" }, loc => loc.Trim().ToLower(), list => list.Trim().ToLower(), (loc, list) => new { loc }).Distinct().Count() > 0)
                        {
                            Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.9);
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 80.3));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 85.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 84.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 79.2));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.8));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 77.6));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 81.2));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 70.3));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 72.7));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 73.2));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 68.8));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 75.9));

                            Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.6);
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 81.1));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 86.0));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 80.3));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 82.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 78.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 82.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 70.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 72.8));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.4));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 72.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 69.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 75.3));

                            Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.2);
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 77.7));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 83.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 77.2));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 76.8));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 75.1));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 79.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.8));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 70.1));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 69.3));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 67.3));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 72.3));

                            Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 74.6);
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 87.0));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 78.8));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 75.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 74.1));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 77.1));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 77.4));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.4));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 70.2));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 67.4));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.6));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 65.7));

                            Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 78.9);
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 89.5));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 87.7));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 83.6));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 82.6));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 81.2));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 78.8));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 79.9));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 79.8));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 77.0));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 71.2));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.6));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 68.5));

                            Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 80.8);
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 90.8));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 89.3));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 86.7));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 84.7));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 82.0));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 79.5));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 81.6));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 81.3));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 78.3));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 74.6));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 76.0));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 70.8));

                            Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.4);
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 88.5));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.7));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 82.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 81.2));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 75.8));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 75.6));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 80.8));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.0));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 71.1));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 69.9));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.8));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 65.7));

                            Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 72.1);
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 85.7));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 81.3));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 77.5));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 76.8));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.4));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 72.3));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 78.6));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 70.0));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 63.8));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 64.2));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.2));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.7));
                        }
                        else
                        {
                            Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.4);
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 79.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 83.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 78.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 79.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 77.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 80.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 76.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 68.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 70.7));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 71.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 67.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 74.1));

                            Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.9);
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 80.3));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 85.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 84.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 79.2));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.8));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 77.6));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 81.2));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 70.3));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 72.7));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 73.2));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 68.8));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 75.9));

                            Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.6);
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 81.1));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 86.0));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 80.3));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 82.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 78.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 82.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 70.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 72.8));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.4));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 72.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 69.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 75.3));

                            Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 74.4);
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 77.7));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 83.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 77.3));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 76.8));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 75.1));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 79.6));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.8));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 70.1));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.1));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 69.3));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 67.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 72.3));

                            Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 70.3);
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 83.2));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 73.6));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 73.0));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 72.2));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 67.0));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 73.0));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 74.6));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 70.5));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 64.0));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 65.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 68.3));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 61.0));

                            Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 72.2);
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 83.2));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.0));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 74.6));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 76.4));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 71.9));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 72.7));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 74.8));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 72.7));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 68.2));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 67.6));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 68.9));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 61.8));

                            Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 72.9);
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 83.4));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 80.4));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 76.7));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 77.2));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 71.4));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 72.2));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 76.5));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 72.7));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 67.4));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 69.9));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.1));
                            Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 62.4));

                            Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 72.1);
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 84.0));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.8));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 77.2));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 76.9));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 70.7));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 71.4));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 77.3));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 71.0));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 65.7));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 66.5));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.7));
                            Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 60.6));

                            Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 66.6);
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 79.8));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 74.5));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 71.0));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 70.2));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 64.9));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 67.2));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 74.1));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 65.2));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 58.4));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 57.8));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 65.5));
                            Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 57.3));
                        }
                        break;
                    }
                case "NEDBANK RETAIL":
                    {
                        Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.5);
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 80.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 81.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 79.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 78.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 78.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 79.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 70.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 74.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 73.6));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 71.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 73.2));

                        Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.5);
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 81.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 82.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 80.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 79.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 80.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 76.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 71.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 75.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 74.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 72.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 74.8));

                        Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.6);
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 81.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 82.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 80.0));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 79.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 80.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 76.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 71.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 76.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 74.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 73.0));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 73.9));

                        Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.4);
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 81));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 87.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 82.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 79.9));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 77.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 79.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 80.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 76));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 72.9));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 75.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.5));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 73.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 73.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 74.3));

                        Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.2);
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 85.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 81.6));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 79.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 76.0));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 75.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 78.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 76.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 72.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 71.6));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 68.3));

                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.8);
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 87.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 82.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 80.6));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 79.6));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 79.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 78.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 77.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 75.5));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 73.4));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.5));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 70.4));

                        Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.0);
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 89.8));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 88.3));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 83.3));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 82.5));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.4));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 78.8));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 80.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 76.1));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 75.5));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 74.6));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 70.6));

                        Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.2);
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 86.4));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 83.9));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 81.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 80.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 76.9));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 76.7));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 77.8));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.1));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 73.4));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 72.2));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.4));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.4));

                        Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 70.6);
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 81.1));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 77.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 73.9));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 74.4));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.4));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 72.9));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 74.8));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 69.2));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 63.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 65.4));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 67.7));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.3));
                        break;
                    }
                case "GROUP RISK":
                    {
                        Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 78.3);
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 80.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 87.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 84.6));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 82.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 81.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 79.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 84.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 81.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 68.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 74.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 75.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 69.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 77.7));

                        Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.4);
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 79.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 84.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 79.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 81.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 78.0));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 83.0));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 79.0));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 68.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 72.8));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 74.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 73.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 69.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 78.9));

                        Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.3);
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 77.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 83.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 80.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 81.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 75.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 81.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 77.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 64.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 70.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 70.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 66.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 74.8));

                        Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.5);
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 78.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 86.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 80.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 79));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 77.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 84));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 77.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 67.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 71.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.5));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 70.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 69.9));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 75.9));

                        Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.3);
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 86.9));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 79.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 76.7));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 75.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 78.5));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 79.8));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 68.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 67.0));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.3));

                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.1);
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 91.4));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 83.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 82.3));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 74.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 79.5));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 80.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 71.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 67.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 60.5));

                        Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 78.2);
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 90.8));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.6));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 84.9));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 83.6));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 78.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 78.2));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 81.1));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 80.2));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 72.5));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 70.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.4));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 64.0));

                        Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.6);
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 89.7));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.7));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 82.7));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 82.9));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 78.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 77.1));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 83.4));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 77.1));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 71.7));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 69.9));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 74.7));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 65.0));

                        Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 71.8);
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 85.2));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.0));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 77.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 75.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 71.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 74.0));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 76.2));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 71.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 63.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 64.0));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 67.8));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 63.0));
                        break;
                    }
                case "GROUP STRATEGIC PLANNING & ECONOMICS":
                    {
                        Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.5);
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 77.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 89.6));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 81.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 84.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 77.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 74.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 61.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 71.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 68.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 73.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 70.5));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 82.3));

                        Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.4);
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 79.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 88.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 83.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 71.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 74.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 78.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 77.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 77.5));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 67.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 74.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 65.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 71.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 70.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 77.3));

                        Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 83.8);
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 80.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 96.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 89.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 84.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 84.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 89.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 85.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 80.1));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 77.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 87.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 77.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 78.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 81.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 87.1));

                        Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.9);
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 81.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 91.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 87.5));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 71.9));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 86.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 80.2));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 88.4));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.5));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 66.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 75));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 82.5));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 70.5));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 82.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 80.2));

                        Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.9);
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 94.7));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 72.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 81.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 70.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 76.7));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 87.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 83.9));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 66.9));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 76.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 76.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 71.1));

                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 84.8);
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 94.6));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 85.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 91.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 90.6));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 91.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 83.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 91.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 86.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 70.4));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 77.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 82.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 73.4));
                        break;
                    }
                case "GROUP FINANCE":
                    {
                        Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 73.6);
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 77.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 81.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 77.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 78.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 74.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 78.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 64.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 66.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 68.5));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 68.5));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 65.6));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 73.0));

                        Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 74.9);
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 77.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 87));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 84.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 78.7));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 83.8));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 75.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 79.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 76.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 65.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 68.0));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 68.6));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 66.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 75.2));

                        Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.2);
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 76.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 84.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 78.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 83.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 75.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 79.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 76.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 68.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 69.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.2));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 69.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 67.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 76.0));

                        Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.2);
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 77.0));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 85.1));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 84.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 78.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 78.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 75.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 79.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.1));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 71.0));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 69.0));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.4));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 68.5));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 67.0));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 75.9));

                        Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.2);
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 87.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 84.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 83.2));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 78.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 78.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 79.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 77.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 80.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 73.8));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 68.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 66.7));

                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 78.9);
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 90.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 88.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 84.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 83.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 82.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 77.6));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 77.6));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 82.1));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 77.4));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 68.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.2));

                        Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 82.0);
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 90.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 91.0));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 88.3));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 86.1));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 83.3));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 81.2));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 81.8));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 85.3));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 79.9));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 72.3));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 77.3));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 71.3));

                        Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 78.0);
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 87.3));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 87.5));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 84.2));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 81.6));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 79.1));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 77.9));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 79.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 79.7));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 75.2));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 68.4));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.8));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.0));

                        Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 77.2);
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 87.0));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.2));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 83.0));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 80.4));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 79.5));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 78.6));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 81.6));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.0));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.6));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 65.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.2));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 69.1));
                        break;
                    }
                case "TOTAL AFRICA":
                    {
                        if (location.Join(new List<string>() { "South Africa", "Not Specified" }, loc => loc.Trim().ToLower(), list => list.Trim().ToLower(), (loc, list) => new { loc }).Distinct().Count() > 0)
                        {
                            Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.0);
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 80.4));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 87.5));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 84.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 84.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 79.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 78.8));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 88.4));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 81.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 70.7));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 72.8));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 81.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 69.7));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 69.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 84.2));

                            Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.5);
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 78.8));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 85.7));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 82.1));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 83.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 81.9));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 78.9));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 85.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 80.9));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 72.9));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 74.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 79.0));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 77.9));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 75.8));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 80.2));

                            Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 79.6);
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 85.6));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 87.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 80.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 82.6));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 86.0));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 80.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 88.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 83.0));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 71.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 70.0));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 78.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 75.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 71.0));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 75.3));

                            Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.7);
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 78.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 82.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 84.3));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 86));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 81.2));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 80.9));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 83.2));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 76.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.1));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 70.9));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 77));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 67.7));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 69.1));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 75.5));

                            Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 73.7);
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 88.1));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 60.0));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 67.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 76.6));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 71.9));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 78.6));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 82.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.4));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 61.9));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 66.0));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 73.3));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 72.7));
                        }
                        else
                        {
                            Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 67.1);
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 74.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 69.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 76.7));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 61.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 71.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 73.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 69.5));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 59.7));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 61.6));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 65.2));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 58.6));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 61.4));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 61.9));

                            Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 67.9);
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 74.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 79.2));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 71.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 76.9));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 61.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 71.4));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 73.1));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 70.5));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 60.1));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 61.2));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 65.7));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 59.6));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 63.7));
                            Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 63.3));

                            Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 63.7);
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 72.4));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 73.8));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 64.0));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 73.9));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 55.3));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 68.9));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 70.9));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 66.3));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 54.7));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 57.6));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 61.2));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 58.5));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 60.4));
                            Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 54.6));

                            Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 63.3);
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 71.7));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 70.9));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 64.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 73.5));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 56.6));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 69.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 70.7));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 64.1));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 54.7));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 58.0));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 60.0));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 57.0));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 59.4));
                            Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 52.3));

                            Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 61.4);
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 75.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 61.1));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 60.9));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 64.8));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 51.1));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 65.1));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 69.1));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 61.0));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 51.5));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 60.3));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 63.7));
                            Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 52.2));

                            Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 57.4);
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 70.0));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 61.0));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 56.1));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 61.6));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 49.7));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 60.0));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 64.0));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 57.4));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 49.8));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 54.5));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 59.0));
                            Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 47.9));
                        }
                        break;
                    }
                case "CIB":
                    {
                        if (location.Join(new List<string>() { "South Africa", "Not Specified" }, loc => loc.Trim().ToLower(), list => list.Trim().ToLower(), (loc, list) => new { loc }).Distinct().Count() > 0)
                        {
                            Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 74.4);
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 78.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 82.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 81.4));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 77.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 77.6));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 76.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 80.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 66.3));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 69.5));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 70.8));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 65.4));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 72.8));
                        }
                        else
                        {
                            Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 74.0);
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 77.8));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 82.5));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 81.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 76.7));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 76.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 76.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 80.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 65.9));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 69.1));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.8));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 70.6));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 65.0));
                            Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 72.4));
                        }

                        break;
                    }
                case "RBB":
                    {
                        Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.4);
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 80.6));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 81.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 79.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 78.5));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 78.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 79.6));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.6));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 74.1));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 73.5));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 70.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 73.0));

                        break;
                    }
                default:
                    {
                        Nedbank2015.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.5);
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Ethics", 79.3));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Strategic Direction", 85.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Leadership", 81.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Communication", 79.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Change & Transformation", 78.0));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Training & Development", 76.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Management Style", 79.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.2));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Policies & Procedures", 67.9));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Diversity", 72.5));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.7));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 72.4));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Employment Equity", 68.8));
                        Nedbank2015.CompDimensions.Add(new ReportDimension("Engagement", 72.6));

                        Nedbank2014.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.4);
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Ethics", 80.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Strategic Direction", 86.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Leadership", 81.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Communication", 79.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Change & Transformation", 79.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Training & Development", 77.8));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Management Style", 80.4));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 76.2));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.0));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Diversity", 73.3));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.1));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 72.8));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Employment Equity", 69.9));
                        Nedbank2014.CompDimensions.Add(new ReportDimension("Engagement", 73.8));

                        Nedbank2013.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 76.7);
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Ethics", 80.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Strategic Direction", 85.9));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Leadership", 82.3));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Communication", 79.7));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Change & Transformation", 80.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Training & Development", 78.0));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Management Style", 81.0));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 76.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.0));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Diversity", 73.8));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.6));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 72.5));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Employment Equity", 70.4));
                        Nedbank2013.CompDimensions.Add(new ReportDimension("Engagement", 73.5));

                        Nedbank2012.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.5);
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Ethics", 78.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Strategic Direction", 86));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Leadership", 81.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Communication", 79));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Change & Transformation", 76.7));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Training & Development", 76.9));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Management Style", 80.1));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.6));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Diversity", 72.8));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Relationships & Trust", 71.1));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 71.3));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Employment Equity", 69.9));
                        Nedbank2012.CompDimensions.Add(new ReportDimension("Engagement", 72.8));

                        Nedbank2011.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 74.4);
                        Nedbank2011.CompDimensions = new List<ReportDimension>();
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Ethics", 86.0));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Strategic Direction", 80.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Leadership", 78.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Communication", 75.6));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Change & Transformation", 74.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Training & Development", 76.7));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Management Style", 76.5));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 75.1));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Diversity", 69.3));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.4));
                        Nedbank2011.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 66.0));

                        Nedbank2010.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.9);
                        Nedbank2010.CompDimensions = new List<ReportDimension>();
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Ethics", 88.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Strategic Direction", 85.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Leadership", 80.4));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Communication", 79.6));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Change & Transformation", 77.9));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Training & Development", 76.2));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Management Style", 77.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 76.8));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Policies & Procedures", 71.0));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Diversity", 68.7));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Relationships & Trust", 70.6));
                        Nedbank2010.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 65.7));

                        Nedbank2009.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 78.8);
                        Nedbank2009.CompDimensions = new List<ReportDimension>();
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Ethics", 90.2));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Strategic Direction", 88.1));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Leadership", 83.8));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Communication", 82.6));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Change & Transformation", 79.9));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Training & Development", 77.9));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Management Style", 80.9));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 78.7));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Policies & Procedures", 74.1));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Diversity", 73.9));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Relationships & Trust", 74.8));
                        Nedbank2009.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 69.1));

                        Nedbank2008.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 75.1);
                        Nedbank2008.CompDimensions = new List<ReportDimension>();
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Ethics", 86.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Strategic Direction", 82.4));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Leadership", 80.0));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Communication", 79.6));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Change & Transformation", 75.4));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Training & Development", 75.3));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Management Style", 78.5));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 74.5));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Policies & Procedures", 69.9));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Diversity", 69.5));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Relationships & Trust", 72.1));
                        Nedbank2008.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 64.7));

                        Nedbank2007.NedbankOverall = new ReportDimension("Nedbank Staff Survey Overall", 71.5);
                        Nedbank2007.CompDimensions = new List<ReportDimension>();
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Ethics", 83.0));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Strategic Direction", 78.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Leadership", 75.7));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Communication", 75.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Change & Transformation", 72.8));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Training & Development", 73.0));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Management Style", 77.8));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Organisational Culture & Values", 70.9));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Policies & Procedures", 62.6));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Diversity", 63.9));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Relationships & Trust", 69.3));
                        Nedbank2007.CompDimensions.Add(new ReportDimension("Rewards, Recognition & Performance Management", 62.4));

                        break;
                    }
            }
        }
    }
}