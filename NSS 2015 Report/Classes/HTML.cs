﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace NSS_2015_Report
{
    public static class HTML
    {
        public static string GetSiteRoot()
        {
            string port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == null || port == "80" || port == "443")
                port = "";
            else
                port = ":" + port;

            string protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
                protocol = "http://";
            else
                protocol = "https://";

            string sOut = protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + System.Web.HttpContext.Current.Request.ApplicationPath;

            if (sOut.EndsWith("/"))
            {
                sOut = sOut.Substring(0, sOut.Length - 1);
            }

            return sOut;
        }

        public static string WordWrap(string label, int split)
        {
            if (label.Length <= split)
                return label;

            int inc = 0;
            string textstring = "";
            string currentchar = label.Substring(split, 1);

            while (currentchar != " ")
            {
                inc++;
                currentchar = label.Substring(split - inc, 1);
            }

            textstring = label.Substring(0, split - inc + 1);

            if (label.Substring(split - inc + 1).Length > split)
                textstring += "{br}" + WordWrap(label.Substring(split - inc + 1), split);
            else
                textstring += "{br}" + label.Substring(split - inc + 1);

            return textstring;
        }

        public static string RadarTable(string[] years, string tabledata)
        {
            return RadarTable(years, tabledata, false);
        }

        public static string RadarTable(string[] years, string tabledata, bool Disagree)
        {
            return RadarTable(years, tabledata, Disagree, false);
        }

        public static string RadarTable(string[] years, string tabledata, bool Disagree, bool shift)
        {
            double Num;
            StringBuilder tableHTML = new StringBuilder();
            tableHTML.Append("<table style=\"width:450px;border-collapse:collapse;\"><tr><td colspan='" + (years.Count() + 1) + "' align='right'>% Respondents(" + (Disagree ? "Disagreed" : "Agreed") + ")</td></tr><tr><td colspan='" + (years.Count() + 1 + (shift ? 1 : 0)) + "' align='right'>&nbsp;</td></tr><tr><td></td>");
            foreach (string year in years) tableHTML.Append("<td width='50px'><b>" + year + "</b></td>");
            tableHTML.Append(shift ? "<td width='50px'><b>" + (double.TryParse(years[0], out Num) ? "Shift" : "Variance") + "</b></td>" : "");
            tableHTML.Append("</tr><tr><td colspan='" + (years.Count() + 1 + (shift ? 1 : 0)) + "' ><hr style='border: 1px solid #000' /></td></tr>");
            tableHTML.Append(tabledata);
            tableHTML.Append("<tr><td colspan='" + (years.Count() + 1 + (shift ? 1 : 0)) + "'><hr style='border: 1px solid #000000' /></td></tr></table>");
            return tableHTML.ToString();
        }

        public static string StatementTable(string[] years, string part1, string part2, string type1, string type2)
        {
            StringBuilder tableHTML = new StringBuilder();
            tableHTML.Append("<tr><td colspan='6' align='right'>% Respondents(Agreed)</td></tr>");
            tableHTML.Append("<tr style='background-color: #003300; color: #FFFFFF;height:20px;'>");
            tableHTML.Append("<td align='left' style='width:75%;font-size:12px;padding-left: 15px' colspan='2'><b>" + type1 + " Statements</b></td>");
            foreach (string year in years) tableHTML.Append("<td align='center' style='width:5%;font-size:12px'><b>" + year + "</b></td>");
            tableHTML.Append("<td align='center' style='width:5%;font-size:12px'><b>Shift</b></td>");
            tableHTML.Append("<td align='center' style='width:10%;font-size:12px'><b>95% Conf</b></td>");
            tableHTML.Append("</tr>");
            tableHTML.Append("<tr><td colspan='6'>&nbsp;</td></tr>");
            tableHTML.Append(part1);
            tableHTML.Append("<tr><td colspan='6' style='height=8px;'>&nbsp;</td></tr>");
            tableHTML.Append("<tr><td colspan='6' style='height=8px;'>&nbsp;</td></tr>");
            tableHTML.Append("<tr style='background-color: #003300; color: #FFFFFF;height:20px;'>");
            tableHTML.Append("<td align='left' style='width:75%;font-size:12px;padding-left: 15px' colspan='2'><b>" + type2 + " Statements</b></td>");
            foreach (string year in years) tableHTML.Append("<td align='center' style='width:5%;font-size:12px'><b>" + year + "</b></td>");
            tableHTML.Append("<td align='center' style='width:5%;font-size:12px'><b>Shift</b></td>");
            tableHTML.Append("<td align='center' style='width:10%;font-size:12px'><b>95% Conf</b></td>");
            tableHTML.Append("</tr>");
            tableHTML.Append("<tr><td colspan='6' style='height=8px;'>&nbsp;</td></tr>");
            tableHTML.Append(part2);
            return tableHTML.ToString();
        }

        public static string StatementLegend(string year2, bool africa)
        {
            return "<tr><td align='left' style='font-style: italic;vertical-align:bottom;' colspan='6'><b>Definitions</b><table style='font-size: 10px;padding-left:10px;font-style: italic' cellspacing='0' cellpadding='0' width='100%'>" +
                    "<tr><td width='50%'><table width='100%' cellpadding='0' cellspacing='0'><tr><td><img src='" + GetSiteRoot() + "/images/red.jpg' /><img src='" + GetSiteRoot() + "/images/green.jpg' /> : Statistically Significant Improvement / Decline since Previous survey</td></tr>" +
                    "<tr><td>No Value : scores not shown due to insufficient sample size</td></tr>" + (africa ? "<tr><td>* Statement omitted in " + year2 + " analysis but re-instated in 2014 (no " + year2 + " comparative score available)</td></tr>" : "") + "</table></td>" +
                    "<td width='50%'><table width='100%' cellpadding='0' cellspacing='0'><tr><td colspan='3'>Classification of Average Statement/Dimension Score:</td></tr>" +
                    "<tr><td style='padding-left:10px;border-color:black;border-width:1px;border-style:solid;background-color: #FF0000; width: 20px; height: 20px;'>&nbsp;</td><td>&nbsp;Decline</td></tr><tr><td align='left' style='border-color:black;border-width:1px;border-style:solid;font-family: Verdana; font-size: 11px;width: 20px; height: 20px;'>&nbsp;</td><td>&nbsp;No movement / No comparative data</td></tr><tr><td style='border-color:black;border-width:1px;border-style:solid;background-color: #33CC33; width: 20px; height: 20px;'>&nbsp;</td><td>&nbsp;Improvement</td></tr>" +
                    "</table></td></tr></table></td></tr>";
        }

        public static string LegendTemplate(string legendbody, string header)
        {
            return "<b>" + header + "</b><table cellpadding='0' cellspacing='0' style='font-size: 11px;' width='100%'>" + legendbody + "</table>";
        }

        public static string GetAllYearsCaption(string cluster, List<string> location)
        {
            if (cluster == "CE OFFICE / COSEC")
                return "2013 n=14; 2012 n=10; 2011 n=18; 2010 n=17; 2009 n=11; 2008 n=0; 2007 n=0";
            else if (cluster == "CENTRAL MANAGEMENT INCL REST OF AFRICA")
                return "2013 n=13";
            else if (cluster == "ENTRPRSE GOV & COMPLIANCE")
                return "2015 n=50; 2014 n=47; 2013 n=30; 2012 n=20; 2011 n=26; 2010 n=13";
            else if (cluster == "GMCCA TOTAL")
                return "2015 n=49; 2014 n=54; 2013 n=49; 2012 n=55; 2011 n=41; 2010 n=39";
            else if (cluster == "GROUP FINANCE")
                return "2015 n=710; 2014 n=657; 2013 n=648; 2012 n=564; 2011 n=562; 2010 n=526";
            else if (cluster == "GROUP HUMAN RESOURCES")
                return "2015 n=70; 2014 n=74; 2013 n=84; 2012 n=75; 2011 n=75; 2010 n=69";
            else if (cluster == "GROUP RISK")
                return "2015 n=389; 2014 n=355; 2013 n=340; 2012 n=320; 2011 n=367; 2010 n=280";
            else if (cluster == "GROUP STRATEGIC PLANNING & ECONOMICS")
                return "2015 n=16; 2014 n=14; 2013 n=18; 2012 n=16; 2011 n=19; 2010 n=16";
            else if (cluster == "GROUP TECHNOLOGY")
                return "2015 n=1489; 2014 n=1519; 2013 n=1464; 2012 n=1315; 2011 n=1237; 2010 n=1163";
            else if (cluster == "Imperial Bank")
                return "2010 n=492; 2009 n=0; 2008 n=0; 2007 n=0";
            else if (cluster == "NEDBANK BUSINESS BANKING")
                return "2015 n=1555; 2014 n=1593; 2013 n=1690; 2012 n=1495; 2011 n=1631; 2010 n=1566";
            else if (cluster == "NEDBANK CAPITAL")
                if (location.Join(new List<string>() { "South Africa", "Not Specified" }, loc => loc.Trim().ToLower(), list => list.Trim().ToLower(), (loc, list) => new { loc }).Distinct().Count() > 0)
                    return "2015 n=459; 2014 n=499; 2013 n=453; 2012 n=504; 2011 n=504; 2010 n=431";
                else
                    return "2015 n=497; 2014 n=546; 2013 n=489; 2012 n=547; 2011 n=546; 2010 n=471";
            else if (cluster == "NEDBANK CORPORATE")
                if (location.Join(new List<string>() { "South Africa", "Not Specified" }, loc => loc.Trim().ToLower(), list => list.Trim().ToLower(), (loc, list) => new { loc }).Distinct().Count() > 0)
                    return "2014 n=1626; 2013 n=1675; 2012 n=1657; 2011 n=1725; 2010 n=1727; 2009 n=1551; 2008 n=2117; 2007 n=2099";
                else
                    return "2015 n=1666; 2014 n=1626; 2013 n=1675; 2012 n=1663; 2011 n=2564; 2010 n=2571";
            else if (cluster == "NEDBANK RETAIL")
                return "2015 n=14003; 2014 n=14099; 2013 n=13533; 2012 n=13930; 2011 n=13003; 2010 n=11491";
            else if (cluster == "NEDBANK WEALTH")
                if (location.Join(new List<string>() { "South Africa", "Not Specified" }, loc => loc.Trim().ToLower(), list => list.Trim().ToLower(), (loc, list) => new { loc }).Distinct().Count() > 0)
                    return "2015 n=1137; 2014 n=1241; 2013 n=1085; 2012 n=1059; 2011 n=1140; 2010 n=1031";
                else
                    return "2015 n=1350; 2014 n=1448; 2013 n=1305; 2012 n=1243; 2011 n=1315; 2010 n=1104";
            else if (cluster == "TOTAL AFRICA")
                if (location.Join(new List<string>() { "South Africa", "Not Specified" }, loc => loc.Trim().ToLower(), list => list.Trim().ToLower(), (loc, list) => new { loc }).Distinct().Count() > 0)
                    return "2015 n=80; 2014 n=42; 2013 n=33; 2012 n=34; 2011 n=30; 2010 n=0";
                else
                    return "2015 n=1385; 2014 n=1262; 2013 n=930; 2012 n=853; 2011 n=869; 2010 n=844";
            else if (cluster == "TOTAL BALANCE SHEET MANAGEMENT")
                return "2015 n=59; 2014 n=87; 2013 n=72; 2012 n=75; 2011 n=83; 2010 n=60";
            else if (cluster == "CIB")
                if (location.Join(new List<string>() { "South Africa", "Not Specified" }, loc => loc.Trim().ToLower(), list => list.Trim().ToLower(), (loc, list) => new { loc }).Distinct().Count() > 0)
                    return "2015 n=2122";
                else
                    return "2015 n=2163";
            else if (cluster == "RBB")
                return "2015 n=15558";
            else
                return "2015 n=11786; 2014 n=12701; 2013 n=12446; 2012 n=12178; 2011 n=12074; 2010 n=9771";
        }

        public static string GetAllYearsGraph(string cluster, List<string> location, string year2, bool africa)
        {
            Constants constants = new Constants(cluster, location);
            StringBuilder DimensionCategoryXML = new StringBuilder();
            StringBuilder DimensionDataset2015XML = new StringBuilder();
            StringBuilder DimensionDataset2014XML = new StringBuilder();
            StringBuilder DimensionDataset2013XML = new StringBuilder();
            StringBuilder DimensionDataset2012XML = new StringBuilder();
            StringBuilder DimensionDataset2011XML = new StringBuilder();
            StringBuilder DimensionDataset2010XML = new StringBuilder();
            StringBuilder DimensionDataset2009XML = new StringBuilder();
            StringBuilder DimensionDataset2008XML = new StringBuilder();
            StringBuilder DimensionDataset2007XML = new StringBuilder();

            string[] years = new string[] { "2015", "2014", "2013", "2012", "2011", "2010" };

            double result = 0;
            bool isSignificant = false;

            Significance.getSignificance(12033, (year2 == "2014" ? 12701 : year2 == "2013" ? 12446 : year2 == "2012" ? 12169 : year2 == "2011" ? 12074 : (year2 == "2010" ? 9771 : 0)), constants.Nedbank2015.NedbankOverall.DimensionPositiveValue, year2 == "2014" ? (constants.Nedbank2014.NedbankOverall != null ? constants.Nedbank2014.NedbankOverall.DimensionPositiveValue : 0) : year2 == "2013" ? (constants.Nedbank2013.NedbankOverall != null ? constants.Nedbank2013.NedbankOverall.DimensionPositiveValue : 0) : year2 == "2012" ? (constants.Nedbank2012.NedbankOverall != null ? constants.Nedbank2012.NedbankOverall.DimensionPositiveValue : 0) : year2 == "2011" ? (constants.Nedbank2011.NedbankOverall != null ? constants.Nedbank2011.NedbankOverall.DimensionPositiveValue : 0) : year2 == "2010" ? constants.Nedbank2010.NedbankOverall != null ? constants.Nedbank2010.NedbankOverall.DimensionPositiveValue : 0 : 0, out result, out isSignificant);
            DimensionCategoryXML.Append("<category label='" + constants.Nedbank2015.NedbankOverall.DimensionName.AfricaNewDimension(year2, africa) + " %(" + result.ToString("0.0") + (isSignificant ? "*" : "") + ")" + "' /> ");

            foreach (string yr in years)
            {
                switch (yr)
                {
                    case "2015":
                        {
                            DimensionDataset2015XML.Append("<dataset seriesname='2015' anchorSides='6' showValues='1' anchorRadius='2' anchorBgAlpha='0' showPlotBorder='1' plotBorderThickness='2' plotBorderAlpha='100' drawAnchors='1' color='00533E' plotBorderColor='00533E'>");
                            DimensionDataset2015XML.Append("<set value='" + constants.Nedbank2015.NedbankOverall.DimensionPositiveValue.ToString("0.0") + "' />");
                            break;
                        }
                    case "2014":
                        {
                            if (constants.Nedbank2014.CompDimensions.Count > 0)
                            {
                                DimensionDataset2014XML.Append("<dataset seriesname='2014' anchorSides='6' showValues='1' anchorRadius='2' anchorBgAlpha='0' showPlotBorder='1' plotBorderThickness='2' plotBorderAlpha='100' drawAnchors='1' color='00533E' plotBorderColor='00533E'>");
                                DimensionDataset2014XML.Append("<set value='" + (constants.Nedbank2014.NedbankOverall != null ? constants.Nedbank2014.NedbankOverall.DimensionPositiveValue.ToString("0.0") : "0.0") + "' />");
                            }
                            break;
                        }
                    case "2013":
                        {
                            if (constants.Nedbank2013.CompDimensions.Count > 0)
                            {
                                DimensionDataset2013XML.Append("<dataset seriesname='2013' anchorSides='6' showValues='0' anchorRadius='2' anchorBgAlpha='0' showPlotBorder='1' plotBorderThickness='2' plotBorderAlpha='100' drawAnchors='1' color='00533E' plotBorderColor='00533E'>");
                                DimensionDataset2013XML.Append("<set value='" + (constants.Nedbank2013.NedbankOverall != null ? constants.Nedbank2013.NedbankOverall.DimensionPositiveValue.ToString("0.0") : "0.0") + "' />");
                            }
                            break;
                        }
                    case "2012":
                        {
                            if (constants.Nedbank2012.CompDimensions.Count > 0)
                            {
                                DimensionDataset2012XML.Append("<dataset seriesname='2012' anchorSides='6' showValues='0' anchorRadius='2' anchorBgAlpha='0' showPlotBorder='1' plotBorderThickness='2' plotBorderAlpha='100' drawAnchors='1' color='00533E' plotBorderColor='00533E'>");
                                DimensionDataset2012XML.Append("<set value='" + (constants.Nedbank2012.NedbankOverall != null ? constants.Nedbank2012.NedbankOverall.DimensionPositiveValue.ToString("0.0") : "0.0") + "' />");
                            }
                            break;
                        }
                    case "2011":
                        {
                            if (constants.Nedbank2011.CompDimensions.Count > 0)
                            {
                                DimensionDataset2011XML.Append("<dataset seriesname='2011' anchorSides='6' showValues='0' anchorRadius='2' anchorBgAlpha='0' showPlotBorder='1' plotBorderThickness='2' plotBorderAlpha='100' drawAnchors='1' color='738639' plotBorderColor='738639'>");
                                DimensionDataset2011XML.Append("<set value='" + (constants.Nedbank2011.NedbankOverall != null ? constants.Nedbank2011.NedbankOverall.DimensionPositiveValue.ToString("0.0") : "0.0") + "' />");
                            }
                            break;
                        }
                    case "2010":
                        {
                            if (constants.Nedbank2010.CompDimensions.Count > 0)
                            {
                                DimensionDataset2010XML.Append("<dataset seriesname='2010' anchorSides='6' showValues='0' anchorRadius='2' anchorBgAlpha='0' showPlotBorder='1' plotBorderThickness='2' plotBorderAlpha='100' drawAnchors='1' color='74A18E' plotBorderColor='74A18E'>");
                                DimensionDataset2010XML.Append("<set value='" + (constants.Nedbank2010.NedbankOverall != null ? constants.Nedbank2010.NedbankOverall.DimensionPositiveValue.ToString("0.0") : "0.0") + "' />");
                            }
                            break;
                        }
                    case "2009":
                        {
                            if (constants.Nedbank2009.CompDimensions.Count > 0)
                            {
                                DimensionDataset2009XML.Append("<dataset seriesname='2009' anchorSides='6' showValues='0' anchorRadius='2' anchorBgAlpha='0' showPlotBorder='1' plotBorderThickness='2' plotBorderAlpha='100' drawAnchors='1' color='BCE18D' plotBorderColor='BCE18D'>");
                                DimensionDataset2009XML.Append("<set value='" + (constants.Nedbank2009.NedbankOverall != null ? constants.Nedbank2009.NedbankOverall.DimensionPositiveValue.ToString("0.0") : "0.0") + "' />");
                            }
                            break;
                        }
                    case "2008":
                        {
                            if (constants.Nedbank2008.CompDimensions.Count > 0)
                            {
                                DimensionDataset2008XML.Append("<dataset seriesname='2008' anchorSides='6' showValues='0' anchorRadius='2' anchorBgAlpha='0' showPlotBorder='1' plotBorderThickness='2' plotBorderAlpha='100' drawAnchors='1' color='867A69' plotBorderColor='867A69'>");
                                DimensionDataset2008XML.Append("<set value='" + (constants.Nedbank2008.NedbankOverall != null ? constants.Nedbank2008.NedbankOverall.DimensionPositiveValue.ToString("0.0") : "0.0") + "' />");
                            }
                            break;
                        }
                    case "2007":
                        {
                            if (constants.Nedbank2007.CompDimensions.Count > 0)
                            {
                                DimensionDataset2007XML.Append("<dataset seriesname='2007' anchorSides='6' showValues='0' anchorRadius='2' anchorBgAlpha='0' showPlotBorder='1' plotBorderThickness='2' plotBorderAlpha='100' drawAnchors='1' color='C8F475' plotBorderColor='C8F475'>");
                                DimensionDataset2007XML.Append("<set value='" + (constants.Nedbank2007.NedbankOverall != null ? constants.Nedbank2007.NedbankOverall.DimensionPositiveValue.ToString("0.0") : "0.0") + "' />");
                            }
                            break;
                        }
                }
            }

            foreach (ReportDimension dimension in constants.Nedbank2015.CompDimensions.OrderBy(p => p.DimensionPositiveValue))
            {
                Significance.getSignificance(12033, (year2 == "2014" ? 12701 : year2 == "2013" ? 12446 : year2 == "2012" ? 12169 : year2 == "2011" ? 12074 : (year2 == "2010" ? 9771 : 0)), dimension.DimensionPositiveValue, year2 == "2014" && constants.Nedbank2014.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2014.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue : year2 == "2013" && constants.Nedbank2013.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2013.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue : year2 == "2012" && constants.Nedbank2012.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2012.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue : year2 == "2011" && constants.Nedbank2011.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2011.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue : (year2 == "2010" && constants.Nedbank2010.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2010.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue : 0), out result, out isSignificant);
                DimensionCategoryXML.Append("<category label='" + WordWrap(dimension.DimensionName.AfricaNewDimension(year2, africa), 30) + " % (" + result.ToString("0.0") + (isSignificant ? "*" : "") + ")" + "' /> ");

                foreach (string yr in years)
                {
                    switch (yr)
                    {
                        case "2015": DimensionDataset2015XML.Append("<set value='" + dimension.DimensionPositiveValue.ToString("0.0") + "' />"); break;
                        case "2014": if (constants.Nedbank2014.CompDimensions.Count > 0) DimensionDataset2014XML.Append("<set value='" + (constants.Nedbank2014.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2014.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "0.0") + "' />"); break;
                        case "2013": if (constants.Nedbank2013.CompDimensions.Count > 0) DimensionDataset2013XML.Append("<set value='" + (constants.Nedbank2013.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2013.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "0.0") + "' />"); break;
                        case "2012": if (constants.Nedbank2012.CompDimensions.Count > 0) DimensionDataset2012XML.Append("<set value='" + (constants.Nedbank2012.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2012.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "0.0") + "' />"); break;
                        case "2011": if (constants.Nedbank2011.CompDimensions.Count > 0) DimensionDataset2011XML.Append("<set value='" + (constants.Nedbank2011.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2011.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "0.0") + "' />"); break;
                        case "2010": if (constants.Nedbank2010.CompDimensions.Count > 0) DimensionDataset2010XML.Append("<set value='" + (constants.Nedbank2010.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2010.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "0.0") + "' />"); break;
                        case "2009": if (constants.Nedbank2009.CompDimensions.Count > 0) DimensionDataset2009XML.Append("<set value='" + (constants.Nedbank2009.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2009.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "0.0") + "' />"); break;
                        case "2008": if (constants.Nedbank2008.CompDimensions.Count > 0) DimensionDataset2008XML.Append("<set value='" + (constants.Nedbank2008.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2008.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "0.0") + "' />"); break;
                        case "2007": if (constants.Nedbank2007.CompDimensions.Count > 0) DimensionDataset2007XML.Append("<set value='" + (constants.Nedbank2007.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2007.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "0.0") + "' />"); break;
                    }
                }
            }

            foreach (string yr in years)
            {
                switch (yr)
                {
                    case "2015":
                        {
                            DimensionDataset2015XML.Append("</dataset>");
                            break;
                        }
                    case "2014":
                        {
                            DimensionDataset2014XML.Append("</dataset>");
                            break;
                        }
                    case "2013":
                        {
                            DimensionDataset2013XML.Append("</dataset>");
                            break;
                        }
                    case "2012":
                        {
                            DimensionDataset2012XML.Append("</dataset>");
                            break;
                        }
                    case "2011":
                        {
                            DimensionDataset2011XML.Append("</dataset>");
                            break;
                        }
                    case "2010":
                        {
                            if (constants.Nedbank2010.CompDimensions.Count > 0) DimensionDataset2010XML.Append("</dataset>");
                            break;
                        }
                    case "2009":
                        {
                            if (constants.Nedbank2009.CompDimensions.Count > 0) DimensionDataset2009XML.Append("</dataset>");
                            break;
                        }
                    case "2008":
                        {
                            if (constants.Nedbank2008.CompDimensions.Count > 0) DimensionDataset2008XML.Append("</dataset>");
                            break;
                        }
                    case "2007":
                        {
                            if (constants.Nedbank2007.CompDimensions.Count > 0) DimensionDataset2007XML.Append("</dataset>");
                            break;
                        }
                }

            }

            return Charts.RadarChart(DimensionCategoryXML.ToString(), DimensionDataset2015XML.ToString() + DimensionDataset2014XML.ToString() + DimensionDataset2013XML.ToString() + DimensionDataset2012XML.ToString() + DimensionDataset2011XML.ToString() + DimensionDataset2010XML.ToString() + DimensionDataset2009XML.ToString() + DimensionDataset2008XML.ToString() + DimensionDataset2007XML.ToString(), "DimensionAllYears", 550, 420);
        }

        public static string DimensionsForAllYearsTable(string cluster, List<string> location, string year2, bool africa)
        {
            string[] years;
            string table = GetAllYearsTableContents(cluster, location, out years, year2, africa);
            return RadarTable(years, table);
        }

        public static string GetAllYearsTableContents(string cluster, List<string> location, out string[] years, string year2, bool africa)
        {
            Constants constants = new Constants(cluster, location);

            List<string> yrs = new List<string> { "2015", "2014", "2013", "2012", "2011", "2010" };
            
            if (constants.Nedbank2010.CompDimensions.Count == 0) yrs.RemoveAt(yrs.Count - 1);
            if (constants.Nedbank2011.CompDimensions.Count == 0) yrs.RemoveAt(yrs.Count - 1);
            if (constants.Nedbank2012.CompDimensions.Count == 0) yrs.RemoveAt(yrs.Count - 1);
            if (constants.Nedbank2013.CompDimensions.Count == 0) yrs.RemoveAt(yrs.Count - 1);
            if (constants.Nedbank2014.CompDimensions.Count == 0) yrs.RemoveAt(yrs.Count - 1);

            years = yrs.ToArray();

            string table = "<tr style=\"font-weight:bold;\"><td style='width:225px;padding:4px;'>" + constants.Nedbank2015.NedbankOverall.DimensionName.AfricaNewDimension(year2, africa) + "</td><td>" + constants.Nedbank2015.NedbankOverall.DimensionPositiveValue.ToString("0.0") + (constants.Nedbank2014.NedbankOverall == null ? "" : "</td><td>" + constants.Nedbank2014.NedbankOverall.DimensionPositiveValue.ToString("0.0")) + (constants.Nedbank2013.NedbankOverall == null ? "" : "</td><td>" + constants.Nedbank2013.NedbankOverall.DimensionPositiveValue.ToString("0.0")) + (constants.Nedbank2012.NedbankOverall == null ? "" : "</td><td>" + constants.Nedbank2012.NedbankOverall.DimensionPositiveValue.ToString("0.0")) + (constants.Nedbank2011.NedbankOverall == null ? "" : "</td><td>" + constants.Nedbank2011.NedbankOverall.DimensionPositiveValue.ToString("0.0")) + (constants.Nedbank2010.NedbankOverall == null ? "" : "</td><td>" + constants.Nedbank2010.NedbankOverall.DimensionPositiveValue.ToString("0.0")) + "</td></tr>";

            bool other = true;

            foreach (ReportDimension dimension in constants.Nedbank2015.CompDimensions.OrderByDescending(p => p.DimensionPositiveValue))
            {
                table += "<tr" + (other ? " style=\"background:#d4e7bc;\"" : "") + "><td style='width:225px;padding:4px;'>" + dimension.DimensionName.AfricaNewDimension(year2, africa);
                foreach (string yr in years)
                {
                    table += "</td><td>";

                    switch (yr)
                    {
                        case "2015": table += dimension.DimensionPositiveValue.ToString("0.0"); break;
                        case "2014": table += constants.Nedbank2014.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2014.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "&nbsp;"; break;
                        case "2013": table += constants.Nedbank2013.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2013.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "&nbsp;"; break;
                        case "2012": table += constants.Nedbank2012.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2012.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "&nbsp;"; break;
                        case "2011": table += constants.Nedbank2011.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2011.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "&nbsp;"; break;
                        case "2010": table += constants.Nedbank2010.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2010.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "&nbsp;"; break;
                        case "2009": table += constants.Nedbank2009.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2009.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "&nbsp;"; break;
                        case "2008": table += constants.Nedbank2008.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2008.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "&nbsp;"; break;
                        case "2007": table += constants.Nedbank2007.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null ? constants.Nedbank2007.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault().DimensionPositiveValue.ToString("0.0") : "&nbsp;"; break;
                    }
                }

                table += "</td></tr>";

                other = !other;
            }

            return table;
        }

        public static string getDesignationChart(string body)
        {
            StringBuilder strHTMLData = new StringBuilder();

            strHTMLData.Append("<table style='width:100%;' cellspacing='0' cellpadding='0' align='center'>");
            strHTMLData.Append("<tr>");
            strHTMLData.Append("<td></td>");
            strHTMLData.Append("<td style='border-color:black;border-width:1px;border-style:solid;' colspan='7' align='center'><b>Designated</b></td>");
            strHTMLData.Append("<td style='border-color:black;border-width:1px;border-style:solid;' align='center'><b>Non - Designated</b></td>");
            strHTMLData.Append("<td></td>");
            strHTMLData.Append("<td align='center'>&nbsp;</td>");
            strHTMLData.Append("</tr>");
            strHTMLData.Append("<tr>");
            strHTMLData.Append("<td>&nbsp;</td>");
            strHTMLData.Append("<td style='border-color:black;border-width:1px;border-style:solid;' colspan='3' align='center'><b>Male</b></td>");
            strHTMLData.Append("<td style='border-color:black;border-width:1px;border-style:solid;' colspan='4' align='center'><b>Female</b></td>");
            strHTMLData.Append("<td style='border-color:black;border-width:1px;border-style:solid;' align='center'><b>Male</b></td>");
            strHTMLData.Append("<td>&nbsp;</td>");
            strHTMLData.Append("<td align='center'>&nbsp;</td>");
            strHTMLData.Append("</tr>");
            strHTMLData.Append("<tr>");
            strHTMLData.Append("<td style='width:250px;'>&nbsp;</td>");
            strHTMLData.Append("<td style='width:110px;border-color:black;border-width:1px;border-style:solid;' align='center'><b>African</b></td>");
            strHTMLData.Append("<td style='width:110px;border-color:black;border-width:1px;border-style:solid;' align='center'><b>Coloured</b></td>");
            strHTMLData.Append("<td style='width:110px;border-color:black;border-width:1px;border-style:solid;' align='center'><b>Indian</b></td>");
            strHTMLData.Append("<td style='width:110px;border-color:black;border-width:1px;border-style:solid;' align='center'><b>African</b></td>");
            strHTMLData.Append("<td style='width:110px;border-color:black;border-width:1px;border-style:solid;' align='center'><b>Coloured</b></td>");
            strHTMLData.Append("<td style='width:110px;border-color:black;border-width:1px;border-style:solid;' align='center'><b>Indian</b></td>");
            strHTMLData.Append("<td style='width:110px;border-color:black;border-width:1px;border-style:solid;' align='center'><b>White</b></td>");
            strHTMLData.Append("<td style='width:110px;border-color:black;border-width:1px;border-style:solid;' align='center'><b>White</b></td>");
            strHTMLData.Append("<td>&nbsp;</td>");
            strHTMLData.Append("<td style='width:110px;border-color:black;border-width:1px;border-style:solid;' align='center'><b>Total</b></td>");
            strHTMLData.Append("</tr>");
            strHTMLData.Append(body);
            strHTMLData.Append("</table>");

            return strHTMLData.ToString();
        }
    }
}