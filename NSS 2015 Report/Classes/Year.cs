﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NSS_2015_Report
{
    public class Year
    {
        private NSS2015Report db = new NSS2015Report();

        public Guid StaffID { get; set; }
        public string StaffCode { get; set; }
        public string Cluster { get; set; }
        public string Division1 { get; set; }
        public string Division2 { get; set; }
        public string Division3 { get; set; }
        public string Division4 { get; set; }
        public string Division5 { get; set; }
        public string Division6 { get; set; }
        public string Branch { get; set; }
        public string Gender { get; set; }
        public string Race { get; set; }        
        public string OccupationalLevel { get; set; }
        public string Disabilities { get; set; }
        public string Age { get; set; }
        public string LOS { get; set; }
        public string Region { get; set; }
        public string Classification { get; set; }
        public string ManagementPassage { get; set; }
        public string JobFamily { get; set; }        
        public string Location { get; set; }
        public double? Q1 { get; set; }
        public double? Q2 { get; set; }
        public double? Q3 { get; set; }
        public double? Q4 { get; set; }
        public double? Q5 { get; set; }
        public double? Q6 { get; set; }
        public double? Q7 { get; set; }
        public double? Q8 { get; set; }
        public double? Q9 { get; set; }
        public double? Q10 { get; set; }
        public double? Q11 { get; set; }
        public double? Q12 { get; set; }
        public double? Q13 { get; set; }
        public double? Q14 { get; set; }
        public double? Q15 { get; set; }
        public double? Q16 { get; set; }
        public double? Q17 { get; set; }
        public double? Q18 { get; set; }
        public double? Q19 { get; set; }
        public double? Q20 { get; set; }
        public double? Q21 { get; set; }
        public double? Q22 { get; set; }
        public double? Q23 { get; set; }
        public double? Q24 { get; set; }
        public double? Q25 { get; set; }
        public double? Q26 { get; set; }
        public double? Q27 { get; set; }
        public double? Q28 { get; set; }
        public double? Q29 { get; set; }
        public double? Q30 { get; set; }
        public double? Q31 { get; set; }
        public double? Q32 { get; set; }
        public double? Q33 { get; set; }
        public double? Q34 { get; set; }
        public double? Q35 { get; set; }
        public double? Q36 { get; set; }
        public double? Q37 { get; set; }
        public double? Q38 { get; set; }
        public double? Q39 { get; set; }
        public double? Q40 { get; set; }
        public double? Q41 { get; set; }
        public double? Q42 { get; set; }
        public double? Q43 { get; set; }
        public double? Q44 { get; set; }
        public double? Q45 { get; set; }
        public double? Q46 { get; set; }
        public double? Q47 { get; set; }
        public double? Q48 { get; set; }
        public double? Q49 { get; set; }
        public double? Q50 { get; set; }
        public double? Q51 { get; set; }
        public double? Q52 { get; set; }
        public double? Q53 { get; set; }
        public double? Q54 { get; set; }
        public double? Q55 { get; set; }
        public double? Q56 { get; set; }
        public double? Q57 { get; set; }
        public double? Q58 { get; set; }
        public double? Q59 { get; set; }
        public double? Q60 { get; set; }
        public double? Q61 { get; set; }
        public double? Q62 { get; set; }
        public double? Q63 { get; set; }
        public double? Q64 { get; set; }
        public double? Q65 { get; set; }
        public double? Q66 { get; set; }
        public double? Q67 { get; set; }
        public double? Q68 { get; set; }
        public double? Q69 { get; set; }
        public double? Q70 { get; set; }
        public double? Q71 { get; set; }
        public double? Q72 { get; set; }
        public double? Q73 { get; set; }
        public double? Q74 { get; set; }
        public double? Q75 { get; set; }
        public double? Q76 { get; set; }
        public double? Q77 { get; set; }
        public double? Q78 { get; set; }
        public double? Q79 { get; set; }
        public double? Q80 { get; set; }
        public double? Q81 { get; set; }
        public double? Q82 { get; set; }
        public double? Q83 { get; set; }
        public double? Q84 { get; set; }
        public double? Q85 { get; set; }
        public double? Q86 { get; set; }
        public double? Q87 { get; set; }
        public double? Q88 { get; set; }
        public double? Q89 { get; set; }
        public double? Q90 { get; set; }
        public double? Q91 { get; set; }
        public double? Q92 { get; set; }
        public double? Q93 { get; set; }
        public double? Q94 { get; set; }
        public double? Q95 { get; set; }
        public double? Q96 { get; set; }
        public double? Q97 { get; set; }
        public double? Q98 { get; set; }
        public double? Q99 { get; set; }
        public double? Q100 { get; set; }
        public double? Q101 { get; set; }
        public double? Q102 { get; set; }
        public double? Q103 { get; set; }
        public double? Q104 { get; set; }
        public double? Q105 { get; set; }
        public double? Q106 { get; set; }
        public double? Q107 { get; set; }
        public double? Q108 { get; set; }
        public double? Q109 { get; set; }
        public double? Q110 { get; set; }
        public double? Q111 { get; set; }
        public double? Q112 { get; set; }
        public double? Q113 { get; set; }
        public double? Q114 { get; set; }
        public double? Q115 { get; set; }
        public double? Q116 { get; set; }
        public double? Q117 { get; set; }
        public double? Q118 { get; set; }
        public double? Q119 { get; set; }
        public double? Q120 { get; set; }
        public double? Q121 { get; set; }
        public double? Q122 { get; set; }
        public double? Q123 { get; set; }
        public double? Q124 { get; set; }
        public double? Q125 { get; set; }
        public double? Q126 { get; set; }
        public double? Q127 { get; set; }
        public double? Q128 { get; set; }
        public double? Q129 { get; set; }
        public double? Q130 { get; set; }
        public double? Q131 { get; set; }
        public double? Q132 { get; set; }
        public double? Q133 { get; set; }
        public double? Q134 { get; set; }
        public double? Q135 { get; set; }
        public double? Q136 { get; set; }
        public double? Q137 { get; set; }
        public double? Q138 { get; set; }
        public double? Q139 { get; set; }
        public double? Q140 { get; set; }
        public double? Q141 { get; set; }
        public double? Q142 { get; set; }
        public double? Q143 { get; set; }
        public double? Q144 { get; set; }
        public double? Q145 { get; set; }
        public double? Q146 { get; set; }
        public double? Q147 { get; set; }
        public double? Q148 { get; set; }
        public double? Q149 { get; set; }
        public double? Q150 { get; set; }
        public double? Q151 { get; set; }
        public double? Q152 { get; set; }
        public double? Q153 { get; set; }
        public double? Q154 { get; set; }
        public double? Q155 { get; set; }
        public double? Q156 { get; set; }
        public double? Q157 { get; set; }
        public double? Q158 { get; set; }
        public double? Q159 { get; set; }
        public double? Q160 { get; set; }
        public double? Q161 { get; set; }
        public double? Q162 { get; set; }
        public double? Q163 { get; set; }
        public double? Q164 { get; set; }
        public double? Q165 { get; set; }
        public double? Q166 { get; set; }
        public double? Q167 { get; set; }
        public double? Q168 { get; set; }
        public double? Q169 { get; set; }
        public double? Q170 { get; set; }
        public double? Q171 { get; set; }
        public double? Q172 { get; set; }
        public string Q173 { get; set; }
        public string Q174 { get; set; }
        public string Q175 { get; set; }
        public double? Q176 { get; set; }
        public double? Q218 { get; set; }
        public double? Q219 { get; set; }
        public double? Q220 { get; set; }
        public double? Q221 { get; set; }
        public double? Q222 { get; set; }
        public double? Q223 { get; set; }
        public double? Q224 { get; set; }
        public double? Q225 { get; set; }
        public double? Q226 { get; set; }
        public double? Q227 { get; set; }
        public double? QNPS1 { get; set; }
        public double? QNPS2 { get; set; }
        public bool? IsSample { get; set; }
        public string QNPS1Comment { get; set; }
        public string QNPS2Comment { get; set; }

        public List<Year> Get2010()
        {
            return db.Staff2010.Select(p => new Year
            {
                StaffID = p.StaffID,
                StaffCode = p.StaffCode,
                Cluster = (p.Cluster == "" ? "Not Specified" : p.Cluster) ?? "Not Specified",
                Division1 = (p.Division1 == "" ? "Not Specified" : p.Division1) ?? "Not Specified",
                Division2 = (p.Division2 == "" ? "Not Specified" : p.Division2) ?? "Not Specified",
                Division3 = (p.Division3 == "" ? "Not Specified" : p.Division3) ?? "Not Specified",
                Division4 = (p.Division4 == "" ? "Not Specified" : p.Division4) ?? "Not Specified",
                Division5 = (p.Division5 == "" ? "Not Specified" : p.Division5) ?? "Not Specified",
                Division6 = (p.Division6 == "" ? "Not Specified" : p.Division6) ?? "Not Specified",
                Branch = (p.Branch == "" ? "Not Specified" : p.Branch) ?? "Not Specified",
                Gender = (p.Gender == "" ? "Not Specified" : p.Gender) ?? "Not Specified",
                Race = (p.Race == "" ? "Not Specified" : p.Race) ?? "Not Specified",
                OccupationalLevel = (p.OccupationalLevel == "" ? "Not Specified" : p.OccupationalLevel) ?? "Not Specified",
                Disabilities = (p.Disabilities == "" ? "Not Specified" : p.Disabilities) ?? "Not Specified",
                Age = (p.Age == "" ? "Not Specified" : p.Age) ?? "Not Specified",
                LOS = (p.LOS == "" ? "Not Specified" : p.LOS) ?? "Not Specified",
                Region = (p.Region == "" ? "Not Specified" : p.Region) ?? "Not Specified",
                Classification = (p.Classification == "" ? "Not Specified" : p.Classification) ?? "Not Specified",
                ManagementPassage = (p.ManagementPassage == "" ? "Not Specified" : p.ManagementPassage) ?? "Not Specified",
                JobFamily = (p.JobFamily == "" ? "Not Specified" : p.JobFamily) ?? "Not Specified",
                Location = (p.Location == "" ? "Not Specified" : p.Location) ?? "Not Specified",
                Q1 = p.Q1,
                Q2 = p.Q2,
                Q3 = p.Q3,
                Q4 = p.Q4,
                Q5 = p.Q5,
                Q6 = p.Q6,
                Q7 = p.Q7,
                Q8 = p.Q8,
                Q9 = p.Q9,
                Q10 = p.Q10,
                Q11 = p.Q11,
                Q12 = p.Q12,
                Q13 = p.Q13,
                Q14 = p.Q14,
                Q15 = p.Q15,
                Q16 = p.Q16,
                Q17 = p.Q17,
                Q18 = p.Q18,
                Q19 = p.Q19,
                Q20 = p.Q20,
                Q21 = p.Q21,
                Q22 = p.Q22,
                Q23 = p.Q23,
                Q24 = p.Q24,
                Q25 = p.Q25,
                Q26 = p.Q26,
                Q27 = p.Q27,
                Q28 = p.Q28,
                Q29 = p.Q29,
                Q30 = p.Q30,
                Q31 = p.Q31,
                Q32 = p.Q32,
                Q33 = p.Q33,
                Q34 = p.Q34,
                Q35 = p.Q35,
                Q36 = p.Q36,
                Q37 = p.Q37,
                Q38 = p.Q38,
                Q39 = p.Q39,
                Q40 = p.Q40,
                Q41 = p.Q41,
                Q42 = p.Q42,
                Q43 = p.Q43,
                Q44 = p.Q44,
                Q45 = p.Q45,
                Q46 = p.Q46,
                Q47 = p.Q47,
                Q48 = p.Q48,
                Q49 = p.Q49,
                Q50 = p.Q50,
                Q51 = p.Q51,
                Q52 = p.Q52,
                Q53 = p.Q53,
                Q54 = p.Q54,
                Q55 = p.Q55,
                Q56 = p.Q56,
                Q57 = p.Q57,
                Q58 = p.Q58,
                Q59 = p.Q59,
                Q60 = p.Q60,
                Q61 = p.Q61,
                Q62 = p.Q62,
                Q63 = p.Q63,
                Q64 = p.Q64,
                Q65 = p.Q65,
                Q66 = p.Q66,
                Q67 = p.Q67,
                Q68 = p.Q68,
                Q69 = p.Q69,
                Q70 = p.Q70,
                Q71 = p.Q71,
                Q72 = p.Q72,
                Q73 = p.Q73,
                Q74 = p.Q74,
                Q75 = p.Q75,
                Q76 = p.Q76,
                Q77 = p.Q77,
                Q78 = p.Q78,
                Q79 = p.Q79,
                Q80 = p.Q80,
                Q81 = p.Q81,
                Q82 = p.Q82,
                Q83 = p.Q83,
                Q84 = p.Q84,
                Q85 = p.Q85,
                Q86 = p.Q86,
                Q87 = p.Q87,
                Q88 = p.Q88,
                Q89 = p.Q89,
                Q90 = p.Q90,
                Q91 = p.Q91,
                Q92 = p.Q92,
                Q93 = p.Q93,
                Q94 = p.Q94,
                Q95 = p.Q95,
                Q96 = p.Q96,
                Q97 = p.Q97,
                Q98 = p.Q98,
                Q99 = p.Q99,
                Q100 = p.Q100,
                Q101 = p.Q101,
                Q102 = p.Q102,
                Q103 = p.Q103,
                Q104 = p.Q104,
                Q105 = p.Q105,
                Q106 = p.Q106,
                Q107 = p.Q107,
                Q108 = p.Q108,
                Q109 = p.Q109,
                Q110 = p.Q110,
                Q111 = p.Q111,
                Q112 = p.Q112,
                Q113 = p.Q113,
                Q114 = p.Q114,
                Q115 = p.Q115,
                Q116 = p.Q116,
                Q117 = p.Q117,
                Q118 = p.Q118,
                Q119 = p.Q119,
                Q120 = p.Q120,
                Q121 = p.Q121,
                Q122 = p.Q122,
                Q123 = p.Q123,
                Q124 = p.Q124,
                Q125 = p.Q125,
                Q126 = p.Q126,
                Q127 = p.Q127,
                Q128 = p.Q128,
                Q129 = p.Q129,
                Q130 = p.Q130,
                Q131 = p.Q131,
                Q132 = p.Q132,
                Q133 = p.Q133,
                Q134 = p.Q134,
                Q135 = p.Q135,
                Q136 = p.Q136,
                Q137 = p.Q137,
                Q138 = p.Q138,
                Q139 = p.Q139,
                Q140 = p.Q140,
                Q141 = p.Q141,
                Q142 = p.Q142,
                Q143 = p.Q143,
                Q144 = p.Q144,
                Q145 = p.Q145,
                Q146 = p.Q146,
                Q147 = p.Q147,
                Q148 = p.Q148,
                Q149 = p.Q149,
                Q150 = p.Q150,
                Q151 = p.Q151,
                Q152 = p.Q152,
                Q153 = p.Q153,
                Q154 = p.Q154,
                Q155 = p.Q155,
                Q156 = p.Q156,
                Q157 = p.Q157,
                Q158 = p.Q158,
                Q159 = p.Q159,
                Q160 = p.Q160,
                Q161 = p.Q161,
                Q162 = p.Q162,
                Q163 = p.Q163,
                Q164 = p.Q164,
                Q165 = p.Q165,
                Q166 = p.Q166,
                Q167 = p.Q167,
                Q168 = p.Q168,
                Q169 = p.Q169,
                Q170 = p.Q170,
                Q171 = p.Q171,
                Q172 = p.Q172,
                Q173 = p.Q173,
                Q174 = p.Q174,
                Q175 = p.Q175,
                Q176 = p.Q176,
                Q218 = null,
                Q219 = null,
                Q220 = null,
                Q221 = null,
                Q222 = null,
                Q223 = null,
                Q224 = null,
                Q225 = null,
                Q226 = null,
                Q227 = null,
                QNPS1 = null,
                QNPS2 = null,
                IsSample = p.IsSample,
                QNPS1Comment = null,
                QNPS2Comment = null
            }).Distinct().ToList();
        }

        public List<Year> Get2011()
        {
            return db.Staff2011.Select(p => new Year
            {
                StaffID = p.StaffID,
                StaffCode = p.StaffCode,
                Cluster = (p.Cluster == "" ? "Not Specified" : p.Cluster) ?? "Not Specified",
                Division1 = (p.Division1 == "" ? "Not Specified" : p.Division1) ?? "Not Specified",
                Division2 = (p.Division2 == "" ? "Not Specified" : p.Division2) ?? "Not Specified",
                Division3 = (p.Division3 == "" ? "Not Specified" : p.Division3) ?? "Not Specified",
                Division4 = (p.Division4 == "" ? "Not Specified" : p.Division4) ?? "Not Specified",
                Division5 = (p.Division5 == "" ? "Not Specified" : p.Division5) ?? "Not Specified",
                Division6 = (p.Division6 == "" ? "Not Specified" : p.Division6) ?? "Not Specified",
                Branch = (p.Branch == "" ? "Not Specified" : p.Branch) ?? "Not Specified",
                Gender = (p.Gender == "" ? "Not Specified" : p.Gender) ?? "Not Specified",
                Race = (p.Race == "" ? "Not Specified" : p.Race) ?? "Not Specified",
                OccupationalLevel = (p.OccupationalLevel == "" ? "Not Specified" : p.OccupationalLevel) ?? "Not Specified",
                Disabilities = (p.Disabilities == "" ? "Not Specified" : p.Disabilities) ?? "Not Specified",
                Age = (p.Age == "" ? "Not Specified" : p.Age) ?? "Not Specified",
                LOS = (p.LOS == "" ? "Not Specified" : p.LOS) ?? "Not Specified",
                Region = (p.Region == "" ? "Not Specified" : p.Region) ?? "Not Specified",
                Classification = (p.Classification == "" ? "Not Specified" : p.Classification) ?? "Not Specified",
                ManagementPassage = (p.ManagementPassage == "" ? "Not Specified" : p.ManagementPassage) ?? "Not Specified",
                JobFamily = (p.JobFamily == "" ? "Not Specified" : p.JobFamily) ?? "Not Specified",
                Location = (p.Location == "" ? "Not Specified" : p.Location) ?? "Not Specified",
                Q1 = p.Q1,
                Q2 = p.Q2,
                Q3 = p.Q3,
                Q4 = p.Q4,
                Q5 = p.Q5,
                Q6 = p.Q6,
                Q7 = p.Q7,
                Q8 = p.Q8,
                Q9 = p.Q9,
                Q10 = p.Q10,
                Q11 = p.Q11,
                Q12 = p.Q12,
                Q13 = p.Q13,
                Q14 = p.Q14,
                Q15 = p.Q15,
                Q16 = p.Q16,
                Q17 = p.Q17,
                Q18 = p.Q18,
                Q19 = p.Q19,
                Q20 = p.Q20,
                Q21 = p.Q21,
                Q22 = p.Q22,
                Q23 = p.Q23,
                Q24 = p.Q24,
                Q25 = p.Q25,
                Q26 = p.Q26,
                Q27 = p.Q27,
                Q28 = p.Q28,
                Q29 = p.Q29,
                Q30 = p.Q30,
                Q31 = p.Q31,
                Q32 = p.Q32,
                Q33 = p.Q33,
                Q34 = p.Q34,
                Q35 = p.Q35,
                Q36 = p.Q36,
                Q37 = p.Q37,
                Q38 = p.Q38,
                Q39 = p.Q39,
                Q40 = p.Q40,
                Q41 = p.Q41,
                Q42 = p.Q42,
                Q43 = p.Q43,
                Q44 = p.Q44,
                Q45 = p.Q45,
                Q46 = p.Q46,
                Q47 = p.Q47,
                Q48 = p.Q48,
                Q49 = p.Q49,
                Q50 = p.Q50,
                Q51 = p.Q51,
                Q52 = p.Q52,
                Q53 = p.Q53,
                Q54 = p.Q54,
                Q55 = p.Q55,
                Q56 = p.Q56,
                Q57 = p.Q57,
                Q58 = p.Q58,
                Q59 = p.Q59,
                Q60 = p.Q60,
                Q61 = p.Q61,
                Q62 = p.Q62,
                Q63 = p.Q63,
                Q64 = p.Q64,
                Q65 = p.Q65,
                Q66 = p.Q66,
                Q67 = p.Q67,
                Q68 = p.Q68,
                Q69 = p.Q69,
                Q70 = p.Q70,
                Q71 = p.Q71,
                Q72 = p.Q72,
                Q73 = p.Q73,
                Q74 = p.Q74,
                Q75 = p.Q75,
                Q76 = p.Q76,
                Q77 = p.Q77,
                Q78 = p.Q78,
                Q79 = p.Q79,
                Q80 = p.Q80,
                Q81 = p.Q81,
                Q82 = p.Q82,
                Q83 = p.Q83,
                Q84 = p.Q84,
                Q85 = p.Q85,
                Q86 = p.Q86,
                Q87 = p.Q87,
                Q88 = p.Q88,
                Q89 = p.Q89,
                Q90 = p.Q90,
                Q91 = p.Q91,
                Q92 = p.Q92,
                Q93 = p.Q93,
                Q94 = p.Q94,
                Q95 = p.Q95,
                Q96 = p.Q96,
                Q97 = p.Q97,
                Q98 = p.Q98,
                Q99 = p.Q99,
                Q100 = p.Q100,
                Q101 = p.Q101,
                Q102 = p.Q102,
                Q103 = p.Q103,
                Q104 = p.Q104,
                Q105 = p.Q105,
                Q106 = p.Q106,
                Q107 = p.Q107,
                Q108 = p.Q108,
                Q109 = p.Q109,
                Q110 = p.Q110,
                Q111 = p.Q111,
                Q112 = p.Q112,
                Q113 = p.Q113,
                Q114 = p.Q114,
                Q115 = p.Q115,
                Q116 = p.Q116,
                Q117 = p.Q117,
                Q118 = p.Q118,
                Q119 = p.Q119,
                Q120 = p.Q120,
                Q121 = p.Q121,
                Q122 = p.Q122,
                Q123 = p.Q123,
                Q124 = p.Q124,
                Q125 = p.Q125,
                Q126 = p.Q126,
                Q127 = p.Q127,
                Q128 = p.Q128,
                Q129 = p.Q129,
                Q130 = p.Q130,
                Q131 = p.Q131,
                Q132 = p.Q132,
                Q133 = p.Q133,
                Q134 = p.Q134,
                Q135 = p.Q135,
                Q136 = p.Q136,
                Q137 = p.Q137,
                Q138 = p.Q138,
                Q139 = p.Q139,
                Q140 = p.Q140,
                Q141 = p.Q141,
                Q142 = p.Q142,
                Q143 = p.Q143,
                Q144 = p.Q144,
                Q145 = p.Q145,
                Q146 = p.Q146,
                Q147 = p.Q147,
                Q148 = p.Q148,
                Q149 = p.Q149,
                Q150 = p.Q150,
                Q151 = p.Q151,
                Q152 = p.Q152,
                Q153 = p.Q153,
                Q154 = p.Q154,
                Q155 = p.Q155,
                Q156 = p.Q156,
                Q157 = p.Q157,
                Q158 = p.Q158,
                Q159 = p.Q159,
                Q160 = p.Q160,
                Q161 = p.Q161,
                Q162 = p.Q162,
                Q163 = p.Q163,
                Q164 = p.Q164,
                Q165 = p.Q165,
                Q166 = p.Q166,
                Q167 = p.Q167,
                Q168 = p.Q168,
                Q169 = p.Q169,
                Q170 = p.Q170,
                Q171 = p.Q171,
                Q172 = p.Q172,
                Q173 = p.Q173,
                Q174 = p.Q174,
                Q175 = p.Q175,
                Q176 = p.Q176,
                Q218 = p.Q218,
                Q219 = p.Q219,
                Q220 = p.Q220,
                Q221 = p.Q221,
                Q222 = p.Q222,
                Q223 = p.Q223,
                Q224 = p.Q224,
                Q225 = null,
                Q226 = null,
                Q227 = null,
                QNPS1 = null,
                QNPS2 = null,
                IsSample = p.IsSample,
                QNPS1Comment = null,
                QNPS2Comment = null
            }).Distinct().ToList();
        }

        public List<Year> Get2012()
        {
            return db.Staff2012.Select(p => new Year
            {
                StaffID = p.StaffID,
                StaffCode = p.StaffCode,
                Cluster = (p.Cluster == "" ? "Not Specified" : p.Cluster) ?? "Not Specified",
                Division1 = (p.Division1 == "" ? "Not Specified" : p.Division1) ?? "Not Specified",
                Division2 = (p.Division2 == "" ? "Not Specified" : p.Division2) ?? "Not Specified",
                Division3 = (p.Division3 == "" ? "Not Specified" : p.Division3) ?? "Not Specified",
                Division4 = (p.Division4 == "" ? "Not Specified" : p.Division4) ?? "Not Specified",
                Division5 = (p.Division5 == "" ? "Not Specified" : p.Division5) ?? "Not Specified",
                Division6 = (p.Division6 == "" ? "Not Specified" : p.Division6) ?? "Not Specified",
                Branch = (p.Branch == "" ? "Not Specified" : p.Branch) ?? "Not Specified",
                Gender = (p.Gender == "" ? "Not Specified" : p.Gender) ?? "Not Specified",
                Race = (p.Race == "" ? "Not Specified" : p.Race) ?? "Not Specified",
                OccupationalLevel = (p.OccupationalLevel == "" ? "Not Specified" : p.OccupationalLevel) ?? "Not Specified",
                Disabilities = (p.Disabilities == "" ? "Not Specified" : p.Disabilities) ?? "Not Specified",
                Age = (p.Age == "" ? "Not Specified" : p.Age) ?? "Not Specified",
                LOS = (p.LOS == "" ? "Not Specified" : p.LOS) ?? "Not Specified",
                Region = (p.Region == "" ? "Not Specified" : p.Region) ?? "Not Specified",
                Classification = (p.Classification == "" ? "Not Specified" : p.Classification) ?? "Not Specified",
                ManagementPassage = (p.ManagementPassage == "" ? "Not Specified" : p.ManagementPassage) ?? "Not Specified",
                JobFamily = (p.JobFamily == "" ? "Not Specified" : p.JobFamily) ?? "Not Specified",
                Location = (p.Location == "" ? "Not Specified" : p.Location) ?? "Not Specified",
                Q1 = p.Q1,
                Q2 = p.Q2,
                Q3 = null,
                Q4 = p.Q4,
                Q5 = p.Q5,
                Q6 = null,
                Q7 = null,
                Q8 = null,
                Q9 = null,
                Q10 = null,
                Q11 = p.Q11,
                Q12 = null,
                Q13 = p.Q13,
                Q14 = p.Q14,
                Q15 = p.Q15,
                Q16 = p.Q16,
                Q17 = null,
                Q18 = p.Q18,
                Q19 = null,
                Q20 = null,
                Q21 = p.Q21,
                Q22 = null,
                Q23 = p.Q23,
                Q24 = p.Q24,
                Q25 = null,
                Q26 = p.Q26,
                Q27 = null,
                Q28 = null,
                Q29 = p.Q29,
                Q30 = p.Q30,
                Q31 = null,
                Q32 = null,
                Q33 = null,
                Q34 = null,
                Q35 = null,
                Q36 = null,
                Q37 = null,
                Q38 = null,
                Q39 = p.Q39,
                Q40 = null,
                Q41 = null,
                Q42 = null,
                Q43 = null,
                Q44 = p.Q44,
                Q45 = null,
                Q46 = null,
                Q47 = null,
                Q48 = p.Q48,
                Q49 = null,
                Q50 = p.Q50,
                Q51 = null,
                Q52 = null,
                Q53 = p.Q53,
                Q54 = null,
                Q55 = null,
                Q56 = null,
                Q57 = null,
                Q58 = null,
                Q59 = p.Q59,
                Q60 = p.Q60,
                Q61 = null,
                Q62 = null,
                Q63 = null,
                Q64 = null,
                Q65 = null,
                Q66 = null,
                Q67 = p.Q67,
                Q68 = p.Q68,
                Q69 = null,
                Q70 = p.Q70,
                Q71 = null,
                Q72 = p.Q72,
                Q73 = p.Q73,
                Q74 = null,
                Q75 = p.Q75,
                Q76 = null,
                Q77 = p.Q77,
                Q78 = p.Q78,
                Q79 = p.Q79,
                Q80 = p.Q80,
                Q81 = p.Q81,
                Q82 = null,
                Q83 = null,
                Q84 = p.Q84,
                Q85 = null,
                Q86 = p.Q86,
                Q87 = null,
                Q88 = p.Q88,
                Q89 = null,
                Q90 = p.Q90,
                Q91 = p.Q91,
                Q92 = p.Q92,
                Q93 = p.Q93,
                Q94 = null,
                Q95 = p.Q95,
                Q96 = null,
                Q97 = null,
                Q98 = null,
                Q99 = p.Q99,
                Q100 = null,
                Q101 = p.Q101,
                Q102 = null,
                Q103 = null,
                Q104 = p.Q104,
                Q105 = null,
                Q106 = null,
                Q107 = null,
                Q108 = p.Q108,
                Q109 = p.Q109,
                Q110 = null,
                Q111 = p.Q111,
                Q112 = p.Q112,
                Q113 = null,
                Q114 = null,
                Q115 = null,
                Q116 = p.Q116,
                Q117 = p.Q117,
                Q118 = p.Q118,
                Q119 = p.Q119,
                Q120 = null,
                Q121 = p.Q121,
                Q122 = p.Q122,
                Q123 = null,
                Q124 = p.Q124,
                Q125 = p.Q125,
                Q126 = p.Q126,
                Q127 = null,
                Q128 = p.Q128,
                Q129 = p.Q129,
                Q130 = null,
                Q131 = p.Q131,
                Q132 = p.Q132,
                Q133 = p.Q133,
                Q134 = null,
                Q135 = p.Q135,
                Q136 = p.Q136,
                Q137 = null,
                Q138 = p.Q138,
                Q139 = null,
                Q140 = p.Q140,
                Q141 = p.Q141,
                Q142 = null,
                Q143 = p.Q143,
                Q144 = null,
                Q145 = p.Q145,
                Q146 = null,
                Q147 = p.Q147,
                Q148 = p.Q148,
                Q149 = p.Q149,
                Q150 = p.Q150,
                Q151 = p.Q151,
                Q152 = p.Q152,
                Q153 = null,
                Q154 = null,
                Q155 = p.Q155,
                Q156 = null,
                Q157 = null,
                Q158 = p.Q158,
                Q159 = p.Q159,
                Q160 = null,
                Q161 = p.Q161,
                Q162 = p.Q162,
                Q163 = null,
                Q164 = p.Q164,
                Q165 = null,
                Q166 = null,
                Q167 = null,
                Q168 = null,
                Q169 = null,
                Q170 = null,
                Q171 = p.Q171,
                Q172 = p.Q172,
                Q173 = null,
                Q174 = null,
                Q175 = null,
                Q176 = p.Q176,
                Q218 = null,
                Q219 = null,
                Q220 = null,
                Q221 = null,
                Q222 = null,
                Q223 = null,
                Q224 = null,
                Q225 = p.Q225,
                Q226 = p.Q226,
                Q227 = p.Q227,
                QNPS1 = p.QNPS1,
                QNPS2 = p.QNPS2,
                IsSample = p.IsSample,
                QNPS1Comment = null,
                QNPS2Comment = null
            }).Distinct().ToList();
        }

        public List<Year> Get2013()
        {
            return db.Staff2013.Select(p => new Year
            {
                StaffID = p.StaffID,
                StaffCode = p.StaffCode,
                Cluster = (p.Cluster == "" ? "Not Specified" : p.Cluster) ?? "Not Specified",
                Division1 = (p.Division1 == "" ? "Not Specified" : p.Division1) ?? "Not Specified",
                Division2 = (p.Division2 == "" ? "Not Specified" : p.Division2) ?? "Not Specified",
                Division3 = (p.Division3 == "" ? "Not Specified" : p.Division3) ?? "Not Specified",
                Division4 = (p.Division4 == "" ? "Not Specified" : p.Division4) ?? "Not Specified",
                Division5 = (p.Division5 == "" ? "Not Specified" : p.Division5) ?? "Not Specified",
                Division6 = (p.Division6 == "" ? "Not Specified" : p.Division6) ?? "Not Specified",
                Branch = (p.Branch == "" ? "Not Specified" : p.Branch) ?? "Not Specified",
                Gender = (p.Gender == "" ? "Not Specified" : p.Gender) ?? "Not Specified",
                Race = (p.Race == "" ? "Not Specified" : p.Race) ?? "Not Specified",
                OccupationalLevel = (p.OccupationalLevel == "" ? "Not Specified" : p.OccupationalLevel) ?? "Not Specified",
                Disabilities = (p.Disabilities == "" ? "Not Specified" : p.Disabilities) ?? "Not Specified",
                Age = (p.Age == "" ? "Not Specified" : p.Age) ?? "Not Specified",
                LOS = (p.LOS == "" ? "Not Specified" : p.LOS) ?? "Not Specified",
                Region = (p.Region == "" ? "Not Specified" : p.Region) ?? "Not Specified",
                Classification = (p.Classification == "" ? "Not Specified" : p.Classification) ?? "Not Specified",
                ManagementPassage = (p.ManagementPassage == "" ? "Not Specified" : p.ManagementPassage) ?? "Not Specified",
                JobFamily = (p.JobFamily == "" ? "Not Specified" : p.JobFamily) ?? "Not Specified",
                Location = (p.Location == "" ? "Not Specified" : p.Location) ?? "Not Specified",
                Q1 = p.Q1,
                Q2 = p.Q2,
                Q3 = null,
                Q4 = p.Q4,
                Q5 = p.Q5,
                Q6 = null,
                Q7 = null,
                Q8 = null,
                Q9 = null,
                Q10 = null,
                Q11 = p.Q11,
                Q12 = null,
                Q13 = p.Q13,
                Q14 = p.Q14,
                Q15 = p.Q15,
                Q16 = p.Q16,
                Q17 = null,
                Q18 = p.Q18,
                Q19 = null,
                Q20 = null,
                Q21 = p.Q21,
                Q22 = null,
                Q23 = p.Q23,
                Q24 = p.Q24,
                Q25 = null,
                Q26 = p.Q26,
                Q27 = null,
                Q28 = null,
                Q29 = p.Q29,
                Q30 = p.Q30,
                Q31 = null,
                Q32 = null,
                Q33 = null,
                Q34 = null,
                Q35 = null,
                Q36 = null,
                Q37 = null,
                Q38 = null,
                Q39 = p.Q39,
                Q40 = null,
                Q41 = null,
                Q42 = null,
                Q43 = null,
                Q44 = p.Q44,
                Q45 = null,
                Q46 = null,
                Q47 = null,
                Q48 = p.Q48,
                Q49 = null,
                Q50 = p.Q50,
                Q51 = null,
                Q52 = null,
                Q53 = p.Q53,
                Q54 = null,
                Q55 = null,
                Q56 = null,
                Q57 = null,
                Q58 = null,
                Q59 = p.Q59,
                Q60 = p.Q60,
                Q61 = null,
                Q62 = null,
                Q63 = null,
                Q64 = null,
                Q65 = null,
                Q66 = null,
                Q67 = p.Q67,
                Q68 = p.Q68,
                Q69 = null,
                Q70 = p.Q70,
                Q71 = null,
                Q72 = p.Q72,
                Q73 = p.Q73,
                Q74 = null,
                Q75 = p.Q75,
                Q76 = null,
                Q77 = p.Q77,
                Q78 = p.Q78,
                Q79 = p.Q79,
                Q80 = p.Q80,
                Q81 = p.Q81,
                Q82 = null,
                Q83 = null,
                Q84 = p.Q84,
                Q85 = null,
                Q86 = p.Q86,
                Q87 = null,
                Q88 = p.Q88,
                Q89 = null,
                Q90 = p.Q90,
                Q91 = p.Q91,
                Q92 = p.Q92,
                Q93 = p.Q93,
                Q94 = null,
                Q95 = p.Q95,
                Q96 = null,
                Q97 = null,
                Q98 = null,
                Q99 = p.Q99,
                Q100 = null,
                Q101 = p.Q101,
                Q102 = null,
                Q103 = null,
                Q104 = p.Q104,
                Q105 = null,
                Q106 = null,
                Q107 = null,
                Q108 = p.Q108,
                Q109 = p.Q109,
                Q110 = null,
                Q111 = p.Q111,
                Q112 = p.Q112,
                Q113 = null,
                Q114 = null,
                Q115 = null,
                Q116 = p.Q116,
                Q117 = p.Q117,
                Q118 = p.Q118,
                Q119 = p.Q119,
                Q120 = null,
                Q121 = p.Q121,
                Q122 = p.Q122,
                Q123 = null,
                Q124 = p.Q124,
                Q125 = p.Q125,
                Q126 = p.Q126,
                Q127 = null,
                Q128 = p.Q128,
                Q129 = p.Q129,
                Q130 = null,
                Q131 = p.Q131,
                Q132 = p.Q132,
                Q133 = p.Q133,
                Q134 = null,
                Q135 = p.Q135,
                Q136 = p.Q136,
                Q137 = null,
                Q138 = p.Q138,
                Q139 = null,
                Q140 = p.Q140,
                Q141 = p.Q141,
                Q142 = null,
                Q143 = p.Q143,
                Q144 = null,
                Q145 = p.Q145,
                Q146 = null,
                Q147 = p.Q147,
                Q148 = p.Q148,
                Q149 = p.Q149,
                Q150 = p.Q150,
                Q151 = p.Q151,
                Q152 = p.Q152,
                Q153 = null,
                Q154 = null,
                Q155 = p.Q155,
                Q156 = null,
                Q157 = null,
                Q158 = p.Q158,
                Q159 = p.Q159,
                Q160 = null,
                Q161 = p.Q161,
                Q162 = p.Q162,
                Q163 = null,
                Q164 = p.Q164,
                Q165 = null,
                Q166 = null,
                Q167 = null,
                Q168 = null,
                Q169 = null,
                Q170 = null,
                Q171 = p.Q171,
                Q172 = p.Q172,
                Q173 = null,
                Q174 = null,
                Q175 = null,
                Q176 = p.Q176,
                Q218 = null,
                Q219 = null,
                Q220 = null,
                Q221 = null,
                Q222 = null,
                Q223 = null,
                Q224 = null,
                Q225 = p.Q225,
                Q226 = p.Q226,
                Q227 = p.Q227,
                QNPS1 = p.QNPS1,
                QNPS2 = p.QNPS2,
                IsSample = p.IsSample,
                QNPS1Comment = p.QNPS1Comments,
                QNPS2Comment = p.QNPS2Comments
            }).Distinct().ToList();
        }

        public List<Year> Get2014()
        {
            return db.Staff2014.Select(p => new Year
            {
                StaffID = p.StaffID,
                StaffCode = p.StaffCode,
                Cluster = (p.Cluster == "" ? "Not Specified" : p.Cluster) ?? "Not Specified",
                Division1 = (p.Division1 == "" ? "Not Specified" : p.Division1) ?? "Not Specified",
                Division2 = (p.Division2 == "" ? "Not Specified" : p.Division2) ?? "Not Specified",
                Division3 = (p.Division3 == "" ? "Not Specified" : p.Division3) ?? "Not Specified",
                Division4 = (p.Division4 == "" ? "Not Specified" : p.Division4) ?? "Not Specified",
                Division5 = (p.Division5 == "" ? "Not Specified" : p.Division5) ?? "Not Specified",
                Division6 = (p.Division6 == "" ? "Not Specified" : p.Division6) ?? "Not Specified",
                Branch = (p.Branch == "" ? "Not Specified" : p.Branch) ?? "Not Specified",
                Gender = (p.Gender == "" ? "Not Specified" : p.Gender) ?? "Not Specified",
                Race = (p.Race == "" ? "Not Specified" : p.Race) ?? "Not Specified",
                OccupationalLevel = (p.OccupationalLevel == "" ? "Not Specified" : p.OccupationalLevel) ?? "Not Specified",
                Disabilities = (p.Disabilities == "" ? "Not Specified" : p.Disabilities) ?? "Not Specified",
                Age = (p.Age == "" ? "Not Specified" : p.Age) ?? "Not Specified",
                LOS = (p.LOS == "" ? "Not Specified" : p.LOS) ?? "Not Specified",
                Region = (p.Region == "" ? "Not Specified" : p.Region) ?? "Not Specified",
                Classification = (p.Classification == "" ? "Not Specified" : p.Classification) ?? "Not Specified",
                ManagementPassage = (p.ManagementPassage == "" ? "Not Specified" : p.ManagementPassage) ?? "Not Specified",
                JobFamily = (p.JobFamily == "" ? "Not Specified" : p.JobFamily) ?? "Not Specified",
                Location = (p.Location == "" ? "Not Specified" : p.Location) ?? "Not Specified",
                Q1 = p.Q1,
                Q2 = p.Q2,
                Q3 = null,
                Q4 = p.Q4,
                Q5 = p.Q5,
                Q6 = null,
                Q7 = null,
                Q8 = null,
                Q9 = null,
                Q10 = null,
                Q11 = p.Q11,
                Q12 = null,
                Q13 = p.Q13,
                Q14 = p.Q14,
                Q15 = p.Q15,
                Q16 = p.Q16,
                Q17 = null,
                Q18 = p.Q18,
                Q19 = null,
                Q20 = null,
                Q21 = p.Q21,
                Q22 = null,
                Q23 = p.Q23,
                Q24 = p.Q24,
                Q25 = null,
                Q26 = p.Q26,
                Q27 = null,
                Q28 = null,
                Q29 = p.Q29,
                Q30 = p.Q30,
                Q31 = null,
                Q32 = null,
                Q33 = null,
                Q34 = null,
                Q35 = null,
                Q36 = null,
                Q37 = null,
                Q38 = null,
                Q39 = p.Q39,
                Q40 = null,
                Q41 = null,
                Q42 = null,
                Q43 = null,
                Q44 = p.Q44,
                Q45 = null,
                Q46 = null,
                Q47 = null,
                Q48 = p.Q48,
                Q49 = null,
                Q50 = p.Q50,
                Q51 = null,
                Q52 = null,
                Q53 = p.Q53,
                Q54 = null,
                Q55 = null,
                Q56 = null,
                Q57 = null,
                Q58 = null,
                Q59 = p.Q59,
                Q60 = p.Q60,
                Q61 = null,
                Q62 = null,
                Q63 = null,
                Q64 = null,
                Q65 = null,
                Q66 = null,
                Q67 = p.Q67,
                Q68 = p.Q68,
                Q69 = null,
                Q70 = p.Q70,
                Q71 = null,
                Q72 = p.Q72,
                Q73 = p.Q73,
                Q74 = null,
                Q75 = p.Q75,
                Q76 = null,
                Q77 = p.Q77,
                Q78 = p.Q78,
                Q79 = p.Q79,
                Q80 = p.Q80,
                Q81 = p.Q81,
                Q82 = null,
                Q83 = null,
                Q84 = p.Q84,
                Q85 = null,
                Q86 = p.Q86,
                Q87 = null,
                Q88 = p.Q88,
                Q89 = null,
                Q90 = p.Q90,
                Q91 = p.Q91,
                Q92 = p.Q92,
                Q93 = p.Q93,
                Q94 = null,
                Q95 = p.Q95,
                Q96 = null,
                Q97 = null,
                Q98 = null,
                Q99 = p.Q99,
                Q100 = null,
                Q101 = p.Q101,
                Q102 = null,
                Q103 = null,
                Q104 = p.Q104,
                Q105 = null,
                Q106 = null,
                Q107 = null,
                Q108 = p.Q108,
                Q109 = p.Q109,
                Q110 = null,
                Q111 = p.Q111,
                Q112 = p.Q112,
                Q113 = null,
                Q114 = null,
                Q115 = null,
                Q116 = p.Q116,
                Q117 = p.Q117,
                Q118 = p.Q118,
                Q119 = p.Q119,
                Q120 = null,
                Q121 = p.Q121,
                Q122 = p.Q122,
                Q123 = null,
                Q124 = p.Q124,
                Q125 = p.Q125,
                Q126 = p.Q126,
                Q127 = null,
                Q128 = p.Q128,
                Q129 = p.Q129,
                Q130 = null,
                Q131 = p.Q131,
                Q132 = p.Q132,
                Q133 = p.Q133,
                Q134 = null,
                Q135 = p.Q135,
                Q136 = p.Q136,
                Q137 = null,
                Q138 = p.Q138,
                Q139 = null,
                Q140 = p.Q140,
                Q141 = p.Q141,
                Q142 = null,
                Q143 = p.Q143,
                Q144 = null,
                Q145 = p.Q145,
                Q146 = null,
                Q147 = p.Q147,
                Q148 = p.Q148,
                Q149 = p.Q149,
                Q150 = p.Q150,
                Q151 = p.Q151,
                Q152 = p.Q152,
                Q153 = null,
                Q154 = null,
                Q155 = p.Q155,
                Q156 = null,
                Q157 = null,
                Q158 = p.Q158,
                Q159 = p.Q159,
                Q160 = null,
                Q161 = p.Q161,
                Q162 = p.Q162,
                Q163 = null,
                Q164 = p.Q164,
                Q165 = null,
                Q166 = null,
                Q167 = null,
                Q168 = null,
                Q169 = null,
                Q170 = null,
                Q171 = p.Q171,
                Q172 = p.Q172,
                Q173 = null,
                Q174 = null,
                Q175 = null,
                Q176 = p.Q176,
                Q218 = null,
                Q219 = null,
                Q220 = null,
                Q221 = null,
                Q222 = null,
                Q223 = null,
                Q224 = null,
                Q225 = p.Q225,
                Q226 = p.Q226,
                Q227 = p.Q227,
                QNPS1 = p.QNPS1,
                QNPS2 = p.QNPS2,
                IsSample = p.IsSample,
                QNPS1Comment = p.QNPS1Comments,
                QNPS2Comment = p.QNPS2Comments
            }).Distinct().ToList();
        }

        public List<Year> Get2015()
        {
            return db.Staff2015.Select(p => new Year
            {
                StaffID = p.StaffID,
                StaffCode = p.StaffCode,
                Cluster = (p.Cluster == "" ? "Not Specified" : p.Cluster) ?? "Not Specified",
                Division1 = (p.Division1 == "" ? "Not Specified" : p.Division1) ?? "Not Specified",
                Division2 = (p.Division2 == "" ? "Not Specified" : p.Division2) ?? "Not Specified",
                Division3 = (p.Division3 == "" ? "Not Specified" : p.Division3) ?? "Not Specified",
                Division4 = (p.Division4 == "" ? "Not Specified" : p.Division4) ?? "Not Specified",
                Division5 = (p.Division5 == "" ? "Not Specified" : p.Division5) ?? "Not Specified",
                Division6 = (p.Division6 == "" ? "Not Specified" : p.Division6) ?? "Not Specified",
                Branch = (p.Branch == "" ? "Not Specified" : p.Branch) ?? "Not Specified",
                Gender = (p.Gender == "" ? "Not Specified" : p.Gender) ?? "Not Specified",
                Race = (p.Race == "" ? "Not Specified" : p.Race) ?? "Not Specified",
                OccupationalLevel = (p.OccupationalLevel == "" ? "Not Specified" : p.OccupationalLevel) ?? "Not Specified",
                Disabilities = (p.Disabilities == "" ? "Not Specified" : p.Disabilities) ?? "Not Specified",
                Age = (p.Age == "" ? "Not Specified" : p.Age) ?? "Not Specified",
                LOS = (p.LOS == "" ? "Not Specified" : p.LOS) ?? "Not Specified",
                Region = (p.Region == "" ? "Not Specified" : p.Region) ?? "Not Specified",
                Classification = (p.Classification == "" ? "Not Specified" : p.Classification) ?? "Not Specified",
                ManagementPassage = (p.ManagementPassage == "" ? "Not Specified" : p.ManagementPassage) ?? "Not Specified",
                JobFamily = (p.JobFamily == "" ? "Not Specified" : p.JobFamily) ?? "Not Specified",
                Location = (p.Location == "" ? "Not Specified" : p.Location) ?? "Not Specified",
                Q1 = p.Q1,
                Q2 = p.Q2,
                Q3 = null,
                Q4 = p.Q4,
                Q5 = p.Q5,
                Q6 = null,
                Q7 = null,
                Q8 = null,
                Q9 = null,
                Q10 = null,
                Q11 = p.Q11,
                Q12 = null,
                Q13 = p.Q13,
                Q14 = p.Q14,
                Q15 = p.Q15,
                Q16 = p.Q16,
                Q17 = null,
                Q18 = p.Q18,
                Q19 = null,
                Q20 = null,
                Q21 = p.Q21,
                Q22 = null,
                Q23 = p.Q23,
                Q24 = p.Q24,
                Q25 = null,
                Q26 = p.Q26,
                Q27 = null,
                Q28 = null,
                Q29 = p.Q29,
                Q30 = p.Q30,
                Q31 = null,
                Q32 = null,
                Q33 = null,
                Q34 = null,
                Q35 = null,
                Q36 = null,
                Q37 = null,
                Q38 = null,
                Q39 = p.Q39,
                Q40 = null,
                Q41 = null,
                Q42 = null,
                Q43 = null,
                Q44 = p.Q44,
                Q45 = null,
                Q46 = null,
                Q47 = null,
                Q48 = p.Q48,
                Q49 = null,
                Q50 = p.Q50,
                Q51 = null,
                Q52 = null,
                Q53 = p.Q53,
                Q54 = null,
                Q55 = null,
                Q56 = null,
                Q57 = null,
                Q58 = null,
                Q59 = p.Q59,
                Q60 = p.Q60,
                Q61 = null,
                Q62 = null,
                Q63 = null,
                Q64 = null,
                Q65 = null,
                Q66 = null,
                Q67 = p.Q67,
                Q68 = p.Q68,
                Q69 = null,
                Q70 = p.Q70,
                Q71 = null,
                Q72 = p.Q72,
                Q73 = p.Q73,
                Q74 = null,
                Q75 = p.Q75,
                Q76 = null,
                Q77 = p.Q77,
                Q78 = p.Q78,
                Q79 = p.Q79,
                Q80 = p.Q80,
                Q81 = p.Q81,
                Q82 = null,
                Q83 = null,
                Q84 = p.Q84,
                Q85 = null,
                Q86 = p.Q86,
                Q87 = null,
                Q88 = p.Q88,
                Q89 = null,
                Q90 = p.Q90,
                Q91 = p.Q91,
                Q92 = p.Q92,
                Q93 = p.Q93,
                Q94 = null,
                Q95 = p.Q95,
                Q96 = null,
                Q97 = null,
                Q98 = null,
                Q99 = p.Q99,
                Q100 = null,
                Q101 = p.Q101,
                Q102 = null,
                Q103 = null,
                Q104 = p.Q104,
                Q105 = null,
                Q106 = null,
                Q107 = null,
                Q108 = p.Q108,
                Q109 = p.Q109,
                Q110 = null,
                Q111 = p.Q111,
                Q112 = p.Q112,
                Q113 = null,
                Q114 = null,
                Q115 = null,
                Q116 = p.Q116,
                Q117 = p.Q117,
                Q118 = p.Q118,
                Q119 = p.Q119,
                Q120 = null,
                Q121 = p.Q121,
                Q122 = p.Q122,
                Q123 = null,
                Q124 = p.Q124,
                Q125 = p.Q125,
                Q126 = p.Q126,
                Q127 = null,
                Q128 = p.Q128,
                Q129 = p.Q129,
                Q130 = null,
                Q131 = p.Q131,
                Q132 = p.Q132,
                Q133 = p.Q133,
                Q134 = null,
                Q135 = p.Q135,
                Q136 = p.Q136,
                Q137 = null,
                Q138 = p.Q138,
                Q139 = null,
                Q140 = p.Q140,
                Q141 = p.Q141,
                Q142 = null,
                Q143 = p.Q143,
                Q144 = null,
                Q145 = p.Q145,
                Q146 = null,
                Q147 = p.Q147,
                Q148 = p.Q148,
                Q149 = p.Q149,
                Q150 = p.Q150,
                Q151 = p.Q151,
                Q152 = p.Q152,
                Q153 = null,
                Q154 = null,
                Q155 = p.Q155,
                Q156 = null,
                Q157 = null,
                Q158 = p.Q158,
                Q159 = p.Q159,
                Q160 = null,
                Q161 = p.Q161,
                Q162 = p.Q162,
                Q163 = null,
                Q164 = p.Q164,
                Q165 = null,
                Q166 = null,
                Q167 = null,
                Q168 = null,
                Q169 = null,
                Q170 = null,
                Q171 = p.Q171,
                Q172 = p.Q172,
                Q173 = null,
                Q174 = null,
                Q175 = null,
                Q176 = p.Q176,
                Q218 = null,
                Q219 = null,
                Q220 = null,
                Q221 = null,
                Q222 = null,
                Q223 = null,
                Q224 = null,
                Q225 = p.Q225,
                Q226 = p.Q226,
                Q227 = p.Q227,
                QNPS1 = p.QNPS1,
                QNPS2 = p.QNPS2,
                IsSample = p.IsSample,
                QNPS1Comment = p.QNPS1Comments,
                QNPS2Comment = p.QNPS2Comments
            }).Distinct().ToList();
        }

        public List<Year> YearDataSet()
        {
            return Get2015();
        }

        public List<Year> YearDataSet(string year)
        {
            if (year == "2015") return Get2015();
            else if (year == "2014") return Get2014();
            else if (year == "2013") return Get2013();
            else if (year == "2012") return Get2012();
            else if (year == "2011") return Get2011();
            else if (year == "2010") return Get2010();
            else return new List<Year>();
        }

        public ComprehensiveDimension GetCompDimensions(List<Year> DataSet, List<Dimension> dimensions, List<Question> questions, string year)
        {
            return GetCompDimensions(DataSet, dimensions, questions, year, false, false);
        }

        public ComprehensiveDimension GetCompDimensions(List<Year> DataSet, List<Dimension> dimensions, List<Question> questions, bool africa)
        {
            return GetCompDimensions(DataSet, dimensions, questions, "2015", africa, false);
        }

        public ComprehensiveDimension GetCompDimensions(List<Year> DataSet, List<Dimension> dimensions, List<Question> questions, string year, bool africa)
        {
            return GetCompDimensions(DataSet, dimensions, questions, year, africa, false);
        }

        public ComprehensiveDimension GetCompDimensions(List<Year> DataSet, List<Dimension> dimensions, List<Question> questions, bool africa, bool excludenewdims)
        {
            return GetCompDimensions(DataSet, dimensions, questions, "2015", africa, false);
        }

        public ComprehensiveDimension GetCompDimensions(List<Year> DataSet, List<Dimension> dimensions, List<Question> questions, string year, bool africa, bool excludenewdims)
        {
            Dimension overall = dimensions.Where(p => p.OverallDimension && p.Comprehensive).Distinct().SingleOrDefault();
            List<Dimension> Yeardimensions = dimensions.Where(p => !p.OverallDimension && p.Comprehensive).Distinct().ToList();

            ComprehensiveDimension CompDim = new ComprehensiveDimension();
            CompDim.CompDimensions = new List<ReportDimension>();
            CompDim.NotIncDimensions = new List<ReportDimension>();
            CompDim.CompStatements = new List<Statement>();
            CompDim.StatementsNotInc = new List<Statement>();

            double negativeoverallcount = 0;
            double neutraloverallcount = 0;
            double totaloverallcount = 0;

            foreach (Dimension dimension in Yeardimensions.OrderBy(p => p.DimensionOrder))
            {
                double negativecount = 0;
                double neutralcount = 0;
                double totalcount = 0;

                if (dimension.GetType().GetProperty("DimensionQuestions" + year).GetValue(dimension) == null) CompDim.CompDimensions.Add(new ReportDimension(dimension.DimensionText, 0, 0, 0));
                else
                {
                    foreach (string questionindex in dimension.GetType().GetProperty("DimensionQuestions" + year).GetValue(dimension).ToString().Split(','))
                    {
                        if ((africa && (bool)questions.Where(p => p.QuestionIndex == "Q" + questionindex).SingleOrDefault().GetType().GetProperty("Survey" + year + "Africa").GetValue(questions.Where(p => p.QuestionIndex == "Q" + questionindex).SingleOrDefault())) || !africa)
                        {
                            double negativestatementcount = 0;
                            double neutralstatementcount = 0;
                            double statementcount = 0;

                            foreach (Year staff in DataSet)
                            {
                                if (staff.GetType().GetProperty("Q" + questionindex).GetValue(staff) != null)
                                {
                                    statementcount++;
                                    double value = Convert.ToDouble(staff.GetType().GetProperty("Q" + questionindex).GetValue(staff).ToString());

                                    if (value == 0 || value == 25) negativestatementcount++;
                                    else if (value == 50) neutralstatementcount++;
                                }
                            }

                            if (Convert.ToDouble(year) <= 2011 || excludenewdims) { if (!Generic.newDimensions().Contains(dimension.DimensionText.ToLower())) { negativeoverallcount += negativestatementcount; neutraloverallcount += neutralstatementcount; totaloverallcount += statementcount; } } else { negativeoverallcount += negativestatementcount; neutraloverallcount += neutralstatementcount; totaloverallcount += statementcount; }
                            negativecount += negativestatementcount;
                            neutralcount += neutralstatementcount;
                            totalcount += statementcount;

                            if (statementcount > 0) if (Convert.ToDouble(year) <= 2011 || excludenewdims) { CompDim.CompStatements.Add(new Statement(dimension.DimensionText, questions.Where(p => p.QuestionIndex == "Q" + questionindex).Distinct().SingleOrDefault().QuestionText, 100 - (Math.Round(negativestatementcount / statementcount * 100, 1, MidpointRounding.AwayFromZero) + Math.Round(neutralstatementcount / statementcount * 100, 1, MidpointRounding.AwayFromZero)), Math.Round(neutralstatementcount / statementcount * 100, 1, MidpointRounding.AwayFromZero), Math.Round(negativestatementcount / statementcount * 100, 1, MidpointRounding.AwayFromZero), "Q" + questionindex, statementcount)); } else CompDim.CompStatements.Add(new Statement(dimension.DimensionText, questions.Where(p => p.QuestionIndex == "Q" + questionindex).Distinct().SingleOrDefault().QuestionText, 100 - (Math.Round(negativestatementcount / statementcount * 100, 1, MidpointRounding.AwayFromZero) + Math.Round(neutralstatementcount / statementcount * 100, 1, MidpointRounding.AwayFromZero)), Math.Round(neutralstatementcount / statementcount * 100, 1, MidpointRounding.AwayFromZero), Math.Round(negativestatementcount / statementcount * 100, 1, MidpointRounding.AwayFromZero), "Q" + questionindex, statementcount));
                        }
                    }

                    if (Convert.ToDouble(year) <= 2011 || excludenewdims) { CompDim.CompDimensions.Add(new ReportDimension(dimension.DimensionText, 100 - (Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero) + Math.Round(neutralcount / totalcount * 100, 1, MidpointRounding.AwayFromZero)), Math.Round(neutralcount / totalcount * 100, 1, MidpointRounding.AwayFromZero), Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero))); } else CompDim.CompDimensions.Add(new ReportDimension(dimension.DimensionText, 100 - (Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero) + Math.Round(neutralcount / totalcount * 100, 1, MidpointRounding.AwayFromZero)), Math.Round(neutralcount / totalcount * 100, 1, MidpointRounding.AwayFromZero), Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero)));
                }

                if (dimension.GetType().GetProperty("DimensionQuestionsNotInc" + year).GetValue(dimension) != null)
                {
                    negativecount = 0;
                    neutralcount = 0;
                    totalcount = 0;

                    foreach (string questionindex in dimension.GetType().GetProperty("DimensionQuestionsNotInc" + year).GetValue(dimension).ToString().Split(','))
                    {
                        double negativestatementcount = 0;
                        double neutralstatementcount = 0;
                        double statementcount = 0;

                        foreach (Year staff in DataSet)
                        {
                            if (staff.GetType().GetProperty("Q" + questionindex).GetValue(staff, null) != null)
                            {
                                statementcount++;
                                double value = Convert.ToDouble(staff.GetType().GetProperty("Q" + questionindex).GetValue(staff).ToString());

                                if (value == 0 || value == 25) negativestatementcount++;
                                else if (value == 50) neutralstatementcount++;
                            }
                        }

                        negativecount += negativestatementcount;
                        neutralcount += neutralstatementcount;
                        totalcount += statementcount;

                        if (statementcount > 0) CompDim.StatementsNotInc.Add(new Statement(dimension.DimensionText, questions.Where(p => p.QuestionIndex == "Q" + questionindex).Distinct().SingleOrDefault().QuestionText, 100 - (Math.Round(negativestatementcount / statementcount * 100, 1, MidpointRounding.AwayFromZero) + Math.Round(neutralstatementcount / statementcount * 100, 1, MidpointRounding.AwayFromZero)), Math.Round(neutralstatementcount / statementcount * 100, 1, MidpointRounding.AwayFromZero), Math.Round(negativestatementcount / statementcount * 100, 1, MidpointRounding.AwayFromZero), "Q" + questionindex, statementcount));
                    }

                    if (totalcount > 0) CompDim.NotIncDimensions.Add(new ReportDimension(dimension.DimensionText, 100 - (Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero) + Math.Round(neutralcount / totalcount * 100, 1, MidpointRounding.AwayFromZero)), Math.Round(neutralcount / totalcount * 100, 1, MidpointRounding.AwayFromZero), Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero))); else CompDim.NotIncDimensions.Add(new ReportDimension(dimension.DimensionText, 0, 0, 0));
                }
            }

            double negativestatcount = 0;
            double neutralstatcount = 0;
            double statcount = 0;

            foreach (Year staff in DataSet)
            {
                if (staff.Q176 != null)
                {
                    statcount++;
                    if (staff.Q176 == 0 || staff.Q176 == 25) negativestatcount++;
                    else if (staff.Q176 == 50) neutralstatcount++;
                }
            }

            if (statcount > 0) CompDim.CompStatements.Add(new Statement("", questions.Where(p => p.QuestionIndex == "Q176").Distinct().SingleOrDefault().QuestionText, 100 - (Math.Round(negativestatcount / statcount * 100, 1, MidpointRounding.AwayFromZero) + Math.Round(neutralstatcount / statcount * 100, 1, MidpointRounding.AwayFromZero)), Math.Round(neutralstatcount / statcount * 100, 1, MidpointRounding.AwayFromZero), Math.Round(negativestatcount / statcount * 100, 1, MidpointRounding.AwayFromZero), "Q176", statcount));

            CompDim.NedbankOverall = new ReportDimension(overall.DimensionText, 100 - (Math.Round(negativeoverallcount / totaloverallcount * 100, 1, MidpointRounding.AwayFromZero) + Math.Round(neutraloverallcount / totaloverallcount * 100, 1, MidpointRounding.AwayFromZero)), Math.Round(neutraloverallcount / totaloverallcount * 100, 1, MidpointRounding.AwayFromZero), Math.Round(negativeoverallcount / totaloverallcount * 100, 1, MidpointRounding.AwayFromZero));

            return CompDim;
        }

        public EEBarrierDimension GetEEBarrierDimensions(List<Year> Dataset, List<Dimension> dimensions, List<Question> questions)
        {
            return GetEEBarrierDimensions(Dataset, dimensions, questions, "2015");
        }

        public EEBarrierDimension GetEEBarrierDimensions(List<Year> Dataset, List<Dimension> dimensions, List<Question> questions, string year)
        {
            EEBarrierDimension eedim = new EEBarrierDimension();
            eedim.EEDimensions = new List<ReportDimension>();

            List<Dimension> Yeardimensions = dimensions.Where(p => p.EEBarrier).Distinct().ToList();

            foreach (Dimension dimension in Yeardimensions)
            {
                double negativecount = 0;
                double neutralcount = 0;
                double totalcount = 0;

                if (dimension.GetType().GetProperty("DimensionQuestions" + year).GetValue(dimension) == null) eedim.EEDimensions.Add(new ReportDimension(dimension.DimensionText, 0, 0, 0));
                else
                {
                    foreach (string questionindex in dimension.GetType().GetProperty("DimensionQuestions" + year).GetValue(dimension).ToString().Split(','))
                    {
                        foreach (Year staff in Dataset)
                        {
                            if (staff.GetType().GetProperty("Q" + questionindex).GetValue(staff) != null)
                            {
                                totalcount++;
                                double value = Convert.ToDouble(staff.GetType().GetProperty("Q" + questionindex).GetValue(staff).ToString());

                                if (value == 0 || value == 25) negativecount++;
                                else if (value == 50) neutralcount++;
                            }
                        }
                    }

                    if ((bool)dimension.OverallDimension)
                    {
                        if (dimension.DimensionText.Contains("Score")) eedim.OverallEEScore = new ReportDimension(dimension.DimensionText, 100 - (Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero) + Math.Round(neutralcount / totalcount * 100, 1, MidpointRounding.AwayFromZero)), Math.Round(neutralcount / totalcount * 100, 1, MidpointRounding.AwayFromZero), Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero));
                        else if (dimension.DimensionText.Contains("Process")) eedim.OverallEEProcess = new ReportDimension(dimension.DimensionText, 100 - (Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero) + Math.Round(neutralcount / totalcount * 100, 1, MidpointRounding.AwayFromZero)), Math.Round(neutralcount / totalcount * 100, 1, MidpointRounding.AwayFromZero), Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero));
                    }
                    else
                        eedim.EEDimensions.Add(new ReportDimension(dimension.DimensionText, 100 - (Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero) + Math.Round(neutralcount / totalcount * 100, 1, MidpointRounding.AwayFromZero)), Math.Round(neutralcount / totalcount * 100, 1, MidpointRounding.AwayFromZero), Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero)));
                }
            }

            return eedim;
        }

        public double getEEBarrierScore(List<Year> Dataset, string dimension, List<Dimension> dimensions)
        {
            return getEEBarrierScore(Dataset, dimension, dimensions, "2015");
        }

        public double getEEBarrierScore(List<Year> Dataset, string dimension, List<Dimension> dimensions, string year)
        {
            Dimension Yeardimension = dimensions.Where(p => p.EEBarrier && p.DimensionText == dimension).Distinct().SingleOrDefault();

            double negativecount = 0;
            double totalcount = 0;

            if (Yeardimension.GetType().GetProperty("DimensionQuestions" + year).GetValue(Yeardimension) != null)
            {
                foreach (string questionindex in Yeardimension.GetType().GetProperty("DimensionQuestions" + year).GetValue(Yeardimension).ToString().Split(','))
                {
                    foreach (Year staff in Dataset)
                    {
                        if (staff.GetType().GetProperty("Q" + questionindex).GetValue(staff) != null)
                        {
                            totalcount++;
                            if (Convert.ToDouble(staff.GetType().GetProperty("Q" + questionindex).GetValue(staff).ToString()) == 0 || Convert.ToDouble(staff.GetType().GetProperty("Q" + questionindex).GetValue(staff).ToString()) == 25) negativecount++;
                        }
                    }
                }
            }

            return Math.Round(negativecount / totalcount * 100, 1, MidpointRounding.AwayFromZero);
        }
    }
}