﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace NSS_2015_Report
{
    public static class Block
    {
        public static string BlockComprehensiveHeaderRow(bool incEngEE)
        {
            return "<tr><td style='text-align: center'>&nbsp;</td><td style='text-align: center' valign='bottom'><b>n</b></td>" +
                    "<td style='text-align: center'><img src='" + GetSiteRoot() + "/Images/01_NSSO.jpg'/></td><td style='text-align: center'></td>" +
                    "<td style='text-align: center'><a href=\"#Ethics\"><img src='" + GetSiteRoot() + "/Images/02_E.jpg'/></a></td><td style='text-align: center'></td>" +
                    "<td style='text-align: center'><a href=\"#StrategicDirection\"><img src='" + GetSiteRoot() + "/Images/03_SD.jpg'/></a></td><td style='text-align: center'></td>" +
                    "<td style='text-align: center'><a href=\"#Leadership\"><img src='" + GetSiteRoot() + "/Images/04_L.jpg'/></a></td><td style='text-align: center'></td>" +
                    "<td style='text-align: center'><a href=\"#Communication\"><img src='" + GetSiteRoot() + "/Images/05_C.jpg'/></a></td><td style='text-align: center'></td>" +
                    "<td style='text-align: center'><a href=\"#ChangeTransformation\"><img src='" + GetSiteRoot() + "/Images/06_CT.jpg'/></a></td><td style='text-align: center'></td>" +
                    "<td style='text-align: center'><a href=\"#TrainingDevelopment\"><img src='" + GetSiteRoot() + "/Images/07_TD.jpg'/></a></td><td style='text-align: center'></td>" +
                    "<td style='text-align: center'><a href=\"#ManagementStyle\"><img src='" + GetSiteRoot() + "/Images/08_MS.jpg'/></a></td><td style='text-align: center'></td>" +
                    "<td style='text-align: center'><a href=\"#OrganisationalCultureValues\"><img src='" + GetSiteRoot() + "/Images/09_OCV.jpg'/></a></td><td style='text-align: center'></td>" +
                    "<td style='text-align: center'><a href=\"#PoliciesProcedures\"><img src='" + GetSiteRoot() + "/Images/10_PP.jpg'/></a></td><td style='text-align: center'></td>" +
                    "<td style='text-align: center'><a href=\"#Diversity\"><img src='" + GetSiteRoot() + "/Images/11_D.jpg'/></a></td><td style='text-align: center'></td>" +
                    "<td style='text-align: center'><a href=\"#RelationshipsTrust\"><img src='" + GetSiteRoot() + "/Images/12_RT.jpg'/></a></td><td style='text-align: center'></td>" +
                    "<td style='text-align: center'><a href=\"#RewardsRecognitionPerformanceManagement\"><img src='" + GetSiteRoot() + "/Images/13_RRPM.jpg'/></a></td><td style='text-align: center'></td>" +
                    (incEngEE == true ? "<td style='text-align: center'><a href=\"#EmploymentEquity\"><img src='" + GetSiteRoot() + "/Images/14_EE.jpg'/></a></td><td style='text-align: center'></td>" + "<td style='text-align: center'><img src='" + GetSiteRoot() + "/Images/15_Engage.jpg'/></td><td style='text-align: center'></td></tr>" : "</tr>");
        }

        public static string getBlockHeaderRows(string label, double rowcount1, double rowcount2, ComprehensiveDimension Comp1Dimensions, ComprehensiveDimension Comp2Dimensions, string year2)
        {
            StringBuilder strRow = new StringBuilder();

            strRow.Append("<tr> ");
            strRow.Append("<td nowrap style='width: 200px; font-family: Verdana; font-size: 10px' align='left'><b>" + label + "</b></td> ");
            strRow.Append("<td nowrap style='font-family: Verdana; font-size: 10px' align='center'><b>" + rowcount1 + "</b></td> ");

            strRow.Append(getCell(rowcount1, rowcount2, Comp1Dimensions.NedbankOverall.DimensionPositiveValue, Comp2Dimensions.NedbankOverall.DimensionPositiveValue));

            foreach (ReportDimension dimension in Comp1Dimensions.CompDimensions.Distinct().ToList())
                if (dimension.DimensionPositiveValue > 0)
                    strRow.Append(getCell(rowcount1, (Convert.ToInt32(year2) >= 2012 || !Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) ? rowcount2 : 0), dimension.DimensionPositiveValue, (Convert.ToInt32(year2) >= 2012 || !Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) ? Comp2Dimensions.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).Select(p => p.DimensionPositiveValue).FirstOrDefault() : 0)));

            strRow.Append("</tr> ");

            return strRow.ToString();
        }

        public static string getCell(double CurrentSample, double PreviousSample, double CurrentValue, double PreviousValue)
        {
            string colour = "";
            string image = "";
            string fontcolor = "";
            double result = 0;
            bool isSignificant;

            Significance.getSignificance(CurrentSample, PreviousSample, CurrentValue, PreviousValue, out result, out isSignificant);
            Significance.getSignificantSettings(result, isSignificant, out colour, out image, out fontcolor);

            return "<td style='border:solid; height:20px;text-align: center;font-family: Verdana; font-size: 10px;background-color:" + colour + ";color:" + (colour == "#FF0000" ? "#fff" : "#000") + ";border-color:black;border-width:1px;border-style:solid;'><b>" + (CurrentValue != 0 ? Math.Round(CurrentValue, 1, MidpointRounding.AwayFromZero).ToString("0.0") : "&nbsp;") + "</b></td><td>" + image + "</td>";
        }

        public static string GenerateBlockChart(string caption, string reportname, List<Year> year1Dataset, List<Year> year2Dataset, Year year, string property, string propertyname, string year2, string headerrows, int headercount, ref int pageno, bool africa, List<Dimension> DimensionList, List<Question> questions, bool pdf)
        {
            string pages = "";

            List<string> items = year1Dataset.Select(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck()).Distinct().ToList().OrderBy(p => p).ToList();

            for (int i = 0; i < items.Count / (11 - headercount) + (items.Count % (11 - headercount) > 0 ? 1 : 0); i++)
            {
                string block = headerrows;

                foreach (string item in items.Skip((i) * (11 - headercount)).Take((11 - headercount)))
                {
                    StringBuilder strRow = new StringBuilder();

                    ComprehensiveDimension compdimyear1 = year.GetCompDimensions(year1Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item).Distinct().ToList(), DimensionList, questions, africa);
                    ComprehensiveDimension compdimyear2 = year.GetCompDimensions(year2Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item).Distinct().ToList(), DimensionList, questions, year2, africa);

                    strRow.Append("<tr> ");
                    strRow.Append("<td nowrap style='width: 200px; font-family: Verdana; font-size: 10px' align='left'>" + item + "</td> ");
                    strRow.Append("<td nowrap style='font-family: Verdana; font-size: 10px' align='center'>" + year1Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item).Count().ToString() + "</td> ");

                    if (year1Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item).Count() >= 10)
                        strRow.Append(getCell(year1Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item).Count(), year2Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item).Count(), compdimyear1.NedbankOverall.DimensionPositiveValue, compdimyear2.NedbankOverall.DimensionPositiveValue));
                    else
                        strRow.Append(getCell(0, 0, 0, 0));

                    foreach (ReportDimension dimension in compdimyear1.CompDimensions.Distinct().ToList())
                        if (dimension.DimensionPositiveValue > 0 || dimension.DimensionNegativeValue > 0 || dimension.DimensionNeutralValue > 0)
                        {
                            if (year1Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item).Count() >= 10)
                                strRow.Append(getCell(year1Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item).Count(), (Convert.ToInt32(year2) >= 2012 || !Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) ? year2Dataset.Where(p => p.GetType().GetProperty(property).GetValue(p, null).NullCheck() == item).Count() : 0), dimension.DimensionPositiveValue, (Convert.ToInt32(year2) >= 2012 || !Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) ? compdimyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue : 0)));
                            else
                                strRow.Append(getCell(0, 0, 0, 0));
                        }

                    strRow.Append("</tr> ");

                    block += strRow.ToString();
                }

                pages += PageTemplate.Template("Nedbank Staff Survey Dimensions - " + propertyname.Replace("Age", "Age Group").Replace("LOS", "Years at Nedbank").Replace("Disabilities", "Disability").Replace("OccupationalLevel", "Occupational Level").Replace("Classification", "Generational Classification").Replace("JobFamily", "Job Family").Replace("StaffCategory", "Staff Category").Replace("ManagementPassage", "Management Passage") + " - " + reportname + (items.Count + headercount > 9 ? i > 0 ? " (Continued) " : "" : ""), reportname, "<tr><td align='center' colspan='2'><h3>" + caption + "</h3></td></tr><tr><td align='center'><table style=\"width:100%;height:425px;vertical-align:top;\"><tr><td style=\"vertical-align:top;\"><table style=\"width:100%;\">" + block + "</table></td></tr></table></td></tr>" + StatementLegend(), pdf, pageno++, false);                            
            }

            return pages;
        }

        public static string GetSiteRoot()
        {
            string port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == null || port == "80" || port == "443")
                port = "";
            else
                port = ":" + port;

            string protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
                protocol = "http://";
            else
                protocol = "https://";

            string sOut = protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + System.Web.HttpContext.Current.Request.ApplicationPath;

            if (sOut.EndsWith("/"))
            {
                sOut = sOut.Substring(0, sOut.Length - 1);
            }

            return sOut;
        }

        public static string StatementLegend()
        {
            return "<tr><td align='left' style='font-style: italic;vertical-align:bottom;' colspan='6'><b>Definitions</b><table style='font-size: 10px;padding-left:10px;font-style: italic;' cellspacing='0' cellpadding='0' width='100%'>" +
                    "<tr><td width='50%'><table width='100%' cellpadding='0' cellspacing='0'><tr><td><img src='" + GetSiteRoot() + "/images/red.jpg' /><img src='" + GetSiteRoot() + "/images/green.jpg' /> : Statistically Significant Improvement / Decline since Previous survey</td></tr>" +
                    "<tr><td>No Value : scores not shown due to insufficient sample size</td></tr></table></td>" +
                    "<td width='50%'><table width='100%' cellpadding='0' cellspacing='0'><tr><td colspan='3'>Classification of Average Statement/Dimension Score:</td></tr>" +
                    "<tr><td style='padding-left:10px;border-color:black;border-width:1px;border-style:solid;background-color: #FF0000; width: 20px; height: 20px;'>&nbsp;</td><td>&nbsp;Decline</td></tr><tr><td align='left' style='border-color:black;border-width:1px;border-style:solid;font-family: Verdana; font-size: 11px;width: 20px; height: 20px;'>&nbsp;</td><td>&nbsp;No movement / No comparative data</td></tr><tr><td style='border-color:black;border-width:1px;border-style:solid;background-color: #33CC33; width: 20px; height: 20px;'>&nbsp;</td><td>&nbsp;Improvement</td></tr>" +
                    "</table></td></tr></table></td></tr>";
        }
    }
}