﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NSS_2015_Report
{
    public static class PageTemplate
    {
        public static string Template(string Heading, string ReportName, string body, bool pdf, int pageno, bool frontpage)
        {
            return "<div id=\"" + Heading.Replace(" ", "") + "\" style=\"width:1123px;height:792px;text-align:center;padding:0px;margin:0px;" + (pdf ? "" : "border:1px solid grey;-webkit-border-radius:0px 0px 5px 5px;border-radius:0px 0px 5px 5px;-webkit-box-shadow:0px 1px 3px 1px #888;-moz-box-shadow:0px 1px 3px 1px #888;box-shadow:0px 1px 3px 1px #888;") + "\">" +
                    "<table style='width:100%;' cellspacing='0' cellpadding='0' align=\"center\">" +
                    (!frontpage ?
                    "<tr><td style=\"width:100%;text-align:left;vertical-align:bottom;height:70px;padding-top:5px;font-family:Arial;font-weight:bold;font-size:20px;color:#275240;padding:0 30px;\"><div style=\"width:auto;border-bottom:3px solid #275240;padding:5px;\"><span style=\"font-weight:Bold;Font-Size:38px;\">Nedbank Staff Survey 2015</span><span style=\"float:right;\"><img src=\"" + GetSiteRoot() + "/Images/Pure Survey logo.jpg\" height=\"40px\" /></span></div></td></tr><tr><td>&nbsp;</td></tr>" +
                    "<tr><td style=\"text-align:center;\"><table style=\"width:95%;text-align:center;border-collapse:collapse;font-family:Arial;font-size:12px;\" align=\"center\" cellspacing='0' cellpadding='0'><tr><td><table width=\"100%\" cellspacing='0' cellpadding='0'><tr><td><h2>" + Heading + "</h2></td></tr>" +
                    "<tr><td style=\"text-align:left;padding:20px;vertical-align:text-top;height:580px;\"><table style=\"width:100%;text-align:left;vertical-align:text-top;\" cellspacing='0' cellpadding='0'>" +
                    body +
                    "</table></td></tr></table></td></tr></table></td></tr>" +
                    "<tr><td style=\"width:100%;vertical-align:bottom;\"><table style=\"width:100%;vertical-align:bottom;\"><tr><td style=\"width:100%;text-align:left;height:50px;font-family:Arial;font-size:20px;background:#275240;color:white;vertical-align:middle;padding-left:20px;margin:0;\">" + pageno + "</td><td style=\"float:right;border-left:2px solid white;background:#275240;\"><img src=\"" + GetSiteRoot() + "/Images/NedbankN.png\" width=\"50px\" /></td></tr>" : body) +
                    "</table></td></tr></table>" +
                    "</div>" +
                    "<div style=\"page-break-after:always;\"></div>" +
                    (pdf ? "" : "&nbsp;");
        }

        public static string GetSiteRoot()
        {
            string port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == null || port == "80" || port == "443")
                port = "";
            else
                port = ":" + port;

            string protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
                protocol = "http://";
            else
                protocol = "https://";

            string sOut = protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + System.Web.HttpContext.Current.Request.ApplicationPath;

            if (sOut.EndsWith("/"))
            {
                sOut = sOut.Substring(0, sOut.Length - 1);
            }

            return sOut;
        }
    }
}