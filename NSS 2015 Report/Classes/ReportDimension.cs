﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NSS_2015_Report
{
    public class ReportDimension
    {
        public string DimensionName { get; set; }
        public double DimensionPositiveValue { get; set; }
        public double DimensionNeutralValue { get; set; }
        public double DimensionNegativeValue { get; set; }
        public List<string> Statements { get; set; }

        public ReportDimension() { }

        public ReportDimension(string _DimensionName, double _DimensionPositiveValue)
        {
            DimensionName = _DimensionName;
            DimensionPositiveValue = _DimensionPositiveValue;
            DimensionNeutralValue = 0;
            DimensionNegativeValue = 0;
            Statements = null;
        }

        public ReportDimension(string _DimensionName, double _DimensionPositiveValue, double _DimensionNeutralValue, double _DimensionNegativeValue)
        {
            DimensionName = _DimensionName;
            DimensionPositiveValue = _DimensionPositiveValue;
            DimensionNeutralValue = _DimensionNeutralValue;
            DimensionNegativeValue = _DimensionNegativeValue;
            Statements = null;
        }

        public ReportDimension(string _DimensionName, double _DimensionPositiveValue, double _DimensionNeutralValue, double _DimensionNegativeValue, List<string> _Statements)
        {
            DimensionName = _DimensionName;
            DimensionPositiveValue = _DimensionPositiveValue;
            DimensionNeutralValue = _DimensionNeutralValue;
            DimensionNegativeValue = _DimensionNegativeValue;
            Statements = _Statements;
        }
    }

    public class Statement
    {
        public string DimensionName { get; set; }
        public string StatementName { get; set; }
        public double StatementPositiveValue { get; set; }
        public double StatementNeutralValue { get; set; }
        public double StatementNegativeValue { get; set; }
        public string QuestionIndex { get; set; }
        public double StatementCount { get; set; }

        public Statement() { }

        public Statement(string _DimensionName, string _StatementName, double _StatementPositiveValue, double _StatementNeutralValue, double _StatementNegativeValue, string _QuestionIndex, double _StatementCount)
        {
            DimensionName = _DimensionName;
            StatementName = _StatementName;
            StatementPositiveValue = _StatementPositiveValue;
            StatementNeutralValue = _StatementNeutralValue;
            StatementNegativeValue = _StatementNegativeValue;
            QuestionIndex = _QuestionIndex;
            StatementCount = _StatementCount;
        }
    }

    public class ComprehensiveDimension
    {
        public ReportDimension NedbankOverall { get; set; }
        public List<ReportDimension> CompDimensions { get; set; }
        public List<Statement> CompStatements { get; set; }
        public List<Statement> StatementsNotInc { get; set; }
        public List<ReportDimension> NotIncDimensions { get; set; }

        public ComprehensiveDimension() { }
    }

    public class EEBarrierDimension
    {
        public ReportDimension OverallEEScore { get; set; }
        public ReportDimension OverallEEProcess { get; set; }
        public List<ReportDimension> EEDimensions { get; set; }

        public EEBarrierDimension() { }
    }

    public class StatementShift
    {
        public string QuestionIndex { get; set; }
        public double Result { get; set; }
        public bool isSignificant { get; set; }

        public StatementShift() { }

        public StatementShift(string _QuestionIndex, double _Result, bool _isSignificant)
        {
            QuestionIndex = _QuestionIndex;
            Result = _Result;
            isSignificant = _isSignificant;
        }
    }

    public class DemographicProfile
    {
        public string Demographic1 { get; set; }
        public List<ReportDimension> Demographic1Values { get; set; }
        public string Demographic2 { get; set; }
        public List<ReportDimension> Demographic2Values { get; set; }

        public DemographicProfile() { }
    }
}