﻿using InfoSoftGlobal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace NSS_2015_Report
{
    public static class Charts
    {
        public static string RadarChart(string categories, string datasets, string chartname, double width, double height)
        {
            return RadarChart(categories, datasets, chartname, width, height, 135);
        }

        public static string RadarChart(string categories, string datasets, string chartname, double width, double height, int radius)
        {
            StringBuilder ChartXML = new StringBuilder();

            ChartXML.Append("<chart animation='0' bgColor='FFFFFF' radarFillColor='FFFFFF' plotFillAlpha='2' plotBorderThickness='2' anchorAlpha='100' numberSuffix='%' baseFontSize='8.5' showYAxisValues='0' numDivLines='0' decimals='1' forceDecimals='1' showRadarBorder='0' radarRadius='" + radius + "' showBorder='0'>");
            ChartXML.Append("<categories font='Arial' fontSize='9'>");
            ChartXML.Append(categories);
            ChartXML.Append("</categories> ");
            ChartXML.Append(datasets);
            ChartXML.Append("<styles>");
            ChartXML.Append("<definition>");
            ChartXML.Append("<style name='myToolTipFont' type='font' font='Arial' color='000000'/>");
            ChartXML.Append("</definition>");
            ChartXML.Append("<application>");
            ChartXML.Append("<apply toObject='ToolTip' styles='myToolTipFont' />");
            ChartXML.Append("</application>");
            ChartXML.Append("</styles>");
            ChartXML.Append("</chart>");

            FusionCharts.SetRenderer("javascript");
            return FusionCharts.RenderChart(GetSiteRoot() + "/Charts/Radar.swf", "", ChartXML.ToString().Replace("&", "&amp;"), chartname, width.ToString(), height.ToString(), false, true, true, "#fff", "exactfit", "EN");
        }

        public static string StackedBarChart(string datasets, string chartname, double width, double height, string caption)
        {
            StringBuilder ChartXML = new StringBuilder();

            ChartXML.Append("<chart animation='0' forceYAxisValueDecimals='1' yAxisValueDecimals='1' adjustDiv='0' plotGradientColor='' caption='" + caption + "' baseFontColor ='000000' numDivLines='3' numberSuffix='%' showValues='1' yAxisMaxValue='100.000001' xAxisMaxValue='100' decimals='1' forceDecimals='1' maxLabelWidthPercent='25' bgColor='FFFFFF' showBorder='0'>");
            ChartXML.Append("<categories>");
            ChartXML.Append("<category label=''/>");
            ChartXML.Append("</categories>");
            ChartXML.Append(datasets);
            ChartXML.Append("<styles>");
            ChartXML.Append("<definition>");
            ChartXML.Append("<style name='myCaptionFont' type='font' font='Arial' size='14' color='000000' bold='1'/>");
            ChartXML.Append("</definition>");
            ChartXML.Append("<application>");
            ChartXML.Append("<apply toObject='Caption' styles='myCaptionFont' />");
            ChartXML.Append("</application>");
            ChartXML.Append("</styles>");
            ChartXML.Append("</chart>");

            FusionCharts.SetRenderer("javascript");
            return FusionCharts.RenderChart(GetSiteRoot() + "/Charts/StackedBar2D.swf", "", ChartXML.ToString().Replace("&", "&amp;"), chartname, width.ToString(), height.ToString(), false, true, true, "#fff", "exactfit", "EN");
        }

        public static string MSStackedBarChart(string categories, string datasets, string chartname, double width, double height, string caption)
        {
            StringBuilder ChartXML = new StringBuilder();

            ChartXML.Append("<chart animation='0' caption='" + caption + "' maxLabelWidthPercent='30' useEllipsesWhenOverflow='0' useRoundEdges='0' plotGradientColor='' showlegend='0' baseFontSize='9' yAxisMaxValue='100.000001' baseFontColor='ffffff' outCnvBaseFontColor='000000' adjustDiv='0' numDivLines='4' decimals='1' forceDecimals='1' captionPadding='5' chartLeftMargin='50' chartRightMargin='10' chartTopMargin='1' chartBottomMargin='1' bgColor='FFFFFF' showBorder='0'>");
            ChartXML.Append("<categories>");
            ChartXML.Append(categories);
            ChartXML.Append("</categories>");
            ChartXML.Append(datasets);
            ChartXML.Append("<styles>");
            ChartXML.Append("<definition>");
            ChartXML.Append("<style name='myCaptionFont' type='font' font='Arial' size='14' color='000000' bold='1'/>");
            ChartXML.Append("</definition>");
            ChartXML.Append("<application>");
            ChartXML.Append("<apply toObject='Caption' styles='myCaptionFont' />");
            ChartXML.Append("</application>");
            ChartXML.Append("</styles>");
            ChartXML.Append("</chart>");

            FusionCharts.SetRenderer("javascript");
            return FusionCharts.RenderChart(GetSiteRoot() + "/Charts/StackedBar2D.swf", "", ChartXML.ToString().Replace("&", "&amp;"), chartname, width.ToString(), height.ToString(), false, true, true, "#fff", "exactfit", "EN");
        }

        public static string BarChartnoCategories(string datasets, string chartname, double width, double height)
        {
            StringBuilder ChartXML = new StringBuilder();

            ChartXML.Append("<chart animation='0' numDivLines='2' numberSuffix='%' plotGradientColor='' baseFontColor ='000000' baseFontSize='9' placeValuesInside='0' showValues='1' yAxisMinValue='0' yAxisMaxValue='40' showLegend='0' decimals='1' forceDecimals='1' bgColor='FFFFFF' showBorder='0'>");
            ChartXML.Append(datasets);
            ChartXML.Append("<styles>");
            ChartXML.Append("<definition>");
            ChartXML.Append("<style name='myToolTipFont' type='font' color='000000' font='Arial' size='10'/>");
            ChartXML.Append("</definition>");
            ChartXML.Append("<application>");
            ChartXML.Append("<apply toObject='ToolTip' styles='myToolTipFont' />");
            ChartXML.Append("</application>");
            ChartXML.Append("</styles>");
            ChartXML.Append("</chart>");

            FusionCharts.SetRenderer("javascript");
            return FusionCharts.RenderChart(GetSiteRoot() + "/Charts/Bar2D.swf", "", ChartXML.ToString().Replace("&", "&amp;"), chartname, width.ToString(), height.ToString(), false, true, true, "#fff", "exactfit", "EN");
        }

        public static string TrendChart(string points, double width, double height, string chartname)
        {
            return Graphs(chartname, height.ToString(), width.ToString(), "xmldata=<content Width='" + width + "' Height='" + height + "' LineWeight='3' LineColour='#333333' CircleColour='#22513d' TextColour='#000000' Static='true' BgColour='#DDDDDD' >" + points + "</content>", GetSiteRoot() + "/Charts/trend_graph.swf", "#dddddd");
        }

        public static string RaceChart(double value, double width, double height, string chartname)
        {
            return Graphs(chartname, height.ToString(), width.ToString(), "xmldata=<content Static='true' Title='" + chartname + "' Width='" + width + "' Height='" + height + "' bgcolor='#02563c' TitleBgColour='#689882' TitleTextColour='#FFFFFF' ToolTipColour='#FFFFFF' ToolTipTextColour='#FFFFFF' CircleBgColour='#778d1e' CircleBorderColour='#c4df9c'><race Data='" + value + "' Colour='#778d1e' /></content>", GetSiteRoot() + "/Charts/graph_race_3_edit.swf", "#02563c");
        }

        public static string StackedPieChart(string points, double width, double height, string chartname)
        {
            return Graphs(chartname, height.ToString(), width.ToString(), "xmldata=<content Static='true' Width='" + width + "' Height='" + height + "' Spacer='8' XPos ='215'>" + points + "</content>", GetSiteRoot() + "/Charts/stacked_pie_chart.swf", "#02563c");
        }

        public static string Graphs(string name, string height, string width, string flashVariables, string source, string bgcolor)
        {
            StringBuilder xmlData = new StringBuilder();
            xmlData.Clear();
            xmlData.Append("<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0\" width=\"" + width + "px\" height=\"" + height + "px\" id=\"" + name + "\" align=\"middle\">");
            xmlData.Append("<param name=\"allowScriptAccess\" value=\"always\" />");
            xmlData.Append("<param name=\"movie\" value=\"" + source + "\" />");
            xmlData.Append("<param name=\"bgcolor\" value=\"" + bgcolor + "\" />");
            xmlData.Append("<param name=\"FlashVars\" value= \"" + flashVariables + " \">");
            xmlData.Append("<embed src=\"" + source + "\" FlashVars=\"" + flashVariables + "\" quality=\"high\" bgcolor=\"" + bgcolor + "\" width=\"" + width + "px\" height=\"" + height + "px\" name=\"" + name + "\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\"/>");
            xmlData.Append("</object>");

            string FlashXML = "<div id='" + name + "Div' align='center' width='" + width + "px' class='Lits'>"; FlashXML += xmlData.ToString() + "</div>";
            return FlashXML;
        }

        public static string GetSiteRoot()
        {
            string port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == null || port == "80" || port == "443")
                port = "";
            else
                port = ":" + port;

            string protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
                protocol = "http://";
            else
                protocol = "https://";

            string sOut = protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + System.Web.HttpContext.Current.Request.ApplicationPath;

            if (sOut.EndsWith("/"))
            {
                sOut = sOut.Substring(0, sOut.Length - 1);
            }

            return sOut;
        }
    }
}