﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace NSS_2015_Report
{
    public class TagCloudItem
    {
        public string Word { get; set; }
        public int Count { get; set; }

        public TagCloudItem() { }

        public TagCloudItem(string word, int count)
        {
            Word = word;
            Count = count;
        }
    }

    public static class TagCloudGenerator
    {
        private static readonly IList<string> exclustionList = new ReadOnlyCollection<string>(new List<String> { "and", "or", "the", "a", "to", "is", "i", "of", "in", "with", "for", "not", "have", "it", "we", "as", "are", "that", "our", "my", "on", "they", "at", "be", "has", "their", "are", "that", "our", "but", "all", "there", "you", "its", "because", "do", "from", "can", "other", "would", "about", "when", "this", "an", "am", "will", "well", "so", "make", "been", "also", "get", "no", "im", "getting", "any", "what", "by", "only", "i've", "4", "100%", "even", "once", "see", "find", "being", "did", "them", "much", "takes", "lot", "bit", "within", "got", "those", "never", "had", "too", "many", "eg", "who", "me", "than", "was", "your", "if", "go", "up", "does" });

        public static string GenerateTagCloud(List<TagCloudItem> Items, int cloudno)
        {
            string TagHTML = "<tr><td><div id=\"wordcloud" + cloudno + "\" style=\"width:1000px;padding:5px;text-align:center;height:520px;vertical-align:middle;\">";

            foreach (TagCloudItem item in Items.OrderByDescending(p => p.Count).Take(100))
                TagHTML += CloudItem(item, Items.OrderByDescending(p => p.Count).Take(100).Sum(p => p.Count));

            return TagHTML += "</div></td></tr><tr><td style=\"text-align:center;\">*Note: The above Wordcloud represents the most frequently stated words and phrases from the open-ended NPS questions</td></tr>";
        }

        public static List<TagCloudItem> GenerateList(List<string> Comments)
        {
            List<TagCloudItem> Items = new List<TagCloudItem>();

            foreach (string comment in Comments)
            {
                string test = comment == null ? string.Empty : comment.Trim().Replace(".", string.Empty).Replace(",", string.Empty).Replace("&", "").Replace(";", string.Empty).Replace(":", string.Empty).Replace("-", string.Empty).Replace('"', new char()).ToLower();

                foreach (string word in test.Split(' ').AsEnumerable().Where(p => p != string.Empty))
                {
                    if (!exclustionList.Contains(word))
                    {
                        if (Items.Where(p => p.Word == word).SingleOrDefault() == null)
                            Items.Add(new TagCloudItem(word, 1));
                        else
                            Items.Where(p => p.Word == word).SingleOrDefault().Count++;
                    }
                }
            }

            Items = Items.OrderByDescending(p => p.Count).Distinct().ToList();

            return Items;
        }

        private static string CloudItem(TagCloudItem item, double itemscount)
        {
            return "<span data-weight=\"" + Math.Round((double)item.Count / (double)itemscount * 1000, 0, MidpointRounding.AwayFromZero) + "\">" + item.Word + "</span>";
        }
    }
}