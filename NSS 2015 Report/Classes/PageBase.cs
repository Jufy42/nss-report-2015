﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Services;
using System.Web.UI;

namespace NSS_2015_Report
{
    public class PageBase : Page
    {
        public NSS2015Report db = new NSS2015Report();
        public Year year = new Year();

        public Guid UserID { get { return Request.Cookies["NSS2015R"] == null ? Guid.NewGuid() : new Guid(Request.Cookies["NSS2015R"]["UserID"].ToString()); } }

        public User GetCurrentUser()
        {
            return UserID == null ? null : db.Users.Where(p => p.UserID == UserID).SingleOrDefault();
        }

        public void AlertShow(string alert)
        {
            System.Text.StringBuilder sbScript = new System.Text.StringBuilder("");
            sbScript.Append("alert('" + alert + "');");
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "ClientScript", sbScript.ToString(), true);
        }

        [WebMethod]
        public static string ValidateLogin(string username, string password)
        {
            using (NSS2015Report db = new NSS2015Report())
            {
                User user = db.Users.Where(p => p.UserName.Trim().ToLower() == username.Trim().ToLower() && p.Password == password).SingleOrDefault();
                return (user != null).ToString();
            }
        }

        public string GetSiteRoot()
        {
            string port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == null || port == "80" || port == "443")
                port = "";
            else
                port = ":" + port;

            string protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
                protocol = "http://";
            else
                protocol = "https://";

            string sOut = protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + System.Web.HttpContext.Current.Request.ApplicationPath;

            if (sOut.EndsWith("/"))
            {
                sOut = sOut.Substring(0, sOut.Length - 1);
            }

            return sOut;
        }

        [WebMethod]
        public static string GetColumns(string filename, string sheetname)
        {
            string conStr = Path.GetExtension(filename) == ".xlsx" ? "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=Yes'" : "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=Yes;'";
            conStr = String.Format(conStr, HostingEnvironment.MapPath("~/Imports/") + filename);
            OleDbConnection connExcel = new OleDbConnection(conStr);
            OleDbCommand cmdExcel = new OleDbCommand();
            OleDbDataAdapter oda = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            cmdExcel.Connection = connExcel;

            connExcel.Open();
            cmdExcel.CommandText = "SELECT * From [" + sheetname + "]";
            oda.SelectCommand = cmdExcel;
            oda.Fill(dt);
            connExcel.Close();
            GC.Collect();

            List<DataColumn> Columns = dt.Columns.Cast<DataColumn>().ToList();

            string columns = "[";

            foreach (DataColumn column in Columns)
                columns += "{ \"label\" : \"" + column.ColumnName + "\" },";

            if (columns.Length > 1) columns = columns.Substring(0, columns.Length - 1) + "]";

            GC.Collect();

            return columns;
        }

        [WebMethod]
        public static string PreviewData(string filename, string sheetname, string columnname)
        {
            string conStr = Path.GetExtension(filename) == ".xlsx" ? "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=Yes'" : "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=Yes;'";
            conStr = String.Format(conStr, HostingEnvironment.MapPath("~/Imports/") + filename);
            OleDbConnection connExcel = new OleDbConnection(conStr);
            OleDbCommand cmdExcel = new OleDbCommand();
            OleDbDataAdapter oda = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            cmdExcel.Connection = connExcel;

            connExcel.Open();
            cmdExcel.CommandText = "SELECT * From [" + sheetname + "]";
            oda.SelectCommand = cmdExcel;
            oda.Fill(dt);
            connExcel.Close();
            GC.Collect();

            List<DataRow> rows = dt.Rows.Cast<DataRow>().ToList();

            string data = "[";

            foreach (DataRow row in rows.Take(10))
                data += "{ \"label\" : \"" + row[columnname].ToString() + "\" },";

            if (data.Length > 1) data = data.Substring(0, data.Length - 1) + "]";

            GC.Collect();

            return data;
        }

        [WebMethod]
        public static string GetSheets(string f, string fileName)
        {
            if (File.Exists(HostingEnvironment.MapPath("~/Imports/") + fileName)) File.Delete(HostingEnvironment.MapPath("~/Imports/") + fileName);
            MemoryStream ms = new MemoryStream(System.Convert.FromBase64String(f));
            FileStream fs = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath("~/Imports/") + fileName, FileMode.Create);

            ms.WriteTo(fs);
            ms.Close();
            fs.Close();
            fs.Dispose();

            string conStr = Path.GetExtension(fileName) == ".xlsx" ? "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=Yes'" : "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=Yes;'";
            conStr = String.Format(conStr, HostingEnvironment.MapPath("~/Imports/") + fileName);
            OleDbConnection connExcel = new OleDbConnection(conStr);
            OleDbCommand cmdExcel = new OleDbCommand();
            OleDbDataAdapter oda = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            cmdExcel.Connection = connExcel;

            connExcel.Open();
            DataTable dtExcelSchema;
            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            List<DataRow> SheetNames = dtExcelSchema.Rows.Cast<DataRow>().ToList();
            connExcel.Close();

            string sheets = "[";

            foreach (DataRow row in SheetNames) sheets += "{ \"label\" : \"" + row["TABLE_NAME"].ToString() + "\" },";
            if (sheets.Length > 1) sheets = sheets.Substring(0, sheets.Length - 1) + "]";

            GC.Collect();

            return sheets;
        }
    }
}