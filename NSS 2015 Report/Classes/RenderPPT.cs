﻿using HiQPdf;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace NSS_2015_Report
{
    public class RenderPPT
    {
        public string GeneratePPT(string filename, string url)
        {
            HtmlToImage htmlToPPTConverter = new HtmlToImage();
            htmlToPPTConverter.SerialNumber = "3pa3j466-uJK3vKy/-rKfm8O7+-7/7s/ubo-5/7t7/Dv-7PDn5+fn";
            htmlToPPTConverter.BrowserWidth = 1123;
            htmlToPPTConverter.HtmlLoadedTimeout = 1000;
            htmlToPPTConverter.RunJavaScript = true;
            htmlToPPTConverter.RunExtensions = true;
            htmlToPPTConverter.TriggerMode = ConversionTriggerMode.WaitTime;
            htmlToPPTConverter.WaitBeforeConvert = 20;
            htmlToPPTConverter.TrimToBrowserWidth = true;

            Image[] images = htmlToPPTConverter.ConvertUrlToImage(url);

            int count = images.Count();
            for (int i = 0; i < images.Count(); i++)
            {
                Bitmap BigImage = (Bitmap)images[i];
                BigImage.Save(filename.Replace(".ppt", "BigImage" + i + ".bmp"), System.Drawing.Imaging.ImageFormat.Bmp);
                images[i].Dispose();
                images[i] = null;
                BigImage.Dispose();
                BigImage = null;
            }

            htmlToPPTConverter = null;
            images = null;
            GC.Collect();
            System.GC.WaitForPendingFinalizers();

            int pageno = 1;
            Microsoft.Office.Interop.PowerPoint.Application objApp = new Microsoft.Office.Interop.PowerPoint.Application();
            _Presentation objPres;
            Slides objSlides;

            try
            {
                objPres = objApp.Presentations.Open(GetSiteRoot() + "/Template/report.pot", MsoTriState.msoFalse, MsoTriState.msoTrue, MsoTriState.msoFalse);
                objApp.DisplayAlerts = PpAlertLevel.ppAlertsNone;
                objSlides = objPres.Slides;

                int pages = 0;
                int gap = 0;
                int oldgap = 0;

                for (int j = 0; j < count; j++)
                {
                    if (count > 1 && j > 0)
                    {
                        Bitmap BigImage1 = (Bitmap)Bitmap.FromFile(filename.Replace(".ppt", "BigImage" + j + ".bmp"));
                        Bitmap im2 = BigImage1.Clone(new Rectangle(0, 0, BigImage1.Width, gap), System.Drawing.Imaging.PixelFormat.DontCare);

                        Bitmap smallImage = (Bitmap)Bitmap.FromFile(filename.Replace(".ppt", "BigImage" + (j - 1) + ".bmp"));
                        Bitmap im1 = smallImage.Clone(new Rectangle(0, pages * 792 + oldgap, smallImage.Width, 792 - gap), System.Drawing.Imaging.PixelFormat.DontCare);

                        Bitmap m_Bitmap1 = new Bitmap(smallImage.Width, 792);
                        Graphics g = Graphics.FromImage(m_Bitmap1);
                        g.DrawImage(im1, 0, 0, smallImage.Width, 792 - gap);
                        g.DrawImage(im2, 0, 792 - gap, smallImage.Width, gap);

                        _Slide objSlide1 = objSlides.Add(pageno, PpSlideLayout.ppLayoutBlank);
                        m_Bitmap1.Save(filename.Replace(".ppt", pageno + ".bmp"), System.Drawing.Imaging.ImageFormat.Bmp);

                        objSlide1.Shapes.AddPicture(filename.Replace(".ppt", pageno + ".bmp"), MsoTriState.msoFalse, MsoTriState.msoTrue, 0, 0, 780, 539);
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(objSlide1);

                        im1.Dispose();
                        im2.Dispose();
                        im1 = null;
                        im2 = null;

                        g.Dispose();
                        g = null;

                        m_Bitmap1.Dispose();
                        m_Bitmap1 = null;

                        smallImage.Dispose();
                        smallImage = null;

                        BigImage1.Dispose();
                        BigImage1 = null;

                        System.IO.FileInfo file1 = new System.IO.FileInfo(filename.Replace(".ppt", pageno + ".bmp"));
                        file1.Delete();
                        file1 = null;

                        GC.Collect();
                        System.GC.WaitForPendingFinalizers();
                        pageno++;
                    }

                    Bitmap BigImage = (Bitmap)Bitmap.FromFile(filename.Replace(".ppt", "BigImage" + j + ".bmp"));
                    pages = (BigImage.Height - gap) / 792;
                    for (int i = 0; i < pages; i++)
                    {
                        Bitmap m_Bitmap = new Bitmap(BigImage.Width, 792);
                        m_Bitmap = BigImage.Clone(new Rectangle(0, i * 792 + gap, BigImage.Width, 792), System.Drawing.Imaging.PixelFormat.DontCare);

                        _Slide objSlide = objSlides.Add(pageno, PpSlideLayout.ppLayoutBlank);
                        m_Bitmap.Save(filename.Replace(".ppt", pageno + ".bmp"), System.Drawing.Imaging.ImageFormat.Bmp);

                        objSlide.Shapes.AddPicture(filename.Replace(".ppt", pageno + ".bmp"), MsoTriState.msoFalse, MsoTriState.msoTrue, 0, 0, 780, 539);
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(objSlide);

                        m_Bitmap.Dispose();
                        m_Bitmap = null;

                        System.IO.FileInfo file = new System.IO.FileInfo(filename.Replace(".ppt", pageno + ".bmp"));
                        file.Delete();
                        file = null;

                        GC.Collect();
                        System.GC.WaitForPendingFinalizers();
                        pageno++;
                    }

                    oldgap = gap;
                    gap = 792 - (BigImage.Height - oldgap - (pages * 792));

                    BigImage.Dispose();
                    BigImage = null;
                    GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                }

                for (int i = 0; i < count; i++)
                {
                    System.IO.FileInfo file = new System.IO.FileInfo(filename.Replace(".ppt", "BigImage" + i + ".bmp"));
                    file.Delete();
                    file = null;
                }

                if (objPres.Slides.Count > pageno - 1) objPres.Slides[objPres.Slides.Count].Delete();

                objPres.SaveCopyAs(filename, PpSaveAsFileType.ppSaveAsDefault, Microsoft.Office.Core.MsoTriState.msoFalse);
                objPres.Close();

                System.Runtime.InteropServices.Marshal.ReleaseComObject(objSlides);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objPres);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objApp);
            }
            catch (Exception ex)
            {
                objApp = null;
                objPres = null;
                objSlides = null;
                return ex.Message;
            }

            objApp = null;
            objPres = null;
            objSlides = null;

            return "";
        }

        public string GetSiteRoot()
        {
            string port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == null || port == "80" || port == "443")
                port = "";
            else
                port = ":" + port;

            string protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
                protocol = "http://";
            else
                protocol = "https://";

            string sOut = protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + System.Web.HttpContext.Current.Request.ApplicationPath;

            if (sOut.EndsWith("/"))
            {
                sOut = sOut.Substring(0, sOut.Length - 1);
            }

            return sOut;
        }
    }
}