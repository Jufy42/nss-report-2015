﻿using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Drawing;

namespace NSS_2015_Report
{
    public class DimensionExcel
    {
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public int StatementId { get; set; }
        public string Statement { get; set; }
        public double Year1A { get; set; }
        public double Year1N { get; set; }
        public double Year1D { get; set; }
        public double Year2A { get; set; }
        public double Year2N { get; set; }
        public double Year2D { get; set; }
        public double Shift { get; set; }
        public bool Significant { get; set; }
        public string Image { get; set; }
        public string Colour { get; set; }

        public DimensionExcel() { }
    }

    public class FocusAreasExcel
    {
        public string Category { get; set; }
        public string Statement { get; set; }
        public double Year1Count { get; set; }
        public double Year1Percentage { get; set; }
        public double Year2Count { get; set; }
        public double Year2Percentage { get; set; }
        public double Shift { get; set; }
        public bool Significant { get; set; }
        public string Image { get; set; }
        public string Colour { get; set; }

        public FocusAreasExcel() { }
    }

    public class RenderExcel
    {
        private NSS2015Report db = new NSS2015Report();
        private static List<DimensionExcel> lstDimension = new List<DimensionExcel>();
        private static List<FocusAreasExcel> lstFocusArea = new List<FocusAreasExcel>();
        private Year ys = new Year();

        public RenderExcel() { }

        public string Generate(Guid ReportHistoryID, string mappedpath)
        {
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == ReportHistoryID).SingleOrDefault();

            List<Year> year1Dataset = new List<Year>();
            List<Year> year2Dataset = new List<Year>();
            ComprehensiveDimension compdimensionyear1 = new ComprehensiveDimension();
            ComprehensiveDimension compdimensionyear2 = new ComprehensiveDimension();
            List<StatementShift> statementshifts = new List<StatementShift>();
            List<StatementShift> statementshiftsee = new List<StatementShift>();

            List<Question> questions = db.Questions.ToList();
            List<Dimension> dimensions = db.Dimensions.ToList();

            string year2 = history.Year2;

            year1Dataset = ys.YearDataSet();
            year2Dataset = ys.YearDataSet(year2);

            string reportname = history.ReportName;
            string cluster = history.Cluster;
            string year1division1 = history.Y1Division1;
            string year1division2 = history.Y1Division2;
            string year1division3 = history.Y1Division3;
            string year1division4 = history.Y1Division4;
            string year1division5 = history.Y1Division5;
            string year1division6 = history.Y1Division6;
            string year1branch = history.Y1Branch;
            string year2division1 = history.Y2Division1;
            string year2division2 = history.Y2Division2;
            string year2division3 = history.Y2Division3;
            string year2division4 = history.Y2Division4;
            string year2division5 = history.Y2Division5;
            string year2division6 = history.Y2Division6;
            string year2branch = history.Y2Branch;

            if (cluster == "All")
            {
                year1Dataset = year1Dataset.Where(p => p.IsSample == true).Distinct().ToList();
                year2Dataset = year2Dataset.Where(p => p.IsSample == true).Distinct().ToList();
            }
            else
            {
                year1Dataset = year1Dataset.Where(p => p.Cluster.ToLower().Trim() == cluster.ToLower().Trim()).Distinct().ToList();

                if ((cluster != "RBB" && cluster != "CIB") || (year2division1 == "All"
                        && year2division2 == "All"
                        && year2division3 == "All"
                        && year2division4 == "All"
                        && year2division5 == "All"
                        && year2division6 == "All"
                        && year2branch == "All")) year2Dataset = year2Dataset.Where(p => p.Cluster.ToLower().Trim() == cluster.ToLower().Trim()).Distinct().ToList();                
            }

            if (year1division1 != "All") year1Dataset = year1Dataset.Where(p => year1division1.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division1.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year2division1 != "All") year2Dataset = year2Dataset.Where(p => year2division1.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division1.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year1division2 != "All") year1Dataset = year1Dataset.Where(p => year1division2.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division2.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year2division2 != "All") year2Dataset = year2Dataset.Where(p => year2division2.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division2.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year1division3 != "All") year1Dataset = year1Dataset.Where(p => year1division3.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division3.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year2division3 != "All") year2Dataset = year2Dataset.Where(p => year2division3.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division3.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year1division4 != "All") year1Dataset = year1Dataset.Where(p => year1division4.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division4.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year2division4 != "All") year2Dataset = year2Dataset.Where(p => year2division4.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division4.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year1division5 != "All") year1Dataset = year1Dataset.Where(p => year1division5.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division5.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year2division5 != "All") year2Dataset = year2Dataset.Where(p => year2division5.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division5.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year1division6 != "All") year1Dataset = year1Dataset.Where(p => year1division6.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division6.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year2division6 != "All") year2Dataset = year2Dataset.Where(p => year2division6.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division6.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year1branch != "All") year1Dataset = year1Dataset.Where(p => year1branch.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList();
            if (year2branch != "All") year2Dataset = year2Dataset.Where(p => year2branch.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList();

            string classification = history.Classification;
            string occlevel = history.OccupationalLevel;
            string los = history.LOS;
            string passage = history.ManagementPassage;
            string location = history.Location;
            string gender = history.Gender;
            string race = history.Race;
            string jobfamily = history.JobFamily;
            string region = history.Region;

            if (history.ManualBranchCodes != null) { year1Dataset = year1Dataset.Where(p => history.ManualBranchCodes.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => history.ManualBranchCodes.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList(); }
            if (history.ManualStaffCodes != null) { year1Dataset = year1Dataset.Where(p => history.ManualStaffCodes.ToLower().Trim().Split('|').AsEnumerable().Contains(p.StaffCode.NullCheck().ToLower().Trim())).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => history.ManualStaffCodes.ToLower().Trim().Split('|').AsEnumerable().Contains(p.StaffCode.NullCheck().ToLower().Trim())).Distinct().ToList(); }


            if (location != "All") { year1Dataset = year1Dataset.Where(p => location.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Location.NullCheck().ToLower().Trim())).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => location.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Location.NullCheck().ToLower().Trim())).Distinct().ToList(); }
            if (classification != "All") { year1Dataset = year1Dataset.Where(p => p.Classification.NullCheck().ToLower().Trim() == classification.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Classification.NullCheck().ToLower().Trim() == classification.ToLower().Trim()).Distinct().ToList(); }
            if (jobfamily != "All") { year1Dataset = year1Dataset.Where(p => p.JobFamily.Normalize().ToLower().Trim() == jobfamily.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.JobFamily.NullCheck().ToLower().Trim() == jobfamily.ToLower().Trim()).Distinct().ToList(); }
            if (los != "All") { year1Dataset = year1Dataset.Where(p => p.LOS.NullCheck().ToLower().Trim() == los.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.LOS.NullCheck().ToLower().Trim() == los.ToLower().Trim()).Distinct().ToList(); }
            if (region != "All") { year1Dataset = year1Dataset.Where(p => p.Region.NullCheck().ToLower().Trim() == region.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Region.NullCheck().ToLower().Trim() == region.ToLower().Trim()).Distinct().ToList(); }
            if (passage != "All") { year1Dataset = year1Dataset.Where(p => p.ManagementPassage.NullCheck().ToLower().Trim() == passage.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.ManagementPassage.NullCheck().ToLower().Trim() == passage.ToLower().Trim()).Distinct().ToList(); }
            if (race != "All") { year1Dataset = year1Dataset.Where(p => p.Race.NullCheck().ToLower().Trim() == race.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Race.NullCheck().ToLower().Trim() == race.ToLower().Trim()).Distinct().ToList(); }
            if (occlevel != "All") { year1Dataset = year1Dataset.Where(p => p.OccupationalLevel.NullCheck().ToLower().Trim() == occlevel.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.OccupationalLevel.NullCheck().ToLower().Trim() == occlevel.ToLower().Trim()).Distinct().ToList(); }
            if (gender != "All") { year1Dataset = year1Dataset.Where(p => p.Gender.NullCheck().ToLower().Trim() == gender.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Gender.NullCheck().ToLower().Trim() == gender.ToLower().Trim()).Distinct().ToList(); }

            bool africa = !location.ToLower().Trim().Split('|').AsEnumerable().Contains("all") && !location.ToLower().Trim().Split('|').AsEnumerable().Contains("south africa") && !location.ToLower().Trim().Split('|').AsEnumerable().Contains("uk");

            compdimensionyear1 = ys.GetCompDimensions(year1Dataset, dimensions, questions, africa);
            compdimensionyear2 = ys.GetCompDimensions(year2Dataset, dimensions, questions, year2, africa);

            if (year2Dataset.Count > 0)
            {
                foreach (Statement statement in compdimensionyear1.CompStatements.Where(p => p.DimensionName != ""))
                {
                    double result = 0;
                    bool isSignificant = false;
                    Significance.getSignificance(statement.StatementCount, (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementCount : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementCount : 0)), statement.StatementPositiveValue, (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementPositiveValue : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementPositiveValue : 0)), out result, out isSignificant);
                    if (statement.DimensionName == "Employment Equity") statementshiftsee.Add(new StatementShift(statement.QuestionIndex, result, isSignificant));
                    statementshifts.Add(new StatementShift(statement.QuestionIndex, result, isSignificant));
                }
            }

            double year1count = year1Dataset.Count();
            double year2count = year2Dataset.Count();

            ComprehensiveDimension comdimyear1excl = ys.GetCompDimensions(year1Dataset, dimensions, questions, africa, true);

            string sample = cluster == "All" ? " Post Sample" : "";

            string filename = "Export_" + DateTime.Now.Ticks.ToString() + ".xlsx";

            using (SpreadsheetDocument s = SpreadsheetDocument.Create(mappedpath + "/" + filename, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookPart = s.AddWorkbookPart();
                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                string relId = workbookPart.GetIdOfPart(worksheetPart);
                Workbook workbook = new Workbook();
                FileVersion fileVersion = new FileVersion { ApplicationName = "Microsoft Office Excel" };
                Worksheet worksheet = new Worksheet();
                SheetData sheetData = new SheetData();

                WorkbookStylesPart wbsp = workbookPart.AddNewPart<WorkbookStylesPart>();
                wbsp.Stylesheet = CreateStylesheet();
                wbsp.Stylesheet.Save();
                worksheet.Append(CreateColumns());
                CreateDimensionHeader(ref sheetData, year2, year1count, year2count, reportname, ReportHistoryID);

                UInt32 RowIndex = 16;

                double result = 0;
                bool isSignificant = false;

                foreach (ReportDimension dimension in compdimensionyear1.CompDimensions)
                {
                    if (compdimensionyear1.CompStatements.Where(p => p.DimensionName == dimension.DimensionName).Count() > 0)
                    {
                        List<string> y1qi = new List<string>();
                        if (year2 != "None") y1qi = (dimensions.Where(p => p.DimensionText == dimension.DimensionName && p.Comprehensive == true).SingleOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(dimensions.Where(p => p.DimensionText == dimension.DimensionName && p.Comprehensive == true).SingleOrDefault(), null) != null ? dimensions.Where(p => p.DimensionText == dimension.DimensionName && p.Comprehensive == true).SingleOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(dimensions.Where(p => p.DimensionText == dimension.DimensionName && p.Comprehensive == true).SingleOrDefault(), null).ToString().Split(',').ToList() : new List<string>());
                        if (year2 != "None") y1qi.AddRange(dimensions.Where(p => p.DimensionText == dimension.DimensionName && p.Comprehensive == true).SingleOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(dimensions.Where(p => p.DimensionText == dimension.DimensionName && p.Comprehensive == true).SingleOrDefault(), null) != null ? dimensions.Where(p => p.DimensionText == dimension.DimensionName && p.Comprehensive == true).SingleOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(dimensions.Where(p => p.DimensionText == dimension.DimensionName && p.Comprehensive == true).SingleOrDefault(), null).ToString().Split(',').ToList() : new List<string>());

                        if (africa && Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) { List<string> Temp = new List<string>(); for (int i = 0; i < y1qi.Count(); i++) { if (Convert.ToBoolean(questions.Where(p => p.QuestionIndex == "QI" + y1qi[i]).Select(p => p.GetType().GetProperty("Survey" + year2 + "Africa").GetValue(p, null)).SingleOrDefault())) Temp.Add(y1qi[i]); } y1qi = Temp; }

                        result = 0;
                        isSignificant = false;
                        if (year2 != "None") Significance.getSignificance(year1count, (year2 != "None" ? year2count : 0), dimension.DimensionPositiveValue, (compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault() != null && year2 != "None" ? compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue : 0), out result, out isSignificant);
                        sheetData.AppendChild(InsertRow(dimension.DimensionName.AfricaNewDimension(year2, africa).NewDimension(year2), dimension.DimensionPositiveValue, dimension.DimensionNeutralValue, dimension.DimensionNegativeValue, (year2 != "None" ? compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue : 0), (year2 != "None" ? compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNeutralValue : 0), (year2 != "None" ? compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNegativeValue : 0), result, isSignificant, RowIndex, 4));
                        RowIndex++;
                        foreach (Statement statement in compdimensionyear1.CompStatements.Where(p => p.DimensionName == dimension.DimensionName).Distinct().ToList())
                        {
                            if (year2 != "None")
                                sheetData.AppendChild(InsertRow(statement.StatementName + (!y1qi.Contains(statement.QuestionIndex.Replace("Q", "")) && year2 != "None" && y1qi.Count > 0 ? (africa ? "*" : " (New Statement in Dimension)") : ""), statement.StatementPositiveValue, statement.StatementNeutralValue, statement.StatementNegativeValue, (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null && year2 != "None" ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.StatementPositiveValue).FirstOrDefault() : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null && year2 != "None" ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.StatementPositiveValue).FirstOrDefault() : 0)), (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null && year2 != "None" ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.StatementNeutralValue).FirstOrDefault() : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null && year2 != "None" ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementNeutralValue : 0)), (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null && year2 != "None" ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.StatementNegativeValue).FirstOrDefault() : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null && year2 != "None" ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.StatementNegativeValue).FirstOrDefault() : 0)), (year2 != "None" ? statementshifts.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.Result).FirstOrDefault() : 0), (year2 != "None" ? statementshifts.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.isSignificant).FirstOrDefault() : false), RowIndex, 5));
                            else
                                sheetData.AppendChild(InsertRow(statement.StatementName + (!y1qi.Contains(statement.QuestionIndex.Replace("Q", "")) && year2 != "None" && y1qi.Count > 0 ? (africa ? "*" : " (New Statement in Dimension)") : ""), statement.StatementPositiveValue, statement.StatementNeutralValue, statement.StatementNegativeValue, 0, 0, 0, 0, false, RowIndex, 5));
                            RowIndex++;
                        }

                        if (compdimensionyear1.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName).Count() > 0)
                        {
                            result = 0;
                            isSignificant = false;
                            if (year2 != "None") { if (compdimensionyear2.NotIncDimensions != null) { if (compdimensionyear2.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue != 0 && compdimensionyear2.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNeutralValue != 0 && compdimensionyear2.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNegativeValue != 0) Significance.getSignificance(year1count, year2count, compdimensionyear1.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue, (compdimensionyear2.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault() != null ? compdimensionyear2.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue : 0), out result, out isSignificant); } }
                            if (year2 != "None" && compdimensionyear2.NotIncDimensions != null) sheetData.AppendChild(InsertRow(compdimensionyear1.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionName.NewDimension(year2) + " (New Questions not included in spidergram calculation)", compdimensionyear1.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue, compdimensionyear1.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNeutralValue, compdimensionyear1.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNegativeValue, compdimensionyear2.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue, compdimensionyear2.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNeutralValue, compdimensionyear2.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNegativeValue, result, isSignificant, RowIndex, 4)); else sheetData.AppendChild(InsertRow(compdimensionyear1.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionName.Replace(" (New Dimension in 2012)", "") + " (New Questions not included in spidergram calculation)", compdimensionyear1.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue, compdimensionyear1.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNeutralValue, compdimensionyear1.NotIncDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNegativeValue, 0, 0, 0, 0, false, RowIndex, 4));
                            RowIndex++;
                            foreach (Statement statement in compdimensionyear1.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName).Distinct().ToList())
                            {
                                string statementaddedtext = "";
                                if (year2 != "None") statementaddedtext = " (" + (Convert.ToInt32(statement.QuestionIndex.ToUpper().Replace("Q", "")) > 176 ? "New Statement" : (compdimensionyear2.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName && p.QuestionIndex == statement.QuestionIndex).Count() > 0 ? "Statement" : "New statement within Dimension")) + " not included in spidergram calculation)";
                                result = 0;
                                isSignificant = false;
                                if (year2 != "None") Significance.getSignificance(statement.StatementCount, (compdimensionyear2.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName && p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName && p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementCount : 0), statement.StatementPositiveValue, (compdimensionyear2.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName && p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName && p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementPositiveValue : 0), out result, out isSignificant);
                                if (year2 != "None") sheetData.AppendChild(InsertRow(statement.StatementName + statementaddedtext, statement.StatementPositiveValue, statement.StatementNeutralValue, statement.StatementNegativeValue, (compdimensionyear2.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName && p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName && p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementPositiveValue : 0), (compdimensionyear2.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName && p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName && p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementNeutralValue : 0), (compdimensionyear2.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName && p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.DimensionName == dimension.DimensionName && p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementNegativeValue : 0), result, isSignificant, RowIndex, 5)); else sheetData.AppendChild(InsertRow(statement.StatementName + statementaddedtext, statement.StatementPositiveValue, statement.StatementNeutralValue, statement.StatementNegativeValue, 0, 0, 0, 0, false, RowIndex, 5));
                                RowIndex++;
                            }
                        }
                    }
                }

                RowIndex++;
                result = 0;
                isSignificant = false;
                if (year2 != "None") Significance.getSignificance(year1count, year2count, compdimensionyear1.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").Select(p => p.StatementPositiveValue).FirstOrDefault(), compdimensionyear2.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").Select(p => p.StatementPositiveValue).FirstOrDefault(), out result, out isSignificant);
                if (year2 != "None") sheetData.AppendChild(InsertRow(compdimensionyear1.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").FirstOrDefault().StatementName, compdimensionyear1.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").FirstOrDefault().StatementPositiveValue, compdimensionyear1.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").Select(p => p.StatementNeutralValue).FirstOrDefault(), compdimensionyear1.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").Select(p => p.StatementNegativeValue).FirstOrDefault(), compdimensionyear2.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").Select(p => p.StatementPositiveValue).FirstOrDefault(), compdimensionyear2.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").Select(p => p.StatementNeutralValue).FirstOrDefault(), compdimensionyear2.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").Select(p => p.StatementNegativeValue).FirstOrDefault(), result, isSignificant, RowIndex, 5)); else sheetData.AppendChild(InsertRow(compdimensionyear1.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").FirstOrDefault().StatementName, compdimensionyear1.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").Select(p => p.StatementPositiveValue).FirstOrDefault(), compdimensionyear1.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").Select(p => p.StatementNeutralValue).FirstOrDefault(), compdimensionyear1.CompStatements.Where(p => p.QuestionIndex.ToUpper().Replace("Q", "") == "176").Select(p => p.StatementNegativeValue).FirstOrDefault(), 0, 0, 0, 0, false, RowIndex, 5));
                RowIndex++;
                RowIndex++;

                result = 0;
                isSignificant = false;
                if (year2 != "None") Significance.getSignificance(year1count, year2count, compdimensionyear1.NedbankOverall.DimensionPositiveValue, compdimensionyear2.NedbankOverall.DimensionPositiveValue, out result, out isSignificant);
                if (year2 != "None") sheetData.AppendChild(InsertRow(compdimensionyear1.NedbankOverall.DimensionName + "(based on 14 dimensions in 2015 vs " + history.Year2 + ")", compdimensionyear1.NedbankOverall.DimensionPositiveValue, compdimensionyear1.NedbankOverall.DimensionNeutralValue, compdimensionyear1.NedbankOverall.DimensionNegativeValue, compdimensionyear2.NedbankOverall.DimensionPositiveValue, compdimensionyear2.NedbankOverall.DimensionNeutralValue, compdimensionyear2.NedbankOverall.DimensionNegativeValue, result, isSignificant, RowIndex, 4)); else sheetData.AppendChild(InsertRow(compdimensionyear1.NedbankOverall.DimensionName + "(based on 14 dimensions in 2012 vs 2011)", compdimensionyear1.NedbankOverall.DimensionPositiveValue, compdimensionyear1.NedbankOverall.DimensionNeutralValue, compdimensionyear1.NedbankOverall.DimensionNegativeValue, 0, 0, 0, 0, false, RowIndex, 4));
                RowIndex++;

                foreach (ReportDimension dimension in compdimensionyear1.CompDimensions)
                {
                    result = 0;
                    isSignificant = false;
                    if (year2 != "None") Significance.getSignificance(year1count, (year2 != "None" ? year2count : 0), dimension.DimensionPositiveValue, (compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).FirstOrDefault() != null && year2 != "None" ? compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).Select(p => p.DimensionPositiveValue).FirstOrDefault() : 0), out result, out isSignificant);
                    sheetData.AppendChild(InsertRow(dimension.DimensionName.AfricaNewDimension(year2, africa).NewDimension(year2), dimension.DimensionPositiveValue, dimension.DimensionNeutralValue, dimension.DimensionNegativeValue, (year2 != "None" ? compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).Select(p => p.DimensionPositiveValue).FirstOrDefault() : 0), (year2 != "None" ? compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).Select(p => p.DimensionNeutralValue).FirstOrDefault() : 0), (year2 != "None" ? compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).Select(p => p.DimensionNegativeValue).FirstOrDefault() : 0), result, isSignificant, RowIndex, 5));
                    RowIndex++;
                }

                RowIndex++;
                CreateNPSHeader(ref sheetData, ref RowIndex);

                double detractor, passive, promoter, count;
                detractor = passive = promoter = count = 0;

                detractor = year1Dataset.Where(p => p.QNPS1 <= 6).Count();
                passive = year1Dataset.Where(p => p.QNPS1 == 7 || p.QNPS1 == 8).Count();
                promoter = year1Dataset.Where(p => p.QNPS1 == 9 || p.QNPS1 == 10).Count();
                count = year1Dataset.Where(p => p.QNPS1 != null).Count();

                double nps = Math.Round(promoter / count * 100, 1, MidpointRounding.AwayFromZero) - Math.Round(detractor / count * 100, 1, MidpointRounding.AwayFromZero);
                sheetData.AppendChild(InsertNPSRow("Likelihood that you would recommend Nedbank as a “great place to bank”?", count, nps, Math.Round(detractor / count * 100, 1, MidpointRounding.AwayFromZero), Math.Round(passive / count * 100, 1, MidpointRounding.AwayFromZero), Math.Round(promoter / count * 100, 1, MidpointRounding.AwayFromZero), RowIndex, 5));
                RowIndex++;

                detractor = year1Dataset.Where(p => p.QNPS2 <= 6).Count();
                passive = year1Dataset.Where(p => p.QNPS2 == 7 || p.QNPS2 == 8).Count();
                promoter = year1Dataset.Where(p => p.QNPS2 == 9 || p.QNPS2 == 10).Count();
                count = year1Dataset.Where(p => p.QNPS2 != null).Count();

                nps = Math.Round(promoter / count * 100, 1, MidpointRounding.AwayFromZero) - Math.Round(detractor / count * 100, 1, MidpointRounding.AwayFromZero);
                sheetData.AppendChild(InsertNPSRow("Likelihood that you would recommend Nedbank as a “great place to work”?", count, nps, Math.Round(detractor / count * 100, 1, MidpointRounding.AwayFromZero), Math.Round(passive / count * 100, 1, MidpointRounding.AwayFromZero), Math.Round(promoter / count * 100, 1, MidpointRounding.AwayFromZero), RowIndex, 5));
                RowIndex++;                

                if (africa)
                {
                    RowIndex++;
                    CreateDefinitions(ref sheetData, year2, ref RowIndex);
                }

                worksheet.Append(sheetData);

                worksheet.Append(new SheetProtection() { Password = "E6D4", Sheet = true, Objects = true, Scenarios = true, FormatRows = false, FormatColumns = false, FormatCells = true, DeleteRows = true, DeleteColumns = true });

                worksheetPart.Worksheet = worksheet;
                worksheetPart.Worksheet.Save();

                ApplyMergeOnCells(worksheet, africa, RowIndex);

                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet { Name = "All Statements", SheetId = 1, Id = relId };
                sheets.Append(sheet);

                workbook.Append(fileVersion);
                workbook.Append(sheets);

                s.WorkbookPart.Workbook = workbook;
                s.WorkbookPart.Workbook.Save();
                s.Close();
            }

            return filename;
        }

        private static Row InsertFARow(string desc, double year1count, double year1score, double year2count, double year2score, double shift, bool significant, UInt32 RowIndex, UInt32 StyleIndex)
        {
            Row _Row = new Row { RowIndex = RowIndex };
            _Row.Append(CreateTextCell(desc, StyleIndex));
            _Row.Append(CreateTextCell("", StyleIndex));
            _Row.Append(CreateTextCell(year1count.ToString(), StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, year1score.ToString(), StyleIndex));
            _Row.Append(CreateTextCell(year2count.ToString(), StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, year2score.ToString(), StyleIndex));
            _Row.Append(CreateTextCell("", StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, shift.ToString(), shift > 0 ? 8 : (shift < 0 ? 9 : StyleIndex)));
            _Row.Append(significant ? (shift > 0 ? CreateTextCell("£", 6) : CreateTextCell("¤", 7)) : CreateTextCell("", 5));

            return _Row;
        }

        private static Row InsertNPSRow(string desc, double count, double score, double detractor, double passive, double promoter, UInt32 RowIndex, UInt32 StyleIndex)
        {
            Row _Row = new Row { RowIndex = RowIndex };
            _Row.Append(CreateTextCell(desc, StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, count.ToString(), 3));
            _Row.Append(CreateTextCell("", StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, score.ToString(), (UInt32)(score < 0 ? 9 : (score > 0 ? 8 : 10))));
            _Row.Append(CreateTextCell("", StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, detractor.ToString(), StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, passive.ToString(), StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, promoter.ToString(), StyleIndex));
            _Row.Append(CreateTextCell("", StyleIndex));

            return _Row;
        }

        private static Row InsertRow(string desc, double year1pos, double year1neu, double year1neg, double year2pos, double year2neu, double year2neg, double shift, bool significant, UInt32 RowIndex, UInt32 StyleIndex)
        {
            Row _Row = new Row { RowIndex = RowIndex };
            _Row.Append(CreateTextCell(desc, StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, year1pos.ToString(), StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, year1neu.ToString(), StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, year1neg.ToString(), StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, year2pos.ToString(), StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, year2neu.ToString(), StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, year2neg.ToString(), StyleIndex));
            _Row.Append(CreateTextCell(CellValues.Number, shift.ToString(), shift > 0 ? 8 : (shift < 0 ? 9 : StyleIndex)));
            _Row.Append(significant ? (shift > 0 ? CreateTextCell("£", 6) : CreateTextCell("¤", 7)) : CreateTextCell("", StyleIndex));

            return _Row;
        }

        private static void ApplyMergeOnCells(Worksheet worksheet, bool africa, UInt32 RowIndex)
        {
            MergeCells _MergeCells;
            if (worksheet.Elements<MergeCells>().Count() > 0)
                _MergeCells = worksheet.Elements<MergeCells>().First();
            else
            {
                _MergeCells = new MergeCells();
                if (worksheet.Elements<CustomSheetView>().Count() > 0)
                    worksheet.InsertAfter(_MergeCells, worksheet.Elements<CustomSheetView>().First());
                else
                    worksheet.InsertAfter(_MergeCells, worksheet.Elements<SheetProtection>().First());
            }

            _MergeCells.Append(new MergeCell() { Reference = new StringValue("A1:I6") });
            _MergeCells.Append(new MergeCell() { Reference = new StringValue("A7:I11") });
            _MergeCells.Append(new MergeCell() { Reference = new StringValue("B13:D13") });
            _MergeCells.Append(new MergeCell() { Reference = new StringValue("E13:G13") });
            _MergeCells.Append(new MergeCell() { Reference = new StringValue("H13:I13") });
            _MergeCells.Append(new MergeCell() { Reference = new StringValue("B14:D14") });
            _MergeCells.Append(new MergeCell() { Reference = new StringValue("E14:G14") });
            _MergeCells.Append(new MergeCell() { Reference = new StringValue("A15:A15") });

            if (africa)
            {
                _MergeCells.Append(new MergeCell() { Reference = new StringValue("A" + (RowIndex - 2) + ":I" + (RowIndex - 2)) });
                _MergeCells.Append(new MergeCell() { Reference = new StringValue("A" + (RowIndex - 1) + ":I" + (RowIndex - 1)) });
            }
        }


        private static Row CreateContentRow(UInt32 index, DataRow DR)
        {
            Row r = new Row { RowIndex = index };

            foreach (object S in DR.ItemArray)
                r.Append(CreateTextCell(S.ToString()));

            return r;
        }

        private static Columns CreateColumns()
        {
            Columns columns = new Columns();
            columns.Append(CreateColumnData(1, 1, 145.17));
            columns.Append(CreateColumnData(2, 2, 10));
            columns.Append(CreateColumnData(3, 3, 10));
            columns.Append(CreateColumnData(4, 4, 10));
            columns.Append(CreateColumnData(5, 5, 10));
            columns.Append(CreateColumnData(6, 6, 10));
            columns.Append(CreateColumnData(7, 7, 10));
            columns.Append(CreateColumnData(8, 8, 13));
            columns.Append(CreateColumnData(9, 9, 13));

            return columns;
        }

        private void CreateDefinitions(ref SheetData sheetData, string year2, ref UInt32 RowIndex)
        {
            Row _Row = new Row { RowIndex = RowIndex };
            _Row.Append(CreateTextCell("Definitions", 4));
            _Row.Append(CreateTextCell("", 4));
            _Row.Append(CreateTextCell("", 4));
            _Row.Append(CreateTextCell("", 4));
            _Row.Append(CreateTextCell("", 4));
            _Row.Append(CreateTextCell("", 4));
            _Row.Append(CreateTextCell("", 4));
            _Row.Append(CreateTextCell("", 4));
            _Row.Append(CreateTextCell("", 4));
            sheetData.AppendChild(_Row);
            RowIndex++;

            _Row = new Row { RowIndex = RowIndex };
            _Row.Append(CreateTextCell("* Statement omitted in " + year2 + " analysis but re-instated in 2014 (no " + year2 + " comparative score available)", 5));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            sheetData.AppendChild(_Row);
            RowIndex++;

            _Row = new Row { RowIndex = RowIndex };
            _Row.Append(CreateTextCell("** " + year2 + " Dimension score based on a reduced set of statements. 2015 dimension score based on inclusion of re-instated statements (year on year comparatives at a dimension level to be treated with caution)", 5));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            sheetData.AppendChild(_Row);
            RowIndex++;
        }

        private void CreateDimensionHeader(ref SheetData sheetData, string year2, double year1count, double year2count, string reportname, Guid ReportHistoryID)
        {
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == ReportHistoryID).SingleOrDefault();

            Row _Row = new Row { RowIndex = 1 };
            _Row.Append(CreateTextCell("Nedbank Staff Survey", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            sheetData.AppendChild(_Row);

            _Row = new Row { RowIndex = 7 };
            _Row.Append(CreateTextCell("Cluster : " + history.Cluster + "\r\n2015 Division 1 : " + history.Y1Division1 + ",   2015 Division 2 : " + history.Y1Division2 + ",   2015 Division 3 : " + history.Y1Division3 + ",   2015 Division 4 : " + history.Y1Division4 + ",  2015 Division 5 : " + history.Y1Division5 + ",   2015 Division 6 : " + history.Y1Division6 + ",   2015 Branch : " + (history.Y1Branch.Length + (history.ManualBranchCodes != null ? history.ManualBranchCodes.Length : 0) > 50 ? "Multiple Values Selected" : (history.ManualBranchCodes != null ? (history.Y1Branch != "All" ? history.Y1Branch + "|" : "") + history.ManualBranchCodes : history.Y1Branch)) + "\r\n" + year2 + " Division 1 : " + history.Y2Division1 + ",   " + year2 + " Division 2 : " + history.Y2Division2 + ",   " + year2 + " Division 3 : " + history.Y2Division3 + ",   " + year2 + " Division 4 : " + (history.Y2Division4.Length > 50 ? "Multiple Values Selected" : history.Y2Division4) + ",   " + year2 + " Division 5 : " + history.Y2Division5 + ",   " + year2 + " Division 6 : " + history.Y2Division6 + ",   " + year2 + " Branch : " + history.Y2Branch + "\r\nGenerational Classification : " + history.Classification + ",   Occupational Level : " + history.OccupationalLevel + ",   Length of Service : " + history.LOS + ",   Management Passage : " + history.ManagementPassage + "\r\nGender : " + history.Gender + ",   Race : " + history.Race + ",   Job Family : " + history.JobFamily + ",   Region : " + history.Region + ",   Location : " + history.Location, 11));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            _Row.Append(CreateTextCell("", 12));
            sheetData.AppendChild(_Row);

            _Row = new Row { RowIndex = 12 };
            _Row.Append(CreateTextCell(reportname + ": n=" + year1count.ToString() + "(2015);n=" + year2count.ToString() + "(" + year2 + ")", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            _Row.Append(CreateTextCell("", 1));
            sheetData.AppendChild(_Row);

            _Row = new Row { RowIndex = 13 };
            _Row.Append(CreateTextCell("", 2));
            _Row.Append(CreateTextCell(CellValues.Number, "2015", 2));
            _Row.Append(CreateTextCell("", 2));
            _Row.Append(CreateTextCell("", 2));
            _Row.Append(CreateTextCell(CellValues.Number, year2, 2));
            _Row.Append(CreateTextCell("", 2));
            _Row.Append(CreateTextCell("", 2));
            _Row.Append(CreateTextCell("2015 vs " + year2, 2));
            _Row.Append(CreateTextCell("", 2));
            sheetData.AppendChild(_Row);

            _Row = new Row { RowIndex = 14 };
            _Row.Append(CreateTextCell("", 3));
            _Row.Append(CreateTextCell("% Respondents", 3));
            _Row.Append(CreateTextCell("", 3));
            _Row.Append(CreateTextCell("", 3));
            _Row.Append(CreateTextCell("% Respondents", 3));
            _Row.Append(CreateTextCell("", 3));
            _Row.Append(CreateTextCell("", 3));
            _Row.Append(CreateTextCell("Difference", 3));
            _Row.Append(CreateTextCell("Direction", 3));
            sheetData.AppendChild(_Row);

            _Row = new Row { RowIndex = 15 };
            _Row.Append(CreateTextCell("", 3));
            _Row.Append(CreateTextCell("Agree", 3));
            _Row.Append(CreateTextCell("Neutral", 3));
            _Row.Append(CreateTextCell("Disagree", 3));
            _Row.Append(CreateTextCell("Agree", 3));
            _Row.Append(CreateTextCell("Neutral", 3));
            _Row.Append(CreateTextCell("Disagree", 3));
            _Row.Append(CreateTextCell("Shift", 3));
            _Row.Append(CreateTextCell("Sign. Change (95% Conf.)", 3));
            sheetData.AppendChild(_Row);
        }

        private static void CreateFocusAreaHeader(ref SheetData sheetData, string year1, string year2, double year1count, double year2count, ref UInt32 RowIndex)
        {
            Row _Row = new Row { RowIndex = RowIndex };
            _Row.Append(CreateTextCell("", 2));
            _Row.Append(CreateTextCell("", 2));
            _Row.Append(CreateTextCell(CellValues.Number, year1, 2));
            _Row.Append(CreateTextCell("", 2));
            _Row.Append(CreateTextCell(CellValues.Number, year2, 2));
            _Row.Append(CreateTextCell("", 2));
            _Row.Append(CreateTextCell("", 2));
            _Row.Append(CreateTextCell(year1 + " vs " + year2, 2));
            _Row.Append(CreateTextCell("", 2));
            sheetData.AppendChild(_Row);
            RowIndex++;

            _Row = new Row { RowIndex = RowIndex };//merge a,b            
            _Row.Append(CreateTextCell("If you were given the opportunity to provide Nedbank management with advice on THREE focus areas impacting you, what would these be?", 4));
            _Row.Append(CreateTextCell("", 4));
            _Row.Append(CreateTextCell("Count", 4));
            _Row.Append(CreateTextCell("%", 4));
            _Row.Append(CreateTextCell("Count", 4));
            _Row.Append(CreateTextCell("%", 4));
            _Row.Append(CreateTextCell("", 4));
            _Row.Append(CreateTextCell("Shift", 4));
            _Row.Append(CreateTextCell("Direction", 4));
            sheetData.AppendChild(_Row);
            RowIndex++;
        }

        private static void CreateNPSHeader(ref SheetData sheetData, ref UInt32 RowIndex)
        {
            Row _Row = new Row { RowIndex = RowIndex };
            _Row.Append(CreateTextCell("Net Promoter Score", 4));
            _Row.Append(CreateTextCell("n", 2));
            _Row.Append(CreateTextCell("", 2));
            _Row.Append(CreateTextCell("Score", 2));
            _Row.Append(CreateTextCell("", 2));
            _Row.Append(CreateTextCell("Detractor", 2));
            _Row.Append(CreateTextCell("Passive", 2));
            _Row.Append(CreateTextCell("Promoter", 2));
            _Row.Append(CreateTextCell("", 2));
            sheetData.AppendChild(_Row);
            RowIndex++;
        }

        private static Cell CreateTextCell(string text)
        {
            return new Cell() { DataType = CellValues.String, CellValue = new CellValue(text) };
        }

        private static Cell CreateTextCell(string text, UInt32Value styleindex)
        {
            return new Cell() { DataType = CellValues.String, CellValue = new CellValue(text), StyleIndex = styleindex };
        }

        private static Cell CreateTextCell(CellValues datatype, string text, UInt32Value styleindex)
        {
            return new Cell() { DataType = datatype, CellValue = new CellValue(text), StyleIndex = styleindex };
        }

        private static Stylesheet CreateStylesheet()
        {
            Stylesheet ss = new Stylesheet();
            uint iExcelIndex = 164;

            uint NumberFormatId = UInt32Value.FromUInt32(iExcelIndex++);

            CellStyleFormats csfs = new CellStyleFormats();
            csfs.Append(new CellFormat() { NumberFormatId = 0, FontId = 0, FillId = 0, BorderId = 0 });
            csfs.Count = UInt32Value.FromUInt32((uint)csfs.ChildElements.Count);

            NumberingFormats nfs = new NumberingFormats();
            CellFormats cfs = new CellFormats();

            cfs.Append(new CellFormat() { NumberFormatId = 0, FontId = 0, FillId = 0, BorderId = 0, FormatId = 0 });
            nfs.Append(new NumberingFormat() { NumberFormatId = NumberFormatId, FormatCode = StringValue.FromString("#,##0.0") });

            cfs.Append(new CellFormat() { FontId = 3, FillId = 0, BorderId = 1, FormatId = 0, ApplyNumberFormat = BooleanValue.FromBoolean(true), Alignment = new Alignment() { Horizontal = HorizontalAlignmentValues.Left } });
            cfs.Append(new CellFormat() { FontId = 1, FillId = 2, BorderId = 1, FormatId = 0, ApplyNumberFormat = BooleanValue.FromBoolean(true), Alignment = new Alignment() { WrapText = true, Horizontal = HorizontalAlignmentValues.Center } });
            cfs.Append(new CellFormat() { FontId = 2, FillId = 0, BorderId = 1, FormatId = 0, ApplyNumberFormat = BooleanValue.FromBoolean(true), Alignment = new Alignment() { WrapText = true, Horizontal = HorizontalAlignmentValues.Center } });
            cfs.Append(new CellFormat() { NumberFormatId = NumberFormatId, FontId = 1, FillId = 2, BorderId = 1, FormatId = 0, ApplyNumberFormat = BooleanValue.FromBoolean(true) });
            cfs.Append(new CellFormat() { NumberFormatId = NumberFormatId, FontId = 0, FillId = 0, BorderId = 1, FormatId = 0, ApplyNumberFormat = BooleanValue.FromBoolean(true) });
            cfs.Append(new CellFormat() { NumberFormatId = NumberFormatId, FontId = 4, FillId = 3, BorderId = 1, FormatId = 0, ApplyNumberFormat = BooleanValue.FromBoolean(true), Alignment = new Alignment() { Horizontal = HorizontalAlignmentValues.Center } });
            cfs.Append(new CellFormat() { NumberFormatId = NumberFormatId, FontId = 4, FillId = 4, BorderId = 1, FormatId = 0, ApplyNumberFormat = BooleanValue.FromBoolean(true), Alignment = new Alignment() { Horizontal = HorizontalAlignmentValues.Center } });
            cfs.Append(new CellFormat() { NumberFormatId = NumberFormatId, FontId = 1, FillId = 3, BorderId = 1, FormatId = 0, ApplyNumberFormat = BooleanValue.FromBoolean(true) });
            cfs.Append(new CellFormat() { NumberFormatId = NumberFormatId, FontId = 1, FillId = 4, BorderId = 1, FormatId = 0, ApplyNumberFormat = BooleanValue.FromBoolean(true) });
            cfs.Append(new CellFormat() { FontId = 0, FillId = 0, BorderId = 1, FormatId = 0, Alignment = new Alignment() { Horizontal = HorizontalAlignmentValues.Left } });
            cfs.Append(new CellFormat() { FontId = 0, FillId = 0, BorderId = 1, FormatId = 0, Alignment = new Alignment() { WrapText = true, Horizontal = HorizontalAlignmentValues.Left } });
            cfs.Append(new CellFormat() { FontId = 5, FillId = 0, BorderId = 1, FormatId = 0, Alignment = new Alignment() { WrapText = true, Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center } });

            nfs.Count = UInt32Value.FromUInt32((uint)nfs.ChildElements.Count);
            cfs.Count = UInt32Value.FromUInt32((uint)cfs.ChildElements.Count);

            ss.Append(nfs);
            ss.Append(CreateFontStyles());
            ss.Append(CreateFillStyles());
            ss.Append(CreateBorderStyles());
            ss.Append(csfs);
            ss.Append(cfs);

            CellStyles css = new CellStyles();
            css.Append(new CellStyle() { Name = StringValue.FromString("Normal"), FormatId = 0, BuiltinId = 0 });
            css.Count = UInt32Value.FromUInt32((uint)css.ChildElements.Count);
            ss.Append(css);

            ss.Append(new DifferentialFormats() { Count = 0 });
            ss.Append(new TableStyles() { Count = 0, DefaultTableStyle = StringValue.FromString("TableStyleMedium9"), DefaultPivotStyle = StringValue.FromString("PivotStyleLight16") });

            return ss;
        }

        private static Fonts CreateFontStyles()
        {
            Fonts fts = new Fonts();

            fts.Append(new DocumentFormat.OpenXml.Spreadsheet.Font() { FontName = new FontName() { Val = StringValue.FromString("Arial") }, FontSize = new FontSize() { Val = DoubleValue.FromDouble(8) } });
            fts.Append(new DocumentFormat.OpenXml.Spreadsheet.Font() { FontName = new FontName() { Val = StringValue.FromString("Arial") }, FontSize = new FontSize() { Val = DoubleValue.FromDouble(8) }, Color = new DocumentFormat.OpenXml.Spreadsheet.Color() { Rgb = HexBinaryValue.FromString("ffffffff") }, Bold = new Bold() { Val = true } });
            fts.Append(new DocumentFormat.OpenXml.Spreadsheet.Font() { FontName = new FontName() { Val = StringValue.FromString("Arial") }, FontSize = new FontSize() { Val = DoubleValue.FromDouble(8) }, Bold = new Bold() { Val = true } });
            fts.Append(new DocumentFormat.OpenXml.Spreadsheet.Font() { FontName = new FontName() { Val = StringValue.FromString("Arial") }, FontSize = new FontSize() { Val = DoubleValue.FromDouble(10) }, Bold = new Bold() { Val = true } });
            fts.Append(new DocumentFormat.OpenXml.Spreadsheet.Font() { FontName = new FontName() { Val = StringValue.FromString("Wingdings 3") }, FontSize = new FontSize() { Val = DoubleValue.FromDouble(8) }, Color = new DocumentFormat.OpenXml.Spreadsheet.Color() { Rgb = HexBinaryValue.FromString("ffffffff") }, Bold = new Bold() { Val = true } });
            fts.Append(new DocumentFormat.OpenXml.Spreadsheet.Font() { FontName = new FontName() { Val = StringValue.FromString("Arial") }, FontSize = new FontSize() { Val = DoubleValue.FromDouble(18) }, Bold = new Bold() { Val = true } });

            fts.Count = UInt32Value.FromUInt32((uint)fts.ChildElements.Count);

            return fts;
        }

        private static Fills CreateFillStyles()
        {
            Fills fills = new Fills();

            fills.Append(new Fill() { PatternFill = new PatternFill() { PatternType = PatternValues.None } });
            fills.Append(new Fill() { PatternFill = new PatternFill() { PatternType = PatternValues.Solid, ForegroundColor = new ForegroundColor() { Rgb = HexBinaryValue.FromString("ffffff") }, BackgroundColor = new BackgroundColor() { Rgb = HexBinaryValue.FromString("ffffff") } } });
            fills.Append(new Fill() { PatternFill = new PatternFill() { PatternType = PatternValues.Solid, ForegroundColor = new ForegroundColor() { Rgb = HexBinaryValue.FromString("003300") }, BackgroundColor = new BackgroundColor() { Rgb = HexBinaryValue.FromString("003300") } } });
            fills.Append(new Fill() { PatternFill = new PatternFill() { PatternType = PatternValues.Solid, ForegroundColor = new ForegroundColor() { Rgb = HexBinaryValue.FromString("33CC33") }, BackgroundColor = new BackgroundColor() { Rgb = HexBinaryValue.FromString("33CC33") } } });
            fills.Append(new Fill() { PatternFill = new PatternFill() { PatternType = PatternValues.Solid, ForegroundColor = new ForegroundColor() { Rgb = HexBinaryValue.FromString("FF0000") }, BackgroundColor = new BackgroundColor() { Rgb = HexBinaryValue.FromString("FF0000") } } });
            fills.Count = UInt32Value.FromUInt32((uint)fills.ChildElements.Count);

            return fills;
        }

        private static Borders CreateBorderStyles()
        {
            //Index 0 - No Borders
            Borders borders = new Borders();

            borders.Append(new Border() { LeftBorder = new LeftBorder(), RightBorder = new RightBorder(), TopBorder = new TopBorder(), BottomBorder = new BottomBorder(), DiagonalBorder = new DiagonalBorder() });
            borders.Append(new Border() { LeftBorder = new LeftBorder() { Style = BorderStyleValues.Thin }, RightBorder = new RightBorder() { Style = BorderStyleValues.Thin }, TopBorder = new TopBorder() { Style = BorderStyleValues.Thin }, BottomBorder = new BottomBorder() { Style = BorderStyleValues.Thin }, DiagonalBorder = new DiagonalBorder() });
            borders.Count = UInt32Value.FromUInt32((uint)borders.ChildElements.Count);

            return borders;
        }

        private static Column CreateColumnData(UInt32 StartColumnIndex, UInt32 EndColumnIndex, double ColumnWidth)
        {
            return new Column() { Min = StartColumnIndex, Max = EndColumnIndex, Width = ColumnWidth, CustomWidth = true };
        }

        private static Drawing InsertBanner(DrawingsPart dp, ImagePart imgp, string sImagePath)
        {
            BlipFill blipFill = new BlipFill() { Blip = new DocumentFormat.OpenXml.Drawing.Blip() { Embed = dp.GetIdOfPart(imgp), CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print }, SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle() };
            blipFill.Append(new DocumentFormat.OpenXml.Drawing.Stretch() { FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle() });

            Bitmap bm = new Bitmap(sImagePath);
            DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents() { Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution), Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution) };
            bm.Dispose();

            ShapeProperties sp = new ShapeProperties() { BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto, Transform2D = new DocumentFormat.OpenXml.Drawing.Transform2D() { Offset = new DocumentFormat.OpenXml.Drawing.Offset() { X = 0, Y = 0 }, Extents = extents } };
            sp.Append(new DocumentFormat.OpenXml.Drawing.PresetGeometry() { Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle, AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList() });
            sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

            AbsoluteAnchor anchor = new AbsoluteAnchor() { Position = new Position() { X = 0, Y = 0 }, Extent = new Extent() { Cx = extents.Cx, Cy = extents.Cy } };
            anchor.Append(new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture() { NonVisualPictureProperties = new NonVisualPictureProperties() { NonVisualDrawingProperties = new NonVisualDrawingProperties() { Id = 1025, Name = "Banner", Description = "Nedbank" }, NonVisualPictureDrawingProperties = new NonVisualPictureDrawingProperties() { PictureLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks() { NoChangeAspect = true, NoChangeArrowheads = true } } }, BlipFill = blipFill, ShapeProperties = sp });
            anchor.Append(new ClientData());
            WorksheetDrawing wsd = new WorksheetDrawing();
            wsd.Append(anchor);

            Drawing drawing = new Drawing() { Id = dp.GetIdOfPart(imgp) };

            wsd.Save(dp);

            return drawing;
        }
        }
}