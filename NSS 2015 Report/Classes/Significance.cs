﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NSS_2015_Report
{
    public static class Significance
    {
        public static void getSignificance(double CurrentSample, double PreviousSample, double CurrentValue, double PreviousValue, out double result, out bool isSignificant)
        {
            double preCalc;
            double compError;
            double difference;

            if (CurrentSample > 0 && PreviousSample > 0)
            {
                preCalc = ((CurrentValue * (100 - CurrentValue)) / CurrentSample) + ((PreviousValue * (100 - PreviousValue)) / PreviousSample);
                compError = Math.Round(1.96 * Math.Sqrt(preCalc) * 10) / 10;
                difference = Math.Abs(CurrentValue - PreviousValue);

                isSignificant = (difference > compError) ? true : false;

                result = Math.Round(CurrentValue - PreviousValue, 1, MidpointRounding.AwayFromZero);
            }
            else
            {
                isSignificant = false;
                result = 0;
            }
        }

        public static bool getSignificance(double CurrentSample, double PreviousSample, double CurrentValue, double PreviousValue)
        {
            double preCalc;
            double compError;
            double difference;

            if (CurrentSample > 0 && PreviousSample > 0)
            {
                preCalc = ((CurrentValue * (100 - CurrentValue)) / CurrentSample) + ((PreviousValue * (100 - PreviousValue)) / PreviousSample);
                compError = Math.Round(1.96 * Math.Sqrt(preCalc) * 10) / 10;
                difference = Math.Abs(CurrentValue - PreviousValue);

                return (difference > compError) ? true : false;
            }
            else return false;
        }

        public static void getSignificantSettings(double Result, bool isSignificant, out string colour, out string image, out string fontcolor)
        {
            if (isSignificant)
            {
                if (Result > 0)
                {
                    image = "<img src='" + GetSiteRoot() + "/images/green.jpg' />";
                    colour = "#33CC33";
                    fontcolor = "#000";
                }
                else if (Result < 0)
                {
                    colour = "#FF0000";
                    image = "<img src='" + GetSiteRoot() + "/images/red.jpg' />";
                    fontcolor = "#fff";
                }
                else
                {
                    colour = "#FFFFFF";
                    image = "";
                    fontcolor = "#000";
                }
            }
            else
            {
                image = "";
                if (Result > 0)
                {
                    colour = "#33CC33";
                    fontcolor = "#000";
                }
                else if (Result < 0)
                {
                    colour = "#FF0000";
                    fontcolor = "#fff";
                }
                else
                {
                    colour = "#FFFFFF";
                    fontcolor = "#000";
                }
            }
        }

        public static string getSignificantSymbol(double Result, bool isSignificant)
        {
            if (isSignificant)
            {
                if (Result > 0)
                    return " ↑";
                else if (Result < 0)
                    return " ↓";
                else
                    return "  ";
            }
            else
                return "  ";
        }

        public static string getEEColor(double Value)
        {
            if (Value < 15) return "00C100";
            else if (Value >= 15 && Value <= 20) return "FFFF00";
            else if (Value > 20) return "FF0000";
            else return "";
        }

        public static string GetSiteRoot()
        {
            string port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == null || port == "80" || port == "443")
                port = "";
            else
                port = ":" + port;

            string protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
                protocol = "http://";
            else
                protocol = "https://";

            string sOut = protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + System.Web.HttpContext.Current.Request.ApplicationPath;

            if (sOut.EndsWith("/"))
            {
                sOut = sOut.Substring(0, sOut.Length - 1);
            }

            return sOut;
        }

        public static string StatSignificantLegend(string year1, string year2, bool pdf, bool comp, bool africa)
        {
            double result;
            if (comp)
                return "<tr><td align='left' style='font-style: italic' colspan='2'><b>Definitions</b><table><tr><td>*: Statistically significant change" + (double.TryParse(year2, out result) ? " since " + year2 : "") + "</td></tr>" + (pdf == false ? "<tr><td>Please note you may click on the legend to select and deselect the years shown in the chart.</td></tr>" : "") + (year1 == "2012" && year2 != "None" ? "<tr><td>No scores available for Engagement and Employment Equity in " + year2 + " as these are new Dimensions in 2012.</td></tr>" : "") + (africa ? "<tr><td>** " + year2 + " Dimension score based on a reduced set of statements. " + year1 + " dimension score based on inclusion of re-instated statements (year on year comparatives at a dimension level to be treated with caution)</td></tr>" : "") + "</table></td></tr>";
            else
                if (!pdf)
                    return "<tr><td align='left' style='font-style: italic' colspan='2'><b>Definitions</b><table><tr><td>Please note you may click on the legend to select and deselect the years shown in the chart.</td></tr>" + (year1 == "2012" && year2 != "None" ? "<tr><td>No scores available for Engagement and Employment Equity in " + year2 + " as these are new Dimensions in 2012.</td></tr>" : "") + "</table></td></tr>";
                else
                    return "";
        }
    }
}