﻿using HiQPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NSS_2015_Report
{
    public class RenderPDF
    {
        public void GeneratePDF(string filename, string url)
        {
            GeneratePDF(filename, url, PdfPageSize.A4, PdfPageOrientation.Landscape, 1123);
        }

        public void GeneratePDF(string filename, string url, PdfPageSize size, PdfPageOrientation orientation, int browserwidth)
        {
            GC.Collect();

            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.SerialNumber = "3pa3j466-uJK3vKy/-rKfm8O7+-7/7s/ubo-5/7t7/Dv-7PDn5+fn";
            htmlToPdfConverter.BrowserWidth = browserwidth;
            htmlToPdfConverter.HtmlLoadedTimeout = 1000;
            htmlToPdfConverter.Document.PageSize = size;
            htmlToPdfConverter.Document.PageOrientation = orientation;
            htmlToPdfConverter.Document.PdfStandard = PdfStandard.Pdf;
            htmlToPdfConverter.Document.Margins = new PdfMargins(0);
            htmlToPdfConverter.Document.FontEmbedding = true;
            htmlToPdfConverter.RunJavaScript = true;
            htmlToPdfConverter.RunExtensions = true;
            htmlToPdfConverter.TriggerMode = ConversionTriggerMode.WaitTime;
            htmlToPdfConverter.WaitBeforeConvert = 40;
            htmlToPdfConverter.Document.ConvertInternalLinks = true;
            htmlToPdfConverter.TrimToBrowserWidth = true;            

            string javaScriptCode = "this.zoom=100;";
            PdfJavaScriptAction javaScriptAction = new PdfJavaScriptAction(javaScriptCode);

            PdfDocument doc = htmlToPdfConverter.ConvertUrlToPdfDocument(url);
            doc.SetOpenAction(javaScriptAction);
            doc.WriteToFile(filename);

            htmlToPdfConverter = null;
            doc = null;
        }
    }
}