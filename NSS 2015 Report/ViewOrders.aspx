﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewOrders.aspx.cs" Inherits="NSS_2015_Report.ViewOrders" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <center>  
        <div style="width:100%;">
            <asp:Panel ID="pnlCriteria" runat="server" BackColor="#BFCF89" HorizontalAlign="Center" Width="100%">
                <table style="width:100%" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="text-align:center"><h2>View Orders</h2></td>
                    </tr>
                    <tr>
                        <td style="text-align:center">The "View Orders" page allows users to track the status of a report that has been requested.</td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td style="text-align:left;vertical-align:top;">
                                        <fieldset style="height: 100px;border-color:#000;border-style: solid;">
                                            <legend><b>Order Status</b></legend>
                                            <ul>
                                                <li>Ordered</li>
                                                <li>Processing</li>
                                                <li>Published</li>
                                            </ul>
                                        </fieldset>
                                    </td>
                                    <td style="text-align:left;vertical-align:top;">
                                        <fieldset style="height: 100px;border-color:#000;border-style: solid;">
                                            <legend><b>Invoice Status</b></legend>
                                            <ul>
                                                <li>Processing</li>
                                                <li>Published</li>
                                                <li>Unpaid</li>
                                                <li>Paid</li>
                                            </ul>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="2" cellspacing="0" style="border:1px solid #6F6F6F;width:100%;">
                                <asp:Repeater ID="rptOrders" runat="server">
                                    <HeaderTemplate>
                                        <tr style="font-weight:bold;color:White;background-color:#53b606;">
                                            <td style="border:1px solid #6F6F6F;">e-Invoice No</td>
                                            <td style="border:1px solid #6F6F6F;">Invoice No</td>
                                            <td style="border:1px solid #6F6F6F;">Requested by</td>
                                            <td style="border:1px solid #6F6F6F;">Date</td>
                                            <td style="border:1px solid #6F6F6F;">Order Status</td>
                                            <td style="border:1px solid #6F6F6F;">Invoice Status</td>
                                            <td style="border:1px solid #6F6F6F;">&nbsp;</td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr style="background-color:#fff;">
                                            <td style="border:1px solid #6F6F6F;"><%#Eval("Seq") %><asp:HiddenField ID="hdOrderID" runat="server" Value='<%#Eval("OrderID") %>'/></td>
                                            <td style="border:1px solid #6F6F6F;"><%#Eval("InvoiceNo") %></td>
                                            <td style="border:1px solid #6F6F6F;"><%#Eval("Fullname") %></td>
                                            <td style="border:1px solid #6F6F6F;"><%#Eval("DateRequested") %></td>
                                            <td style="border:1px solid #6F6F6F;"><%#Eval("OrderStatus") %></td>
                                            <td style="border:1px solid #6F6F6F;"><%#Eval("InvoiceStatus") %></td>
                                            <td style="border:1px solid #6F6F6F;"><asp:LinkButton ID="hpViewOrder" runat="server" OnClick="hpViewOrder_Click">View</asp:LinkButton></td>
                                        </tr>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tr style="background-color:#ececd8;">
                                            <td style="border:1px solid #6F6F6F;"><%#Eval("Seq") %><asp:HiddenField ID="hdOrderID" runat="server" Value='<%#Eval("OrderID") %>'/></td>
                                            <td style="border:1px solid #6F6F6F;"><%#Eval("InvoiceNo") %></td>
                                            <td style="border:1px solid #6F6F6F;"><%#Eval("Fullname") %></td>
                                            <td style="border:1px solid #6F6F6F;"><%#Eval("DateRequested") %></td>
                                            <td style="border:1px solid #6F6F6F;"><%#Eval("OrderStatus") %></td>
                                            <td style="border:1px solid #6F6F6F;"><%#Eval("InvoiceStatus") %></td>
                                            <td style="border:1px solid #6F6F6F;"><asp:LinkButton ID="hpViewOrder" runat="server" OnClick="hpViewOrder_Click">View</asp:LinkButton></td>
                                        </tr>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                            </table> 
                        </td>
                    </tr>
                    <tr>
                        <td><b><i>Please note it will take 3 – 4 working days to complete the report. <br />No orders will be processed until proof of payment is provided. Once proof of payment is received we will process the report order.</i></b></td>
                    </tr>
                    <tr>
                        <td style="text-align:center" colspan="2"><asp:Button ID="cmdBack" runat="server" Text="Return to filter selection" CssClass="btn" onclick="cmdBack_Click" /></td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </center>
</asp:Content>
