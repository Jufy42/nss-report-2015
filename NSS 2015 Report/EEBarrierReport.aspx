﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EEBarrierReport.aspx.cs" Inherits="NSS_2015_Report.EEBarrierReport" %>

<!DOCTYPE html>
<html lang="en">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title>NSS 2015</title>
    <asp:PlaceHolder ID="PlaceHolder1" runat="server">     
          <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>  
    <webopt:BundleReference ID="BundleReference1" runat="server" Path="~/Content/css" /> 
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <meta name="viewport" content="width=device-width" />
    <!--[if lt IE 9]>
        <script src="<%=GetSiteRoot()%>/Scripts/jquery-1.11.2.js"></script>
    <![endif]-->
    <!--[if (gte IE 9) | (!IE)]><!-->
    <script src="<%=GetSiteRoot()%>/Scripts/jquery-2.1.4.js"></script>
    <!--<![endif]-->
    <link href="Content/themes/base/all.css" rel="stylesheet" />
    <link href="Content/themes/base/all.css" rel="stylesheet" />
    <script type="text/javascript" src="../Charts/FusionCharts.js"></script>  
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" EnablePageMethods="true" EnablePartialRendering="true" ID="scriptmanager1" AsyncPostBackTimeout="0">
        <Scripts>
            <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=272931&clcid=0x409 --%>
            <%--Framework Scripts--%>
            
            <asp:ScriptReference Name="MsAjaxBundle" />            
            <asp:ScriptReference Name="jquery.ui.combined" />
            <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
            <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
            <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
            <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
            <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
            <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
            <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
            <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
            <asp:ScriptReference Name="WebFormsBundle" />
            <%--Site Scripts--%>

        </Scripts>
    </asp:ScriptManager>
    <asp:UpdatePanel ID="pnComp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width:100%;background-color:White;color:Black;Height:768px;overflow-y:scroll;text-align:center;padding:0;margin:0;" id="Reportdiv" runat="server">
                <div id="survey" style="width:1123px;margin: 0 auto;">
                    <asp:Literal ID="litReport" runat="server"></asp:Literal>
                </div>
            </div>
            <div style="width:100%;background-color:White;color:Black;border-top:1px solid black" align="center" id="BottomButtons" runat="server"> 
                <asp:Panel ID="Buttons" runat="server" style="background:#22513d;text-align:center;height:100%;" align="center">
                    <table cellpadding="5" cellspacing="0" width="1024px;" align="center">
                        <tr>
                            <td>
                                <table width="75%" align="center">
                                    <tr>
                                        <td><asp:Button ID="cmdPDF" runat="server" Text="Export to PDF" onclick="cmdPDF_Click" CssClass="btn"/></td>
                                        <td><asp:Button ID="cmdPPT" runat="server" Text="Export to PowerPoint" onclick="cmdPPT_Click" CssClass="btn"/></td>
                                        <td><asp:Button ID="cmdBack" runat="server" Text="Return to filter selection" CssClass="btn" onclick="cmdBack_Click" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>   
                <center>
                    <br />
                    <img src="<%=GetSiteRoot() %>/Images/Pure-logo-small.jpg" width="150px"/>
                </center>      
            </div>
            <iframe id="iframe" src="" runat="server" width="0px" height="0px"></iframe>
        </ContentTemplate>
        <Triggers>
               
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="prgLoadingStatus" runat="server" AssociatedUpdatePanelID="pnComp">
        <ProgressTemplate>
            <div class="overlay" />
            <div class="overlayContent">                    
                <img src="<%=GetSiteRoot() %>/Images/processing.gif" alt="Loading" border="1" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    </form>
</body>
</html>
