//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NSS_2015_Report
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReportHistory
    {
        public System.Guid ReportHistoryID { get; set; }
        public string Year2 { get; set; }
        public string Cluster { get; set; }
        public string Y1Division1 { get; set; }
        public string Y1Division2 { get; set; }
        public string Y1Division3 { get; set; }
        public string Y1Division4 { get; set; }
        public string Y1Division5 { get; set; }
        public string Y1Division6 { get; set; }
        public string Y1Branch { get; set; }
        public string Y2Division1 { get; set; }
        public string Y2Division2 { get; set; }
        public string Y2Division3 { get; set; }
        public string Y2Division4 { get; set; }
        public string Y2Division5 { get; set; }
        public string Y2Division6 { get; set; }
        public string Y2Branch { get; set; }
        public string Location { get; set; }
        public string ManualBranchCodes { get; set; }
        public string ManualStaffCodes { get; set; }
        public string ManualFileName { get; set; }
        public string ReportName { get; set; }
        public string Classification { get; set; }
        public string JobFamily { get; set; }
        public string LOS { get; set; }
        public string Region { get; set; }
        public string ManagementPassage { get; set; }
        public string Race { get; set; }
        public string OccupationalLevel { get; set; }
        public string Gender { get; set; }
        public string Composition { get; set; }
        public Nullable<System.DateTime> OnlineCompDate { get; set; }
        public Nullable<System.DateTime> OnlineEEDate { get; set; }
        public Nullable<System.DateTime> OnlineExecDate { get; set; }
        public Nullable<int> OnlineCompCount { get; set; }
        public Nullable<int> OnlineEECount { get; set; }
        public Nullable<int> OnlineExecCount { get; set; }
        public Nullable<System.DateTime> PDFCompDate { get; set; }
        public Nullable<System.DateTime> PDFEEDate { get; set; }
        public Nullable<System.DateTime> PDFExecDate { get; set; }
        public Nullable<int> PDFCompCount { get; set; }
        public Nullable<int> PDFEECount { get; set; }
        public Nullable<int> PDFExecCount { get; set; }
        public Nullable<System.DateTime> PPTCompDate { get; set; }
        public Nullable<System.DateTime> PPTEEDate { get; set; }
        public Nullable<System.DateTime> PPTExecDate { get; set; }
        public Nullable<int> PPTCompCount { get; set; }
        public Nullable<int> PPTEECount { get; set; }
        public Nullable<int> PPTExecCount { get; set; }
        public Nullable<System.DateTime> ExcelDate { get; set; }
        public Nullable<int> ExcelCount { get; set; }
        public Nullable<System.DateTime> InfographDate { get; set; }
        public Nullable<int> InfographCount { get; set; }
        public string PDFCompFilename { get; set; }
        public string PDFEEFilename { get; set; }
        public string PDFExecFilename { get; set; }
        public string PPTCompFilename { get; set; }
        public string PPTEEFilename { get; set; }
        public string PPTExecFilename { get; set; }
        public string ExcelFilename { get; set; }
        public string InfographFilename { get; set; }
        public Nullable<int> CurrentSampleSize { get; set; }
        public Nullable<int> PreviousSampleSize { get; set; }
        public System.Guid UserID { get; set; }
    }
}
