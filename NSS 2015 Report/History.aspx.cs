﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace NSS_2015_Report
{
    public partial class History : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (GetCurrentUser() == null) Response.Redirect("~/");
                else
                {
                    User user = GetCurrentUser();
                    tdNameHeader.Visible = (bool)user.IsAdmin;

                    rptHistory.DataSource = db.ReportGenerations.Where(p => (p.IsVisible == true || p.IsVisible == null ? true : false)).ToList().Where(p => ((bool)user.IsAdmin ? (user.AccessLevel != "All" ? (user.AccessLevel.Split('|').Contains(db.Users.Where(r => r.UserID == p.UserID).SingleOrDefault().AccessLevel) || user.AccessLevel == db.Users.Where(r => r.UserID == p.UserID).SingleOrDefault().AccessLevel) : true) : p.UserID == user.UserID) && (ddlType.SelectedIndex == 0 ? true : p.ReportType == ddlType.SelectedItem.Text)).ToList().Where(p => (fromdate.Text == "" ? true : p.DateDrawn.Date >= Convert.ToDateTime(fromdate.Text).Date) && (todate.Text == "" ? true : p.DateDrawn.Date <= Convert.ToDateTime(todate.Text).Date)).Select(p => new { ReportGenerationID = p.GenerationID, Date = (DateTime)p.DateDrawn, Name = db.Users.Where(r => r.UserID == p.UserID).Select(r => r.Name).SingleOrDefault(), SampleSize = db.ReportHistories.Where(r => r.ReportHistoryID == p.ReportHistoryID).SingleOrDefault().CurrentSampleSize, ReportName = db.ReportHistories.Where(r => r.ReportHistoryID == p.ReportHistoryID).SingleOrDefault().ReportName, ReportType = p.ReportType, OutputType = p.OutputType }).Distinct().ToList().OrderByDescending(p => p.Date);
                    rptHistory.DataBind();
                }
            }
        }

        protected void cmdBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }

        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            User user = GetCurrentUser();
            rptHistory.DataSource = db.ReportGenerations.Where(p => (p.IsVisible == true || p.IsVisible == null ? true : false)).ToList().Where(p => ((bool)user.IsAdmin ? (user.AccessLevel != "All" ? (user.AccessLevel.Split('|').Contains(db.Users.Where(r => r.UserID == p.UserID).SingleOrDefault().AccessLevel) || user.AccessLevel == db.Users.Where(r => r.UserID == p.UserID).SingleOrDefault().AccessLevel) : true) : p.UserID == user.UserID) && (ddlType.SelectedIndex == 0 ? true : p.ReportType == ddlType.SelectedItem.Text)).ToList().Where(p => (fromdate.Text == "" ? true : p.DateDrawn.Date >= Convert.ToDateTime(fromdate.Text).Date) && (todate.Text == "" ? true : p.DateDrawn.Date <= Convert.ToDateTime(todate.Text).Date)).Select(p => new { ReportGenerationID = p.GenerationID, Date = (DateTime)p.DateDrawn, Name = db.Users.Where(r => r.UserID == p.UserID).Select(r => r.Name).SingleOrDefault(), SampleSize = db.ReportHistories.Where(r => r.ReportHistoryID == p.ReportHistoryID).SingleOrDefault().CurrentSampleSize, ReportName = db.ReportHistories.Where(r => r.ReportHistoryID == p.ReportHistoryID).SingleOrDefault().ReportName, ReportType = p.ReportType, OutputType = p.OutputType }).Distinct().ToList().OrderByDescending(p => p.Date);
            rptHistory.DataBind();
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem item = e.Item as RepeaterItem;
            LinkButton cmdExcel = item.FindControl("cmdExcel") as LinkButton;
            HiddenField hdReportGenerationID = item.FindControl("hdReportGenerationID") as HiddenField;
            HtmlTableCell tdName = item.FindControl("tdName") as HtmlTableCell;

            tdName.Visible = (bool)GetCurrentUser().IsAdmin;

            LinkButton cmdRemove = item.FindControl("cmdRemove") as LinkButton;
            AddConfirmRequest(cmdRemove, "Confirm Delete", "Are you sure you wish to remove the history item from the list?");

            ReportGeneration generation = db.ReportGenerations.Where(p => p.GenerationID == new Guid(hdReportGenerationID.Value)).SingleOrDefault();

            cmdExcel.Visible = generation.ReportType == "Comprehensive";
        }

        protected void cmdView_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/history/" + (((sender as LinkButton).Parent as RepeaterItem).FindControl("hdReportGenerationID") as HiddenField).Value);
        }

        protected void cmdOnline_Click(object sender, EventArgs e)
        {
            ReportGeneration generation = db.ReportGenerations.ToList().Where(p => p.GenerationID == new Guid((((sender as LinkButton).Parent as RepeaterItem).FindControl("hdReportGenerationID") as HiddenField).Value)).SingleOrDefault();
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == generation.ReportHistoryID).SingleOrDefault();

            if (generation.ReportType == "Comprehensive")
            {
                if (history.OnlineCompDate == null) history.OnlineCompDate = DateTime.Now;
                history.OnlineCompCount = history.OnlineCompCount == null ? 1 : history.OnlineCompCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "Online";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Comprehensive";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                Response.Redirect("~/comp/" + history.ReportHistoryID);
            }
            else if (generation.ReportType == "Executive Summary")
            {
                if (history.OnlineExecDate == null) history.OnlineExecDate = DateTime.Now;
                history.OnlineExecCount = history.OnlineExecCount == null ? 1 : history.OnlineExecCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "Online";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Executive Summary";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                Response.Redirect("~/exec/" + history.ReportHistoryID);
            }
            else if (generation.ReportType == "EE Barrier")
            {
                if (history.OnlineEEDate == null) history.OnlineEEDate = DateTime.Now;
                history.OnlineEECount = history.OnlineEECount == null ? 1 : history.OnlineEECount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "Online";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "EE Barrier";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                Response.Redirect("~/ee/" + history.ReportHistoryID);
            }
        }

        protected void cmdPDF_Click(object sender, EventArgs e)
        {
            ReportGeneration generation = db.ReportGenerations.ToList().Where(p => p.GenerationID == new Guid((((sender as LinkButton).Parent as RepeaterItem).FindControl("hdReportGenerationID") as HiddenField).Value)).SingleOrDefault();
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == generation.ReportHistoryID).SingleOrDefault();

            string filename = "";

            if (generation.ReportType == "Comprehensive")
            {
                if (history.PDFCompDate == null) history.PDFCompDate = DateTime.Now;
                history.PDFCompCount = history.PDFCompCount == null ? 1 : history.PDFCompCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PDF";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Comprehensive";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PDFCompFilename == null || history.ManualFileName != null)
                {
                    RenderPDF renderPDF = new RenderPDF();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                    renderPDF.GeneratePDF(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/comppdf/" + history.ReportHistoryID);
                    history.PDFCompFilename = filename;
                    db.SaveChanges();

                    renderPDF = null;
                }
                else
                    filename = history.PDFCompFilename;
            }
            else if (generation.ReportType == "Executive Summary")
            {
                if (history.PDFExecDate == null) history.PDFExecDate = DateTime.Now;
                history.PDFExecCount = history.PDFExecCount == null ? 1 : history.PDFExecCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PDF";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Executive Summary";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PDFExecFilename == null || history.ManualFileName != null)
                {
                    RenderPDF renderPDF = new RenderPDF();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                    renderPDF.GeneratePDF(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/execpdf/" + history.ReportHistoryID);
                    history.PDFExecFilename = filename;
                    db.SaveChanges();

                    renderPDF = null;
                }
                else
                    filename = history.PDFExecFilename;
            }
            else if (generation.ReportType == "EE Barrier")
            {
                if (history.PDFEEDate == null) history.PDFEEDate = DateTime.Now;
                history.PDFEECount = history.PDFEECount == null ? 1 : history.PDFEECount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PDF";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "EE Barrier";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PDFEEFilename == null || history.ManualFileName != null)
                {
                    RenderPDF renderPDF = new RenderPDF();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                    renderPDF.GeneratePDF(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/eepdf/" + history.ReportHistoryID);
                    history.PDFEEFilename = filename;
                    db.SaveChanges();

                    renderPDF = null;
                }
                else
                    filename = history.PDFEEFilename;
            }

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=pdf&File=" + filename);

            GC.Collect();

            User user = GetCurrentUser();

            rptHistory.DataSource = db.ReportGenerations.Where(p => (p.IsVisible == true || p.IsVisible == null ? true : false)).ToList().Where(p => ((bool)user.IsAdmin ? (user.AccessLevel != "All" ? (user.AccessLevel.Split('|').Contains(db.Users.Where(r => r.UserID == p.UserID).SingleOrDefault().AccessLevel) || user.AccessLevel == db.Users.Where(r => r.UserID == p.UserID).SingleOrDefault().AccessLevel) : true) : p.UserID == user.UserID) && (ddlType.SelectedIndex == 0 ? true : p.ReportType == ddlType.SelectedItem.Text)).ToList().Where(p => (fromdate.Text == "" ? true : p.DateDrawn.Date >= Convert.ToDateTime(fromdate.Text).Date) && (todate.Text == "" ? true : p.DateDrawn.Date <= Convert.ToDateTime(todate.Text).Date)).Select(p => new { ReportGenerationID = p.GenerationID, Date = (DateTime)p.DateDrawn, Name = db.Users.Where(r => r.UserID == p.UserID).Select(r => r.Name).SingleOrDefault(), SampleSize = db.ReportHistories.Where(r => r.ReportHistoryID == p.ReportHistoryID).SingleOrDefault().CurrentSampleSize, ReportName = db.ReportHistories.Where(r => r.ReportHistoryID == p.ReportHistoryID).SingleOrDefault().ReportName, ReportType = p.ReportType, OutputType = p.OutputType }).Distinct().ToList().OrderByDescending(p => p.Date);
            rptHistory.DataBind();
        }

        protected void cmdPPT_Click(object sender, EventArgs e)
        {
            ReportGeneration generation = db.ReportGenerations.ToList().Where(p => p.GenerationID == new Guid((((sender as LinkButton).Parent as RepeaterItem).FindControl("hdReportGenerationID") as HiddenField).Value)).SingleOrDefault();
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == generation.ReportHistoryID).SingleOrDefault();

            string filename = "";

            if (generation.ReportType == "Comprehensive")
            {
                if (history.PPTCompDate == null) history.PPTCompDate = DateTime.Now;
                history.PPTCompCount = history.PPTCompCount == null ? 1 : history.PPTCompCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PPT";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Comprehensive";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PPTCompFilename == null || history.ManualFileName != null)
                {
                    RenderPPT renderPPT = new RenderPPT();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".ppt";

                    renderPPT.GeneratePPT(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/compppt/" + history.ReportHistoryID);
                    history.PPTCompFilename = filename;
                    db.SaveChanges();

                    renderPPT = null;
                }
                else
                    filename = history.PPTCompFilename;
            }
            else if (generation.ReportType == "Executive Summary")
            {
                if (history.PPTExecDate == null) history.PPTExecDate = DateTime.Now;
                history.PPTExecCount = history.PPTExecCount == null ? 1 : history.PPTExecCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PPT";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Executive Summary";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PPTExecFilename == null || history.ManualFileName != null)
                {
                    RenderPPT renderPPT = new RenderPPT();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".ppt";

                    renderPPT.GeneratePPT(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/execppt/" + history.ReportHistoryID);
                    history.PPTExecFilename = filename;
                    db.SaveChanges();

                    renderPPT = null;
                }
                else
                    filename = history.PPTExecFilename;
            }
            else if (generation.ReportType == "EE Barrier")
            {
                if (history.PPTEEDate == null) history.PPTEEDate = DateTime.Now;
                history.PPTEECount = history.PPTEECount == null ? 1 : history.PPTEECount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PPT";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "EE Barrier";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PPTEEFilename == null || history.ManualFileName != null)
                {
                    RenderPPT renderPPT = new RenderPPT();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".ppt";

                    renderPPT.GeneratePPT(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/eeppt/" + history.ReportHistoryID);
                    history.PPTEEFilename = filename;
                    db.SaveChanges();

                    renderPPT = null;
                }
                else
                    filename = history.PPTEEFilename;
            }

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=ppt&File=" + filename);

            GC.Collect();

            User user = GetCurrentUser();

            rptHistory.DataSource = db.ReportGenerations.Where(p => (p.IsVisible == true || p.IsVisible == null ? true : false)).ToList().Where(p => ((bool)user.IsAdmin ? (user.AccessLevel != "All" ? (user.AccessLevel.Split('|').Contains(db.Users.Where(r => r.UserID == p.UserID).SingleOrDefault().AccessLevel) || user.AccessLevel == db.Users.Where(r => r.UserID == p.UserID).SingleOrDefault().AccessLevel) : true) : p.UserID == user.UserID) && (ddlType.SelectedIndex == 0 ? true : p.ReportType == ddlType.SelectedItem.Text)).ToList().Where(p => (fromdate.Text == "" ? true : p.DateDrawn.Date >= Convert.ToDateTime(fromdate.Text).Date) && (todate.Text == "" ? true : p.DateDrawn.Date <= Convert.ToDateTime(todate.Text).Date)).Select(p => new { ReportGenerationID = p.GenerationID, Date = (DateTime)p.DateDrawn, Name = db.Users.Where(r => r.UserID == p.UserID).Select(r => r.Name).SingleOrDefault(), SampleSize = db.ReportHistories.Where(r => r.ReportHistoryID == p.ReportHistoryID).SingleOrDefault().CurrentSampleSize, ReportName = db.ReportHistories.Where(r => r.ReportHistoryID == p.ReportHistoryID).SingleOrDefault().ReportName, ReportType = p.ReportType, OutputType = p.OutputType }).Distinct().ToList().OrderByDescending(p => p.Date);
            rptHistory.DataBind();
        }

        protected void cmdExcel_Click(object sender, EventArgs e)
        {
            string filename = "";

            ReportGeneration generation = db.ReportGenerations.ToList().Where(p => p.GenerationID == new Guid((((sender as LinkButton).Parent as RepeaterItem).FindControl("hdReportGenerationID") as HiddenField).Value)).SingleOrDefault();
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == generation.ReportHistoryID).SingleOrDefault();

            if (history.ExcelDate == null) history.ExcelDate = DateTime.Now;
            history.ExcelCount = history.ExcelCount == null ? 1 : history.ExcelCount++;

            ReportGeneration gen = new ReportGeneration();
            gen.DateDrawn = DateTime.Now;
            gen.GenerationID = Guid.NewGuid();
            gen.OutputType = "Excel";
            gen.ReportHistoryID = history.ReportHistoryID;
            gen.ReportType = "Comprehensive";
            gen.UserID = UserID;
            db.ReportGenerations.Add(gen);
            db.SaveChanges();

            //if (history.ExcelFilename == null)
            //{
                RenderExcel excel = new RenderExcel();
                filename = excel.Generate(new Guid(RouteData.Values["ReportHistoryID"].ToString()), Server.MapPath("~/Exports"));
                history.ExcelFilename = filename;
                db.SaveChanges();
                excel = null;
            //}
            //else
            //    filename = history.ExcelFilename;

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=excel&File=" + filename);

            User user = GetCurrentUser();

            rptHistory.DataSource = db.ReportGenerations.Where(p => (p.IsVisible == true || p.IsVisible == null ? true : false) && ((bool)user.IsAdmin ? (user.AccessLevel != "All" ? (user.AccessLevel.Split('|').Contains(db.Users.Where(r => r.UserID == p.UserID).SingleOrDefault().AccessLevel) || user.AccessLevel == db.Users.Where(r => r.UserID == p.UserID).SingleOrDefault().AccessLevel) : true) : p.UserID == user.UserID) && (ddlType.SelectedIndex == 0 ? true : p.ReportType == ddlType.SelectedItem.Text)).ToList().Where(p => (fromdate.Text == "" ? true : p.DateDrawn.Date >= Convert.ToDateTime(fromdate.Text).Date) && (todate.Text == "" ? true : p.DateDrawn.Date <= Convert.ToDateTime(todate.Text).Date)).Select(p => new { ReportGenerationID = p.GenerationID, Date = (DateTime)p.DateDrawn, Name = db.Users.Where(r => r.UserID == p.UserID).Select(r => r.Name).SingleOrDefault(), SampleSize = db.ReportHistories.Where(r => r.ReportHistoryID == p.ReportHistoryID).SingleOrDefault().CurrentSampleSize, ReportName = db.ReportHistories.Where(r => r.ReportHistoryID == p.ReportHistoryID).SingleOrDefault().ReportName, ReportType = p.ReportType, OutputType = p.OutputType }).Distinct().ToList().OrderByDescending(p => p.Date);
            rptHistory.DataBind();
        }

        protected void cmdRemove_Click(object sender, EventArgs e)
        {
            ReportGeneration generation = db.ReportGenerations.ToList().Where(p => p.GenerationID == new Guid((((sender as LinkButton).Parent as RepeaterItem).FindControl("hdReportGenerationID") as HiddenField).Value)).SingleOrDefault();

            generation.IsVisible = false;
            db.SaveChanges();

            User user = GetCurrentUser();

            rptHistory.DataSource = db.ReportGenerations.Where(p => (p.IsVisible == true || p.IsVisible == null ? true : false)).ToList().Where(p => ((bool)user.IsAdmin ? (user.AccessLevel != "All" ? (user.AccessLevel.Split('|').Contains(db.Users.Where(r => r.UserID == p.UserID).SingleOrDefault().AccessLevel) || user.AccessLevel == db.Users.Where(r => r.UserID == p.UserID).SingleOrDefault().AccessLevel) : true) : p.UserID == user.UserID) && (ddlType.SelectedIndex == 0 ? true : p.ReportType == ddlType.SelectedItem.Text)).ToList().Where(p => (fromdate.Text == "" ? true : p.DateDrawn.Date >= Convert.ToDateTime(fromdate.Text).Date) && (todate.Text == "" ? true : p.DateDrawn.Date <= Convert.ToDateTime(todate.Text).Date)).Select(p => new { ReportGenerationID = p.GenerationID, Date = (DateTime)p.DateDrawn, Name = db.Users.Where(r => r.UserID == p.UserID).Select(r => r.Name).SingleOrDefault(), SampleSize = db.ReportHistories.Where(r => r.ReportHistoryID == p.ReportHistoryID).SingleOrDefault().CurrentSampleSize, ReportName = db.ReportHistories.Where(r => r.ReportHistoryID == p.ReportHistoryID).SingleOrDefault().ReportName, ReportType = p.ReportType, OutputType = p.OutputType }).Distinct().ToList().OrderByDescending(p => p.Date);
            rptHistory.DataBind();
        }

        protected void AddConfirmRequest(WebControl control, string title, string message)
        {
            string postBackReference = Page.ClientScript.GetPostBackEventReference(control, String.Empty);
            string function = String.Format("javascript:showConfirmRequest(function() {{ {0} }}, '{1}', '{2}'); return false;", postBackReference, title, message);
            control.Attributes.Add("onclick", function);
        }
    }
}