﻿using HiQPdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NSS_2015_Report
{
    public partial class _Default : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (GetCurrentUser() != null)
                {
                    ddlYear2.DataSource = new List<string>() { "2014", "2013", "2012", "2011", "2010" };
                    ddlYear2.DataBind();
                    ddlYear2.SelectedIndex = 0;

                    rbReportType.SelectedIndex = 0;

                    txtReportName.Text = "Nedbank Group Overall Report";

                    rbType.SelectedIndex = 1;

                    hdFileName.Value = hdSheet.Value = hdColumn.Value = "";
                    AutoUpload.Visible = false;

                    ddlCluster.DataSource = GetCurrentUser().AccessLevel == "All" ? db.Staff2015.Select(p => (p.Cluster ?? "Not Specified").ToUpper()).Distinct().ToList().OrderBy(p => p) : GetCurrentUser().AccessLevel.ToUpper().Split('|').Distinct().ToList().OrderBy(p => p);                    
                    ddlCluster.DataBind();
                    if (ddlCluster.Items.Count > 1) ddlCluster.Items.Insert(0, new ListItem("All"));
                    ddlCluster.SelectedIndex = 0;

                    lblYear1Division1.Text = lblYear1Division2.Text = lblYear1Division3.Text = lblYear1Division4.Text = lblYear1Division5.Text = lblYear1Division6.Text = lblYear1Branch.Text = lblYear2Division1.Text = lblYear2Division2.Text = lblYear2Division3.Text = lblYear2Division4.Text = lblYear2Division5.Text = lblYear2Division6.Text = lblYear2Branch.Text = lblLocation.Text = "All";

                    ddlClassification.Items.Clear();
                    ddlJobFamily.Items.Clear();
                    ddlLOS.Items.Clear();
                    ddlRegion.Items.Clear();
                    ddlPassage.Items.Clear();
                    ddlRace.Items.Clear();
                    ddlOccupationalLevel.Items.Clear();
                    ddlGender.Items.Clear();

                    cblComposition.Items[0].Selected = cblComposition.Items[1].Selected = cblComposition.Items[2].Selected = cblComposition.Items[4].Selected = cblComposition.Items[5].Selected = cblComposition.Items[9].Selected = cblComposition.Items[10].Selected = true;
                    lblYear2.Text = ddlYear2.SelectedValue;

                    Filters();
                }
            }
        }

        private void Filters()
        {
            this.iframe.Attributes.Add("src", "");

            bool proceed = !(GetCurrentUser().RestrictAccess ?? false);

            string sampletext = "Post Sample";
            txtReportName.Text = "Nedbank Group Overall Report";

            lblYear2.Text = ddlYear2.SelectedValue + (ddlCluster.SelectedValue != "RBB" && ddlCluster.SelectedValue != "CIB" ? "" : "<br/><i>(Due to the change in structure for RBB and CIB this year the group’s Division 1 options in 2014 will be shown for these clusters)</i>");

            cmdInfograph.Visible = (ddlYear2.SelectedItem.Text != "None" && ddlYear2.SelectedItem.Text != "2010" && rbReportType.SelectedIndex == 0);

            Year1Label.Visible = tdYear1Division1.Visible = tdYear1Division2.Visible = tdYear1Division3.Visible = tdYear1Division4.Visible = tdYear1Division5.Visible = tdYear1Division6.Visible = tdYear1Branch.Visible = true;

            List<Year> CurrentSelected1 = year.YearDataSet("2015");
            List<Year> CurrentSelected2 = year.YearDataSet(ddlYear2.SelectedValue);

            hdTotal1.Value = CurrentSelected1.Count.ToString();
            hdTotal2.Value = CurrentSelected2.Count.ToString();

            if (rbReportType.SelectedIndex == 2)
            {
                sampletext = "Sample";
                CurrentSelected1 = CurrentSelected1.Where(p => p.Location.ToLower().Trim() == "south africa").Distinct().ToList();
                CurrentSelected2 = CurrentSelected2.Where(p => p.Location.ToLower().Trim() == "south africa").Distinct().ToList();
            }

            AutoUpload.Visible = ddlCluster.SelectedIndex != 0 || ddlCluster.Items.Count == 1;

            if (ddlCluster.SelectedIndex == 0 && ddlCluster.SelectedValue == "All")
            {
                cmdYear1Division1.Visible = cmdYear1Division2.Visible = cmdYear1Division3.Visible = cmdYear1Division4.Visible = cmdYear1Division5.Visible = cmdYear1Division6.Visible = cmdYear1Branch.Visible = cmdYear2Division1.Visible = cmdYear2Division2.Visible = cmdYear2Division3.Visible = cmdYear2Division4.Visible = cmdYear2Division5.Visible = cmdYear2Division6.Visible = cmdYear2Branch.Visible = cmdLocation.Visible = cmdImport.Visible = false;
                lblYear1Division1.Text = lblYear1Division2.Text = lblYear1Division3.Text = lblYear1Division4.Text = lblYear1Division5.Text = lblYear1Division6.Text = lblYear1Branch.Text = lblYear2Division1.Text = lblYear2Division2.Text = lblYear2Division3.Text = lblYear2Division4.Text = lblYear2Division5.Text = lblYear2Division6.Text = lblYear2Branch.Text = lblLocation.Text = "All";
            }
            else
            {
                CurrentSelected1 = CurrentSelected1.Where(p => p.Cluster.ToLower().Trim() == ddlCluster.SelectedItem.Text.ToLower().Trim()).Distinct().ToList();
                if (ddlCluster.SelectedValue != "RBB" && ddlCluster.SelectedValue != "CIB") CurrentSelected2 = CurrentSelected2.Where(p => p.Cluster.ToLower().Trim() == ddlCluster.SelectedItem.Text.ToLower().Trim()).Distinct().ToList();
                cmdYear1Division1.Visible = cmdYear1Division2.Visible = cmdYear1Division3.Visible = cmdYear1Division4.Visible = cmdYear1Division5.Visible = cmdYear1Division6.Visible = cmdYear1Branch.Visible = cmdYear2Division1.Visible = cmdYear2Division2.Visible = cmdYear2Division3.Visible = cmdYear2Division4.Visible = cmdYear2Division5.Visible = cmdYear2Division6.Visible = cmdYear2Branch.Visible = cmdLocation.Visible = cmdImport.Visible = true;

                sampletext = "Sample";
                txtReportName.Text = ddlCluster.SelectedItem.Text;

                if (chkYear1Division1.Items.Count == 0)
                {
                    lblYear1Division1.Text = "All";

                    chkYear1Division1.Items.Clear();
                    chkYear1Division1.DataSource = CurrentSelected1.Select(p => p.Division1).Distinct().ToList().OrderBy(p => p);
                    chkYear1Division1.DataBind();

                    chkY1D1All.Checked = true;

                    foreach (ListItem item in chkYear1Division1.Items) item.Selected = true;
                }
                else
                {
                    if (chkY1D1All.Checked)
                        lblYear1Division1.Text = "All";
                    else
                    {
                        List<string> Division1s = new List<string>();
                        foreach (ListItem item in chkYear1Division1.Items)
                            if (item.Selected) Division1s.Add(item.Text);

                        CurrentSelected1 = CurrentSelected1.Join(Division1s, sel1 => sel1.Division1, d1 => d1, (sel1, d1) => new { sel1 }).Select(p => p.sel1).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear1Division2.Items.Count == 0)
                {
                    lblYear1Division2.Text = "All";

                    chkYear1Division2.Items.Clear();
                    chkYear1Division2.DataSource = CurrentSelected1.Select(p => p.Division2).Distinct().ToList().OrderBy(p => p);
                    chkYear1Division2.DataBind();

                    chkY1D2All.Checked = true;

                    foreach (ListItem item in chkYear1Division2.Items) item.Selected = true;
                }
                else
                {
                    if (chkY1D2All.Checked)
                        lblYear1Division2.Text = "All";
                    else
                    {
                        List<string> Division2s = new List<string>();
                        foreach (ListItem item in chkYear1Division2.Items)
                            if (item.Selected) Division2s.Add(item.Text);

                        CurrentSelected1 = CurrentSelected1.Join(Division2s, sel1 => sel1.Division2, d2 => d2, (sel1, d2) => new { sel1 }).Select(p => p.sel1).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear1Division3.Items.Count == 0)
                {
                    lblYear1Division3.Text = "All";

                    chkYear1Division3.Items.Clear();
                    chkYear1Division3.DataSource = CurrentSelected1.Select(p => p.Division3).Distinct().ToList().OrderBy(p => p);
                    chkYear1Division3.DataBind();

                    chkY1D3All.Checked = true;

                    foreach (ListItem item in chkYear1Division3.Items) item.Selected = true;
                }
                else
                {
                    if (chkY1D3All.Checked)
                        lblYear1Division3.Text = "All";
                    else
                    {
                        List<string> Division3s = new List<string>();
                        foreach (ListItem item in chkYear1Division3.Items)
                            if (item.Selected) Division3s.Add(item.Text);

                        CurrentSelected1 = CurrentSelected1.Join(Division3s, sel1 => sel1.Division3, d3 => d3, (sel1, d3) => new { sel1 }).Select(p => p.sel1).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear1Division4.Items.Count == 0)
                {
                    lblYear1Division4.Text = "All";

                    chkYear1Division4.Items.Clear();
                    chkYear1Division4.DataSource = CurrentSelected1.Select(p => p.Division4).Distinct().ToList().OrderBy(p => p);
                    chkYear1Division4.DataBind();

                    chkY1D4All.Checked = true;

                    foreach (ListItem item in chkYear1Division4.Items) item.Selected = true;
                }
                else
                {
                    if (chkY1D4All.Checked)
                        lblYear1Division4.Text = "All";
                    else
                    {
                        List<string> Division4s = new List<string>();
                        foreach (ListItem item in chkYear1Division4.Items)
                            if (item.Selected) Division4s.Add(item.Text);

                        CurrentSelected1 = CurrentSelected1.Join(Division4s, sel1 => sel1.Division4, d4 => d4, (sel1, d4) => new { sel1 }).Select(p => p.sel1).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear1Division5.Items.Count == 0)
                {
                    lblYear1Division5.Text = "All";

                    chkYear1Division5.Items.Clear();
                    chkYear1Division5.DataSource = CurrentSelected1.Select(p => p.Division5).Distinct().ToList().OrderBy(p => p);
                    chkYear1Division5.DataBind();

                    chkY1D5All.Checked = true;

                    foreach (ListItem item in chkYear1Division5.Items) item.Selected = true;
                }
                else
                {
                    if (chkY1D5All.Checked)
                        lblYear1Division5.Text = "All";
                    else
                    {
                        List<string> Division5s = new List<string>();
                        foreach (ListItem item in chkYear1Division5.Items)
                            if (item.Selected) Division5s.Add(item.Text);

                        CurrentSelected1 = CurrentSelected1.Join(Division5s, sel1 => sel1.Division5, d5 => d5, (sel1, d5) => new { sel1 }).Select(p => p.sel1).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear1Division6.Items.Count == 0)
                {
                    lblYear1Division6.Text = "All";

                    chkYear1Division6.Items.Clear();
                    chkYear1Division6.DataSource = CurrentSelected1.Select(p => p.Division6).Distinct().ToList().OrderBy(p => p);
                    chkYear1Division6.DataBind();

                    chkY1D6All.Checked = true;

                    foreach (ListItem item in chkYear1Division6.Items) item.Selected = true;
                }
                else
                {
                    if (chkY1D6All.Checked)
                        lblYear1Division6.Text = "All";
                    else
                    {
                        List<string> Division6s = new List<string>();
                        foreach (ListItem item in chkYear1Division6.Items)
                            if (item.Selected) Division6s.Add(item.Text);

                        CurrentSelected1 = CurrentSelected1.Join(Division6s, sel1 => sel1.Division6, d6 => d6, (sel1, d6) => new { sel1 }).Select(p => p.sel1).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear1Branch.Items.Count == 0)
                {
                    lblYear1Branch.Text = "All";

                    chkYear1Branch.Items.Clear();
                    chkYear1Branch.DataSource = CurrentSelected1.Select(p => p.Branch).Distinct().ToList().OrderBy(p => p);
                    chkYear1Branch.DataBind();

                    chkY1BAll.Checked = true;

                    foreach (ListItem item in chkYear1Branch.Items) item.Selected = true;
                }
                else
                {
                    if (chkY1BAll.Checked)
                        lblYear1Branch.Text = "All";
                    else
                    {
                        List<string> Branchs = new List<string>();
                        foreach (ListItem item in chkYear1Branch.Items)
                            if (item.Selected) Branchs.Add(item.Text);

                        CurrentSelected1 = CurrentSelected1.Join(Branchs, sel1 => sel1.Branch, b => b, (sel1, b) => new { sel1 }).Select(p => p.sel1).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear2Division1.Items.Count == 0)
                {
                    lblYear2Division1.Text = "All";

                    chkYear2Division1.Items.Clear();
                    chkYear2Division1.DataSource = CurrentSelected2.Select(p => p.Division1).Distinct().ToList().OrderBy(p => p);
                    chkYear2Division1.DataBind();

                    chkY2D1All.Checked = true;

                    foreach (ListItem item in chkYear2Division1.Items) item.Selected = true;
                }
                else
                {
                    if (chkY2D1All.Checked)
                        lblYear2Division1.Text = "All";
                    else
                    {
                        List<string> Division1s = new List<string>();
                        foreach (ListItem item in chkYear2Division1.Items)
                            if (item.Selected) Division1s.Add(item.Text);

                        CurrentSelected2 = CurrentSelected2.Join(Division1s, sel2 => sel2.Division1, d1 => d1, (sel2, d1) => new { sel2 }).Select(p => p.sel2).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear2Division2.Items.Count == 0)
                {
                    lblYear2Division2.Text = "All";

                    chkYear2Division2.Items.Clear();
                    chkYear2Division2.DataSource = CurrentSelected2.Select(p => p.Division2).Distinct().ToList().OrderBy(p => p);
                    chkYear2Division2.DataBind();

                    chkY2D2All.Checked = true;

                    foreach (ListItem item in chkYear2Division2.Items) item.Selected = true;
                }
                else
                {
                    if (chkY2D2All.Checked)
                        lblYear2Division2.Text = "All";
                    else
                    {
                        List<string> Division2s = new List<string>();
                        foreach (ListItem item in chkYear2Division2.Items)
                            if (item.Selected) Division2s.Add(item.Text);

                        CurrentSelected2 = CurrentSelected2.Join(Division2s, sel2 => sel2.Division2, d2 => d2, (sel2, d2) => new { sel2 }).Select(p => p.sel2).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear2Division3.Items.Count == 0)
                {
                    lblYear2Division3.Text = "All";

                    chkYear2Division3.Items.Clear();
                    chkYear2Division3.DataSource = CurrentSelected2.Select(p => p.Division3).Distinct().ToList().OrderBy(p => p);
                    chkYear2Division3.DataBind();

                    chkY2D3All.Checked = true;

                    foreach (ListItem item in chkYear2Division3.Items) item.Selected = true;
                }
                else
                {
                    if (chkY2D3All.Checked)
                        lblYear2Division3.Text = "All";
                    else
                    {
                        List<string> Division3s = new List<string>();
                        foreach (ListItem item in chkYear2Division3.Items)
                            if (item.Selected) Division3s.Add(item.Text);

                        CurrentSelected2 = CurrentSelected2.Join(Division3s, sel2 => sel2.Division3, d3 => d3, (sel2, d3) => new { sel2 }).Select(p => p.sel2).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear2Division4.Items.Count == 0)
                {
                    lblYear2Division4.Text = "All";

                    chkYear2Division4.Items.Clear();
                    chkYear2Division4.DataSource = CurrentSelected2.Select(p => p.Division4).Distinct().ToList().OrderBy(p => p);
                    chkYear2Division4.DataBind();

                    chkY2D4All.Checked = true;

                    foreach (ListItem item in chkYear2Division4.Items) item.Selected = true;
                }
                else
                {
                    if (chkY2D4All.Checked)
                        lblYear2Division4.Text = "All";
                    else
                    {
                        List<string> Division4s = new List<string>();
                        foreach (ListItem item in chkYear2Division4.Items)
                            if (item.Selected) Division4s.Add(item.Text);

                        CurrentSelected2 = CurrentSelected2.Join(Division4s, sel2 => sel2.Division4, d4 => d4, (sel2, d4) => new { sel2 }).Select(p => p.sel2).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear2Division5.Items.Count == 0)
                {
                    lblYear2Division5.Text = "All";

                    chkYear2Division5.Items.Clear();
                    chkYear2Division5.DataSource = CurrentSelected2.Select(p => p.Division5).Distinct().ToList().OrderBy(p => p);
                    chkYear2Division5.DataBind();

                    chkY2D5All.Checked = true;

                    foreach (ListItem item in chkYear2Division5.Items) item.Selected = true;
                }
                else
                {
                    if (chkY2D5All.Checked)
                        lblYear2Division5.Text = "All";
                    else
                    {
                        List<string> Division5s = new List<string>();
                        foreach (ListItem item in chkYear2Division5.Items)
                            if (item.Selected) Division5s.Add(item.Text);

                        CurrentSelected2 = CurrentSelected2.Join(Division5s, sel2 => sel2.Division5, d5 => d5, (sel2, d5) => new { sel2 }).Select(p => p.sel2).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear2Division6.Items.Count == 0)
                {
                    lblYear2Division6.Text = "All";

                    chkYear2Division6.Items.Clear();
                    chkYear2Division6.DataSource = CurrentSelected2.Select(p => p.Division6).Distinct().ToList().OrderBy(p => p);
                    chkYear2Division6.DataBind();

                    chkY2D6All.Checked = true;

                    foreach (ListItem item in chkYear2Division6.Items) item.Selected = true;
                }
                else
                {
                    if (chkY2D6All.Checked)
                        lblYear2Division6.Text = "All";
                    else
                    {
                        List<string> Division6s = new List<string>();
                        foreach (ListItem item in chkYear2Division6.Items)
                            if (item.Selected) Division6s.Add(item.Text);

                        CurrentSelected2 = CurrentSelected2.Join(Division6s, sel2 => sel2.Division6, d6 => d6, (sel2, d6) => new { sel2 }).Select(p => p.sel2).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }

                if (chkYear2Branch.Items.Count == 0)
                {
                    lblYear2Branch.Text = "All";

                    chkYear2Branch.Items.Clear();
                    chkYear2Branch.DataSource = CurrentSelected2.Select(p => p.Branch).Distinct().ToList().OrderBy(p => p);
                    chkYear2Branch.DataBind();

                    chkY2BAll.Checked = true;

                    foreach (ListItem item in chkYear2Branch.Items) item.Selected = true;
                }
                else
                {
                    if (chkY2BAll.Checked)
                        lblYear2Branch.Text = "All";
                    else
                    {
                        List<string> Branchs = new List<string>();
                        foreach (ListItem item in chkYear2Branch.Items)
                            if (item.Selected) Branchs.Add(item.Text);

                        CurrentSelected2 = CurrentSelected2.Join(Branchs, sel2 => sel2.Branch, b => b, (sel2, b) => new { sel2 }).Select(p => p.sel2).Distinct().ToList();

                        sampletext = "Sample";
                        if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                        if (!proceed) proceed = true;
                    }
                }
            }

            if (chkLocation.Items.Count == 0)
            {
                lblLocation.Text = "All";

                chkLocation.Items.Clear();
                chkLocation.DataSource = CurrentSelected1.Select(p => p.Location).Distinct().ToList().OrderBy(p => p);
                chkLocation.DataBind();

                chkLocationAll.Checked = true;

                foreach (ListItem item in chkLocation.Items) item.Selected = true;
            }
            else
            {
                if (chkLocationAll.Checked)
                    lblLocation.Text = "All";
                else
                {
                    List<string> Locations = new List<string>();
                    foreach (ListItem item in chkLocation.Items)
                        if (item.Selected) Locations.Add(item.Text);

                    CurrentSelected1 = CurrentSelected1.Join(Locations, sel1 => sel1.Location, l => l, (sel1, l) => new { sel1 }).Select(p => p.sel1).Distinct().ToList();
                    CurrentSelected2 = CurrentSelected2.Join(Locations, sel2 => sel2.Location, l => l, (sel2, l) => new { sel2 }).Select(p => p.sel2).Distinct().ToList();

                    sampletext = "Sample";
                    if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                    if (!proceed) proceed = true;
                }
            }

            if (hdFileName.Value != "" && hdSheet.Value != "" && hdColumn.Value != "")
            {
                string conStr = Path.GetExtension(hdFileName.Value) == ".xlsx" ? "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=Yes'" : "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=Yes;'";
                conStr = String.Format(conStr, HostingEnvironment.MapPath("~/Imports/") + hdFileName.Value);
                OleDbConnection connExcel = new OleDbConnection(conStr);
                OleDbCommand Excel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                DataTable dt = new DataTable();
                Excel.Connection = connExcel;

                connExcel.Open();
                Excel.CommandText = "SELECT * From [" + hdSheet.Value + "]";
                oda.SelectCommand = Excel;
                oda.Fill(dt);
                connExcel.Close();
                GC.Collect();

                List<DataRow> rows = dt.Rows.Cast<DataRow>().ToList();

                List<string> Values = new List<string>();

                foreach (DataRow row in rows)
                    Values.Add(row[hdColumn.Value].ToString());

                GC.Collect();

                lblImportResult.Text = rows.Count().ToString() + " records imported";

                CurrentSelected1 = CurrentSelected1.Join(Values, sel1 => rbType.SelectedIndex == 0 ? sel1.Branch : sel1.StaffCode, v => v, (sel1, v) => new { sel1 }).Select(p => p.sel1).Distinct().ToList();
                CurrentSelected2 = CurrentSelected2.Join(Values, sel2 => rbType.SelectedIndex == 0 ? sel2.Branch : sel2.StaffCode, v => v, (sel2, v) => new { sel2 }).Select(p => p.sel2).Distinct().ToList();
                if (!proceed) proceed = true;
            }

            if (ddlClassification.Items.Count == 0)
            {
                ddlClassification.DataSource = CurrentSelected1.Select(p => p.Classification.Trim()).Distinct().ToList().OrderBy(p => p);
                ddlClassification.DataBind();
                ddlClassification.Items.Insert(0, new ListItem("All"));
                ddlClassification.SelectedIndex = 0;
            }
            else
            {                
                if (ddlClassification.SelectedIndex != 0)
                {
                    CurrentSelected1 = CurrentSelected1.Where(p => p.Classification.Trim().ToLower() == ddlClassification.SelectedValue.Trim().ToLower()).Distinct().ToList();
                    CurrentSelected2 = CurrentSelected2.Where(p => p.Classification.Trim().ToLower() == ddlClassification.SelectedValue.Trim().ToLower()).Distinct().ToList();

                    sampletext = "Sample";
                    if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                    if (!proceed) proceed = true;
                }
            }

            if (ddlJobFamily.Items.Count == 0)
            {
                ddlJobFamily.DataSource = CurrentSelected1.Select(p => p.JobFamily.Trim()).Distinct().ToList().OrderBy(p => p);
                ddlJobFamily.DataBind();
                ddlJobFamily.Items.Insert(0, new ListItem("All"));
                ddlJobFamily.SelectedIndex = 0;
            }
            else
            {                
                if (ddlJobFamily.SelectedIndex != 0)
                {
                    CurrentSelected1 = CurrentSelected1.Where(p => p.JobFamily.Trim().ToLower() == ddlJobFamily.SelectedValue.Trim().ToLower()).Distinct().ToList();
                    CurrentSelected2 = CurrentSelected2.Where(p => p.JobFamily.Trim().ToLower() == ddlJobFamily.SelectedValue.Trim().ToLower()).Distinct().ToList();

                    sampletext = "Sample";
                    if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                    if (!proceed) proceed = true;
                }
            }

            if (ddlLOS.Items.Count == 0)
            {
                ddlLOS.DataSource = CurrentSelected1.Select(p => p.LOS.Trim()).Distinct().ToList().OrderBy(p => p);
                ddlLOS.DataBind();
                ddlLOS.Items.Insert(0, new ListItem("All"));
                ddlLOS.SelectedIndex = 0;
            }
            else
            {                
                if (ddlLOS.SelectedIndex != 0)
                {
                    CurrentSelected1 = CurrentSelected1.Where(p => p.LOS.Trim().ToLower() == ddlLOS.SelectedValue.Trim().ToLower()).Distinct().ToList();
                    CurrentSelected2 = CurrentSelected2.Where(p => p.LOS.Trim().ToLower() == ddlLOS.SelectedValue.Trim().ToLower()).Distinct().ToList();

                    sampletext = "Sample";
                    if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                    if (!proceed) proceed = true;
                }
            }

            if (ddlRegion.Items.Count == 0)
            {
                ddlRegion.DataSource = CurrentSelected1.Select(p => p.Region.Trim()).Distinct().ToList().OrderBy(p => p);
                ddlRegion.DataBind();
                ddlRegion.Items.Insert(0, new ListItem("All"));
                ddlRegion.SelectedIndex = 0;
            }
            else
            {                
                if (ddlRegion.SelectedIndex != 0)
                {
                    CurrentSelected1 = CurrentSelected1.Where(p => p.Region.Trim().ToLower() == ddlRegion.SelectedValue.Trim().ToLower()).Distinct().ToList();
                    CurrentSelected2 = CurrentSelected2.Where(p => p.Region.Trim().ToLower() == ddlRegion.SelectedValue.Trim().ToLower()).Distinct().ToList();

                    sampletext = "Sample";
                    if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                    if (!proceed) proceed = true;
                }
            }

            if (ddlPassage.Items.Count == 0)
            {
                ddlPassage.DataSource = CurrentSelected1.Select(p => p.ManagementPassage.Trim()).Distinct().ToList().OrderBy(p => p);
                ddlPassage.DataBind();
                ddlPassage.Items.Insert(0, new ListItem("All"));
                ddlPassage.SelectedIndex = 0;
            }
            else
            {                
                if (ddlPassage.SelectedIndex != 0)
                {
                    CurrentSelected1 = CurrentSelected1.Where(p => p.ManagementPassage.Trim().ToLower() == ddlPassage.SelectedValue.Trim().ToLower()).Distinct().ToList();
                    CurrentSelected2 = CurrentSelected2.Where(p => p.ManagementPassage.Trim().ToLower() == ddlPassage.SelectedValue.Trim().ToLower()).Distinct().ToList();

                    sampletext = "Sample";
                    if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                    if (!proceed) proceed = true;
                }
            }

            if (ddlRace.Items.Count == 0)
            {
                ddlRace.DataSource = CurrentSelected1.Select(p => p.Race.Trim()).Distinct().ToList().OrderBy(p => p);
                ddlRace.DataBind();
                ddlRace.Items.Insert(0, new ListItem("All"));
                ddlRace.SelectedIndex = 0;
            }
            else
            {                
                if (ddlRace.SelectedIndex != 0)
                {
                    CurrentSelected1 = CurrentSelected1.Where(p => p.Race.Trim().ToLower() == ddlRace.SelectedValue.Trim().ToLower()).Distinct().ToList();
                    CurrentSelected2 = CurrentSelected2.Where(p => p.Race.Trim().ToLower() == ddlRace.SelectedValue.Trim().ToLower()).Distinct().ToList();

                    sampletext = "Sample";
                    if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                    if (!proceed) proceed = true;
                }
            }

            if (ddlOccupationalLevel.Items.Count == 0)
            {
                ddlOccupationalLevel.DataSource = CurrentSelected1.Select(p => p.OccupationalLevel.Trim()).Distinct().ToList().OrderBy(p => p);
                ddlOccupationalLevel.DataBind();
                ddlOccupationalLevel.Items.Insert(0, new ListItem("All"));
                ddlOccupationalLevel.SelectedIndex = 0;
            }
            else
            {                
                if (ddlOccupationalLevel.SelectedIndex != 0)
                {
                    CurrentSelected1 = CurrentSelected1.Where(p => p.OccupationalLevel.Trim().ToLower() == ddlOccupationalLevel.SelectedValue.Trim().ToLower()).Distinct().ToList();
                    CurrentSelected2 = CurrentSelected2.Where(p => p.OccupationalLevel.Trim().ToLower() == ddlOccupationalLevel.SelectedValue.Trim().ToLower()).Distinct().ToList();

                    sampletext = "Sample";
                    if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                    if (!proceed) proceed = true;
                }
            }

            if (ddlGender.Items.Count == 0)
            {
                ddlGender.DataSource = CurrentSelected1.Select(p => p.Gender.Trim()).Distinct().ToList().OrderBy(p => p);
                ddlGender.DataBind();
                ddlGender.Items.Insert(0, new ListItem("All"));
                ddlGender.SelectedIndex = 0;
            }
            else
            {                
                if (ddlGender.SelectedIndex != 0)
                {
                    CurrentSelected1 = CurrentSelected1.Where(p => p.Gender.Trim().ToLower() == ddlGender.SelectedValue.Trim().ToLower()).Distinct().ToList();
                    CurrentSelected2 = CurrentSelected2.Where(p => p.Gender.Trim().ToLower() == ddlGender.SelectedValue.Trim().ToLower()).Distinct().ToList();

                    sampletext = "Sample";
                    if (!txtReportName.Text.Contains(" (Multiple filters applied)")) txtReportName.Text += " (Multiple filters applied)";
                    if (!proceed) proceed = true;
                }
            }

            if (sampletext == "Post Sample")
            {
                CurrentSelected1 = CurrentSelected1.Where(p => p.IsSample ?? false).Distinct().ToList();
                CurrentSelected2 = CurrentSelected2.Where(p => p.IsSample ?? false).Distinct().ToList();
            }

            hdSample1.Value = CurrentSelected1.Count.ToString();
            hdSample2.Value = CurrentSelected2.Count.ToString();
            lblSampleSize.Text = sampletext + " : " + hdSample1.Value + " / " + hdTotal1.Value + " ( " + Math.Round(Convert.ToDouble(hdSample1.Value) / Convert.ToDouble(hdTotal1.Value) * 100, 0, MidpointRounding.AwayFromZero) + "% )";

            AddConfirmRequest(cmdReport, (proceed ? "Confirm Sample Size" : "Insufficient Rights to draw report."), (proceed ? "The sample size for the report you want to draw is <b>" + hdSample1.Value + "</b>.<br/><br/>Are you sure you wish to draw the report with the current filters and subsequent sample size?" : "Unfortunately you do not have sufficient rights to draw this report"), proceed.ToString());
            AddConfirmRequest(cmdPDF, (proceed ? "Confirm Sample Size" : "Insufficient Rights to draw report."), (proceed ? "The sample size for the report you want to draw is <b>" + hdSample1.Value + "</b>.<br/><br/>Are you sure you wish to draw the report with the current filters and subsequent sample size?" : "Unfortunately you do not have sufficient rights to draw this report"), proceed.ToString());
            AddConfirmRequest(cmdPPT, (proceed ? "Confirm Sample Size" : "Insufficient Rights to draw report."), (proceed ? "The sample size for the report you want to draw is <b>" + hdSample1.Value + "</b>.<br/><br/>Are you sure you wish to draw the report with the current filters and subsequent sample size?" : "Unfortunately you do not have sufficient rights to draw this report"), proceed.ToString());
            AddConfirmRequest(cmdExcel, (proceed ? "Confirm Sample Size" : "Insufficient Rights to draw report."), (proceed ? "The sample size for the report you want to draw is <b>" + hdSample1.Value + "</b>.<br/><br/>Are you sure you wish to draw the report with the current filters and subsequent sample size?" : "Unfortunately you do not have sufficient rights to draw this report"), proceed.ToString());
            AddConfirmRequest(cmdInfograph, (proceed ? "Confirm Sample Size" : "Insufficient Rights to draw report."), (proceed ? "The sample size for the report you want to draw is <b>" + hdSample1.Value + "</b>.<br/><br/>Are you sure you wish to draw the report with the current filters and subsequent sample size?" : "Unfortunately you do not have sufficient rights to draw this report"), proceed.ToString());
        }

        protected void rbReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCluster.SelectedIndex = 0;
            ReportComposition.Visible = cmdExcel.Visible = rbReportType.SelectedIndex == 0;
            cmdInfograph.Visible = ((ddlYear2.SelectedItem.Text == "2012" || ddlYear2.SelectedItem.Text == "2011") && rbReportType.SelectedIndex == 0);
            advee.Visible = advRace1.Visible = advRace2.Visible = trLocation.Visible = rbReportType.SelectedIndex != 2;
            cmdReport.Visible = cmdPDF.Visible = cmdPPT.Visible = cmdHistory.Visible = rbReportType.SelectedIndex != 3;
            cmdOrder.Visible = cmdViewOrder.Visible = rbReportType.SelectedIndex == 3;

            cblComposition.Items[0].Selected = cblComposition.Items[1].Selected = cblComposition.Items[2].Selected = cblComposition.Items[4].Selected = cblComposition.Items[5].Selected = cblComposition.Items[9].Selected = cblComposition.Items[10].Selected = true;

            chkYear1Division1.Items.Clear();
            chkYear1Division2.Items.Clear();
            chkYear1Division3.Items.Clear();
            chkYear1Division4.Items.Clear();
            chkYear1Division5.Items.Clear();
            chkYear1Division6.Items.Clear();
            chkYear1Branch.Items.Clear();
            chkYear2Division1.Items.Clear();
            chkYear2Division2.Items.Clear();
            chkYear2Division3.Items.Clear();
            chkYear2Division4.Items.Clear();
            chkYear2Division5.Items.Clear();
            chkYear2Division6.Items.Clear();
            chkYear2Branch.Items.Clear();
            chkLocation.Items.Clear();

            chkY1D1All.Checked = chkY1D2All.Checked = chkY1D3All.Checked = chkY1D4All.Checked = chkY1D5All.Checked = chkY1D6All.Checked = chkY1BAll.Checked = chkY2D1All.Checked = chkY2D2All.Checked = chkY2D3All.Checked = chkY2D4All.Checked = chkY2D5All.Checked = chkY2D6All.Checked = chkY2BAll.Checked = chkLocationAll.Checked = false;

            ddlClassification.Items.Clear();
            ddlJobFamily.Items.Clear();
            ddlLOS.Items.Clear();
            ddlRegion.Items.Clear();
            ddlPassage.Items.Clear();
            ddlRace.Items.Clear();
            ddlOccupationalLevel.Items.Clear();
            ddlGender.Items.Clear();

            Filters();
        }

        private Guid CheckHistory()
        {
            string year1division1, year1division2, year1division3, year1division4, year1division5, year1division6, year1branch, year2division1, year2division2, year2division3, year2division4, year2division5, year2division6, year2branch, location;
            year1division1 = year1division2 = year1division3 = year1division4 = year1division5 = year1division6 = year1branch = year2division1 = year2division2 = year2division3 = year2division4 = year2division5 = year2division6 = year2branch = location = "";

            if (ddlCluster.SelectedIndex == 0 && ddlCluster.SelectedValue == "All")
            {
                year1division1 = "All";
                year1division2 = "All";
                year1division3 = "All";
                year1division4 = "All";
                year1division5 = "All";
                year1division6 = "All";
                year1branch = "All";
                year2division1 = "All";
                year2division2 = "All";
                year2division3 = "All";
                year2division4 = "All";
                year2division5 = "All";
                year2division6 = "All";
                year2branch = "All";
                location = "All";                
            }
            else
            {
                if (chkY1D1All.Checked) year1division1 = "All"; else foreach (ListItem item in chkYear1Division1.Items) if (item.Selected) year1division1 += (year1division1.Length == 0 ? "" : "|") + item.Text;
                if (chkY1D2All.Checked) year1division2 = "All"; else foreach (ListItem item in chkYear1Division2.Items) if (item.Selected) year1division2 += (year1division2.Length == 0 ? "" : "|") + item.Text;
                if (chkY1D3All.Checked) year1division3 = "All"; else foreach (ListItem item in chkYear1Division3.Items) if (item.Selected) year1division3 += (year1division3.Length == 0 ? "" : "|") + item.Text;
                if (chkY1D4All.Checked) year1division4 = "All"; else foreach (ListItem item in chkYear1Division4.Items) if (item.Selected) year1division4 += (year1division4.Length == 0 ? "" : "|") + item.Text;
                if (chkY1D5All.Checked) year1division5 = "All"; else foreach (ListItem item in chkYear1Division5.Items) if (item.Selected) year1division5 += (year1division5.Length == 0 ? "" : "|") + item.Text;
                if (chkY1D6All.Checked) year1division6 = "All"; else foreach (ListItem item in chkYear1Division6.Items) if (item.Selected) year1division6 += (year1division6.Length == 0 ? "" : "|") + item.Text;
                if (chkY1BAll.Checked) year1branch = "All"; else foreach (ListItem item in chkYear1Branch.Items) if (item.Selected) year1branch += (year1branch.Length == 0 ? "" : "|") + item.Text;
                if (chkY2D1All.Checked) year2division1 = "All"; else foreach (ListItem item in chkYear2Division1.Items) if (item.Selected) year2division1 += (year2division1.Length == 0 ? "" : "|") + item.Text;
                if (chkY2D2All.Checked) year2division2 = "All"; else foreach (ListItem item in chkYear2Division2.Items) if (item.Selected) year2division2 += (year2division2.Length == 0 ? "" : "|") + item.Text;
                if (chkY2D3All.Checked) year2division3 = "All"; else foreach (ListItem item in chkYear2Division3.Items) if (item.Selected) year2division3 += (year2division3.Length == 0 ? "" : "|") + item.Text;
                if (chkY2D4All.Checked) year2division4 = "All"; else foreach (ListItem item in chkYear2Division4.Items) if (item.Selected) year2division4 += (year2division4.Length == 0 ? "" : "|") + item.Text;
                if (chkY2D5All.Checked) year2division5 = "All"; else foreach (ListItem item in chkYear2Division5.Items) if (item.Selected) year2division5 += (year2division5.Length == 0 ? "" : "|") + item.Text;
                if (chkY2D6All.Checked) year2division6 = "All"; else foreach (ListItem item in chkYear2Division6.Items) if (item.Selected) year2division6 += (year2division6.Length == 0 ? "" : "|") + item.Text;
                if (chkY2BAll.Checked) year2branch = "All"; else foreach (ListItem item in chkYear2Branch.Items) if (item.Selected) year2branch += (year2branch.Length == 0 ? "" : "|") + item.Text;
                if (chkLocationAll.Checked) location = "All"; else foreach (ListItem item in chkLocation.Items) if (item.Selected) location += (location.Length == 0 ? "" : "|") + item.Text;
            }

            ReportHistory history = new ReportHistory();
            history.Year2 = ddlYear2.SelectedItem.Text;
            history.Cluster = ddlCluster.SelectedItem.Text;
            history.Y1Division1 = year1division1;
            history.Y1Division2 = year1division2;
            history.Y1Division3 = year1division3;
            history.Y1Division4 = year1division4;
            history.Y1Division5 = year1division5;
            history.Y1Division6 = year1division6;
            history.Y1Branch = year1branch;
            history.Y2Division1 = year2division1;
            history.Y2Division2 = year2division2;
            history.Y2Division3 = year2division3;
            history.Y2Division4 = year2division4;
            history.Y2Division5 = year2division5;
            history.Y2Division6 = year2division6;
            history.Y2Branch = year2branch;
            history.Location = location;
            history.Classification = ddlClassification.SelectedItem.Text;
            history.JobFamily = ddlJobFamily.SelectedItem.Text;
            history.LOS = ddlLOS.SelectedItem.Text;
            history.Region = ddlRegion.SelectedItem.Text;
            history.ManagementPassage = ddlPassage.SelectedItem.Text;
            history.Race = ddlRace.SelectedItem.Text;
            history.OccupationalLevel = ddlOccupationalLevel.SelectedItem.Text;
            history.Gender = ddlGender.SelectedItem.Text;

            if (hdFileName.Value != "" && hdSheet.Value != "" && hdColumn.Value != "")
            {
                string conStr = Path.GetExtension(hdFileName.Value) == ".xlsx" ? "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=Yes'" : "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=Yes;'";
                conStr = String.Format(conStr, HostingEnvironment.MapPath("~/Imports/") + hdFileName.Value);
                OleDbConnection connExcel = new OleDbConnection(conStr);
                OleDbCommand Excel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                DataTable dt = new DataTable();
                Excel.Connection = connExcel;

                connExcel.Open();
                Excel.CommandText = "SELECT * From [" + hdSheet.Value + "]";
                oda.SelectCommand = Excel;
                oda.Fill(dt);
                connExcel.Close();
                GC.Collect();

                List<DataRow> rows = dt.Rows.Cast<DataRow>().ToList();

                string Values = "";

                foreach (DataRow row in rows)
                    Values += "|" + row[hdColumn.Value].ToString();

                GC.Collect();

                if (rbType.SelectedIndex == 0) history.ManualBranchCodes = Values.Length > 0 ? Values.Substring(1) : "";
                else history.ManualStaffCodes = Values.Length > 0 ? Values.Substring(1) : "";
                history.ManualFileName = hdFileName.Value;
            }

            string demographics = "";

            foreach (ListItem item in cblComposition.Items)
                if (item.Selected) demographics += "|" + item.Text;

            history.Composition = demographics.Length > 0 ? demographics.Substring(1) : "";

            history.ReportName = txtReportName.Text;
            history.UserID = UserID;
            history.CurrentSampleSize = Convert.ToInt32(hdSample1.Value);
            history.PreviousSampleSize = Convert.ToInt32(hdSample2.Value);

            ReportHistory historycheck = db.ReportHistories.Where(p => p.Year2 == history.Year2 && p.Cluster == history.Cluster && p.Y1Division1 == history.Y1Division1 && p.Y1Division2 == history.Y1Division2 && p.Y1Division3 == history.Y1Division3 && p.Y1Division4 == history.Y1Division4 && p.Y1Division5 == history.Y1Division5 && p.Y1Division6 == history.Y1Division6 && p.Y1Branch == history.Y1Branch && p.Y2Division1 == history.Y2Division1 && p.Y2Division2 == history.Y2Division2 && p.Y2Division3 == history.Y2Division3 && p.Y2Division4 == history.Y2Division4 && p.Y2Division5 == history.Y2Division5 && p.Y2Division6 == history.Y2Division6 && p.Y2Branch == history.Y2Branch && p.Location == history.Location && p.Classification == history.Classification && p.JobFamily == history.JobFamily && p.LOS == history.LOS && p.Region == history.Region && p.ManagementPassage == history.ManagementPassage && p.Race == history.Race && p.OccupationalLevel == history.OccupationalLevel && p.Gender == history.Gender && p.Composition == history.Composition && p.ReportName == history.ReportName && p.ManualBranchCodes == history.ManualBranchCodes && p.ManualStaffCodes == history.ManualStaffCodes && p.ManualFileName == history.ManualFileName).FirstOrDefault();

            if (historycheck == null)
            {
                history.ReportHistoryID = Guid.NewGuid();
                db.ReportHistories.Add(history);
                db.SaveChanges();

                return history.ReportHistoryID;
            }
            else
                return historycheck.ReportHistoryID;
        }

        protected void ddlYear2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCluster.Items.Clear();
            chkYear1Division1.Items.Clear();
            chkYear1Division2.Items.Clear();
            chkYear1Division3.Items.Clear();
            chkYear1Division4.Items.Clear();
            chkYear1Division5.Items.Clear();
            chkYear1Division6.Items.Clear();
            chkYear1Branch.Items.Clear();
            chkYear2Division1.Items.Clear();
            chkYear2Division2.Items.Clear();
            chkYear2Division3.Items.Clear();
            chkYear2Division4.Items.Clear();
            chkYear2Division5.Items.Clear();
            chkYear2Division6.Items.Clear();
            chkYear2Branch.Items.Clear();
            chkLocation.Items.Clear();

            chkY1D1All.Checked = chkY1D2All.Checked = chkY1D3All.Checked = chkY1D4All.Checked = chkY1D5All.Checked = chkY1D6All.Checked = chkY1BAll.Checked = chkY2D1All.Checked = chkY2D2All.Checked = chkY2D3All.Checked = chkY2D4All.Checked = chkY2D5All.Checked = chkY2D6All.Checked = chkY2BAll.Checked = chkLocationAll.Checked = false;

            ddlClassification.Items.Clear();
            ddlJobFamily.Items.Clear();
            ddlLOS.Items.Clear();
            ddlRegion.Items.Clear();
            ddlPassage.Items.Clear();
            ddlRace.Items.Clear();
            ddlOccupationalLevel.Items.Clear();
            ddlGender.Items.Clear();

            Filters();
        }

        protected void ddlCluster_SelectedIndexChanged(object sender, EventArgs e)
        {
            chkYear1Division1.Items.Clear();
            chkYear1Division2.Items.Clear();
            chkYear1Division3.Items.Clear();
            chkYear1Division4.Items.Clear();
            chkYear1Division5.Items.Clear();
            chkYear1Division6.Items.Clear();
            chkYear1Branch.Items.Clear();
            chkYear2Division1.Items.Clear();
            chkYear2Division2.Items.Clear();
            chkYear2Division3.Items.Clear();
            chkYear2Division4.Items.Clear();
            chkYear2Division5.Items.Clear();
            chkYear2Division6.Items.Clear();
            chkYear2Branch.Items.Clear();
            chkLocation.Items.Clear();

            chkY1D1All.Checked = chkY1D2All.Checked = chkY1D3All.Checked = chkY1D4All.Checked = chkY1D5All.Checked = chkY1D6All.Checked = chkY1BAll.Checked = chkY2D1All.Checked = chkY2D2All.Checked = chkY2D3All.Checked = chkY2D4All.Checked = chkY2D5All.Checked = chkY2D6All.Checked = chkY2BAll.Checked = chkLocationAll.Checked = false;

            ddlClassification.Items.Clear();
            ddlJobFamily.Items.Clear();
            ddlLOS.Items.Clear();
            ddlRegion.Items.Clear();
            ddlPassage.Items.Clear();
            ddlRace.Items.Clear();
            ddlOccupationalLevel.Items.Clear();
            ddlGender.Items.Clear();

            hdFileName.Value = hdSheet.Value = hdColumn.Value = lblImportResult.Text = "";
            rbType.SelectedIndex = 1;            

            Filters();
        }

        protected void cmdYear1Division1_Click(object sender, EventArgs e)
        {
            lblYear1Division1.Text = "";
            foreach (ListItem item in chkYear1Division1.Items)
                if (item.Selected) lblYear1Division1.Text += item.Text + "<br/>";

            chkYear1Division2.Items.Clear();
            chkYear1Division3.Items.Clear();
            chkYear1Division4.Items.Clear();
            chkYear1Division5.Items.Clear();
            chkYear1Division6.Items.Clear();
            chkYear1Branch.Items.Clear();
            chkLocation.Items.Clear();

            chkY1D2All.Checked = chkY1D3All.Checked = chkY1D4All.Checked = chkY1D5All.Checked = chkY1D6All.Checked = chkY1BAll.Checked = chkLocationAll.Checked = false;

            ddlClassification.Items.Clear();
            ddlJobFamily.Items.Clear();
            ddlLOS.Items.Clear();
            ddlRegion.Items.Clear();
            ddlPassage.Items.Clear();
            ddlRace.Items.Clear();
            ddlOccupationalLevel.Items.Clear();
            ddlGender.Items.Clear();

            Filters();
        }

        protected void cmdYear2Division1_Click(object sender, EventArgs e)
        {
            lblYear2Division1.Text = "";
            foreach (ListItem item in chkYear2Division1.Items)
                if (item.Selected) lblYear2Division1.Text += item.Text + "<br/>";

            chkYear2Division2.Items.Clear();
            chkYear2Division3.Items.Clear();
            chkYear2Division4.Items.Clear();
            chkYear2Division5.Items.Clear();
            chkYear2Division6.Items.Clear();
            chkYear2Branch.Items.Clear();

            chkY2D2All.Checked = chkY2D3All.Checked = chkY2D4All.Checked = chkY2D5All.Checked = chkY2D6All.Checked = chkY2BAll.Checked = false;

            Filters();
        }

        protected void cmdYear1Division2_Click(object sender, EventArgs e)
        {
            lblYear1Division2.Text = "";
            foreach (ListItem item in chkYear1Division2.Items)
                if (item.Selected) lblYear1Division2.Text += item.Text + "<br/>";

            chkYear1Division3.Items.Clear();
            chkYear1Division4.Items.Clear();
            chkYear1Division5.Items.Clear();
            chkYear1Division6.Items.Clear();
            chkYear1Branch.Items.Clear();
            chkLocation.Items.Clear();

            chkY1D3All.Checked = chkY1D4All.Checked = chkY1D5All.Checked = chkY1D6All.Checked = chkY1BAll.Checked = chkLocationAll.Checked = false;

            ddlClassification.Items.Clear();
            ddlJobFamily.Items.Clear();
            ddlLOS.Items.Clear();
            ddlRegion.Items.Clear();
            ddlPassage.Items.Clear();
            ddlRace.Items.Clear();
            ddlOccupationalLevel.Items.Clear();
            ddlGender.Items.Clear();

            Filters();
        }

        protected void cmdYear2Division2_Click(object sender, EventArgs e)
        {
            lblYear2Division2.Text = "";
            foreach (ListItem item in chkYear2Division2.Items)
                if (item.Selected) lblYear2Division2.Text += item.Text + "<br/>";

            chkYear2Division3.Items.Clear();
            chkYear2Division4.Items.Clear();
            chkYear2Division5.Items.Clear();
            chkYear2Division6.Items.Clear();
            chkYear2Branch.Items.Clear();

            chkY2D3All.Checked = chkY2D4All.Checked = chkY2D5All.Checked = chkY2D6All.Checked = chkY2BAll.Checked = false;

            Filters();
        }

        protected void cmdYear1Division3_Click(object sender, EventArgs e)
        {
            lblYear1Division3.Text = "";
            foreach (ListItem item in chkYear1Division3.Items)
                if (item.Selected) lblYear1Division3.Text += item.Text + "<br/>";

            chkYear1Division4.Items.Clear();
            chkYear1Division5.Items.Clear();
            chkYear1Division6.Items.Clear();
            chkYear1Branch.Items.Clear();
            chkLocation.Items.Clear();

            chkY1D4All.Checked = chkY1D5All.Checked = chkY1D6All.Checked = chkY1BAll.Checked = chkLocationAll.Checked = false;

            ddlClassification.Items.Clear();
            ddlJobFamily.Items.Clear();
            ddlLOS.Items.Clear();
            ddlRegion.Items.Clear();
            ddlPassage.Items.Clear();
            ddlRace.Items.Clear();
            ddlOccupationalLevel.Items.Clear();
            ddlGender.Items.Clear();

            Filters();
        }

        protected void cmdYear2Division3_Click(object sender, EventArgs e)
        {
            lblYear2Division3.Text = "";
            foreach (ListItem item in chkYear2Division3.Items)
                if (item.Selected) lblYear2Division3.Text += item.Text + "<br/>";

            chkYear2Division4.Items.Clear();
            chkYear2Division5.Items.Clear();
            chkYear2Division6.Items.Clear();
            chkYear2Branch.Items.Clear();

            chkY2D4All.Checked = chkY2D5All.Checked = chkY2D6All.Checked = chkY2BAll.Checked = false;

            Filters();
        }

        protected void cmdYear1Division4_Click(object sender, EventArgs e)
        {
            lblYear1Division4.Text = "";
            foreach (ListItem item in chkYear1Division4.Items)
                if (item.Selected) lblYear1Division4.Text += item.Text + "<br/>";

            chkYear1Division5.Items.Clear();
            chkYear1Division6.Items.Clear();
            chkYear1Branch.Items.Clear();
            chkLocation.Items.Clear();

            chkY1D5All.Checked = chkY1D6All.Checked = chkY1BAll.Checked = chkLocationAll.Checked = false;

            ddlClassification.Items.Clear();
            ddlJobFamily.Items.Clear();
            ddlLOS.Items.Clear();
            ddlRegion.Items.Clear();
            ddlPassage.Items.Clear();
            ddlRace.Items.Clear();
            ddlOccupationalLevel.Items.Clear();
            ddlGender.Items.Clear();

            Filters();
        }

        protected void cmdYear2Division4_Click(object sender, EventArgs e)
        {
            lblYear2Division4.Text = "";
            foreach (ListItem item in chkYear2Division4.Items)
                if (item.Selected) lblYear2Division4.Text += item.Text + "<br/>";

            chkYear2Division5.Items.Clear();
            chkYear2Division6.Items.Clear();
            chkYear2Branch.Items.Clear();

            chkY2D5All.Checked = chkY2D6All.Checked = chkY2BAll.Checked = false;

            Filters();
        }

        protected void cmdYear1Division5_Click(object sender, EventArgs e)
        {
            lblYear1Division5.Text = "";
            foreach (ListItem item in chkYear1Division5.Items)
                if (item.Selected) lblYear1Division5.Text += item.Text + "<br/>";

            chkYear1Division6.Items.Clear();
            chkYear1Branch.Items.Clear();
            chkLocation.Items.Clear();

            chkY1D6All.Checked = chkY1BAll.Checked = chkLocationAll.Checked = false;

            ddlClassification.Items.Clear();
            ddlJobFamily.Items.Clear();
            ddlLOS.Items.Clear();
            ddlRegion.Items.Clear();
            ddlPassage.Items.Clear();
            ddlRace.Items.Clear();
            ddlOccupationalLevel.Items.Clear();
            ddlGender.Items.Clear();

            Filters();
        }

        protected void cmdYear2Division5_Click(object sender, EventArgs e)
        {
            lblYear2Division5.Text = "";
            foreach (ListItem item in chkYear2Division5.Items)
                if (item.Selected) lblYear2Division5.Text += item.Text + "<br/>";

            chkYear2Division6.Items.Clear();
            chkYear2Branch.Items.Clear();

            chkY2D6All.Checked = chkY2BAll.Checked = false;

            Filters();
        }

        protected void cmdYear1Division6_Click(object sender, EventArgs e)
        {
            lblYear1Division6.Text = "";
            foreach (ListItem item in chkYear1Division6.Items)
                if (item.Selected) lblYear1Division6.Text += item.Text + "<br/>";

            chkYear1Branch.Items.Clear();
            chkLocation.Items.Clear();

            chkY1BAll.Checked = chkLocationAll.Checked = false;

            ddlClassification.Items.Clear();
            ddlJobFamily.Items.Clear();
            ddlLOS.Items.Clear();
            ddlRegion.Items.Clear();
            ddlPassage.Items.Clear();
            ddlRace.Items.Clear();
            ddlOccupationalLevel.Items.Clear();
            ddlGender.Items.Clear();

            Filters();
        }

        protected void cmdYear2Division6_Click(object sender, EventArgs e)
        {
            lblYear2Division6.Text = "";
            foreach (ListItem item in chkYear2Division6.Items)
                if (item.Selected) lblYear2Division6.Text += item.Text + "<br/>";

            chkYear2Branch.Items.Clear();

            chkY2BAll.Checked = false;

            Filters();
        }

        protected void cmdYear1Branch_Click(object sender, EventArgs e)
        {
            lblYear1Branch.Text = "";
            foreach (ListItem item in chkYear1Branch.Items)
                if (item.Selected) lblYear1Branch.Text += item.Text + "<br/>";

            chkLocation.Items.Clear();

            chkLocationAll.Checked = false;

            ddlClassification.Items.Clear();
            ddlJobFamily.Items.Clear();
            ddlLOS.Items.Clear();
            ddlRegion.Items.Clear();
            ddlPassage.Items.Clear();
            ddlRace.Items.Clear();
            ddlOccupationalLevel.Items.Clear();
            ddlGender.Items.Clear();

            Filters();
        }

        protected void cmdYear2Branch_Click(object sender, EventArgs e)
        {
            lblYear2Branch.Text = "";
            foreach (ListItem item in chkYear2Branch.Items)
                if (item.Selected) lblYear2Branch.Text += item.Text + "<br/>";

            Filters();
        }

        protected void cmdLocation_Click(object sender, EventArgs e)
        {
            lblLocation.Text = "";
            foreach (ListItem item in chkLocation.Items)
                if (item.Selected) lblLocation.Text += item.Text + "<br/>";

            ddlClassification.Items.Clear();
            ddlJobFamily.Items.Clear();
            ddlLOS.Items.Clear();
            ddlRegion.Items.Clear();
            ddlPassage.Items.Clear();
            ddlRace.Items.Clear();
            ddlOccupationalLevel.Items.Clear();
            ddlGender.Items.Clear();

            Filters();
        }

        protected void cmdImport_Click(object sender, EventArgs e)
        {
            Filters();
        }

        protected void ddlClassification_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filters();
        }

        protected void ddlJobFamily_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filters();
        }

        protected void ddlLOS_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filters();
        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filters();
        }

        protected void ddlPassage_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filters();
        }

        protected void ddlRace_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filters();
        }

        protected void ddlOccupationalLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filters();
        }

        protected void ddlGender_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filters();
        }

        protected void cmdReport_Click(object sender, EventArgs e)
        {
            if (rbReportType.SelectedIndex == 2 && Convert.ToInt32(hdSample1.Value) < 150)
                AlertShow("Unable to draw report - less than 10 respondents.");
            else if (rbReportType.SelectedIndex != 2 && Convert.ToInt32(hdSample1.Value) < 10)
                AlertShow("Unable to draw report - less than 10 respondents.");
            else
            {
                Guid historyguid = CheckHistory();

                ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == historyguid).Distinct().SingleOrDefault();

                if (rbReportType.SelectedIndex == 0)
                {
                    if (history.OnlineCompDate == null) history.OnlineCompDate = DateTime.Now;
                    history.OnlineCompCount = history.OnlineCompCount == null ? 1 : history.OnlineCompCount++;

                    ReportGeneration gen = new ReportGeneration();
                    gen.DateDrawn = DateTime.Now;
                    gen.GenerationID = Guid.NewGuid();
                    gen.OutputType = "Online";
                    gen.ReportHistoryID = history.ReportHistoryID;
                    gen.ReportType = "Comprehensive";
                    gen.UserID = UserID;
                    db.ReportGenerations.Add(gen);
                    db.SaveChanges();

                    Response.Redirect("~/comp/" + history.ReportHistoryID);
                }
                else if (rbReportType.SelectedIndex == 1)
                {
                    if (history.OnlineExecDate == null) history.OnlineExecDate = DateTime.Now;
                    history.OnlineExecCount = history.OnlineExecCount == null ? 1 : history.OnlineExecCount++;

                    ReportGeneration gen = new ReportGeneration();
                    gen.DateDrawn = DateTime.Now;
                    gen.GenerationID = Guid.NewGuid();
                    gen.OutputType = "Online";
                    gen.ReportHistoryID = history.ReportHistoryID;
                    gen.ReportType = "Executive Summary";
                    gen.UserID = UserID;
                    db.ReportGenerations.Add(gen);
                    db.SaveChanges();

                    Response.Redirect("~/exec/" + history.ReportHistoryID);
                }
                else if (rbReportType.SelectedIndex == 2)
                {
                    if (history.OnlineEEDate == null) history.OnlineEEDate = DateTime.Now;
                    history.OnlineEECount = history.OnlineEECount == null ? 1 : history.OnlineEECount++;

                    ReportGeneration gen = new ReportGeneration();
                    gen.DateDrawn = DateTime.Now;
                    gen.GenerationID = Guid.NewGuid();
                    gen.OutputType = "Online";
                    gen.ReportHistoryID = history.ReportHistoryID;
                    gen.ReportType = "EE Barrier";
                    gen.UserID = UserID;
                    db.ReportGenerations.Add(gen);
                    db.SaveChanges();

                    Response.Redirect("~/ee/" + history.ReportHistoryID);
                }
            }
        }

        protected void cmdPDF_Click(object sender, EventArgs e)
        {
            if (rbReportType.SelectedIndex == 2 && Convert.ToInt32(hdSample1.Value) < 150)
                AlertShow("Unable to draw report - less than 10 respondents.");
            else if (rbReportType.SelectedIndex != 2 && Convert.ToInt32(hdSample1.Value) < 10)
                AlertShow("Unable to draw report - less than 10 respondents.");
            else
            {
                Guid historyguid = CheckHistory();

                ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == historyguid).Distinct().SingleOrDefault();

                string filename = "";

                if (rbReportType.SelectedIndex == 0)
                {
                    if (history.PDFCompDate == null) history.PDFCompDate = DateTime.Now;
                    history.PDFCompCount = history.PDFCompCount == null ? 1 : history.PDFCompCount++;

                    ReportGeneration gen = new ReportGeneration();
                    gen.DateDrawn = DateTime.Now;
                    gen.GenerationID = Guid.NewGuid();
                    gen.OutputType = "PDF";
                    gen.ReportHistoryID = history.ReportHistoryID;
                    gen.ReportType = "Comprehensive";
                    gen.UserID = UserID;
                    db.ReportGenerations.Add(gen);
                    db.SaveChanges();

                    if (history.PDFCompFilename == null || history.ManualFileName != null)
                    {
                        RenderPDF renderPDF = new RenderPDF();
                        filename = txtReportName.Text.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                        renderPDF.GeneratePDF(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/comppdf/" + history.ReportHistoryID);
                        history.PDFCompFilename = filename;
                        db.SaveChanges();

                        renderPDF = null;
                    }
                    else
                        filename = history.PDFCompFilename;
                }
                else if (rbReportType.SelectedIndex == 1)
                {
                    if (history.PDFExecDate == null) history.PDFExecDate = DateTime.Now;
                    history.PDFExecCount = history.PDFExecCount == null ? 1 : history.PDFExecCount++;

                    ReportGeneration gen = new ReportGeneration();
                    gen.DateDrawn = DateTime.Now;
                    gen.GenerationID = Guid.NewGuid();
                    gen.OutputType = "PDF";
                    gen.ReportHistoryID = history.ReportHistoryID;
                    gen.ReportType = "Executive Summary";
                    gen.UserID = UserID;
                    db.ReportGenerations.Add(gen);
                    db.SaveChanges();

                    if (history.PDFExecFilename == null || history.ManualFileName != null)
                    {
                        RenderPDF renderPDF = new RenderPDF();
                        filename = txtReportName.Text.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                        renderPDF.GeneratePDF(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/execpdf/" + history.ReportHistoryID);
                        history.PDFExecFilename = filename;
                        db.SaveChanges();

                        renderPDF = null;
                    }
                    else
                        filename = history.PDFExecFilename;
                }
                else if (rbReportType.SelectedIndex == 2)
                {
                    if (history.PDFEEDate == null) history.PDFEEDate = DateTime.Now;
                    history.PDFEECount = history.PDFEECount == null ? 1 : history.PDFEECount++;

                    ReportGeneration gen = new ReportGeneration();
                    gen.DateDrawn = DateTime.Now;
                    gen.GenerationID = Guid.NewGuid();
                    gen.OutputType = "PDF";
                    gen.ReportHistoryID = history.ReportHistoryID;
                    gen.ReportType = "EE Barrier";
                    gen.UserID = UserID;
                    db.ReportGenerations.Add(gen);
                    db.SaveChanges();

                    if (history.PDFEEFilename == null || history.ManualFileName != null)
                    {
                        RenderPDF renderPDF = new RenderPDF();
                        filename = txtReportName.Text.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                        renderPDF.GeneratePDF(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/eepdf/" + history.ReportHistoryID);
                        history.PDFEEFilename = filename;
                        db.SaveChanges();

                        renderPDF = null;
                    }
                    else
                        filename = history.PDFEEFilename;
                }

                this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=pdf&File=" + filename);

                GC.Collect();
            }
        }

        protected void cmdPPT_Click(object sender, EventArgs e)
        {
            if (rbReportType.SelectedIndex == 2 && Convert.ToInt32(hdSample1.Value) < 150)
                AlertShow("Unable to draw report - less than 10 respondents.");
            else if (rbReportType.SelectedIndex != 2 && Convert.ToInt32(hdSample1.Value) < 10)
                AlertShow("Unable to draw report - less than 10 respondents.");
            else
            {
                Guid historyguid = CheckHistory();

                ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == historyguid).Distinct().SingleOrDefault();

                string filename = "";

                if (rbReportType.SelectedIndex == 0)
                {
                    if (history.PPTCompDate == null) history.PPTCompDate = DateTime.Now;
                    history.PPTCompCount = history.PPTCompCount == null ? 1 : history.PPTCompCount++;

                    ReportGeneration gen = new ReportGeneration();
                    gen.DateDrawn = DateTime.Now;
                    gen.GenerationID = Guid.NewGuid();
                    gen.OutputType = "PPT";
                    gen.ReportHistoryID = history.ReportHistoryID;
                    gen.ReportType = "Comprehensive";
                    gen.UserID = UserID;
                    db.ReportGenerations.Add(gen);
                    db.SaveChanges();

                    if (history.PPTCompFilename == null || history.ManualFileName != null)
                    {
                        RenderPPT renderPPT = new RenderPPT();
                        filename = txtReportName.Text.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".ppt";

                        renderPPT.GeneratePPT(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/compppt/" + history.ReportHistoryID);
                        history.PPTCompFilename = filename;
                        db.SaveChanges();

                        renderPPT = null;
                    }
                    else
                        filename = history.PPTCompFilename;
                }
                else if (rbReportType.SelectedIndex == 1)
                {
                    if (history.PPTExecDate == null) history.PPTExecDate = DateTime.Now;
                    history.PPTExecCount = history.PPTExecCount == null ? 1 : history.PPTExecCount++;

                    ReportGeneration gen = new ReportGeneration();
                    gen.DateDrawn = DateTime.Now;
                    gen.GenerationID = Guid.NewGuid();
                    gen.OutputType = "PPT";
                    gen.ReportHistoryID = history.ReportHistoryID;
                    gen.ReportType = "Executive Summary";
                    gen.UserID = UserID;
                    db.ReportGenerations.Add(gen);
                    db.SaveChanges();

                    if (history.PPTExecFilename == null || history.ManualFileName != null)
                    {
                        RenderPPT renderPPT = new RenderPPT();
                        filename = txtReportName.Text.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".ppt";

                        renderPPT.GeneratePPT(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/execppt/" + history.ReportHistoryID);
                        history.PPTExecFilename = filename;
                        db.SaveChanges();

                        renderPPT = null;
                    }
                    else
                        filename = history.PPTExecFilename;
                }
                else if (rbReportType.SelectedIndex == 2)
                {
                    if (history.PPTEEDate == null) history.PPTEEDate = DateTime.Now;
                    history.PPTEECount = history.PPTEECount == null ? 1 : history.PPTEECount++;

                    ReportGeneration gen = new ReportGeneration();
                    gen.DateDrawn = DateTime.Now;
                    gen.GenerationID = Guid.NewGuid();
                    gen.OutputType = "PPT";
                    gen.ReportHistoryID = history.ReportHistoryID;
                    gen.ReportType = "EE Barrier";
                    gen.UserID = UserID;
                    db.ReportGenerations.Add(gen);
                    db.SaveChanges();

                    if (history.PPTEEFilename == null || history.ManualFileName != null)
                    {
                        RenderPPT renderPPT = new RenderPPT();
                        filename = txtReportName.Text.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".ppt";

                        renderPPT.GeneratePPT(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/eeppt/" + history.ReportHistoryID);
                        history.PPTEEFilename = filename;
                        db.SaveChanges();

                        renderPPT = null;
                    }
                    else
                        filename = history.PPTEEFilename;
                }

                this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=ppt&File=" + filename);

                GC.Collect();
            }
        }

        protected void cmdExcel_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(hdSample1.Value) < 10)
                AlertShow("Unable to draw report - less than 10 respondents.");
            else
            {
                string filename = "";

                Guid historyguid = CheckHistory();

                ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == historyguid).Distinct().SingleOrDefault();

                if (history.ExcelDate == null) history.ExcelDate = DateTime.Now;
                history.ExcelCount = history.ExcelCount == null ? 1 : history.ExcelCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "Excel";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Comprehensive";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                //if (history.ExcelFilename == null)
                //{
                    RenderExcel excel = new RenderExcel();
                    filename = excel.Generate(historyguid, Server.MapPath("~/Exports"));
                    history.ExcelFilename = filename;
                    db.SaveChanges();
                    excel = null;
                //}
                //else
                //    filename = history.ExcelFilename;

                this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=excel&File=" + filename);
            }
        }

        protected void cmdOrder_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(hdSample1.Value) < 150)
                AlertShow("Unable to draw report - less than 150 respondents.");
            else
                Response.Redirect("~/neworder/" + CheckHistory());
        }

        protected void cmdViewOrder_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/orders");
        }

        protected void cmdInfograph_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(hdSample1.Value) < 10)
                AlertShow("Unable to draw report - less than 10 respondents.");
            else
            {
                string filename = "";

                Guid historyguid = CheckHistory();

                ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == historyguid).Distinct().SingleOrDefault();

                if (history.InfographDate == null) history.InfographDate = DateTime.Now;
                history.InfographCount = history.InfographCount == null ? 1 : history.InfographCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PDF";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Infograph";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.InfographFilename == null || history.ManualFileName != null)
                {
                    RenderPDF renderPDF = new RenderPDF();
                    filename = txtReportName.Text.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                    renderPDF.GeneratePDF(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/infograph/" + history.ReportHistoryID, PdfPageSize.A3, PdfPageOrientation.Portrait, 1754);
                    history.InfographFilename = filename;
                    db.SaveChanges();

                    renderPDF = null;
                }
                else
                    filename = history.InfographFilename;

                this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=pdf&File=" + filename);
            }
        }

        protected void cmdHistory_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/history");
        }

        protected void AddConfirmRequest(WebControl control, string title, string message, string proceed)
        {
            control.Attributes.Remove("onclick");
            string postBackReference = Page.ClientScript.GetPostBackEventReference(control, String.Empty);
            string function = String.Format("javascript:showConfirmRequest(function() {{ {0} }}, '{1}', '{2}', '{3}'); return false;", postBackReference, title, message, proceed);
            control.Attributes.Add("onclick", function);
        }
    }
}