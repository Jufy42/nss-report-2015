﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Infograph.aspx.cs" Inherits="NSS_2015_Report.Infograph" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="Content/Site.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Charts/FusionCharts.js"></script>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
</head>
<body>
    <form id="form1" runat="server">
    <div style="background-image:url('<%=GetSiteRoot() + "/Images/Infograph background1.jpg" %>');background-repeat:no-repeat;width:1754px;height:2470px;background-position:center;font-family:Arial;">
        <div>
            <table style="width:100%" cellpadding="0" cellspacing="0">
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="font-size:18px;padding-left:65px;color:White;padding-top:15px;width:700px"><asp:Label ID="lblReportName" runat="server"></asp:Label></td>
                                <td style="font-size:22px;padding-left:5px;color:White;padding-top:19px;text-align:left;"><asp:Label ID="lbl2012" runat="server" Text="Label"></asp:Label></td>
                                <td style="font-size:22px;padding-left:60px;color:White;padding-top:19px;text-align:left;"><asp:Label ID="lbl2011" runat="server" Text="Label"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table style="font-weight:bold;">
                            <tr>
                                <td style="padding-left:650px;color:White;text-align:left;padding-top:10px;"><asp:Literal id="overalltrenddiv" runat="server"></asp:Literal></td>
                                <td style="padding-left:150px;padding-top:5px">
                                    <table>
                                        <tr>
                                            <td style="font-size:22px;text-align:left;"><asp:Label ID="lblYear1" runat="server"></asp:Label></td>
                                            <td style="font-size:22px;color:#778D1D;text-align:left;"><asp:Label ID="lbl2012Score" runat="server"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:22px;text-align:left;"><asp:Label ID="lblYear2" runat="server"></asp:Label></td>
                                            <td style="font-size:22px;color:#778D1D;text-align:left;"><asp:Label ID="lbl2011Score" runat="server"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:22px;text-align:left;"><asp:Label ID="lblYear3" runat="server"></asp:Label></td>
                                            <td style="font-size:22px;color:#778D1D;text-align:left;"><asp:Label ID="lbl2010Score" runat="server"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table style="font-weight:bold;">
                            <tr>
                                <td style="padding-left:85px;width:652px;padding-top:52px;vertical-align:top;" rowspan="4"><asp:Literal id="dimensionradardiv" runat="server"></asp:Literal></td>
                                <td style="vertical-align:top;padding-top:52px;">
                                    <table>
                                        <tr>
                                            <td style="vertical-align:top;padding-top:13px;">
                                                <table>
                                                    <tr>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:638px;"><b><asp:Label ID="lblYear1a" runat="server"></asp:Label></b></td>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:54px;"><b><asp:Label ID="lblYear2a" runat="server"></asp:Label></b></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align:top;padding-top:64px;">
                                                <table>
                                                    <tr>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:108px;"><asp:Label ID="lblHigh1a" runat="server"></asp:Label></td>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:30px;"><asp:Label ID="lblHigh1b" runat="server"></asp:Label></td>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:150px;"><asp:Label ID="lblHigh2a" runat="server"></asp:Label></td>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:28px;"><asp:Label ID="lblHigh2b" runat="server"></asp:Label></td>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:189px;"><asp:Label ID="lblHigh3a" runat="server"></asp:Label></td>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:30px;"><asp:Label ID="lblHigh3b" runat="server"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align:top;padding-top:15px;padding-left:11px;">
                                                <table>
                                                    <tr>
                                                        <td style="font-size:20px;color:#000;text-align:center;width:288px;height:60px;"><asp:Label ID="lblHigh1Name" runat="server"></asp:Label></td>
                                                        <td style="font-size:20px;color:#000;text-align:center;width:288px;height:60px;"><asp:Label ID="lblHigh2Name" runat="server"></asp:Label></td>
                                                        <td style="font-size:20px;color:#000;text-align:center;width:288px;height:60px;"><asp:Label ID="lblHigh3Name" runat="server"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align:top;padding-top:49px;">
                                                <table>
                                                    <tr>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:643px;"><b><asp:Label ID="lblYear1b" runat="server"></asp:Label></b></td>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:57px;"><b><asp:Label ID="lblYear2b" runat="server"></asp:Label></b></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align:top;padding-top:63px;">
                                                <table>
                                                    <tr>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:108px;"><asp:Label ID="lblDiff1a" runat="server"></asp:Label></td>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:30px;"><asp:Label ID="lblDiff1b" runat="server"></asp:Label></td>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:150px;"><asp:Label ID="lblDiff2a" runat="server"></asp:Label></td>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:28px;"><asp:Label ID="lblDiff2b" runat="server"></asp:Label></td>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:189px;"><asp:Label ID="lblDiff3a" runat="server"></asp:Label></td>
                                                        <td style="font-size:22px;color:#00573C;text-align:left;padding-left:30px;"><asp:Label ID="lblDiff3b" runat="server"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align:top;padding-top:15px;padding-left:11px;">
                                                <table>
                                                    <tr>
                                                        <td style="font-size:20px;color:#000;text-align:center;width:288px;height:60px;"><asp:Label ID="lblDiff1Name" runat="server"></asp:Label></td>
                                                        <td style="font-size:20px;color:#000;text-align:center;width:288px;height:60px;"><asp:Label ID="lblDiff2Name" runat="server"></asp:Label></td>
                                                        <td style="font-size:20px;color:#000;text-align:center;width:288px;height:60px;"><asp:Label ID="lblDiff3Name" runat="server"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>   
                                    </table>
                                </td>
                            </tr>                         
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td style="vertical-align:top;padding-top:0px;">
                        <table>
                            <tr>
                                <td style="font-size:20px;color:#00573C;text-align:left;padding-left:117px;"><b><asp:Label ID="lblYear1c" runat="server"></asp:Label></b></td>
                                <td style="font-size:20px;color:#00573C;text-align:left;padding-left:557px;"><b><asp:Label ID="lblYear2c" runat="server"></asp:Label></b></td>
                                <td style="font-size:20px;color:#00573C;text-align:left;padding-left:172px;"><b><asp:Label ID="lblYear1d" runat="server"></asp:Label></b></td>
                                <td style="font-size:20px;color:#00573C;text-align:left;padding-left:608px;"><b><asp:Label ID="lblYear2d" runat="server"></asp:Label></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="font-size:15px;color:#000;text-align:left;width:430px;padding-left:243px;padding-top:30px;vertical-align:middle;height:60px;"><asp:Label ID="lblTop1" runat="server"></asp:Label></td>
                                <td style="font-size:22px;color:#00573C;text-align:left;padding-left:29px;vertical-align:middle;padding-top:27px;width:40px;"><b><asp:Label ID="lblTop12012" runat="server"></asp:Label></b></td>
                                <td style="font-size:22px;color:#00573C;text-align:left;padding-left:19px;vertical-align:middle;padding-top:27px;width:40px;"><b><asp:Label ID="lblTop12011" runat="server"></asp:Label></b></td>
                                <td style="font-size:15px;color:#000;text-align:left;width:415px;padding-left:123px;vertical-align:middle;height:60px;padding-top:42px;"><asp:Label ID="lblImproved1" runat="server"></asp:Label></td>
                                <td style="font-size:22px;color:#00573C;text-align:left;padding-left:16px;vertical-align:middle;padding-top:29px;width:40px;"><b><asp:Label ID="lblImproved12012" runat="server"></asp:Label></b></td>
                                <td style="font-size:22px;color:#00573C;text-align:left;padding-left:19px;vertical-align:middle;padding-top:29px;width:40px;"><b><asp:Label ID="lblImproved12011" runat="server"></asp:Label></b></td>
                            </tr>
                            <tr><td colspan="6">&nbsp;</td></tr>
                            <tr>
                                <td style="font-size:15px;color:#000;text-align:left;width:430px;padding-left:243px;padding-top:15px;vertical-align:middle;height:60px;"><asp:Label ID="lblTop2" runat="server"></asp:Label></td>
                                <td style="font-size:22px;color:#00573C;text-align:left;padding-left:29px;vertical-align:middle;padding-top:10px;width:40px;"><b><asp:Label ID="lblTop22012" runat="server"></asp:Label></b></td>
                                <td style="font-size:22px;color:#00573C;text-align:left;padding-left:19px;vertical-align:middle;padding-top:10px;width:40px;"><b><asp:Label ID="lblTop22011" runat="server"></asp:Label></b></td>
                                <td style="font-size:15px;color:#000;text-align:left;width:415px;padding-left:123px;vertical-align:middle;height:60px;padding-top:15px;"><asp:Label ID="lblImproved2" runat="server"></asp:Label></td>
                                <td style="font-size:22px;color:#00573C;text-align:left;padding-left:16px;vertical-align:middle;padding-top:10px;width:40px;"><b><asp:Label ID="lblImproved22012" runat="server"></asp:Label></b></td>
                                <td style="font-size:22px;color:#00573C;text-align:left;padding-left:19px;vertical-align:middle;padding-top:10px;width:40px;"><b><asp:Label ID="lblImproved22011" runat="server"></asp:Label></b></td>
                            </tr>
                            <tr><td colspan="6">&nbsp;</td></tr>
                            <tr>
                                <td style="font-size:15px;color:#000;text-align:left;width:430px;padding-left:243px;padding-top:17px;vertical-align:middle;height:65px;"><asp:Label ID="lblTop3" runat="server"></asp:Label></td>
                                <td style="font-size:22px;color:#00573C;text-align:left;padding-left:29px;vertical-align:middle;padding-top:22px;width:40px;"><b><asp:Label ID="lblTop32012" runat="server"></asp:Label></b></td>
                                <td style="font-size:22px;color:#00573C;text-align:left;padding-left:19px;vertical-align:middle;padding-top:22px;width:40px;"><b><asp:Label ID="lblTop32011" runat="server"></asp:Label></b></td>
                                <td style="font-size:15px;color:#000;text-align:left;width:415px;padding-left:123px;vertical-align:middle;height:65px;padding-top:17px;"><asp:Label ID="lblImproved3" runat="server"></asp:Label></td>
                                <td style="font-size:22px;color:#00573C;text-align:left;padding-left:16px;vertical-align:middle;padding-top:15px;width:40px;"><b><asp:Label ID="lblImproved32012" runat="server"></asp:Label></b></td>
                                <td style="font-size:22px;color:#00573C;text-align:left;padding-left:19px;vertical-align:middle;padding-top:15px;width:40px;"><b><asp:Label ID="lblImproved32011" runat="server"></asp:Label></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table style="font-weight:bold;">
                            <tr>
                                <td style="padding-left:650px;color:White;text-align:left;padding-top:5px;"><div id="EngagementTrenddiv" runat="server"></div></td>
                                <td style="padding-left:150px;padding-top:17px">
                                    <table>
                                        <tr>
                                            <td style="font-size:22px;text-align:left;"><asp:Label ID="lblEngYear1" runat="server"></asp:Label></td>
                                            <td style="font-size:22px;color:#778D1D;text-align:left;"><asp:Label ID="lblEngagement2012" runat="server"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:22px;text-align:left;"><asp:Label ID="lblEngYear2" runat="server"></asp:Label></td>
                                            <td style="font-size:22px;color:#778D1D;text-align:left;"><asp:Label ID="lblEngagement2011" runat="server"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:22px;text-align:left;"><asp:Label ID="lblEngYear3" runat="server"></asp:Label></td>
                                            <td style="font-size:22px;color:#778D1D;text-align:left;"><asp:Label ID="lblEngagement2010" runat="server"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td style="vertical-align:top;padding-top:0px;">
                        <table>
                            <tr>
                                <td style="font-size:20px;color:#00573C;text-align:left;padding-left:510px;"><b><asp:Label ID="lblYear1e" runat="server"></asp:Label></b></td>
                                <td style="font-size:20px;color:#00573C;text-align:left;padding-left:661px;"><b><asp:Label ID="lblYear2e" runat="server"></asp:Label></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td style="font-size:28px;padding-left:125px;"><div id="EngagementStatementsdiv" runat="server"></div></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="font-size:40px;padding-left:180px;padding-top:42px;width:80px;"><asp:Label ID="lblMale" runat="server"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:40px;padding-left:180px;padding-top:25px;width:80px;"><asp:Label ID="lblFemale" runat="server"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding-left:175px;"><div id="RaceChartsDiv" runat="server" align="center"></div></td>
                                <td style="padding-left:50px;"><div id="AgeChartdiv" runat="server" align="center"></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <table width="1400px">                                                                                       
                            <tr>
                                <td align="left" style="padding-left:360px;vertical-align:text-top;">
                                    <div id="filtersdiv" runat="server" width="1400px"></div>
                                </td>                                                                                                    
                            </tr>
                            <tr>
                                <td align="left" style="padding-left:360px;vertical-align:text-top;" width="1400px">• The trend line reflects the Nedbank Group Engagement Score based on the Hewitt analysis.</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>