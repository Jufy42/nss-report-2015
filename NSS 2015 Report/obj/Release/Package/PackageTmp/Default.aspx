﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="NSS_2015_Report._Default" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        $(document).ready(function () {
            if (("FormData" in window)) $("#<%=hdManual.ClientID%>").val("auto"); else $("#<%=hdManual.ClientID%>").val("manual");

            $("#dialog-Import").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, height: 540, width: 640, modal: true,
                buttons: {
                    "Import": function () {
                        if ($('#<%=hdFileName.ClientID%>').val() == "") alert("Please upload a file");
                        else if ($('#<%=hdSheet.ClientID%>').val() == "") alert("Please select a sheet");
                        else if ($('#<%=hdColumn.ClientID%>').val() == "") alert("Please select a column");
                        else {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdImport, string.Empty) %>;
                            $(this).dialog("close");
                        }
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                },
                open: function (event, ui) {
                    $('#<%=hdFileName.ClientID%>').empty();
                    $('#<%=hdSheet.ClientID%>').empty();
                    $('#<%=hdColumn.ClientID%>').empty();
                    $('.trsheet').hide();
                    $('.trcolumn').hide();
                    $("#textPreview").empty();
                    $('#combobox').change(function () {
                        if ($("#combobox").val() != "Please Select") {
                            $('.trcolumn').show();
                            $('#<%=hdSheet.ClientID%>').val($("#combobox").val());

                            $.ajax({
                                type: "POST",
                                url: "<%=GetSiteRoot()%>/Default.aspx/GetColumns",
                                data: JSON.stringify({ "filename": $('#<%=hdFileName.ClientID%>').val(), "sheetname": $("#combobox").val() }),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (data) {
                                    var options = $("#combobox1");
                                    options.find('option').remove().end();
                                    options.append($("<option />").val('Please Select').text('Please Select'));
                                    $.each($.parseJSON(data.d), function () {
                                        options.append($("<option />").val(this.label).text(this.label));
                                    });
                                },
                                failure: function (data) {
                                    alert(response.d);
                                }
                            });
                        }
                        else {
                            $('.trcolumn').hide();
                        }
                    });

                    $('#combobox1').change(function () {
                        if ($("#combobox1").val() != "Please Select") {
                            $('#<%=hdColumn.ClientID%>').val($("#combobox1").val());

                            $.ajax({
                                type: "POST",
                                url: "<%=GetSiteRoot()%>/Default.aspx/PreviewData",
                                data: JSON.stringify({ "filename": $('#<%=hdFileName.ClientID%>').val(), "sheetname": $("#combobox").val(), "columnname": $("#combobox1").val() }),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (data) {
                                    var text = "<table style=\"width:100%;background:white;border:1px solid black;border-collapse:collapse;padding:5px;\"><tr><td style=\"width:100%;background:white;border:1px solid black;border-collapse:collapse;padding:5px;\"><b>Data Preview (first 10 records)</b></td></tr>";
                                    $.each($.parseJSON(data.d), function () {
                                        text += "<tr><td style=\"width:100%;background:white;border:1px solid black;border-collapse:collapse;padding:5px;text-align:right;\">" + this.label + "</td></tr>";
                                    });
                                    text += "</table>";

                                    $("#textPreview").append(text);
                                },
                                failure: function (data) {
                                    alert(response.d);
                                }
                            });
                        }
                        else {
                            $("#textPreview").empty();
                        }
                    });
                },
                close: function () {

                }
            });

            $("#dialog-Year1Division1").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear1Division1, string.Empty) %>;
                            $("#dialog-Year1Division1").dialog("close");
                        }
                    }
                }
            });

            $("#dialog-Year1Division2").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear1Division2, string.Empty) %>;
                            $("#dialog-Year1Division2").dialog("close");
                        }
                    }
                }
            });

            $("#dialog-Year1Division3").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear1Division3, string.Empty) %>;
                            $("#dialog-Year1Division3").dialog("close");
                        }
                    }
                }
            });

            $("#dialog-Year1Division4").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear1Division4, string.Empty) %>;
                            $("#dialog-Year1Division4").dialog("close");
                        }
                    }
                }
            });

            $("#dialog-Year1Division5").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear1Division5, string.Empty) %>;
                            $("#dialog-Year1Division5").dialog("close");
                        }
                    }
                }
            });

            $("#dialog-Year1Division6").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear1Division6, string.Empty) %>;
                            $("#dialog-Year1Division6").dialog("close");
                        }
                    }
                }
            });

            $("#dialog-Year1Branch").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear1Branch, string.Empty) %>;
                            $("#dialog-Year1Branch").dialog("close");
                        }
                    }
                }
            });

            $("#dialog-Year2Division1").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear2Division1, string.Empty) %>;
                            $("#dialog-Year2Division1").dialog("close");
                        }
                    }
                },
                close: function () {

                }
            });

            $("#dialog-Year2Division2").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear2Division2, string.Empty) %>;
                            $("#dialog-Year2Division2").dialog("close");
                        }
                    }
                }
            });

            $("#dialog-Year2Division3").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear2Division3, string.Empty) %>;
                            $("#dialog-Year2Division3").dialog("close");
                        }
                   }
                }
            });

            $("#dialog-Year2Division4").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear2Division4, string.Empty) %>;
                            $("#dialog-Year2Division4").dialog("close");
                        }
                    }
                }
            });

            $("#dialog-Year2Division5").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear2Division5, string.Empty) %>;
                            $("#dialog-Year2Division5").dialog("close");
                        }
                    }
                }
            });

            $("#dialog-Year2Division6").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear2Division6, string.Empty) %>;
                            $("#dialog-Year2Division6").dialog("close");
                        }
                    }
                }
            });

            $("#dialog-Year2Branch").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdYear2Branch, string.Empty) %>;
                            $("#dialog-Year2Branch").dialog("close");
                        }
                    }
                }
            });

            $("#dialog-Location").dialog({
                appendTo: "form", dialogClass: 'ui-dialog', autoOpen: false, resizable: false, draggable: false, width: 'auto', modal: true,
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close").hide();
                },
                buttons: {
                    "Apply": {
                        text: "Apply",
                        priority: "primary",
                        "class": "greenbtn",
                        click: function () {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdLocation, string.Empty) %>;
                            $("#dialog-Location").dialog("close");
                        }
                    }
                }
            });
        });

        function showConfirmRequest(callBackFunction, title, content, proceed) {
            var buttons = {
                'Cancel': function () {
                    $("#divConfirm").html("");
                    $("#divConfirm").dialog("destroy");
                }
            };

            if (proceed == 'True') {
                buttons['Draw Report'] = function () {
                    callBackFunction();
                    $("#divConfirm").html("");
                    $("#divConfirm").dialog("destroy");
                }
            };

            $("#divConfirm").html(content).dialog({
                autoOpen: true,
                modal: true,
                title: title,
                draggable: true,
                resizable: false,
                close: function (event, ui) { $("#divConfirm").dialog("destroy"); },
                buttons: buttons,
                overlay: {
                    opacity: 0.45,
                    background: "black"
                }
            });
        }

        function fileupload(filecontrol) {
            var fileList = filecontrol.files;

            var file;
            if (fileList == null) {
                file = filecontrol.value;
            }
            else {
                file = fileList[0];
            }

            var r = new FileReader();
            r.onload = function () {
                if (file.name.toLowerCase().indexOf("xlsx") == -1 && file.name.toLowerCase().indexOf("xls") == -1) alert("Only Microsoft Excel Files are permitted");
                else if (file.size > 10485760) alert("No files over 10mb in size are permitted");
                else {
                    $('#<%=hdFileName.ClientID%>').val(file.name);

                    $.ajax({
                        type: "POST",
                        url: "<%=GetSiteRoot()%>/Default.aspx/GetSheets",
                        data: JSON.stringify({ "f": r.result.match(/,(.*)$/)[1], "fileName": file.name }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('.trsheet').show();
                            var options = $("#combobox");
                            options.find('option').remove().end();
                            options.append($("<option />").val('Please Select').text('Please Select'));
                            $.each($.parseJSON(data.d), function () {
                                options.append($("<option />").val(this.label).text(this.label));
                            });
                        },
                        failure: function (data) {
                            $('.trsheet').hide();
                            $('#<%=hdFileName.ClientID%>').val("");
                            alert(response.d);
                        }
                    });
                }
            };
        r.readAsDataURL(file);
    }
    </script>
    <asp:HiddenField ID="hdManual" runat="server" />
    <div style="width:100%;text-align:center;">
        <asp:UpdatePanel ID="upFilters" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <br />
                <div id="ReportType" runat="server" style="text-align:center;width:100%;">
                    <center>
                        <table style="width:100%;padding:5px;margin:0;background:#DBF8A5;text-align:center;-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;margin: 0 auto;">
                            <tr>
                                <td style="text-align:center;width:100%;"><h4>Report Type</h4></td>
                            </tr>
                            <tr>
                                <td style="text-align:center;width:100%;margin: 0 auto;">
                                    <asp:RadioButtonList ID="rbReportType" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" CssClass="ReportButtons" onselectedindexchanged="rbReportType_SelectedIndexChanged" AutoPostBack="true" ToolTip="Ordered reports are purely for the EE Barrier Excel and Demographic Split reports only">
                                        <asp:ListItem Value="Comprehensive">Comprehensive Report<br /><i>(no less than 10 respondents)</i></asp:ListItem>
                                        <asp:ListItem Value="Executive">Executive Summary Report<br /><i>(no less than 10 respondents)</i></asp:ListItem>
                                        <asp:ListItem Value="EEBarrier">EE Barrier Report<br /><i>(no less than 150 respondents)</i></asp:ListItem>
                                        <asp:ListItem Value="Order">Ordered Report<br /><i>(no less than 150 respondents)</i></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>                    
                        </table>
                    </center>
                </div>
                <br />
                <div ID="ReportCriteria" runat="server" style="text-align:center;width:100%;">
                    <table style="width:100%;padding:5px;margin:0;background:#C8F475;text-align:center;-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;margin: 0 auto;">
                        <tr>
                            <td style="text-align:center;width:100%;"><h4>Report Criteria</h4></td>
                        </tr>
                        <tr>
                            <td><i>‘Not specified’ refers to unassigned data for that Demographic / Division</i></td>
                        </tr>
                        <tr>
                            <td style="text-align:center;width:100%;">
                                <table style="text-align:center;width:100%;">
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;">Compare Year:</td>
                                        <td>
                                            <table style="width:725px;padding:0;margin:0;">
                                                <tr>
                                                    <td style="text-align:left;width:100px;">2015</td>
                                                    <td style="text-align:center;width:25px;">vs</td>
                                                    <td style="text-align:left;width:100px;"><asp:DropDownList ID="ddlYear2" runat="server" width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlYear2_SelectedIndexChanged" CssClass="npbddl-ui"></asp:DropDownList></td>
                                                    <td style="width:425px">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;">Cluster:</td>
                                        <td>
                                            <table style="width:725px;padding:0;margin:0;">
                                                <tr>
                                                    <td style="text-align:left;">
                                                        <asp:DropDownList ID="ddlCluster" runat="server" Width="650px" AutoPostBack="true" OnSelectedIndexChanged="ddlCluster_SelectedIndexChanged" CssClass="npbddl-ui"></asp:DropDownList>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>                                
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <td style="width:100px">&nbsp;</td>
                                        <td>
                                            <table style="width:725px;padding:0;margin:0;">
                                                <tr>
                                                    <td style="width:350px" id="Year1Label" runat="server" align="center"><h4>2015</h4></td>
                                                    <td style="width:360px" id="Year2Label" runat="server" align="center"><h4><asp:Label ID="lblYear2" runat="server"></asp:Label></h4></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>                                
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;" title="Refers to the first business unit level directly below the cluster in question">Division 1:</td>
                                        <td>
                                            <table style="width:725px;padding:0;margin:0;">
                                                <tr>
                                                    <td style="width:350px" id="tdYear1Division1" runat="server" align="left"><asp:Label ID="lblYear1Division1" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear1Division1" runat="server" Text="..." width="20px" OnClick="cmdYear1Division1_Click" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year1Division1").dialog("open");return false;'/></td>
                                                    <td style="width:350px" id="tdYear2Division1" runat="server" align="left"><asp:Label ID="lblYear2Division1" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear2Division1" runat="server" Text="..." width="20px" OnClick="cmdYear2Division1_Click" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year2Division1").dialog("open");return false;'/></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;" title="Refers to the second business unit level directly below the cluster and first business unit level in question">Division 2:</td>
                                        <td>
                                            <table style="width:725px;padding:0;margin:0;">
                                                <tr>
                                                    <td style="width:350px" id="tdYear1Division2" runat="server" align="left"><asp:Label ID="lblYear1Division2" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear1Division2" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year1Division2").dialog("open");return false;' OnClick="cmdYear1Division2_Click"/></td>
                                                    <td style="width:350px" id="tdYear2Division2" runat="server" align="left"><asp:Label ID="lblYear2Division2" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear2Division2" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year2Division2").dialog("open");return false;' OnClick="cmdYear2Division2_Click"/></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;" title="Refers to the third business unit level directly below the cluster and first two business unit levels in question">Division 3:</td>
                                        <td>
                                            <table style="width:725px;padding:0;margin:0;">
                                                <tr>
                                                    <td style="width:350px" id="tdYear1Division3" runat="server" align="left"><asp:Label ID="lblYear1Division3" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear1Division3" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year1Division3").dialog("open");return false;' OnClick="cmdYear1Division3_Click"/></td>
                                                    <td style="width:350px" id="tdYear2Division3" runat="server" align="left"><asp:Label ID="lblYear2Division3" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear2Division3" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year2Division3").dialog("open");return false;' OnClick="cmdYear2Division3_Click"/></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;" title="Refers to the fourth business unit level directly below the cluster and first three business unit levels in question">Division 4:</td>
                                        <td>
                                            <table style="width:725px;padding:0;margin:0;">
                                                <tr>
                                                    <td style="width:350px" id="tdYear1Division4" runat="server" align="left"><asp:Label ID="lblYear1Division4" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear1Division4" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year1Division4").dialog("open");return false;' OnClick="cmdYear1Division4_Click"/></td>
                                                    <td style="width:350px" id="tdYear2Division4" runat="server" align="left"><asp:Label ID="lblYear2Division4" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear2Division4" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year2Division4").dialog("open");return false;' OnClick="cmdYear2Division4_Click"/></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;" title="Refers to the fifth business unit level directly below the cluster and first four business unit levels in question">Division 5:</td>
                                        <td>
                                            <table style="width:725px;padding:0;margin:0;">
                                                <tr>
                                                    <td style="width:350px" id="tdYear1Division5" runat="server" align="left"><asp:Label ID="lblYear1Division5" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear1Division5" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year1Division5").dialog("open");return false;' OnClick="cmdYear1Division5_Click"/></td>
                                                    <td style="width:350px" id="tdYear2Division5" runat="server" align="left"><asp:Label ID="lblYear2Division5" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear2Division5" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year2Division5").dialog("open");return false;' OnClick="cmdYear2Division5_Click"/></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;" title="Refers to the sixth business unit level directly below the cluster and first five business unit levels in question">Division 6:</td>
                                        <td>
                                            <table style="width:725px;padding:0;margin:0;">
                                                <tr>
                                                    <td style="width:350px" id="tdYear1Division6" runat="server" align="left"><asp:Label ID="lblYear1Division6" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear1Division6" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year1Division6").dialog("open");return false;' OnClick="cmdYear1Division6_Click"/></td>
                                                    <td style="width:350px" id="tdYear2Division6" runat="server" align="left"><asp:Label ID="lblYear2Division6" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear2Division6" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year2Division6").dialog("open");return false;' OnClick="cmdYear2Division6_Click"/></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;">Branch:</td>
                                        <td>
                                            <table style="width:725px;padding:0;margin:0;">
                                                <tr>
                                                    <td style="width:350px" id="tdYear1Branch" runat="server" align="left"><asp:Label ID="lblYear1Branch" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear1Branch" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year1Branch").dialog("open");return false;' OnClick="cmdYear1Branch_Click"/></td>
                                                    <td style="width:350px" id="tdYear2Branch" runat="server" align="left"><asp:Label ID="lblYear2Branch" runat="server" CssClass="singletextbox" Width="325px"></asp:Label>&nbsp;<asp:Button ID="cmdYear2Branch" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Year2Branch").dialog("open");return false;' OnClick="cmdYear2Branch_Click"/></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr id="trLocation" runat="server">
                                        <td style="padding-left:10px;text-align:left;width:125px;" title="Refers to the geographical location from a country/continent perspective e.g. UK, Africa,  Zimbabwe, International etc.">Location:</td>
                                        <td>
                                            <table style="width:725px;padding:0;margin:0;">
                                                <tr>
                                                    <td id="tdLocation" width="700px" runat="server" align="left"><asp:Label ID="lblLocation" runat="server" CssClass="singletextbox" width="650px"></asp:Label>&nbsp;<asp:Button ID="cmdLocation" runat="server" Text="..." width="20px" CssClass="btn" style="padding-left:2px;padding-right:2px;" OnClientClick='$("#dialog-Location").dialog("open");return false;' OnClick="cmdLocation_Click"/></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="AutoUpload" runat="server">
                                        <td style="padding-left:10px;text-align:center;" colspan="2"><asp:Button ID="cmdImport" runat="server" Text="Import Branch Codes or Staff Codes" CssClass="btn" OnClick="cmdImport_Click" OnClientClick='$("#dialog-Import").dialog("open");return false;' />&nbsp;<asp:Label id="lblImportResult" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;">Report Name:</td>
                                        <td>
                                            <table style="width:725px;padding:0;margin:0;">
                                                <tr>
                                                    <td style="text-align:left">
                                                        <asp:TextBox ID="txtReportName" runat="server" Width="650px" CssClass="singletextbox"></asp:TextBox>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>       
                </div>
                <br />
                <div ID="AdvancedCriteria" runat="server" style="text-align:center;width:100%;">
                    <table style="width:100%;padding:5px;margin:0;background:#BFCF89;text-align:center;-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;margin: 0 auto;">
                        <tr>
                            <td style="text-align:center;"><h4>Advanced Criteria</h4></td>
                        </tr>
                        <tr>
                            <td><i>Demographic filters available to further analyse</i></td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width:100%;">
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;" title="Refers to respondents within this demographic group that have been classified according to generation categories i.e. Baby Boomers, Generation Y etc.">Generational Classification:</td>
                                        <td><asp:DropDownList ID="ddlClassification" runat="server" Width="275px" AutoPostBack="true" OnSelectedIndexChanged="ddlClassification_SelectedIndexChanged" CssClass="npbddl-ui"></asp:DropDownList></td>
                                        <td style="padding-left:10px;text-align:left;width:125px;" title="Refers to pre-defined Nedbank classifications categorising staff into different job types/functions.  For example, finance, credit, business management, human resources etc.">Job Family:</td>
                                        <td><asp:DropDownList ID="ddlJobFamily" runat="server" Width="275px" AutoPostBack="true" OnSelectedIndexChanged="ddlJobFamily_SelectedIndexChanged" CssClass="npbddl-ui"></asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;">Length of service:</td>
                                        <td><asp:DropDownList ID="ddlLOS" runat="server" Width="275px" AutoPostBack="true" OnSelectedIndexChanged="ddlLOS_SelectedIndexChanged" CssClass="npbddl-ui"></asp:DropDownList></td>
                                        <td style="padding-left:10px;text-align:left;width:125px;" title="Refers to the provincial districts within South Africa e.g. Gauteng, Eastern Cape etc.">Region:</td>
                                        <td><asp:DropDownList ID="ddlRegion" runat="server" Width="275px" AutoPostBack="true" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" CssClass="npbddl-ui"></asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left:10px;text-align:left;width:125px;">Management Passage:</td>
                                        <td><asp:DropDownList ID="ddlPassage" runat="server" Width="275px" AutoPostBack="true" OnSelectedIndexChanged="ddlPassage_SelectedIndexChanged" CssClass="npbddl-ui"></asp:DropDownList></td>
                                        <td style="padding-left:10px;text-align:left;width:125px;" id="advRace1" runat="server">Race:</td>
                                        <td id="advRace2" runat="server"><asp:DropDownList ID="ddlRace" runat="server" Width="275px" AutoPostBack="true" OnSelectedIndexChanged="ddlRace_SelectedIndexChanged" CssClass="npbddl-ui"></asp:DropDownList></td>
                                    </tr>
                                    <tr id="advee" runat="server">
                                        <td style="padding-left:10px;text-align:left;width:125px;" title="Refers to respondents that are categorised according to pre-determined employment equity occupation levels. For example, senior management, junior management, secondees etc.">EE Occupational Level:</td>
                                        <td><asp:DropDownList ID="ddlOccupationalLevel" runat="server" Width="275px" AutoPostBack="true" OnSelectedIndexChanged="ddlOccupationalLevel_SelectedIndexChanged" CssClass="npbddl-ui"></asp:DropDownList></td>
                                        <td style="padding-left:10px;text-align:left;width:125px;">Gender:</td>
                                        <td><asp:DropDownList ID="ddlGender" runat="server" Width="275px" AutoPostBack="true" OnSelectedIndexChanged="ddlGender_SelectedIndexChanged" CssClass="npbddl-ui"></asp:DropDownList></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <div ID="ReportComposition" runat="server" style="text-align:center;width:100%;">
                    <table style="width:100%;padding:5px;margin:0;background:#94B458;text-align:center;-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;margin: 0 auto;">
                        <tr>
                            <td style="text-align:center;"><h4>Report Composition</h4></td>
                        </tr>
                        <tr>
                            <td><i>Select the demographic parameters you would like shown in the report</i></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="cblComposition" runat="server" RepeatDirection="Horizontal" RepeatColumns="4" CssClass="ReportButtons" Width="100%">
                                    <asp:ListItem>Gender</asp:ListItem>
                                    <asp:ListItem title="Refers to the provincial districts within South Africa e.g. Gauteng, Eastern Cape etc.">Region</asp:ListItem>
                                    <asp:ListItem>Race</asp:ListItem>
                                    <asp:ListItem title="Refers to pre-defined Nedbank classifications categorising staff into different job types/functions.  For example, finance, credit, business management, human resources etc.">Job Family</asp:ListItem>
                                    <asp:ListItem>Age</asp:ListItem>
                                    <asp:ListItem>Disability</asp:ListItem>
                                    <asp:ListItem>Staff Category</asp:ListItem>
                                    <asp:ListItem>Management Passage</asp:ListItem>
                                    <asp:ListItem>Length of service</asp:ListItem>
                                    <asp:ListItem title="Refers to respondents that are categorised according to pre-determined employment equity occupation levels. For example, senior management, junior management, secondees etc.">EE Occupational Level</asp:ListItem>
                                    <asp:ListItem title="Refers to respondents within this demographic group that have been classified according to generation categories i.e. Baby Boomers, Generation Y etc.">Generational Classification</asp:ListItem>
                                    <asp:ListItem title="Refers to the geographical location from a country/continent perspective e.g. UK, Africa,  Zimbabwe, International etc.">Location</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <div ID="Buttons" runat="server" style="text-align:center;width:100%;">
                    <center>
                        <table style="width:100%;padding:5px;margin:0;background:#94B458;text-align:center;-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;margin: 0 auto;">
                            <tr>
                                <td style="text-align:center;width:100%;">
                                    <table style="width:100%;text-align:center;">
                                        <tr>
                                            <td><asp:Button ID="cmdReport" runat="server" Text="View Report" onclick="cmdReport_Click" CssClass="btn"/><asp:Button ID="cmdPDF" runat="server" Text="Export to PDF" onclick="cmdPDF_Click" CssClass="btn"/><asp:Button ID="cmdPPT" runat="server" Text="Export to PowerPoint" onclick="cmdPPT_Click" CssClass="btn"/><asp:Button ID="cmdExcel" runat="server" Text="Export to Excel" onclick="cmdExcel_Click" CssClass="btn" /><asp:Button ID="cmdOrder" runat="server" Text="Order Report" onclick="cmdOrder_Click" CssClass="btn" /><asp:Button ID="cmdViewOrder" runat="server" Text="View Orders" onclick="cmdViewOrder_Click" CssClass="btn" /><asp:Button ID="cmdInfograph" runat="server" Text="Generate Infograph" CssClass="btn" onclick="cmdInfograph_Click" /><asp:Button ID="cmdHistory" runat="server" Text="Report History" CssClass="btn" onclick="cmdHistory_Click"/></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="color:#fff;text-align:center;"><b><asp:Label ID="lblSampleSize" runat="server"></asp:Label></b><asp:HiddenField ID="hdSample1" runat="server" /><asp:HiddenField ID="hdSample2" runat="server" /><asp:HiddenField ID="hdTotal1" runat="server" /><asp:HiddenField ID="hdTotal2" runat="server" /></td>
                            </tr>
                        </table>
                    </center>
                </div>
                <br />
                <div>
                    <i>Please note that a detailed description will appear if you should hover over the following labels: Division 1, Division 2, Division 3, Division 4, Division 5, Division 6, Branch, Location, Generational Classification, EE Occupational Level, Management Passage, Job Family and Region.</i>
                </div>
                <iframe id="iframe" src="" runat="server" width="0px" height="0px"></iframe>
            </ContentTemplate>
            <Triggers>                
                <asp:AsyncPostBackTrigger ControlID="ddlCluster" EventName="SelectedIndexChanged"/>
            </Triggers>
        </asp:UpdatePanel>    
        <asp:UpdateProgress ID="udpFilters" runat="server" AssociatedUpdatePanelID="upFilters">
            <ProgressTemplate>
                <div class="overlay"/>
                <div class="overlayContent">                    
                    <img src="<%=GetSiteRoot() %>/Images/processing.gif" alt="Loading" border="1" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>           
    </div>
    <div id="dialog-Year1Division1" title="Division 1" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear1Division1" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y1D1All() {
                        $("#<%=chkYear1Division1.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY1D1All.ClientID%>').is(':checked'));
                    }

                    function CheckY1D1All() {
                        if ($("#<%=chkYear1Division1.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear1Division1.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY1D1All.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY1D1All.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY1D1All" runat="server" Text="All" onclick="Y1D1All();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear1Division1" runat="server" RepeatDirection="Vertical" onclick="CheckY1D1All();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year1Division2" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear1Division2" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                     function Y1D2All() {
                         $("#<%=chkYear1Division2.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY1D2All.ClientID%>').is(':checked'));
                    }

                    function CheckY1D2All() {
                        if ($("#<%=chkYear1Division2.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear1Division2.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY1D2All.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY1D2All.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY1D2All" runat="server" Text="All" onclick="Y1D2All();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear1Division2" runat="server" RepeatDirection="Vertical" onclick="CheckY1D2All();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year1Division3" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear1Division3" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y1D3All() {
                        $("#<%=chkYear1Division3.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY1D3All.ClientID%>').is(':checked'));
                     }

                     function CheckY1D3All() {
                         if ($("#<%=chkYear1Division3.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear1Division3.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY1D3All.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY1D3All.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY1D3All" runat="server" Text="All" onclick="Y1D3All();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear1Division3" runat="server" RepeatDirection="Vertical" onclick="CheckY1D3All();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year1Division4" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear1Division4" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y1D4All() {
                        $("#<%=chkYear1Division4.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY1D4All.ClientID%>').is(':checked'));
                    }

                    function CheckY1D4All() {
                        if ($("#<%=chkYear1Division4.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear1Division4.ClientID%> input[type='checkbox']").length) {
                             $('#<%=chkY1D4All.ClientID%>').prop('checked', true);
                         }
                         else {
                             $('#<%=chkY1D4All.ClientID%>').prop('checked', false);
                         }
                     }
                </script>
                <asp:CheckBox ID="chkY1D4All" runat="server" Text="All" onclick="Y1D4All();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear1Division4" runat="server" RepeatDirection="Vertical" onclick="CheckY1D4All();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year1Division5" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear1Division5" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y1D5All() {
                        $("#<%=chkYear1Division5.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY1D5All.ClientID%>').is(':checked'));
                    }

                    function CheckY1D5All() {
                        if ($("#<%=chkYear1Division5.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear1Division5.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY1D5All.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY1D5All.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY1D5All" runat="server" Text="All" onclick="Y1D5All();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear1Division5" runat="server" RepeatDirection="Vertical" onclick="CheckY1D5All();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year1Division6" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear1Division6" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y1D6All() {
                        $("#<%=chkYear1Division6.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY1D6All.ClientID%>').is(':checked'));
                    }

                    function CheckY1D6All() {
                        if ($("#<%=chkYear1Division6.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear1Division6.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY1D6All.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY1D6All.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY1D6All" runat="server" Text="All" onclick="Y1D6All();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear1Division6" runat="server" RepeatDirection="Vertical" onclick="CheckY1D6All();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year1Branch" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear1Branch" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y1BranchAll() {
                        $("#<%=chkYear1Branch.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY1BAll.ClientID%>').is(':checked'));
                    }

                    function CheckY1BranchAll() {
                        if ($("#<%=chkYear1Branch.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear1Branch.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY1BAll.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY1BAll.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY1BAll" runat="server" Text="All" onclick="Y1BranchAll();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear1Branch" runat="server" RepeatDirection="Vertical" onclick="CheckY1BranchAll();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year2Division1" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear2Division1" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y2D1All() {
                        $("#<%=chkYear2Division1.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY2D1All.ClientID%>').is(':checked'));
                    }

                    function CheckY2D1All() {
                        if ($("#<%=chkYear2Division1.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear2Division1.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY2D1All.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY2D1All.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY2D1All" runat="server" Text="All" onclick="Y2D1All();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear2Division1" runat="server" RepeatDirection="Vertical" onclick="CheckY2D1All();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year2Division2" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear2Division2" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y2D2All() {
                        $("#<%=chkYear2Division2.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY2D2All.ClientID%>').is(':checked'));
                    }

                    function CheckY2D2All() {
                        if ($("#<%=chkYear2Division2.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear2Division2.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY2D2All.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY2D2All.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY2D2All" runat="server" Text="All" onclick="Y2D2All();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear2Division2" runat="server" RepeatDirection="Vertical" onclick="CheckY2D2All();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year2Division3" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear2Division3" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y2D3All() {
                        $("#<%=chkYear2Division3.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY2D3All.ClientID%>').is(':checked'));
                    }

                    function CheckY2D3All() {
                        if ($("#<%=chkYear2Division3.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear2Division3.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY2D3All.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY2D3All.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY2D3All" runat="server" Text="All" onclick="Y2D3All();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear2Division3" runat="server" RepeatDirection="Vertical" onclick="CheckY2D3All();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year2Division4" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear2Division4" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y2D4All() {
                        $("#<%=chkYear2Division4.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY2D4All.ClientID%>').is(':checked'));
                    }

                    function CheckY2D4All() {
                        if ($("#<%=chkYear2Division4.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear2Division4.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY2D4All.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY2D4All.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY2D4All" runat="server" Text="All" onclick="Y2D4All();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear2Division4" runat="server" RepeatDirection="Vertical" onclick="CheckY2D4All();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year2Division5" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear2Division5" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y2D5All() {
                        $("#<%=chkYear2Division5.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY2D5All.ClientID%>').is(':checked'));
                    }

                    function CheckY2D5All() {
                        if ($("#<%=chkYear2Division5.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear2Division5.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY2D5All.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY2D5All.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY2D5All" runat="server" Text="All" onclick="Y2D5All();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear2Division5" runat="server" RepeatDirection="Vertical" onclick="CheckY2D5All();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year2Division6" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear2Division6" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y2D6All() {
                        $("#<%=chkYear2Division6.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY2D6All.ClientID%>').is(':checked'));
                    }

                    function CheckY2D6All() {
                        if ($("#<%=chkYear2Division6.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear2Division6.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY2D6All.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY2D6All.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY2D6All" runat="server" Text="All" onclick="Y2D6ll();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear2Division6" runat="server" RepeatDirection="Vertical" onclick="CheckY2D6All();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Year2Branch" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upYear2Branch" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function Y2BranchAll() {
                        $("#<%=chkYear2Branch.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkY2BAll.ClientID%>').is(':checked'));
                    }

                    function CheckY2BranchAll() {
                        if ($("#<%=chkYear2Branch.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkYear2Branch.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkY2BAll.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkY2BAll.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkY2BAll" runat="server" Text="All" onclick="Y2BranchAll();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkYear2Branch" runat="server" RepeatDirection="Vertical" onclick="CheckY2BranchAll();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Location" title="Please Select" class="ModalWindow">           
        <asp:UpdatePanel ID="upLocation" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <script type="text/javascript">
                    function LocationAll() {
                        $("#<%=chkLocation.ClientID%> input[type='checkbox']").prop('checked', $('#<%=chkLocationAll.ClientID%>').is(':checked'));
                    }

                    function CheckLocationAll() {
                        if ($("#<%=chkLocation.ClientID%> input[type='checkbox']:checked").length == $("#<%=chkLocation.ClientID%> input[type='checkbox']").length) {
                            $('#<%=chkLocationAll.ClientID%>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkLocationAll.ClientID%>').prop('checked', false);
                        }
                    }
                </script>
                <asp:CheckBox ID="chkLocationAll" runat="server" Text="All" onclick="LocationAll();"/>
                <hr />
                <div style="max-height:200px;overflow-y:scroll;">
                    <asp:CheckBoxList ID="chkLocation" runat="server" RepeatDirection="Vertical" onclick="CheckLocationAll();"></asp:CheckBoxList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="dialog-Import" title="Import Branch Codes or Staff Codes" class="ModalWindow">           
        <asp:UpdatePanel ID="upImport" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <table style="width:100%;">
                    <tr>
                        <td colspan="2"><asp:RadioButtonList ID="rbType" runat="server" RepeatDirection="Horizontal"><asp:ListItem>Branch Codes</asp:ListItem><asp:ListItem>Staff Codes</asp:ListItem></asp:RadioButtonList></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input id="image_upload" type="file" onchange="fileupload(this);"/><br /> (Note that only Microsoft Excel files are allowed)</td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td style="width:50%;vertical-align:top;">
                            <table>
                                <tr class="trsheet"><td><b>Sheet : </b></td><td><select id=combobox></select></td></tr>
                                <tr class="trcolumn"><td><b>Column : </b></td><td><select id=combobox1></select></td></tr>
                            </table>
                        </td>
                        <td style="width:50%;">
                            <table style="width:100%;">
                                <tr><td style="width:100%;"><text id=textPreview></text></td></tr> 
                            </table>
                        </td>
                    </tr>
                    <asp:HiddenField ID="hdFileName" runat="server" />
                    <asp:HiddenField ID="hdSheet" runat="server" />
                    <asp:HiddenField ID="hdColumn" runat="server" />                                 
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>         
    </div>
    <div id="divConfirm" class="ModalWindow"></div>
</asp:Content>
