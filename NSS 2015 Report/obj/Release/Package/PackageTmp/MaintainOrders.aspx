﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MaintainOrders.aspx.cs" Inherits="NSS_2015_Report.MaintainOrders" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <center>  
        <div style="width:100%;">
            <asp:Panel ID="pnlCriteria" runat="server" BackColor="#BFCF89" HorizontalAlign="Center" Width="100%">
                <table style="width:100%;" cellpadding="5" cellspacing="0">
                    <tr>
                        <td style="text-align:center"><h2>Maintain Orders</h2></td>
                    </tr>
                    <tr>
                        <td style="text-align:center">Please update the relevant fields mark by an asterisk(*)</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 15px;padding-right:15px;text-align:left;">
                            <fieldset style="border:1px solid black;-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;">
                                <legend><b>Report Details</b></legend>
                                <table style="width:100%;">
                                    <tr>
                                        <td style="text-align:left;">Branch Code:</td>
                                        <td style="text-align:left;vertical-align:top;" colspan="3"><asp:Label ID="lblBranchCode" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Financial Officer’s Name:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblOName" runat="server"></asp:Label></td>
                                        <td style="text-align:left;">Financial Officer’s Email:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblOEmail" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Contact Person:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblPerson" runat="server"></asp:Label></td>
                                        <td style="text-align:left;">Contact e-mail:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblEmail" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Manual Invoice No(*):</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:TextBox ID="txtMInvoiceNo" runat="server" CssClass="Textbox"></asp:TextBox><asp:Label ID="lblMInvoiceNo" runat="server" Visible="false"></asp:Label></td>
                                        <td style="text-align:left;">Price:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblPrice" runat="server" Text="R3,400"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Order Status(*):</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:DropDownList ID="ddlOrderStatus" runat="server" CssClass="DropDownlist" Width="157px"></asp:DropDownList><asp:Label ID="lblOrderStatus" runat="server" Visible="false"></asp:Label></td>
                                        <td style="text-align:left;">Invoice Status(*):</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:DropDownList ID="ddlInvoiceStatus" runat="server" CssClass="DropDownlist" Width="157px"></asp:DropDownList><asp:Label ID="lblInvoiceStatus" runat="server" Visible="false"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Generated by:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblUser" runat="server"></asp:Label></td>
                                        <td style="text-align:left;">Date Requested:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblDateRequested" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Report Type:</td>
                                        <td style="text-align:left;vertical-align:top;" colspan="3"><asp:Label ID="lblRType" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Report Name:</td>
                                        <td style="text-align:left;vertical-align:top;" colspan="3"><asp:Label ID="lblReportName" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Comparison Set:</td>
                                        <td style="text-align:left;vertical-align:top;" colspan="3"><asp:Label ID="lblDataset" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Cluster:</td>
                                        <td style="text-align:left;vertical-align:top;" colspan="3"><asp:Label ID="lblCluster" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr>                                                                                                    
                                        <td style="text-align:left;vertical-align:top;" colspan="2"><b>Applied Division Filters for <asp:Label ID="lblYear1" runat="server"></asp:Label> :</b></td>
                                        <td style="text-align:left;vertical-align:top;" colspan="2"><b>Applied Division Filters for <asp:Label ID="lblYear2" runat="server"></asp:Label> :</b></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Division 1:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Division1" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Division 1:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Division1" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Division 2:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Division2" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Division 2:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Division2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Division 3:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Division3" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Division 3:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Division3" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;"">Division 4:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Division4" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Division 4:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Division4" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Division 5:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Division5" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Division 5:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Division5" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Division 6:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Division6" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Division 6:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Division6" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Branch:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear1Branch" runat="server"></asp:Label></td>
                                        <td style="text-align:left;vertical-align:top;">Branch:</td>
                                        <td style="text-align:left;vertical-align:top;"><asp:Label ID="lblYear2Branch" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr>
                                        <td style="text-align:left;vertical-align:top;">Location:</td>
                                        <td style="text-align:left;vertical-align:top;" colspan="3"><asp:Label ID="lblLocation" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Generational Classification:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblGeneralClassification" runat="server"></asp:Label></td>
                                        <td style="text-align:left;">Job Family:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblJobFamily" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Length of service:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblLOS" runat="server"></asp:Label></td>
                                        <td style="text-align:left;">Region:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblRegion" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Management Passage:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblMngPassage" runat="server"></asp:Label></td>
                                        <td style="text-align:left;">Race:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblRace" runat="server"></asp:Label></td>           
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">EE Occupational Level:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblOccLevel" runat="server"></asp:Label></td>
                                        <td style="text-align:left;">Gender:</td>
                                        <td style="text-align:left;"><asp:Label ID="lblGender" runat="server"></asp:Label></td> 
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">Compare Year:</td>
                                        <td style="text-align:left;" colspan="3"><asp:Label ID="lblYear" runat="server"></asp:Label></td>    
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center"><asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn" onclick="btnSubmit_Click" /><asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn" onclick="btnClose_Click"/></td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </center>
</asp:Content>
