﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="NSS_2015_Report.Login" %>

<!DOCTYPE html>
<html lang="en">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title>NSS 2015</title>
    <asp:PlaceHolder ID="PlaceHolder1" runat="server">     
          <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>  
    <webopt:BundleReference ID="BundleReference1" runat="server" Path="~/Content/css" /> 
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <meta name="viewport" content="width=device-width" />
    <!--[if lt IE 9]>
        <script src="<%=GetSiteRoot()%>/Scripts/jquery-1.11.2.js"></script>
    <![endif]-->
    <!--[if (gte IE 9) | (!IE)]><!-->
    <script src="<%=GetSiteRoot()%>/Scripts/jquery-2.1.4.js"></script>
    <!--<![endif]-->
    <link href="Content/themes/base/all.css" rel="stylesheet" />
    <link href="Content/themes/base/all.css" rel="stylesheet" />    
    <script src="<%=GetSiteRoot()%>/Scripts/jquery.placeholder.js"></script>
    <script type="text/javascript">
        $(function () {
            $('input, textarea').placeholder();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=272931&clcid=0x409 --%>
            <%--Framework Scripts--%>

            <asp:ScriptReference Name="MsAjaxBundle" />
            <asp:ScriptReference Name="jquery.ui.combined" />
            <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
            <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
            <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
            <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
            <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
            <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
            <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
            <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
            <asp:ScriptReference Name="WebFormsBundle" />
            <%--Site Scripts--%>
        </Scripts>
    </asp:ScriptManager>
    <script type="text/javascript">                    
        function Login() {
            if ($('#<%=txtUsername.ClientID%>').val() == "" || $('#<%=txtPassword.ClientID%>').val() == "") alert("Please supply both a username and password.");
            else {
                $.ajax({
                    type: "POST",
                    url: "<%=GetSiteRoot()%>/login.aspx/ValidateLogin",
                    data: JSON.stringify({ "username": $('#<%=txtUsername.ClientID %>').val(), "password": $('#<%=txtPassword.ClientID%>').val() }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d) {
                            <%= Page.ClientScript.GetPostBackEventReference(cmdLogin, string.Empty) %>;
                            return true;
                        }
                        else {
                            $('#<%=txtPassword.ClientID%>').val("");
                            alert("Invalid username or password");
                        }
                    },
                    failure: function (data) {
                        alert(response.d);
                    }
                });
            }
        }
    </script>
    <div class="standalone">
        <div class="header">
            <div class="logos clearfix">
                <a class="logo left">
                    <img src="<%=GetSiteRoot() %>/images/NSS_survey.jpg" /></a>
            </div>
        </div>
        <div class="login-wrapper clearfix">
            <ul class="form" style="text-align:center;margin:0 auto;">
                <li>
                    <asp:TextBox ID="txtUsername" runat="server" placeholder="Your email address" CssClass="logintextbox email-icon"></asp:TextBox></li>
                <li>
                    <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" TextMode="Password" CssClass="logintextbox password-icon"></asp:TextBox></li>
                <li class="clearfix">
                    <asp:CheckBox ID="chkRememberMe" runat="server" CssClass="left" Text="Remember me" />                    
                </li>
                <li>
                    <asp:Button ID="cmdLogin" runat="server" Text="LOGIN" CssClass="btn" OnClick="cmdLogin_Click" OnClientClick="Login();return false;" />
                </li>
            </ul>
        </div>
    </div>
    </form>
</body>
</html>
