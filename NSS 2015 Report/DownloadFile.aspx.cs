﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NSS_2015_Report
{
    public partial class DownloadFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["File"] != null)
            {
                DownLoadFile(Request.QueryString["File"].ToString());
            }
        }

        public void DownLoadFile(string filename)
        {
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/" + (Request.QueryString["Type"].ToString() == "excel" ? "vnd.ms-excel" : Request.QueryString["Type"].ToString());
            Response.AppendHeader("content-disposition", "attachment; filename=" + filename);
            Response.TransmitFile(Server.MapPath("~/exports" + "/" + filename));
            Response.Flush();
        }
    }
}