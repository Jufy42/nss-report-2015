﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NSS_2015_Report
{
    public partial class MaintainOrders : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (GetCurrentUser() == null) Response.Redirect("~/login");
                else
                {
                    OrderDetail order = db.OrderDetails.Where(p => p.OrderID == new Guid(RouteData.Values["OrderID"].ToString())).Distinct().SingleOrDefault();

                    if ((bool)GetCurrentUser().IsAdmin)
                    {
                        ddlInvoiceStatus.Visible = ddlOrderStatus.Visible = btnSubmit.Visible = true;
                        lblInvoiceStatus.Visible = lblOrderStatus.Visible = false;

                        ddlInvoiceStatus.DataSource = db.InvoiceStatus.Select(p => new { text = p.Description, value = p.InvoiceSeq }).Distinct().ToList();
                        ddlInvoiceStatus.DataTextField = "text";
                        ddlInvoiceStatus.DataValueField = "value";
                        ddlInvoiceStatus.DataBind();
                        ddlInvoiceStatus.SelectedValue = order.InvoiceSeq.ToString();

                        ddlOrderStatus.DataSource = db.OrderStatus.Select(p => new { text = p.Description, value = p.OrderSeq }).Distinct().ToList();
                        ddlOrderStatus.DataTextField = "text";
                        ddlOrderStatus.DataValueField = "value";
                        ddlOrderStatus.DataBind();
                        ddlOrderStatus.SelectedValue = order.OrderSeq.ToString();
                    }
                    else
                    {
                        ddlInvoiceStatus.Visible = ddlOrderStatus.Visible = btnSubmit.Visible = false;
                        lblInvoiceStatus.Visible = lblOrderStatus.Visible = true;

                        lblInvoiceStatus.Text = db.InvoiceStatus.Where(p => p.InvoiceSeq == order.InvoiceSeq).Distinct().SingleOrDefault().Description;
                        lblOrderStatus.Text = db.OrderStatus.Where(p => p.OrderSeq == order.OrderSeq).Distinct().SingleOrDefault().Description;
                    }

                    lblBranchCode.Text = order.BranchCode;
                    lblOName.Text = order.FOName;
                    lblOEmail.Text = order.FOEmail;
                    lblPerson.Text = order.ContactPerson;
                    lblEmail.Text = order.Email;
                    lblUser.Text = GetCurrentUser().Name;
                    lblDateRequested.Text = Convert.ToDateTime(order.DateRequested).Date.ToString("dd/MM/yyyy");

                    ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == order.FilterSelection).SingleOrDefault();

                    string reportname = history.ReportName;
                    string cluster = history.Cluster;
                    lblYear1Division1.Text = history.Y1Division1;
                    lblYear1Division2.Text = history.Y1Division2;
                    lblYear1Division3.Text = history.Y1Division3;
                    lblYear1Division4.Text = history.Y1Division4;
                    lblYear1Division5.Text = history.Y1Division5;
                    lblYear1Division6.Text = history.Y1Division6;
                    lblYear1Branch.Text = history.Y1Branch;
                    lblYear2Division1.Text = history.Y2Division1;
                    lblYear2Division2.Text = history.Y2Division2;
                    lblYear2Division3.Text = history.Y2Division3;
                    lblYear2Division4.Text = history.Y2Division4;
                    lblYear2Division5.Text = history.Y2Division5;
                    lblYear2Division6.Text = history.Y2Division6;
                    lblYear2Branch.Text = history.Y2Branch;

                    string sample = cluster == "All" ? "Post Sample" : "";
                    string year2 = history.Year2;

                    lblLocation.Text = history.Location;
                    lblGeneralClassification.Text = history.Classification;
                    lblJobFamily.Text = history.JobFamily;
                    lblLOS.Text = history.LOS;
                    lblRegion.Text = history.Region;
                    lblMngPassage.Text = history.ManagementPassage;
                    lblRace.Text = history.Race;
                    lblOccLevel.Text = history.OccupationalLevel;
                    lblGender.Text = history.Gender;

                    lblYear.Text = "2015" + (year2 == "" ? "" : " vs " + year2);

                    lblReportName.Text = reportname + " " + sample;
                }
            }
        }
    }
}