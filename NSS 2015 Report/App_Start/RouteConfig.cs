using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace NSS_2015_Report
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("Filters", "", "~/default.aspx", false, new RouteValueDictionary { });
            routes.MapPageRoute("History", "history", "~/history.aspx", false, new RouteValueDictionary { });
            routes.MapPageRoute("Orders", "orders", "~/vieworders.aspx", false, new RouteValueDictionary { });
            routes.MapPageRoute("Login", "login", "~/login.aspx", false, new RouteValueDictionary { });
            routes.MapPageRoute("View History", "history/{ReportGenerationID}", "~/historyview.aspx", false, new RouteValueDictionary { { "ReportGenerationID", null } });
            routes.MapPageRoute("Comp", "comp/{ReportHistoryID}", "~/CompExecReport.aspx", false, new RouteValueDictionary { { "ReportHistoryID", null }, { "Report", "comp" }, { "Format", "online" } });
            routes.MapPageRoute("EE", "ee/{ReportHistoryID}", "~/EEBarrierReport.aspx", false, new RouteValueDictionary { { "ReportHistoryID", null }, { "Report", "ee" }, { "Format", "online" } });
            routes.MapPageRoute("Exec", "exec/{ReportHistoryID}", "~/CompExecReport.aspx", false, new RouteValueDictionary { { "ReportHistoryID", null }, { "Report", "exec" }, { "Format", "online" } });
            routes.MapPageRoute("CompPDF", "comppdf/{ReportHistoryID}", "~/CompExecReport.aspx", false, new RouteValueDictionary { { "ReportHistoryID", null }, { "Report", "comp" }, { "Format", "pdf" } });
            routes.MapPageRoute("EEPDF", "eepdf/{ReportHistoryID}", "~/EEBarrierReport.aspx", false, new RouteValueDictionary { { "ReportHistoryID", null }, { "Report", "ee" }, { "Format", "pdf" } });
            routes.MapPageRoute("ExecPDF", "execpdf/{ReportHistoryID}", "~/CompExecReport.aspx", false, new RouteValueDictionary { { "ReportHistoryID", null }, { "Report", "exec" }, { "Format", "pdf" } });
            routes.MapPageRoute("CompPPT", "compppt/{ReportHistoryID}", "~/CompExecReport.aspx", false, new RouteValueDictionary { { "ReportHistoryID", null }, { "Report", "comp" }, { "Format", "ppt" } });
            routes.MapPageRoute("EEPPT", "eeppt/{ReportHistoryID}", "~/EEBarrierReport.aspx", false, new RouteValueDictionary { { "ReportHistoryID", null }, { "Report", "ee" }, { "Format", "ppt" } });
            routes.MapPageRoute("ExecPPT", "execppt/{ReportHistoryID}", "~/CompExecReport.aspx", false, new RouteValueDictionary { { "ReportHistoryID", null }, { "Report", "exec" }, { "Format", "ppt" } });
            routes.MapPageRoute("Infograph", "infograph/{ReportHistoryID}", "~/infograph.aspx", false, new RouteValueDictionary { { "ReportHistoryID", null } });
            routes.MapPageRoute("Infograph2", "infograph2/{ReportHistoryID}", "~/infograph2.aspx", false, new RouteValueDictionary { { "ReportHistoryID", null } });
            routes.MapPageRoute("New Order", "neworder/{ReportHistoryID}", "~/orderdetails.aspx", false, new RouteValueDictionary { { "ReportHistoryID", null } });
            routes.MapPageRoute("View Order", "vieworder/{OrderID}", "~/maintainorders.aspx", false, new RouteValueDictionary { { "OrderID", null } });
            routes.EnableFriendlyUrls();
        }
    }
}
