﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NSS_2015_Report
{
    public partial class Login : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected void cmdLogin_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text == "" || txtPassword.Text == "")
                AlertShow("Please supply both a username and password.");
            else
            {
                using (NSS2015Report db = new NSS2015Report())
                {
                    User user = db.Users.Where(p => p.UserName.Trim().ToLower() == txtUsername.Text.Trim().ToLower() && p.Password == txtPassword.Text).SingleOrDefault();

                    if (user == null)
                        AlertShow("Invalid username or password.");
                    else
                    {
                        HttpCookie cookie = new HttpCookie("NSS2015R");
                        cookie["UserID"] = user.UserID.ToString();
                        cookie.Expires = DateTime.Now.AddDays(7);
                        Response.Cookies.Add(cookie);

                        Response.Redirect("~/");
                    }
                }
            }
        }
    }
}