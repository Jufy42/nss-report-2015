﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NSS_2015_Report
{
    public partial class EEBarrierReport : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.iframe.Attributes.Add("src", "");

                bool pdf = RouteData.Values["Format"].ToString() == "pdf" || RouteData.Values["Format"].ToString() == "ppt";

                if (pdf)
                {
                    BottomButtons.Visible = iframe.Visible = prgLoadingStatus.Visible = false;
                    Reportdiv.Attributes["style"] = "width:100%;background-color:White;color:Black;text-align:center;padding:0;margin:0;";
                }
                else
                {
                    BottomButtons.Visible = iframe.Visible = prgLoadingStatus.Visible = true;
                    Reportdiv.Attributes["style"] = "width:100%;background-color:White;color:Black;Height:768px;overflow-y:scroll;text-align:center;padding:0;margin:0;";
                }

                Guid HistoryID = new Guid(RouteData.Values["ReportHistoryID"].ToString());
                ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == HistoryID).SingleOrDefault();

                List<Year> year1Dataset = new List<Year>();
                List<Year> year2Dataset = new List<Year>();
                List<Year> year1NonPermDataset = new List<Year>();
                List<Year> year2NonPermDataset = new List<Year>();
                List<Year> year1GroupDataset = new List<Year>();
                List<Year> year2GroupDataset = new List<Year>();
                List<Year> year1ClustDataset = new List<Year>();
                List<Year> year2ClustDataset = new List<Year>();
                EEBarrierDimension eedimensionyear1 = new EEBarrierDimension();
                EEBarrierDimension eedimensionyear2 = new EEBarrierDimension();
                List<StatementShift> statementshifts = new List<StatementShift>();

                List<Question> questions = db.Questions.ToList();
                List<Dimension> dimensions = db.Dimensions.ToList();

                string reportname = history.ReportName;
                string cluster = history.Cluster;
                string year1division1 = history.Y1Division1;
                string year1division2 = history.Y1Division2;
                string year1division3 = history.Y1Division3;
                string year1division4 = history.Y1Division4;
                string year1division5 = history.Y1Division5;
                string year1division6 = history.Y1Division6;
                string year1branch = history.Y1Branch;
                string year2division1 = history.Y2Division1;
                string year2division2 = history.Y2Division2;
                string year2division3 = history.Y2Division3;
                string year2division4 = history.Y2Division4;
                string year2division5 = history.Y2Division5;
                string year2division6 = history.Y2Division6;
                string year2branch = history.Y2Branch;

                string year2 = history.Year2;

                string classification = history.Classification;
                string occlevel = history.OccupationalLevel;
                string los = history.LOS;
                string passage = history.ManagementPassage;
                string location = history.Location;
                string gender = history.Gender;
                string race = history.Race;
                string jobfamily = history.JobFamily;
                string region = history.Region;

                year1Dataset = year.YearDataSet();
                year2Dataset = year.YearDataSet(year2);

                year1GroupDataset = year1Dataset = year1Dataset.Where(p => p.Location.Trim().ToLower() == "south africa").Distinct().ToList();
                year2GroupDataset = year2Dataset = year2Dataset.Where(p => p.Location.Trim().ToLower() == "south africa").Distinct().ToList();

                if (year2 == "2010") year2Dataset = year2Dataset.Where(p => p.Cluster.Trim().ToLower() != "imperial bank").Distinct().ToList();

                if (cluster != "All")
                {
                    year1Dataset = year1Dataset.Where(p => p.Cluster.ToLower().Trim() == cluster.ToLower().Trim()).Distinct().ToList();

                    if ((cluster != "RBB" && cluster != "CIB") || (year2division1 == "All"
                        && year2division2 == "All"
                        && year2division3 == "All"
                        && year2division4 == "All"
                        && year2division5 == "All"
                        && year2division6 == "All"
                        && year2branch == "All")) year2Dataset = year2Dataset.Where(p => p.Cluster.ToLower().Trim() == cluster.ToLower().Trim()).Distinct().ToList();

                    if (cluster != "RBB" && cluster != "CIB") year2Dataset = year2Dataset.Where(p => p.Cluster.ToLower().Trim() == cluster.ToLower().Trim()).Distinct().ToList();

                    year1ClustDataset = year1Dataset.Where(p => new List<string>() { "junior management", "middle management", "senior management", "semi-skilled", "top management", "secondees", "not specified" }.Contains(p.OccupationalLevel.NullCheck().Trim().ToLower())).Distinct().ToList();
                    year2ClustDataset = year2Dataset.Where(p => new List<string>() { "junior management", "middle management", "senior management", "semi-skilled", "top management", "secondees", "not specified" }.Contains(p.OccupationalLevel.NullCheck().Trim().ToLower())).Distinct().ToList();
                }

                if (year1division1 != "All") year1Dataset = year1Dataset.Where(p => year1division1.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division1.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2division1 != "All") year2Dataset = year2Dataset.Where(p => year2division1.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division1.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year1division2 != "All") year1Dataset = year1Dataset.Where(p => year1division2.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division2.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2division2 != "All") year2Dataset = year2Dataset.Where(p => year2division2.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division2.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year1division3 != "All") year1Dataset = year1Dataset.Where(p => year1division3.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division3.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2division3 != "All") year2Dataset = year2Dataset.Where(p => year2division3.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division3.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year1division4 != "All") year1Dataset = year1Dataset.Where(p => year1division4.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division4.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2division4 != "All") year2Dataset = year2Dataset.Where(p => year2division4.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division4.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year1division5 != "All") year1Dataset = year1Dataset.Where(p => year1division5.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division5.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2division5 != "All") year2Dataset = year2Dataset.Where(p => year2division5.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division5.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year1division6 != "All") year1Dataset = year1Dataset.Where(p => year1division6.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division6.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2division6 != "All") year2Dataset = year2Dataset.Where(p => year2division6.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division6.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year1branch != "All") year1Dataset = year1Dataset.Where(p => year1branch.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2branch != "All") year2Dataset = year2Dataset.Where(p => year2branch.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList();

                if (history.ManualBranchCodes != null) { year1Dataset = year1Dataset.ToList().Where(p => history.ManualBranchCodes.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => history.ManualBranchCodes.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList(); }
                if (history.ManualStaffCodes != null) { year1Dataset = year1Dataset.ToList().Where(p => history.ManualStaffCodes.ToLower().Trim().Split('|').AsEnumerable().Contains(p.StaffCode.NullCheck().ToLower().Trim())).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => history.ManualStaffCodes.ToLower().Trim().Split('|').AsEnumerable().Contains(p.StaffCode.NullCheck().ToLower().Trim())).Distinct().ToList(); }

                if (classification != "All") { year1Dataset = year1Dataset.Where(p => p.Classification.NullCheck().ToLower().Trim() == classification.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Classification.NullCheck().ToLower().Trim() == classification.ToLower().Trim()).Distinct().ToList(); }
                if (jobfamily != "All") { year1Dataset = year1Dataset.Where(p => p.JobFamily.NullCheck().ToLower().Trim() == jobfamily.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.JobFamily.NullCheck().ToLower().Trim() == jobfamily.ToLower().Trim()).Distinct().ToList(); }
                if (los != "All") { year1Dataset = year1Dataset.Where(p => p.LOS.NullCheck().ToLower().Trim() == los.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.LOS.NullCheck().ToLower().Trim() == los.ToLower().Trim()).Distinct().ToList(); }
                if (region != "All") { year1Dataset = year1Dataset.Where(p => p.Region.NullCheck().ToLower().Trim() == region.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Region.NullCheck().ToLower().Trim() == region.ToLower().Trim()).Distinct().ToList(); }
                if (passage != "All") { year1Dataset = year1Dataset.Where(p => p.ManagementPassage.NullCheck().ToLower().Trim() == passage.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.ManagementPassage.NullCheck().ToLower().Trim() == passage.ToLower().Trim()).Distinct().ToList(); }
                if (race != "All") { year1Dataset = year1Dataset.Where(p => p.Race.NullCheck().ToLower().Trim() == race.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Race.NullCheck().ToLower().Trim() == race.ToLower().Trim()).Distinct().ToList(); }
                if (occlevel != "All") { year1Dataset = year1Dataset.Where(p => p.OccupationalLevel.NullCheck().ToLower().Trim() == occlevel.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.OccupationalLevel.NullCheck().ToLower().Trim() == occlevel.ToLower().Trim()).Distinct().ToList(); }
                if (gender != "All") { year1Dataset = year1Dataset.Where(p => p.Gender.NullCheck().ToLower().Trim() == gender.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Gender.NullCheck().ToLower().Trim() == gender.ToLower().Trim()).Distinct().ToList(); }

                string caption = "2015 n = " + year1Dataset.Count + "; " + (year2 != "" ? year2 + " n = " + year2Dataset.Count : "");

                if (cluster != "All")
                {
                    year1NonPermDataset = year1Dataset.Where(p => new List<string>() { "non - permanent", "non-payroll" }.Contains(p.OccupationalLevel.NullCheck().Trim().ToLower())).Distinct().ToList();
                    year2NonPermDataset = year2Dataset.Where(p => new List<string>() { "non - permanent", "non-payroll" }.Contains(p.OccupationalLevel.NullCheck().Trim().ToLower())).Distinct().ToList();
                    year1Dataset = year1Dataset.Where(p => new List<string>() { "junior management", "middle management", "senior management", "semi-skilled", "top management", "secondees", "not specified" }.Contains(p.OccupationalLevel.NullCheck().Trim().ToLower())).Distinct().ToList();
                    year2Dataset = year2Dataset.Where(p => new List<string>() { "junior management", "middle management", "senior management", "semi-skilled", "top management", "secondees", "not specified" }.Contains(p.OccupationalLevel.NullCheck().Trim().ToLower())).Distinct().ToList();
                }
                else
                {
                    year1NonPermDataset = year1Dataset.Where(p => new List<string>() { "non - permanent", "non-payroll" }.Contains(p.OccupationalLevel.NullCheck().Trim().ToLower())).Distinct().ToList();
                    if (year2 == "2010") year2NonPermDataset = year2Dataset.Where(p => p.OccupationalLevel.NullCheck().Trim().ToLower() == "non - permanent").Distinct().ToList(); else year2NonPermDataset = year2Dataset.Where(p => new List<string>() { "non - permanent", "non-payroll" }.Contains(p.OccupationalLevel.NullCheck().Trim().ToLower())).Distinct().ToList();
                    year1Dataset = year1Dataset.Where(p => new List<string>() { "junior management", "middle management", "senior management", "semi-skilled", "top management", "secondees", "not specified" }.Contains(p.OccupationalLevel.NullCheck().Trim().ToLower())).Distinct().ToList();
                    if (year2 == "2010") year2Dataset = year2Dataset.Where(p => new List<string>() { "junior management", "middle management", "senior management", "semi-skilled", "top management", "secondees", "not specified", "non-payroll" }.Contains(p.OccupationalLevel.NullCheck().Trim().ToLower())).Distinct().ToList(); else year2Dataset = year2Dataset.Where(p => new List<string>() { "junior management", "middle management", "senior management", "semi-skilled", "top management", "secondees", "not specified" }.Contains(p.OccupationalLevel.NullCheck().Trim().ToLower())).Distinct().ToList();
                }

                eedimensionyear1 = year.GetEEBarrierDimensions(year1Dataset, dimensions, questions);
                eedimensionyear2 = year.GetEEBarrierDimensions(year2Dataset, dimensions, questions, year2);

                int pageno = 1;

                litReport.Text += PageTemplate.Template("", history.ReportName, "<tr><td style=\"width:100%;height:792px;text-align:left;vertical-align:text-top;background-image:url('" + GetSiteRoot() + "/Images/frontpage-2013.jpg');background-repeat:no-repeat;background-position:center;font-family:Arial;font-weight:bold;font-size:28px;margin:0;padding:0;\"><span style=\"top:75px;left:150px;color:black;position:relative;\">Nedbank Staff Survey 2015<br /><span style=\"Color:#000000;font-weight:Bold;Font-Size:28px;\">Employment Equity Barrier Report</span><br /><br /><span style=\"Color:#000000;font-weight:Bold;Font-Size:28px;\">" + reportname + "</span><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /></span><center><span style=\"Color:#000000;Font-Size:16px;font-weight:normal;\">" + (pdf ? "" : "<br/>") + "Confidential<br><br>Contributors: Market and Customer Insights, Group Human Resources,<br>Pure Survey Research (Pty) Ltd, Consulta Research (Pty) Ltd, and Aon Hewitt Ltd.</span></center></span></td></tr>", pdf, pageno++, true);

                litReport.Text += FiltersPage(history, pdf, pageno++);
                litReport.Text += TOCPage(history, caption, pdf, pageno++);
                litReport.Text += ScalePage(history, caption, pdf, pageno++);

                litReport.Text += DimensionChart("2015", year2, eedimensionyear1, eedimensionyear2, year1Dataset.Count, year2Dataset.Count, "2015 n = " + year1Dataset.Count + "; " + (year2 != "" ? year2 + " n = " + year2Dataset.Count : "") + " (SA Permanent Staff only)", "MEAN scores for EE Dimensions Compared (Top 14)", reportname, ref pageno, pdf, true);
                if (cluster != "All") litReport.Text += DimensionChart("Nedbank Group", cluster, year.GetEEBarrierDimensions(year1GroupDataset, dimensions, questions), year.GetEEBarrierDimensions(year1ClustDataset, dimensions, questions), year1GroupDataset.Count, year1ClustDataset.Count, "Nedbank Group n = " + year1GroupDataset.Count + "; " + cluster + " n = " + year1ClustDataset.Count, cluster + " compared to Nedbank Group", reportname, ref pageno, pdf, false);
                if (year1division1 != "All") litReport.Text += DimensionChart(cluster, "Selected Sample", year.GetEEBarrierDimensions(year1ClustDataset, dimensions, questions), year.GetEEBarrierDimensions(year1Dataset, dimensions, questions), year1ClustDataset.Count, year1Dataset.Count, cluster + " n = " + year1ClustDataset.Count + "; " + cluster + " n = " + year1Dataset.Count, cluster + " compared to " + cluster + " (Multiple Filters Applied)", reportname, ref pageno, pdf, false);

                litReport.Text += EEBarrierDimensionChart(year2, eedimensionyear1, eedimensionyear2, year1Dataset.Count, year2Dataset.Count, "2015 n = " + year1Dataset.Count + "; " + (year2 != "" ? year2 + " n = " + year2Dataset.Count : "") + " (SA Permanent Staff only)", "Dimensions of EE Barrier Report Ranked", reportname, ref pageno, pdf);
                if (year1Dataset.Where(p => p.Disabilities.NullCheck().Trim().ToLower() == "yes").Count() > 0) litReport.Text += EEBarrierDimensionChart(year2, year.GetEEBarrierDimensions(year1Dataset.Where(p => p.Disabilities.NullCheck().Trim().ToLower() == "yes").Distinct().ToList(), dimensions, questions), year.GetEEBarrierDimensions(year2Dataset.Where(p => p.Disabilities.NullCheck().Trim().ToLower() == "yes").Distinct().ToList(), dimensions, questions, year2), year1Dataset.Where(p => p.Disabilities.NullCheck().Trim().ToLower() == "yes").Count(), year2Dataset.Where(p => p.Disabilities.NullCheck().Trim().ToLower() == "yes").Count(), "2015 n = " + year1Dataset.Where(p => p.Disabilities.NullCheck().Trim().ToLower() == "yes").Count() + "; " + (year2 != "" ? year2 + " n = " + year2Dataset.Where(p => p.Disabilities.NullCheck().Trim().ToLower() == "yes").Count() : "") + " (Disabled Staff Only)", "EE Barrier Dimensions Ranked - People with Disability (PWD)", reportname, ref pageno, pdf);

                litReport.Text += EEBarrierDimensionBlockChart(year2, year1Dataset, year2Dataset, year1NonPermDataset, year2NonPermDataset, caption, reportname, ref pageno, dimensions, questions, pdf);
            }
        }

        private string FiltersPage(ReportHistory history, bool pdf, int pageno)
        {
            string html = "<tr><td><table style=\"width:100%;text-align:left;\"><tr><td><h1>Applied Filters</h1></td></tr><tr><td>&nbsp;</td></tr>";

            html += "<tr><td style=\"width:100px;\"><b>Cluster : </b></td><td style=\"text-align:left\">" + history.Cluster.Replace("|", ", ") + "</td></tr>";

            html += "<tr><td colspan=\"2\">&nbsp;</td></tr><tr><td colspan=\"2\"><table style='width:100%;' cellspacing='0' cellpadding='0'>";
            html += "<tr><td style=\"text-align:left;font-size:16px;\" colspan=\"2\"><b>Applied Division Filters for 2015 :</b></td>" + (history.Year2 != "None" ? "<td style=\"text-align:left;font-size:16px;\" colspan=\"2\"><b>Applied Division Filters for " + history.Year2 + " :</b></td>" : "") + "</tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Division 1:</b></td><td style=\"text-align:left\">" + history.Y1Division1 + "</td>" + (history.Year2 != "None" ? "<td style=\"text-align:left;width:100px;\"><b>Division 1:</b></td><td style=\"text-align:left\">" + history.Y2Division1 + "</td>" : "") + "</tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Division 2:</b></td><td style=\"text-align:left\">" + history.Y1Division2 + "</td>" + (history.Year2 != "None" ? "<td style=\"text-align:left;width:100px;\"><b>Division 2:</b></td><td style=\"text-align:left\">" + history.Y2Division2 + "</td>" : "") + "</tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Division 3:</b></td><td style=\"text-align:left\">" + history.Y1Division3 + "</td>" + (history.Year2 != "None" ? "<td style=\"text-align:left;width:100px;\"><b>Division 3:</b></td><td style=\"text-align:left\">" + history.Y2Division3 + "</td>" : "") + "</tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Division 4:</b></td><td style=\"text-align:left\">" + history.Y1Division4 + "</td>" + (history.Year2 != "None" ? "<td style=\"text-align:left;width:100px;\"><b>Division 4:</b></td><td style=\"text-align:left\">" + history.Y2Division4 + "</td>" : "") + "</tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Division 5:</b></td><td style=\"text-align:left\">" + history.Y1Division5 + "</td>" + (history.Year2 != "None" ? "<td style=\"text-align:left;width:100px;\"><b>Division 5:</b></td><td style=\"text-align:left\">" + history.Y2Division5 + "</td>" : "") + "</tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Division 6:</b></td><td style=\"text-align:left\">" + history.Y1Division6 + "</td>" + (history.Year2 != "None" ? "<td style=\"text-align:left;width:100px;\"><b>Division 6:</b></td><td style=\"text-align:left\">" + history.Y2Division6 + "</td>" : "") + "</tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Branch:</b></td><td style=\"text-align:left\">" + history.Y1Branch + "</td>" + (history.Year2 != "None" ? "<td style=\"text-align:left;width:100px;\"><b>Branch:</b></td><td style=\"text-align:left\">" + history.Y2Branch + "</td>" : "") + "</tr>";
            html += "</table></td></tr><tr><td colspan=\"2\">&nbsp;</td></tr>";

            if (history.ManualBranchCodes != null) html += "<tr><td align=\"left\" colspan=\"2\">" + history.ManualBranchCodes.ToLower().Trim().Split('|').Count() + " Manual Branch Codes applied.</td></tr><tr><td colspan=\"2\">&nbsp;</td></tr>";
            if (history.ManualStaffCodes != null) html += "<tr><td align=\"left\" colspan=\"2\">" + history.ManualStaffCodes.ToLower().Trim().Split('|').Count() + " Manual Staff Codes applied.</td></tr><tr><td colspan=\"2\">&nbsp;</td></tr>";

            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Generational Classification:</b></td><td style=\"text-align:left\">" + history.Classification + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>EE Occupational Level:</b></td><td style=\"text-align:left\">" + history.OccupationalLevel + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Length of service:</b></td><td style=\"text-align:left\">" + history.LOS + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Management Passage:</b></td><td style=\"text-align:left\">" + history.ManagementPassage + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Location:</b></td><td style=\"text-align:left\">" + history.Location + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Gender:</b></td><td style=\"text-align:left\">" + history.Gender + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Race:</b></td><td style=\"text-align:left\">" + history.Race + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Job Family:</b></td><td style=\"text-align:left\">" + history.JobFamily + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Region:</b></td><td style=\"text-align:left\">" + history.Region + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Compare Year:</b></td><td style=\"text-align:left\">2015 vs " + history.Year2 + "</td></tr>";

            html += "</table></td></tr>";

            return PageTemplate.Template("Report Filters", history.ReportName, html, pdf, pageno, false);
        }

        private string TOCPage(ReportHistory history, string caption, bool pdf, int pageno)
        {
            string html = "<tr><td align='center'><b>" + caption + "</b></td></tr>" +
                    "<tr><td align='left'><b>Table of Contents</b>" +
                    "<ul><li>Approach; Reading the Report & Interpretation Guide</li>" +
                    "<li>Overall and Dimension Scores</li>" +
                    "<li>EE Barrier Report Dimensions Ranked</li>" +
                    "<li>People with Disabilty - EE Barrier Report Dimensions Ranked</li>" +
                    "<li>Barrier Tables</li></ul></td></tr>";

            html += "<tr><td align='center'><h2>" + history.ReportName + " - Approach</h2></td></tr><tr><td>&nbsp;</td></tr><tr><td align='center'><b>" + caption + "</b></td></tr>";

            html += "<tr><td align='left'><h3><b>Integration of categories, as specified by:</b></h3>" +
                    "<ul><li>The EE Act (Section 15 of 23 EEA2),</li>" +
                    "<li>DoL categories,</li>" +
                    "<li>Nedbank specific categories identified in 2007</li></ul><br/>" +
                    "<h3><b>Content analysis of all NSS statements to newly defined categories:</b></h3>" +
                    "<ul><li>Consulted with PCay Consulting and Transformation Unit</li>" +
                    "<li>Code of Good Practice Definitions from the EE Act</li>" +
                    "<li>Qualitative judgement such as:" +
                    "<ul><li>exclusion of statements that tend to be inflated due to self-reporting bias</li>" +
                    "<li>exclusion of broad statements that do not directly relate to the EE plan</li></ul></li></ul></td></tr>";

            return PageTemplate.Template(history.ReportName + " - Table of Contents", history.ReportName, html, pdf, pageno, false);
        }

        private string ScalePage(ReportHistory history, string caption, bool pdf, int pageno)
        {
            string html = "<tr><td align='center'><b>" + caption + "</b></td></tr>" +
                    "<tr><td align='left'>Scale utilised (as per NSS 2007) - Five point Likert scale, with a focus on total 'disagreement' for the purposes of EE reporting template, as a way of identifying barriers</td></tr>" +
                    "<tr><td align='center'><table cellpadding='5' cellspacing='0'>" +
                    "<tr><td style='border:1px solid black;' align='center'>Strongly<br />Agree</td><td style='border:1px solid black;' align='center'>Agree</td><td style='border:1px solid black;' align='center'>Neither Agree<br />nor Disagree</td><td style='border:1px solid black;' align='center'>Disagree</td><td style='border:1px solid black;' align='center'>Strongly<br />Disagree</td></tr>" +
                    "<tr><td colspan='2' style='border: 1px solid #000;background-color: #009933' align='center'>Strength</td><td style='border: 1px solid #000; background-color: #999999' align='center'>Neutral</td><td colspan='2' style='border: 1px solid #000;background-color: #FF0000' align='center'>Improvement required</td></tr>" +
                    "</table></tr></td><tr><td align='left'>" +
                    "<ul><li>Overall mean score (across all statements)</li>" +
                    "<li>Overall EE barrier mean score for the statement:</li>" +
                    "<li>'In Nedbank, the Employment Equity process is well managed and successfully implemented'</li>" +
                    "<li>Scores per statement within each of the 26 dimensions and mean score per dimension</li>" +
                    "<li>Full report includes breakdown by</li>" +
                    "<ul><li>Gender, Race, Black Female, African Female, EE Occupational level, EE Occupational category, Disability (Permanent staff only)</li>" +
                    "<li>Non Permanent staff</li>" +
                    "<li>Race x gender x EE occupational levels (Permanent staff only)</li>" +
                    "<li>Race x gender x EE occupational categories (Permanent staff only)</li></ul>" +
                    "<li>Score to be reported only if sample size is statistically reliable (more than 10 respondents per category)</li></ul></td></tr>";

            html += "<tr><td align='center'><h2>" + history.ReportName + " - Interpreting the results</h2></td></tr><tr><td>&nbsp;</td></tr><tr><td align='center'><b>" + caption + "</b></td></tr>";

            html += "<tr><td align='left'>The report includes disagreement scores (strongly disagree and disagree selections) which indicates the degree of 'barrier' perceived by staff</td></tr>" +
                    "<tr><td align='left'>Based on the analysis of the frequency distribution of the disagreement scores, it was determined that the scores that exceed 15% are deemed a barrier</td></tr>" +
                    "<tr><td align='left'>A qualitative judgement is applied to determine meaningful differences between the scores above 15% in terms of three risk bands as follows:</td></tr>" +
                    "<tr><td align='left'><table cellspacing='1' cellpadding='1' style='font-size:11px'>" +
                    "<tr><td style='background-color: #FF0000; width: 30px; height: 22px;'></td><td style='width:120px;padding-left:10px;'>Above 20%:</td><td style='width:200px'>Transformational reputation risk</td></tr>" +
                    "<tr><td style='background-color: yellow; width: 30px; height: 22px;'>&nbsp;</td><td style='padding-left:10px;'>15 - 20%:</td><td>EE barriers exist require careful monitoring</td></tr>" +
                    "<tr><td style='background-color: #33CC33; width: 30px; height: 22px;'></td><td style='padding-left:10px;'>&lt; 15%</td><td>Healthy functioning</td></tr>" +
                    "</table></td></tr>";

            return PageTemplate.Template(history.ReportName + " - How to read the report", history.ReportName, html, pdf, pageno, false);
        }

        private string DimensionChart(string year1, string year2, EEBarrierDimension eedimensionyear1, EEBarrierDimension eedimensionyear2, double year1count, double year2count, string caption, string graphname, string reportname, ref int pageno, bool pdf, bool comp)
        {
            StringBuilder DimensionChartHTML = new StringBuilder();
            StringBuilder DimensionCategoryXML = new StringBuilder();
            StringBuilder DimensionDataset1XML = new StringBuilder();
            StringBuilder DimensionDataset2XML = new StringBuilder();
            StringBuilder DimensionChartTableHTML = new StringBuilder();

            double result = 0;
            bool isSignificant = false;

            Significance.getSignificance(year1count, year2count, eedimensionyear1.OverallEEScore.DimensionNegativeValue, eedimensionyear2.OverallEEScore.DimensionNegativeValue, out result, out isSignificant);
            DimensionChartTableHTML.Append("<tr style=\"font-weight:bold;\"><td style='padding:4px;'>" + eedimensionyear1.OverallEEScore.DimensionName + " (" + result.ToString("0.0") + (isSignificant ? "*" : "") + ")" + "</td><td style='width:50px;padding:4px;'>" + eedimensionyear1.OverallEEScore.DimensionNegativeValue.ToString("0.0") + "</td><td style='width:50px;padding:4px;'>" + eedimensionyear2.OverallEEScore.DimensionNegativeValue.ToString("0.0") + "</td></tr>");
            DimensionCategoryXML.Append("<category label='" + eedimensionyear1.OverallEEScore.DimensionName + " (" + result.ToString("0.0") + (isSignificant ? "*" : "") + ")" + "' /> ");

            Significance.getSignificance(year1count, year2count, eedimensionyear1.OverallEEProcess.DimensionNegativeValue, eedimensionyear2.OverallEEProcess.DimensionNegativeValue, out result, out isSignificant);
            DimensionChartTableHTML.Append("<tr style=\"font-weight:bold;\"><td style='padding:4px;'>" + eedimensionyear1.OverallEEProcess.DimensionName + " (" + result.ToString("0.0") + (isSignificant ? "*" : "") + ")" + "</td><td style='width:50px;padding:4px;'>" + eedimensionyear1.OverallEEProcess.DimensionNegativeValue.ToString("0.0") + "</td><td style='width:50px;padding:4px;'>" + eedimensionyear2.OverallEEProcess.DimensionNegativeValue.ToString("0.0") + "</td></tr>");

            DimensionDataset1XML.Append("<dataset seriesname='" + year1 + "' color='22513d' anchorSides='6' showValues='1' anchorRadius='2' anchorBorderColor='22513d' anchorBgAlpha='0' showPlotBorder='1' plotBorderThickness='2' plotBorderColor='22513d' plotBorderAlpha='100' drawAnchors='1'>");
            if (year2 != "") DimensionDataset2XML.Append("<dataset seriesname='" + year2 + "' color='0066CC' anchorSides='10' anchorBorderColor='0066CC' anchorBgAlpha='0' anchorRadius='2' showPlotBorder='1' plotBorderThickness='2' plotBorderColor='0066CC' plotBorderAlpha='100' drawAnchors='1'>");

            DimensionDataset1XML.Append("<set value='" + eedimensionyear1.OverallEEScore.DimensionNegativeValue.ToString("0.0") + "' />");
            if (year2 != "") DimensionDataset2XML.Append("<set value='" + eedimensionyear2.OverallEEScore.DimensionNegativeValue.ToString("0.0") + "' />");

            foreach (ReportDimension dimension in eedimensionyear1.EEDimensions.Distinct().ToList().OrderByDescending(p => p.DimensionNegativeValue).Take(14).OrderBy(p => p.DimensionNegativeValue))
            {
                Significance.getSignificance(year1count, year2count, dimension.DimensionNegativeValue, eedimensionyear2.EEDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNegativeValue, out result, out isSignificant);
                DimensionCategoryXML.Append("<category label='" + HTML.WordWrap(dimension.DimensionName, 20) + " (" + result.ToString("0.0") + (isSignificant ? "*" : "") + ")" + "' /> ");

                DimensionDataset1XML.Append("<set value='" + dimension.DimensionNegativeValue.ToString("0.0") + "' />");
                if (year2 != "") DimensionDataset2XML.Append("<set value='" + eedimensionyear2.EEDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNegativeValue.ToString("0.0") + "' />");
            }

            bool other = true;

            foreach (ReportDimension dimension in eedimensionyear1.EEDimensions.Distinct().ToList().OrderByDescending(p => p.DimensionNegativeValue).Take(14))
            {
                Significance.getSignificance(year1count, year2count, dimension.DimensionNegativeValue, eedimensionyear2.EEDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNegativeValue, out result, out isSignificant);
                DimensionChartTableHTML.Append("<tr" + (other ? " style=\"background:#d4e7bc;\"" : "") + "><td style=\"padding:4px;\">" + dimension.DimensionName + " (" + result.ToString("0.0") + (isSignificant ? "*" : "") + ")" + "</td><td style='width:50px;padding:4px;'>" + dimension.DimensionNegativeValue.ToString("0.0") + "</td><td style='width:50px;padding:4px;'>" + eedimensionyear2.EEDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNegativeValue.ToString("0.0") + "</td></tr>");

                other = !other;
            }

            Significance.getSignificance(year1count, year2count, eedimensionyear1.OverallEEProcess.DimensionNegativeValue, eedimensionyear2.OverallEEProcess.DimensionNegativeValue, out result, out isSignificant);
            DimensionCategoryXML.Append("<category label='" + eedimensionyear1.OverallEEProcess.DimensionName + " (" + result.ToString("0.0") + (isSignificant ? "*" : "") + ")" + "' /> ");
            DimensionDataset1XML.Append("<set value='" + eedimensionyear1.OverallEEProcess.DimensionNegativeValue.ToString("0.0") + "' />");
            if (year2 != "") DimensionDataset2XML.Append("<set value='" + eedimensionyear2.OverallEEProcess.DimensionNegativeValue.ToString("0.0") + "' />");

            DimensionDataset1XML.Append("</dataset>");
            if (year2 != "") DimensionDataset2XML.Append("</dataset>");

            DimensionChartHTML.Append("<tr><td align='left' valign='top'>");
            DimensionChartHTML.Append(Charts.RadarChart(DimensionCategoryXML.ToString(), DimensionDataset1XML.ToString() + DimensionDataset2XML.ToString(), "Dimension" + pageno, 550, 420));
            DimensionChartHTML.Append("</td><td align='left' valign='top'>");
            DimensionChartHTML.Append(HTML.RadarTable(new string[] { year1, year2 }, DimensionChartTableHTML.ToString(), true));
            DimensionChartHTML.Append("</td></tr>");
            string legend = "";
            if (year2 != "" && comp) legend = "<tr><td align='left' style='font-style: italic'>" + Significance.StatSignificantLegend("", year2, pdf, true, false) + "</td><tr>"; else legend = "<tr><td align='left' style='font-style: italic'>" + Significance.StatSignificantLegend("", year2, pdf, false, false) + "</td><tr>";

            return PageTemplate.Template(graphname, reportname, "<tr><td align='center' colspan='2'><h3>" + caption + "</h3></td></tr><tr><td>&nbsp;</td></tr><tr><td align='center'>" + DimensionChartHTML.ToString() + "</td></tr><tr><td>&nbsp;</td></tr>" + legend, pdf, pageno++, false);
        }

        private string EEBarrierDimensionChart(string year2, EEBarrierDimension eedimensionyear1, EEBarrierDimension eedimensionyear2, double year1count, double year2count, string caption, string graphname, string reportname, ref int pageno, bool pdf)
        {
            string pages = "";

            for (int i = 0; i < ((eedimensionyear1.EEDimensions.Count / 9) + (eedimensionyear1.EEDimensions.Count % 9 > 0 ? 1 : 0)); i++)
            {
                StringBuilder DimensionDatasetXML = new StringBuilder();

                double result = 0;
                bool isSignificant = false;

                Significance.getSignificance(year1count, year2count, eedimensionyear1.OverallEEScore.DimensionNegativeValue, eedimensionyear2.OverallEEScore.DimensionNegativeValue, out result, out isSignificant);
                DimensionDatasetXML.Append("<set label='" + eedimensionyear1.OverallEEScore.DimensionName + " 2015" + Significance.getSignificantSymbol(result, isSignificant) + "' value='" + eedimensionyear1.OverallEEScore.DimensionNegativeValue.ToString("0.0") + "' color='" + Significance.getEEColor(eedimensionyear1.OverallEEScore.DimensionNegativeValue) + "' />");
                DimensionDatasetXML.Append("<set label='" + year2 + "  ' value='" + eedimensionyear2.OverallEEScore.DimensionNegativeValue.ToString("0.0") + "' color='" + Significance.getEEColor(eedimensionyear2.OverallEEScore.DimensionNegativeValue) + "' />");
                DimensionDatasetXML.Append("<vLine color='000000' thickness='1' alpha='80' />");

                Significance.getSignificance(year1count, year2count, eedimensionyear1.OverallEEProcess.DimensionNegativeValue, eedimensionyear2.OverallEEProcess.DimensionNegativeValue, out result, out isSignificant);
                DimensionDatasetXML.Append("<set label='" + eedimensionyear1.OverallEEProcess.DimensionName + " 2015" + Significance.getSignificantSymbol(result, isSignificant) + "' value='" + eedimensionyear1.OverallEEProcess.DimensionNegativeValue.ToString("0.0") + "' color='" + Significance.getEEColor(eedimensionyear1.OverallEEProcess.DimensionNegativeValue) + "' />");
                DimensionDatasetXML.Append("<set label='" + year2 + "  ' value='" + eedimensionyear2.OverallEEProcess.DimensionNegativeValue.ToString("0.0") + "' color='" + Significance.getEEColor(eedimensionyear2.OverallEEProcess.DimensionNegativeValue) + "' />");
                DimensionDatasetXML.Append("<vLine color='000000' thickness='1' alpha='80' />");

                foreach (ReportDimension dimension in eedimensionyear1.EEDimensions.OrderByDescending(p => p.DimensionNegativeValue).Skip(i * 9).Take(9))
                {
                    if (dimension.DimensionNegativeValue > 0)
                    {
                        Significance.getSignificance(year1count, year2count, dimension.DimensionNegativeValue, eedimensionyear2.EEDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNegativeValue, out result, out isSignificant);
                        DimensionDatasetXML.Append("<set label='" + dimension.DimensionName + " 2015" + Significance.getSignificantSymbol(result, isSignificant) + "' value='" + dimension.DimensionNegativeValue.ToString("0.0") + "' color='" + Significance.getEEColor(dimension.DimensionNegativeValue) + "' />");
                        DimensionDatasetXML.Append("<set label='" + year2 + "  ' value='" + eedimensionyear2.EEDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNegativeValue.ToString("0.0") + "' color='" + Significance.getEEColor(eedimensionyear2.EEDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionNegativeValue) + "' />");
                        DimensionDatasetXML.Append("<vLine color='000000' thickness='1' alpha='80' />");
                    }
                }

                pages += PageTemplate.Template(graphname + " - Page " + (i + 1), reportname, "<tr><td align='center' colspan='2'><h3>" + caption + "</h3></td></tr><tr><td>&nbsp;</td></tr><tr><td align='center'>" + Charts.BarChartnoCategories(DimensionDatasetXML.ToString(), "AllDimensions" + pageno, 1024, 500) + "</td></tr>", pdf, pageno++, false);
            }

            return pages;
        }

        private string EEBarrierDimensionBlockChart(string year2, List<Year> year1dataset, List<Year> year2dataset, List<Year> year1nonpermdataset, List<Year> year2nonpermdataset, string caption, string reportname, ref int pageno, List<Dimension> dimensions, List<Question> questions, bool pdf)
        {
            string pages = "";

            pages += PageTemplate.Template(reportname + " : Overall (overall mean score across all categories)", reportname, "<tr><td align='center' colspan='2'><h3>" + caption + "</h3></td></tr><tr><td>&nbsp;</td></tr><tr><td align='center'>" + HTML.getDesignationChart(BlockRows(year2, year1dataset, year2dataset, year1nonpermdataset, year2nonpermdataset, "Overall EE Score", dimensions)) + "</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>" + EEDimensionLegend(year2), pdf, pageno++, false);
            pages += PageTemplate.Template(reportname + " : Overall EE Process", reportname, "<tr><td align='center' colspan='2'><h3>" + caption + "</h3></td></tr><tr><td>&nbsp;</td></tr><tr><td align='center'>" + HTML.getDesignationChart(BlockRows(year2, year1dataset, year2dataset, year1nonpermdataset, year2nonpermdataset, "Overall EE Process", dimensions)) + "</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>" + EEDimensionLegend(year2), pdf, pageno++, false);

            foreach (Dimension dimension in dimensions.Where(p => p.EEBarrier == true && p.OverallDimension == false).OrderBy(p => (p.OtherDimension == true ? "Other - " : "") + p.DimensionText))
                if (year.getEEBarrierScore(year1dataset, dimension.DimensionText, dimensions) > 0)
                    pages += PageTemplate.Template(reportname + " : " + (dimension.OtherDimension == true ? "Other - " : "") + dimension.DimensionText, reportname, "<tr><td align='center' colspan='2'><h3>" + caption + "</h3></td></tr><tr><td>&nbsp;</td></tr><tr><td align='center'>" + HTML.getDesignationChart(BlockRows(year2, year1dataset, year2dataset, year1nonpermdataset, year2nonpermdataset, dimension.DimensionText, dimensions)) + "</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>" + EEDimensionLegend(year2) + "<tr><td>&nbsp;</td></tr><tr><td align='left' style='font-style: italic;vertical-align:bottom;' colspan='11'>" + BlockLegend(dimension.DimensionText, questions, dimensions) + "</td></tr>", pdf, pageno++, false);

            return pages;
        }

        private string BlockRows(string year2, List<Year> year1dataset, List<Year> year2dataset, List<Year> year1nonpermdataset, List<Year> year2nonpermdataset, string dimension, List<Dimension> dimensions)
        {
            StringBuilder rows = new StringBuilder();

            foreach (string occlevel in new string[] { "Senior Management", "Middle Management", "Junior Management", "Semi-Skilled", "Permanent Total", "Non - permanent" })
            {
                rows.Append("<tr>");
                if (occlevel == "Permanent Total")
                    rows.Append("<td align='left'>" + occlevel + " (" + year1dataset.Count + ") </td>");
                else if (occlevel == "Non - permanent")
                    rows.Append("<td align='left'>" + occlevel + " (" + year1nonpermdataset.Count + ") </td>");
                else
                    rows.Append("<td align='left'>" + occlevel + " (" + year1dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel).Count() + ") </td>");
                string malewhite = "";

                foreach (string gender in new string[] { "Male", "Female" })
                {
                    foreach (string race in new string[] { "African", "Coloured", "Indian", "White" })
                    {
                        if (gender == "Male" && race == "White")
                            if (occlevel == "Permanent Total")
                                malewhite = getEECell(year.getEEBarrierScore(year1dataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Distinct().ToList(), dimension, dimensions), year.getEEBarrierScore(year2dataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Distinct().ToList(), dimension, dimensions, year2), year1dataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Count(), year2dataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Count());
                            else if (occlevel == "Non - permanent")
                                malewhite = getEECell(year.getEEBarrierScore(year1nonpermdataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Distinct().ToList(), dimension, dimensions), year.getEEBarrierScore(year2nonpermdataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Distinct().ToList(), dimension, dimensions, year2), year1nonpermdataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Count(), year2nonpermdataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Count());
                            else
                                malewhite = getEECell(year.getEEBarrierScore(year1dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel && p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Distinct().ToList(), dimension, dimensions), year.getEEBarrierScore(year2dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel && p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Distinct().ToList(), dimension, dimensions, year2), year1dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel && p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Count(), year2dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel && p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Count());
                        else
                            if (occlevel == "Permanent Total")
                                rows.Append(getEECell(year.getEEBarrierScore(year1dataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Distinct().ToList(), dimension, dimensions), year.getEEBarrierScore(year2dataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Distinct().ToList(), dimension, dimensions, year2), year1dataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Count(), year2dataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Count()));
                            else if (occlevel == "Non - permanent")
                                rows.Append(getEECell(year.getEEBarrierScore(year1nonpermdataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Distinct().ToList(), dimension, dimensions), year.getEEBarrierScore(year2nonpermdataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Distinct().ToList(), dimension, dimensions, year2), year1nonpermdataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Count(), year2nonpermdataset.Where(p => p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Count()));
                            else
                                rows.Append(getEECell(year.getEEBarrierScore(year1dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel && p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Distinct().ToList(), dimension, dimensions), year.getEEBarrierScore(year2dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel && p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Distinct().ToList(), dimension, dimensions, year2), year1dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel && p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Count(), year2dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel && p.Gender.NullCheck() == gender && p.Race.NullCheck() == race).Count()));
                    }
                }
                rows.Append(malewhite);
                rows.Append("<td>&nbsp;</td>");
                if (occlevel == "Permanent Total")
                    rows.Append(getEECell(year.getEEBarrierScore(year1dataset, dimension, dimensions), year.getEEBarrierScore(year2dataset, dimension, dimensions, year2), year1dataset.Count, year2dataset.Count));
                else if (occlevel == "Non - permanent")
                    rows.Append(getEECell(year.getEEBarrierScore(year1nonpermdataset, dimension, dimensions), year.getEEBarrierScore(year2nonpermdataset, dimension, dimensions, year2), year1nonpermdataset.Count, year2nonpermdataset.Count));
                else
                    rows.Append(getEECell(year.getEEBarrierScore(year1dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel).Distinct().ToList(), dimension, dimensions), year.getEEBarrierScore(year2dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel).Distinct().ToList(), dimension, dimensions, year2), year1dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel).Count(), year2dataset.Where(p => p.OccupationalLevel.NullCheck().Trim() == occlevel).Count()));
                rows.Append("</tr>");
            }

            return rows.ToString();
        }

        private string BlockLegend(string dimension, List<Question> questions, List<Dimension> dimensions)
        {
            string body = "";
            int count = 0;

            foreach (string statement in dimensions.Where(p => p.DimensionText == dimension && p.EEBarrier == true).Select(p => p.DimensionQuestions2014).Distinct().SingleOrDefault().Split(',').ToList())
            {
                if (count == 0) body += "<tr>";

                body += "<td>• " + questions.Where(p => p.QuestionIndex == "Q" + statement).SingleOrDefault().QuestionText + "</td>";

                if (count == 1)
                {
                    count = 0;
                    body += "</tr>";
                }
                else count++;
            }

            return HTML.LegendTemplate(body, "Dimension made up of the following statement(s):");
        }

        private string getEECell(double CurrentValue, double PreviousValue, double CurrentResponse, double PreviousResponse)
        {
            double result;
            bool isSignificant;

            if (CurrentValue > 0)
            {
                if (CurrentResponse >= 10)
                {
                    if (PreviousValue > 0 && PreviousResponse >= 10)
                        Significance.getSignificance(CurrentResponse, PreviousResponse, CurrentValue, PreviousValue, out result, out isSignificant);
                    else
                        Significance.getSignificance(CurrentResponse, PreviousResponse, CurrentValue, 0, out result, out isSignificant);

                    if (result > 0)
                        return "<td align='center' style='border-color:black;border-width:1px;border-style:solid;background-color:#" + Significance.getEEColor(CurrentValue) + (CurrentValue > 20 ? ";color:#fff;" : ";color:#000;") + "height:25px;'>" + CurrentValue.ToString("0.0") + " (" + "+" + Math.Abs(result).ToString("0.0") + (isSignificant ? "*" : "") + ")</td>";
                    else if (result == 0)
                        return "<td align='center' style='border-color:black;border-width:1px;border-style:solid;background-color:#" + Significance.getEEColor(CurrentValue) + (CurrentValue > 20 ? ";color:#fff;" : ";color:#000;") + "height:25px;'>" + CurrentValue.ToString("0.0") + " (" + Math.Abs(result).ToString("0.0") + (isSignificant ? "*" : "") + ")</td>";
                    else
                        return "<td align='center' style='border-color:black;border-width:1px;border-style:solid;background-color:#" + Significance.getEEColor(CurrentValue) + (CurrentValue > 20 ? ";color:#fff;" : ";color:#000;") + "height:25px;'>" + CurrentValue.ToString("0.0") + " (" + "-" + Math.Abs(result).ToString("0.0") + (isSignificant ? "*" : "") + ")</td>";
                }
                else
                    return "<td style='border-color:black;border-width:1px;border-style:solid;background-color:white#height:25px;'>&nbsp;</td>";
            }
            else
                return "<td style='border-color:black;border-width:1px;border-style:solid;background-color:white#height:25px;'>&nbsp;</td>";
        }

        private string EEDimensionLegend(string year)
        {
            return "<tr><td align='left' style='font-style: italic' colspan='11'>" +
                        "<b>Definitions</b>" +
                            "<table cellpadding='0' cellspacing='0' width='80%'>" +
                                "<tr><td><table cellpadding='0' cellspacing='0' width='100%'><tr>" +
                                    "<td>% Disagreement</td>" +
                                    "<td>&nbsp;</td>" +
                                    "<td style='border:1px solid black;background-color:Red;width:20px;height:20px'>&nbsp;</td>" +
                                    "<td>&nbsp;</td>" +
                                    "<td>Above 20%</td>" +
                                    "<td style='border:1px solid black;background-color:yellow;width:20px;height:20px'>&nbsp;</td>" +
                                    "<td>&nbsp;</td>" +
                                    "<td>15 - 20%</td>" +
                                    "<td style='border:1px solid black;background-color:#00C100;width:20px;height:20px'>&nbsp;</td>" +
                                    "<td>&lt; 15%</td>" +
                                    "<td>&nbsp;</td>" +
                                    "<td>Other</td>" +
                                    "<td>&nbsp;</td>" +
                                    "<td style='border:1px solid black;background-color:white;width:20px;height:20px'>&nbsp;</td>" +
                                    "<td>&lt; 10 Respondents/ Group not calculated</td>" +
                                "</tr></table></td></tr>" +
                                "<tr><td>&nbsp;</td></tr>" +
                                "<tr><td><table cellpadding='0' cellspacing='0' width='100%'><tr>" +
                                    "<td>Number in brackets</td>" +
                                    "<td>&nbsp;</td>" +
                                    "<td colspan='5'>Shift in % Disagreement score since " + year + "</td>" +
                                "</tr></table></td></tr>" +
                                "<tr><td>&nbsp;</td></tr>" +
                                "<tr><td><table cellpadding='0' cellspacing='0' width='100%'><tr>" +
                                    "<td>*</td>" +
                                    "<td>&nbsp;</td>" +
                                    "<td>Indicates significance difference since " + year + "</td>" +
                                "</tr></table></td></tr>" +
                        "</table>" +
            "</td></tr>";
        }

        protected void cmdPDF_Click(object sender, EventArgs e)
        {
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == new Guid(RouteData.Values["ReportHistoryID"].ToString())).Distinct().SingleOrDefault();

            string filename = "";

            if (history.PDFEEDate == null) history.PDFEEDate = DateTime.Now;
            history.PDFEECount = history.PDFEECount == null ? 1 : history.PDFEECount++;

            ReportGeneration gen = new ReportGeneration();
            gen.DateDrawn = DateTime.Now;
            gen.GenerationID = Guid.NewGuid();
            gen.OutputType = "PDF";
            gen.ReportHistoryID = history.ReportHistoryID;
            gen.ReportType = "EE Barrier";
            gen.UserID = UserID;
            db.ReportGenerations.Add(gen);
            db.SaveChanges();

            if (history.PDFEEFilename == null || history.ManualFileName != null)
            {
                RenderPDF renderPDF = new RenderPDF();
                filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                renderPDF.GeneratePDF(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/eepdf/" + history.ReportHistoryID);
                history.PDFEEFilename = filename;
                db.SaveChanges();

                renderPDF = null;
            }
            else
                filename = history.PDFEEFilename;

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=pdf&File=" + filename);

            GC.Collect();
        }

        protected void cmdPPT_Click(object sender, EventArgs e)
        {
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == new Guid(RouteData.Values["ReportHistoryID"].ToString())).Distinct().SingleOrDefault();

            string filename = "";

            if (history.PPTEEDate == null) history.PPTEEDate = DateTime.Now;
            history.PPTEECount = history.PPTEECount == null ? 1 : history.PPTEECount++;

            ReportGeneration gen = new ReportGeneration();
            gen.DateDrawn = DateTime.Now;
            gen.GenerationID = Guid.NewGuid();
            gen.OutputType = "PPT";
            gen.ReportHistoryID = history.ReportHistoryID;
            gen.ReportType = "EE Barrier";
            gen.UserID = UserID;
            db.ReportGenerations.Add(gen);
            db.SaveChanges();

            if (history.PPTEEFilename == null || history.ManualFileName != null)
            {
                RenderPPT renderPPT = new RenderPPT();
                filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".ppt";

                renderPPT.GeneratePPT(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/eeppt/" + history.ReportHistoryID);
                history.PPTEEFilename = filename;
                db.SaveChanges();

                renderPPT = null;
            }
            else
                filename = history.PPTEEFilename;

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=ppt&File=" + filename);

            GC.Collect();
        }

        protected void cmdBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }
    }
}