﻿using HiQPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NSS_2015_Report
{
    public partial class HistoryView : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Guid GenerationID = new Guid(RouteData.Values["ReportGenerationID"].ToString());
                ReportGeneration generation = db.ReportGenerations.Where(p => p.GenerationID == GenerationID).SingleOrDefault();
                ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == generation.ReportHistoryID).SingleOrDefault();

                lblReportName.Text = history.ReportName;
                lblReportType.Text = generation.ReportType;
                lblGeneratedby.Text = db.Users.Where(p => p.UserID == generation.UserID).Select(p => p.Name).SingleOrDefault();
                lblOutputType.Text = generation.OutputType;
                lblGeneratedDate.Text = generation.DateDrawn.ToString("dd MMM yyyy");
                lblResponses.Text = history.CurrentSampleSize.ToString();

                if (history.CurrentSampleSize < 150)
                {
                    rbReportType.Items.Clear();
                    rbReportType.Items.Add(new ListItem("Comprehensive Report<br /><i>(no less than 10 respondents)</i>", "Comprehensive"));
                    rbReportType.Items.Add(new ListItem("Executive Summary Report<br /><i>(no less than 10 respondents)</i>", "Executive"));
                }
                else
                {
                    rbReportType.Items.Clear();
                    rbReportType.Items.Add(new ListItem("Comprehensive Report<br /><i>(no less than 10 respondents)</i>", "Comprehensive"));
                    rbReportType.Items.Add(new ListItem("Executive Summary Report<br /><i>(no less than 10 respondents)</i>", "Executive"));
                    rbReportType.Items.Add(new ListItem("EE Barrier Report<br /><i>(no less than 150 respondents)</i>", "EEBarrier"));
                }

                lblYear1.Text = "2014";
                lblYear2.Text = history.Year2;
                lblCluster.Text = history.Cluster;
                lblY1Division1.Text = history.Y1Division1;
                lblY1Division2.Text = history.Y1Division2;
                lblY1Division3.Text = history.Y1Division3;
                lblY1Division4.Text = history.Y1Division4;
                lblY1Division5.Text = history.Y1Division5;
                lblY1Division6.Text = history.Y1Division6;
                lblY1Branch.Text = history.Y1Branch;
                lblY2Division1.Text = history.Y2Division1;
                lblY2Division2.Text = history.Y2Division2;
                lblY2Division3.Text = history.Y2Division3;
                lblY2Division4.Text = history.Y2Division4;
                lblY2Division5.Text = history.Y2Division5;
                lblY2Division6.Text = history.Y2Division6;
                lblY2Branch.Text = history.Y2Branch;
                lblLocation.Text = history.Location;

                lblManual.Text = (history.ManualBranchCodes != null ? history.ManualBranchCodes.ToLower().Trim().Split('|').Count() + " Manual Branch Codes applied." : history.ManualStaffCodes != null ? history.ManualStaffCodes.ToLower().Trim().Split('|').Count() + " Manual Staff Codes applied." : "") + " File used : <a target='blank' href='" + GetSiteRoot() + "/Imports/" + history.ManualFileName + "'>" + history.ManualFileName + "</a>";

                lblClassification.Text = history.Classification;
                lblJobFamily.Text = history.JobFamily;
                lblLOS.Text = history.LOS;
                lblRegion.Text = history.Region;
                lblPassage.Text = history.ManagementPassage;
                lblRace.Text = history.Race;
                lblOccLevel.Text = history.OccupationalLevel;
                lblGender.Text = history.Gender;

                if (generation.ReportType == "Comprehensive" || generation.ReportType == "Infograph 1" || generation.ReportType == "Infograph 2") rbReportType.SelectedIndex = 0; else if (generation.ReportType == "Executive Summary") rbReportType.SelectedIndex = 1; else rbReportType.SelectedIndex = 2;

                trComp.Visible = cmdExcel.Visible = cmdInfograph.Visible = rbReportType.SelectedIndex == 0;
                lblComp.Text = history.Composition.Replace("|", ", ");

                if (rbReportType.SelectedIndex == 0 && history.PPTCompFilename != null) txtPPT.Text = history.PPTCompFilename;
                if (rbReportType.SelectedIndex == 1 && history.PPTExecFilename != null) txtPPT.Text = history.PPTExecFilename;
                if (rbReportType.SelectedIndex == 2 && history.PPTEEFilename != null) txtPPT.Text = history.PPTEEFilename;
            }
        }

        protected void cmdClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/history");
        }

        protected void rbReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid GenerationID = new Guid(RouteData.Values["ReportGenerationID"].ToString());
            ReportGeneration generation = db.ReportGenerations.Where(p => p.GenerationID == GenerationID).SingleOrDefault();
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == generation.ReportHistoryID).SingleOrDefault();

            this.iframe.Attributes.Add("src", "");

            trComp.Visible = cmdExcel.Visible = cmdInfograph.Visible = rbReportType.SelectedIndex == 0;

            if (rbReportType.SelectedIndex == 0 && history.PPTCompFilename != null) txtPPT.Text = history.PPTCompFilename;
            if (rbReportType.SelectedIndex == 1 && history.PPTExecFilename != null) txtPPT.Text = history.PPTExecFilename;
            if (rbReportType.SelectedIndex == 2 && history.PPTEEFilename != null) txtPPT.Text = history.PPTEEFilename;
        }

        protected void cmdReport_Click(object sender, EventArgs e)
        {
            Guid GenerationID = new Guid(RouteData.Values["ReportGenerationID"].ToString());
            ReportGeneration generation = db.ReportGenerations.Where(p => p.GenerationID == GenerationID).SingleOrDefault();
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == generation.ReportHistoryID).SingleOrDefault();

            if (rbReportType.SelectedIndex == 0)
            {
                if (history.OnlineCompDate == null) history.OnlineCompDate = DateTime.Now;
                history.OnlineCompCount = history.OnlineCompCount == null ? 1 : history.OnlineCompCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "Online";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Comprehensive";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                Response.Redirect("~/comp/" + history.ReportHistoryID);
            }
            else if (rbReportType.SelectedIndex == 1)
            {
                if (history.OnlineExecDate == null) history.OnlineExecDate = DateTime.Now;
                history.OnlineExecCount = history.OnlineExecCount == null ? 1 : history.OnlineExecCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "Online";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Executive Summary";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                Response.Redirect("~/exec/" + history.ReportHistoryID);
            }
            else if (rbReportType.SelectedIndex == 2)
            {
                if (history.OnlineEEDate == null) history.OnlineEEDate = DateTime.Now;
                history.OnlineEECount = history.OnlineEECount == null ? 1 : history.OnlineEECount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "Online";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "EE Barrier";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                Response.Redirect("~/ee/" + history.ReportHistoryID);
            }
        }

        protected void cmdPDF_Click(object sender, EventArgs e)
        {
            Guid GenerationID = new Guid(RouteData.Values["ReportGenerationID"].ToString());
            ReportGeneration generation = db.ReportGenerations.Where(p => p.GenerationID == GenerationID).SingleOrDefault();
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == generation.ReportHistoryID).SingleOrDefault();

            string filename = "";

            if (rbReportType.SelectedIndex == 0)
            {
                if (history.PDFCompDate == null) history.PDFCompDate = DateTime.Now;
                history.PDFCompCount = history.PDFCompCount == null ? 1 : history.PDFCompCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PDF";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Comprehensive";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PDFCompFilename == null || history.ManualFileName != null)
                {
                    RenderPDF renderPDF = new RenderPDF();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                    renderPDF.GeneratePDF(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/comppdf/" + history.ReportHistoryID);
                    history.PDFCompFilename = filename;
                    db.SaveChanges();

                    renderPDF = null;
                }
                else
                    filename = history.PDFCompFilename;
            }
            else if (rbReportType.SelectedIndex == 1)
            {
                if (history.PDFExecDate == null) history.PDFExecDate = DateTime.Now;
                history.PDFExecCount = history.PDFExecCount == null ? 1 : history.PDFExecCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PDF";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Executive Summary";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PDFExecFilename == null || history.ManualFileName != null)
                {
                    RenderPDF renderPDF = new RenderPDF();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                    renderPDF.GeneratePDF(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/execpdf/" + history.ReportHistoryID);
                    history.PDFExecFilename = filename;
                    db.SaveChanges();

                    renderPDF = null;
                }
                else
                    filename = history.PDFExecFilename;
            }
            else if (rbReportType.SelectedIndex == 2)
            {
                if (history.PDFEEDate == null) history.PDFEEDate = DateTime.Now;
                history.PDFEECount = history.PDFEECount == null ? 1 : history.PDFEECount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PDF";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "EE Barrier";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PDFEEFilename == null || history.ManualFileName != null)
                {
                    RenderPDF renderPDF = new RenderPDF();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                    renderPDF.GeneratePDF(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/eepdf/" + history.ReportHistoryID);
                    history.PDFEEFilename = filename;
                    db.SaveChanges();

                    renderPDF = null;
                }
                else
                    filename = history.PDFEEFilename;
            }

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=pdf&File=" + filename);

            GC.Collect();
        }

        protected void cmdPPT_Click(object sender, EventArgs e)
        {
            Guid GenerationID = new Guid(RouteData.Values["ReportGenerationID"].ToString());
            ReportGeneration generation = db.ReportGenerations.Where(p => p.GenerationID == GenerationID).SingleOrDefault();
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == generation.ReportHistoryID).SingleOrDefault();

            string filename = "";

            if (rbReportType.SelectedIndex == 0)
            {
                if (history.PPTCompDate == null) history.PPTCompDate = DateTime.Now;
                history.PPTCompCount = history.PPTCompCount == null ? 1 : history.PPTCompCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PPT";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Comprehensive";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PPTCompFilename == null || history.ManualFileName != null)
                {
                    RenderPPT renderPPT = new RenderPPT();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".ppt";

                    renderPPT.GeneratePPT(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/compppt/" + history.ReportHistoryID);
                    history.PPTCompFilename = filename;
                    db.SaveChanges();

                    renderPPT = null;
                }
                else
                    filename = history.PPTCompFilename;
            }
            else if (rbReportType.SelectedIndex == 1)
            {
                if (history.PPTExecDate == null) history.PPTExecDate = DateTime.Now;
                history.PPTExecCount = history.PPTExecCount == null ? 1 : history.PPTExecCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PPT";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Executive Summary";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PPTExecFilename == null || history.ManualFileName != null)
                {
                    RenderPPT renderPPT = new RenderPPT();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".ppt";

                    renderPPT.GeneratePPT(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/execppt/" + history.ReportHistoryID);
                    history.PPTExecFilename = filename;
                    db.SaveChanges();

                    renderPPT = null;
                }
                else
                    filename = history.PPTExecFilename;
            }
            else if (rbReportType.SelectedIndex == 2)
            {
                if (history.PPTEEDate == null) history.PPTEEDate = DateTime.Now;
                history.PPTEECount = history.PPTEECount == null ? 1 : history.PPTEECount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PPT";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "EE Barrier";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PPTEEFilename == null || history.ManualFileName != null)
                {
                    RenderPPT renderPPT = new RenderPPT();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".ppt";

                    renderPPT.GeneratePPT(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/eeppt/" + history.ReportHistoryID);
                    history.PPTEEFilename = filename;
                    db.SaveChanges();

                    renderPPT = null;
                }
                else
                    filename = history.PPTEEFilename;
            }

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=ppt&File=" + filename);

            GC.Collect();
        }

        protected void cmdExcel_Click(object sender, EventArgs e)
        {
            string filename = "";

            ReportGeneration generation = db.ReportGenerations.Where(p => p.GenerationID == new Guid(RouteData.Values["ReportGenerationID"].ToString())).SingleOrDefault();
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == generation.ReportHistoryID).SingleOrDefault();

            if (history.ExcelDate == null) history.ExcelDate = DateTime.Now;
            history.ExcelCount = history.ExcelCount == null ? 1 : history.ExcelCount++;

            ReportGeneration gen = new ReportGeneration();
            gen.DateDrawn = DateTime.Now;
            gen.GenerationID = Guid.NewGuid();
            gen.OutputType = "Excel";
            gen.ReportHistoryID = history.ReportHistoryID;
            gen.ReportType = "Comprehensive";
            gen.UserID = UserID;
            db.ReportGenerations.Add(gen);
            db.SaveChanges();

            //if (history.ExcelFilename == null)
            //{
                RenderExcel excel = new RenderExcel();
                filename = excel.Generate(new Guid(RouteData.Values["ReportHistoryID"].ToString()), Server.MapPath("~/Exports"));
                history.ExcelFilename = filename;
                db.SaveChanges();
                excel = null;
            //}
            //else
            //    filename = history.ExcelFilename;

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=excel&File=" + filename);
        }

        protected void cmdInfograph_Click(object sender, EventArgs e)
        {
            string filename = "";

            Guid GenerationID = new Guid(RouteData.Values["ReportGenerationID"].ToString());
            ReportGeneration generation = db.ReportGenerations.Where(p => p.GenerationID == GenerationID).SingleOrDefault();
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == generation.ReportHistoryID).SingleOrDefault();

            if (history.InfographDate == null) history.InfographDate = DateTime.Now;
            history.InfographCount = history.InfographCount == null ? 1 : history.InfographCount++;

            ReportGeneration gen = new ReportGeneration();
            gen.DateDrawn = DateTime.Now;
            gen.GenerationID = Guid.NewGuid();
            gen.OutputType = "PDF";
            gen.ReportHistoryID = history.ReportHistoryID;
            gen.ReportType = "Infograph";
            gen.UserID = UserID;
            db.ReportGenerations.Add(gen);
            db.SaveChanges();

            if (history.InfographFilename == null || history.ManualFileName != null)
            {
                RenderPDF renderPDF = new RenderPDF();
                filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                renderPDF.GeneratePDF(Server.MapPath("~/exports") + "/" + filename, GetSiteRoot() + "/infograph/" + history.ReportHistoryID, PdfPageSize.A3, PdfPageOrientation.Portrait, 1754);
                history.InfographFilename = filename;
                db.SaveChanges();

                renderPDF = null;
            }
            else
                filename = history.InfographFilename;

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=pdf&File=" + filename);
        }        
    }
}