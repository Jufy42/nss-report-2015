﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NSS_2015_Report
{
    public partial class ViewOrders : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (GetCurrentUser() == null) Response.Redirect("~/login");
                else
                {
                    User user = GetCurrentUser();

                    rptOrders.DataSource = db.OrderDetails.Where(p => (bool)user.IsAdmin ? true : p.userSeq == user.UserID).Select(p => new { OrderID = p.OrderID, Seq = p.Seq, InvoiceNo = p.ManualInvoiceNumber, Fullname = db.Users.Where(r => r.UserID == p.userSeq).Select(r => r.Name).SingleOrDefault(), DateRequested = Convert.ToDateTime(p.DateRequested).Date.ToString(), OrderStatus = db.OrderStatus.Where(r => r.OrderSeq == p.OrderSeq).SingleOrDefault().Description, InvoiceStatus = db.InvoiceStatus.Where(r => r.InvoiceSeq == p.InvoiceSeq).SingleOrDefault().Description }).Distinct().ToList();
                    rptOrders.DataBind();
                }
            }
        }

        protected void cmdBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }

        protected void hpViewOrder_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/vieworder/" + (((sender as LinkButton).Parent as RepeaterItem).FindControl("hdOrderID") as HiddenField).Value);
        }
    }
}