﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HistoryView.aspx.cs" Inherits="NSS_2015_Report.HistoryView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="width:100%;padding:10px;">
        <asp:Panel ID="pnHistory" runat="server"  style="width:98%;background-color:#bfce89;-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;">
            <table style="width:100%;padding:5px;">
                <tr>
                    <td style="text-align:center;"><h3>Report Filters</h3></td>
                </tr>
                <tr>
                    <td style="text-align:left;">
                        <fieldset style="border:1px solid black;-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;">
                            <legend><b>Report Filters</b></legend>
                            <table style="width:100%;padding:5px;">
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Report Name</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblReportName" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;"><b>Report Type</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblReportType" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Generated by</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblGeneratedby" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;"><b>Output Type</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblOutputType" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Generated on</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblGeneratedDate" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;"><b>Responses</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblResponses" runat="server"></asp:Label></td>
                                </tr>
                                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Year 1</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblYear1" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;"><b>Year 2</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblYear2" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Cluster</b></td>
                                    <td style="text-align:left;width:85%;" colspan="3"><asp:Label ID="lblCluster" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Division 1</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY1Division1" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;">&nbsp;</td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY2Division1" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Division 2</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY1Division2" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;">&nbsp;</td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY2Division2" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Division 3</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY1Division3" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;">&nbsp;</td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY2Division3" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Division 4</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY1Division4" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;">&nbsp;</td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY2Division4" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Division 5</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY1Division5" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;">&nbsp;</td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY2Division5" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Division 6</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY1Division6" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;">&nbsp;</td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY2Division6" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Branch</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY1Branch" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;">&nbsp;</td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblY2Branch" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Location</b></td>
                                    <td style="text-align:left;width:85%;" colspan="3"><asp:Label ID="lblLocation" runat="server"></asp:Label></td>
                                </tr>
                                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="4" align="left"><asp:Label ID="lblManual" runat="server" width="175px"></asp:Label></td>
                                </tr>
                                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Classification</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblClassification" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;"><b>Job Family</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblJobFamily" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Length of Service</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblLOS" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;"><b>Region</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblRegion" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Management Passage</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblPassage" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;"><b>Race</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblRace" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;width:15%;"><b>Occupational Level</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblOccLevel" runat="server"></asp:Label></td>
                                    <td style="text-align:left;width:15%;"><b>Gender</b></td>
                                    <td style="text-align:left;width:35%;"><asp:Label ID="lblGender" runat="server"></asp:Label></td>
                                </tr>
                                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr id="trComp" runat="server">
                                    <td style="text-align:left;width:15%;"><b>Composition</b></td>
                                    <td style="text-align:left;width:85%;" colspan="3"><asp:Label ID="lblComp" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;"><h3>Actions</h3></td>
                </tr>
                <tr>
                    <td style="text-align:left;">
                        <fieldset style="border:1px solid black;-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;">
                            <legend><b>Actions</b></legend>
                            <table style="width:100%;padding:5px;">                                
                                <tr>
                                    <td style="text-align:center;width:100%;"><h4>Report Type</h4></td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;width:100%;margin: 0 auto;">
                                        <asp:RadioButtonList ID="rbReportType" runat="server" RepeatDirection="Horizontal" CssClass="ReportButtons" onselectedindexchanged="rbReportType_SelectedIndexChanged" AutoPostBack="true" ToolTip="Ordered reports are purely for the EE Barrier Excel and Demographic Split reports only"></asp:RadioButtonList>
                                    </td>
                                </tr>        
                                <tr><td>&nbsp;</td></tr>            
                                <tr><td><b>Powerpoint report link</b><asp:TextBox ID="txtPPT" runat="server" Width="800px"></asp:TextBox></td></tr>
                                <tr><td>&nbsp;</td></tr>            
                                <tr style="text-align:center;">
                                    <td><asp:Button ID="cmdReport" runat="server" Text="View Report" onclick="cmdReport_Click" CssClass="btn"/><asp:Button ID="cmdPDF" runat="server" Text="Export to PDF" onclick="cmdPDF_Click" CssClass="btn" OnClientClick="ShowProgress();"/><asp:Button ID="cmdPPT" runat="server" Text="Export to PowerPoint" onclick="cmdPPT_Click" CssClass="btn" OnClientClick="ShowProgress();" /><asp:Button ID="cmdExcel" runat="server" Text="Export to Excel" onclick="cmdExcel_Click" CssClass="btn" OnClientClick="ShowProgress();" /><asp:Button ID="cmdInfograph" runat="server" Text="Generate Infograph" CssClass="btn" onclick="cmdInfograph_Click" OnClientClick="ShowProgress();" /><asp:Button ID="cmdClose" runat="server" Text="Close" CssClass="btn" OnClick="cmdClose_Click" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <iframe id="iframe" src="" runat="server" width="0px" height="0px"></iframe>
        </asp:Panel>
    </div>
</asp:Content>
