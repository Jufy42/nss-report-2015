﻿using HiQPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NSS_2015_Report
{
    public partial class CompExecReport : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.iframe.Attributes.Add("src", "");

                cmdExcel.Visible = cmdPDF.Visible = cmdPPT.Visible = cmdInfograph.Visible = GetCurrentUser() != null;

                bool pdf = RouteData.Values["Format"].ToString() == "pdf" || RouteData.Values["Format"].ToString() == "ppt";

                if (pdf)
                {
                    BottomButtons.Visible = iframe.Visible = prgLoadingStatus.Visible = false;
                    Reportdiv.Attributes["style"] = "width:100%;background-color:White;color:Black;text-align:center;padding:0;margin:0;";
                }
                else
                {
                    BottomButtons.Visible = iframe.Visible = prgLoadingStatus.Visible = true;
                    Reportdiv.Attributes["style"] = "width:100%;background-color:White;color:Black;Height:768px;overflow-y:scroll;text-align:center;padding:0;margin:0;";
                }

                Guid HistoryID = new Guid(RouteData.Values["ReportHistoryID"].ToString());
                ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == HistoryID).FirstOrDefault();
                List<Question> questions = db.Questions.Distinct().ToList();

                List<Dimension> Dimensionslist = db.Dimensions.Distinct().ToList();
                bool execsum = RouteData.Values["Report"].ToString() == "exec";

                List<Year> year1Dataset = new List<Year>();
                List<Year> year2Dataset = new List<Year>();
                List<Year> year1NPSDataset = new List<Year>();
                List<Year> year2NPSDataset = new List<Year>();
                List<Year> year1GroupDataset = new List<Year>();
                List<Year> year2GroupDataset = new List<Year>();
                List<Year> year1ClustDataset = new List<Year>();
                List<Year> year2ClustDataset = new List<Year>();
                List<Year> year1Div1Dataset = new List<Year>();
                List<Year> year2Div1Dataset = new List<Year>();
                List<Year> year1Div2Dataset = new List<Year>();
                List<Year> year2Div2Dataset = new List<Year>();
                List<Year> year1Div3Dataset = new List<Year>();
                List<Year> year2Div3Dataset = new List<Year>();
                List<Year> year1Div4Dataset = new List<Year>();
                List<Year> year2Div4Dataset = new List<Year>();
                List<Year> year1Div5Dataset = new List<Year>();
                List<Year> year2Div5Dataset = new List<Year>();
                List<Year> year1Div6Dataset = new List<Year>();
                List<Year> year2Div6Dataset = new List<Year>();
                List<Year> year1BranchDataset = new List<Year>();
                List<Year> year2BranchDataset = new List<Year>();

                ComprehensiveDimension compdimensionyear1 = new ComprehensiveDimension();
                ComprehensiveDimension compdimensionyear2 = new ComprehensiveDimension();
                List<StatementShift> statementshifts = new List<StatementShift>();
                List<StatementShift> statementshiftsee = new List<StatementShift>();

                string reportname = history.ReportName;
                string cluster = history.Cluster;
                List<string> year1division1 = history.Y1Division1.Split('|').ToList();
                List<string> year1division2 = history.Y1Division2.Split('|').ToList();
                List<string> year1division3 = history.Y1Division3.Split('|').ToList();
                List<string> year1division4 = history.Y1Division4.Split('|').ToList();
                List<string> year1division5 = history.Y1Division5.Split('|').ToList();
                List<string> year1division6 = history.Y1Division6.Split('|').ToList();
                List<string> year1branch = history.Y1Branch.Split('|').ToList();
                List<string> year2division1 = history.Y2Division1.Split('|').ToList();
                List<string> year2division2 = history.Y2Division2.Split('|').ToList();
                List<string> year2division3 = history.Y2Division3.Split('|').ToList();
                List<string> year2division4 = history.Y2Division4.Split('|').ToList();
                List<string> year2division5 = history.Y2Division5.Split('|').ToList();
                List<string> year2division6 = history.Y2Division6.Split('|').ToList();
                List<string> year2branch = history.Y2Branch.Split('|').ToList();

                string demographics = history.Composition.Replace("|", ", ").Replace(" ", "").Replace("Disability", "Disabilities").Replace("Lengthofservice", "LOS").Replace("EEOccupationalLevel", "OccupationalLevel").Replace("GenerationalClassification", "Classification");
                string sample = cluster == "All" ? "Post Sample" : "";
                string year2 = history.Year2;

                string header = Block.BlockComprehensiveHeaderRow(true);
                int headercount = 0;

                year1NPSDataset = year1Dataset = year.YearDataSet();
                year2NPSDataset = year2Dataset = year.YearDataSet(year2);

                string npsheader1 = NPS.getNPSHeaderRows("Nedbank Group (Post Sample)", year1Dataset.Where(p => p.QNPS1 != null && (p.IsSample ?? false)).Distinct().ToList(), "1");
                int npsheadercount1 = 1;
                string npsheader2 = NPS.getNPSHeaderRows("Nedbank Group (Post Sample)", year1Dataset.Where(p => p.QNPS2 != null && (p.IsSample ?? false)).Distinct().ToList(), "2");
                int npsheadercount2 = 1;

                year1GroupDataset = year1Dataset.Where(p => (p.IsSample ?? false)).Distinct().ToList();
                year2GroupDataset = year2Dataset.Where(p => (p.IsSample ?? false)).Distinct().ToList();

                if (sample == "Post Sample")
                {
                    year1Dataset = year1GroupDataset;
                    year2Dataset = year2GroupDataset;
                }

                string classification = history.Classification;
                string occlevel = history.OccupationalLevel;
                string los = history.LOS;
                string passage = history.ManagementPassage;
                List<string> location = history.Location.Split('|').ToList();
                string gender = history.Gender;
                string race = history.Race;
                string jobfamily = history.JobFamily;
                string region = history.Region;

                bool africa = location.FirstOrDefault().ToLower() != "all" && location.FirstOrDefault().ToLower() != "south africa" && location.FirstOrDefault().ToLower() != "uk";

                header += Block.getBlockHeaderRows("Nedbank Group", year1GroupDataset.Count, year2GroupDataset.Count, year.GetCompDimensions(year1GroupDataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2GroupDataset, Dimensionslist, questions, year2, africa), year2);
                headercount++;

                if (history.ManualBranchCodes != null) {
                    List<string> ManualBranchCodes = history.ManualBranchCodes.ToLower().Trim().Split('|').ToList();
                    year1Dataset = year1Dataset.Join(ManualBranchCodes, y1 => y1.Branch.NullCheck().ToLower().Trim(), manual => manual.ToLower().Trim(), (y1, manual) => new { y1 }).Select(p => p.y1).Distinct().ToList();
                    year2Dataset = year2Dataset.Join(ManualBranchCodes, y2 => y2.Branch.NullCheck().ToLower().Trim(), manual => manual.ToLower().Trim(), (y2, manual) => new { y2 }).Select(p => p.y2).Distinct().ToList(); 
                }
                if (history.ManualStaffCodes != null) {
                    List<string> ManualStaffCodes = history.ManualStaffCodes.ToLower().Trim().Split('|').ToList();
                    year1Dataset = year1Dataset.Join(ManualStaffCodes, y1 => y1.StaffCode.NullCheck().ToLower().Trim(), manual => manual.ToLower().Trim(), (y1, manual) => new { y1 }).Select(p => p.y1).Distinct().ToList();
                    year2Dataset = year2Dataset.Join(ManualStaffCodes, y2 => y2.StaffCode.NullCheck().ToLower().Trim(), manual => manual.ToLower().Trim(), (y2, manual) => new { y2 }).Select(p => p.y2).Distinct().ToList(); 
                }

                if (location.FirstOrDefault().ToLower() != "all") { year1Dataset = year1Dataset.Join(location, y1 => y1.Location.NullCheck().ToLower().Trim(), loc => loc.ToLower().Trim(), (y1, loc) => new { y1 }).Select(p => p.y1).Distinct().ToList(); year2Dataset = year2Dataset.Join(location, y2 => y2.Location.NullCheck().ToLower().Trim(), loc => loc.ToLower().Trim(), (y2, loc) => new { y2 }).Select(p => p.y2).Distinct().ToList(); }
                if (classification != "All") { year1Dataset = year1Dataset.Where(p => p.Classification.NullCheck().ToLower().Trim() == classification.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Classification.NullCheck().ToLower().Trim() == classification.ToLower().Trim()).Distinct().ToList(); }
                if (jobfamily != "All") { year1Dataset = year1Dataset.Where(p => p.JobFamily.Normalize().ToLower().Trim() == jobfamily.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.JobFamily.NullCheck().ToLower().Trim() == jobfamily.ToLower().Trim()).Distinct().ToList(); }
                if (los != "All") { year1Dataset = year1Dataset.Where(p => p.LOS.NullCheck().ToLower().Trim() == los.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.LOS.NullCheck().ToLower().Trim() == los.ToLower().Trim()).Distinct().ToList(); }
                if (region != "All") { year1Dataset = year1Dataset.Where(p => p.Region.NullCheck().ToLower().Trim() == region.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Region.NullCheck().ToLower().Trim() == region.ToLower().Trim()).Distinct().ToList(); }
                if (passage != "All") { year1Dataset = year1Dataset.Where(p => p.ManagementPassage.NullCheck().ToLower().Trim() == passage.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.ManagementPassage.NullCheck().ToLower().Trim() == passage.ToLower().Trim()).Distinct().ToList(); }
                if (race != "All") { year1Dataset = year1Dataset.Where(p => p.Race.NullCheck().ToLower().Trim() == race.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Race.NullCheck().ToLower().Trim() == race.ToLower().Trim()).Distinct().ToList(); }
                if (occlevel != "All") { year1Dataset = year1Dataset.Where(p => p.OccupationalLevel.NullCheck().ToLower().Trim() == occlevel.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.OccupationalLevel.NullCheck().ToLower().Trim() == occlevel.ToLower().Trim()).Distinct().ToList(); }
                if (gender != "All") { year1Dataset = year1Dataset.Where(p => p.Gender.NullCheck().ToLower().Trim() == gender.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Gender.NullCheck().ToLower().Trim() == gender.ToLower().Trim()).Distinct().ToList(); }

                if (cluster != "All")
                {
                    year1Dataset = year1ClustDataset = year1Dataset.Where(p => p.Cluster.ToLower().Trim() == cluster.ToLower().Trim()).Distinct().ToList();
                    year2ClustDataset = year2Dataset.Where(p => p.Cluster.ToLower().Trim() == cluster.ToLower().Trim()).Distinct().ToList();

                    if ((cluster != "RBB" && cluster != "CIB") || (year2division1.FirstOrDefault().ToLower() == "all"
                        && year2division2.FirstOrDefault().ToLower() == "all"
                        && year2division3.FirstOrDefault().ToLower() == "all"
                        && year2division4.FirstOrDefault().ToLower() == "all"
                        && year2division5.FirstOrDefault().ToLower() == "all"
                        && year2division6.FirstOrDefault().ToLower() == "all"
                        && year2branch.FirstOrDefault().ToLower() == "all")) year2Dataset = year2ClustDataset;

                    header += Block.getBlockHeaderRows(cluster, year1ClustDataset.Count, year2ClustDataset.Count, year.GetCompDimensions(year1ClustDataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2ClustDataset, Dimensionslist, questions, year2, africa), year2);
                    headercount++;

                    npsheader1 += NPS.getNPSHeaderRows(cluster, year1ClustDataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                    npsheadercount1++;
                    npsheader2 += NPS.getNPSHeaderRows(cluster, year1ClustDataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                    npsheadercount2++;
                }

                if (year1division1.FirstOrDefault().ToLower() != "all") year1Dataset = year1Div1Dataset = year1Dataset.Join(year1division1, y1 => y1.Division1.NullCheck().ToLower().Trim(), d1 => d1.ToLower().Trim(), (y1, d1) => new { y1 }).Select(p => p.y1).Distinct().ToList();
                if (year2division1.FirstOrDefault().ToLower() != "all") year2Dataset = year2Div1Dataset = year2Dataset.Join(year2division1, y2 => y2.Division1.NullCheck().ToLower().Trim(), d1 => d1.ToLower().Trim(), (y2, d1) => new { y2 }).Select(p => p.y2).Distinct().ToList();
                if (year1division2.FirstOrDefault().ToLower() != "all") year1Dataset = year1Div2Dataset = year1Dataset.Join(year1division2, y1 => y1.Division2.NullCheck().ToLower().Trim(), d2 => d2.ToLower().Trim(), (y1, d2) => new { y1 }).Select(p => p.y1).Distinct().ToList();
                if (year2division2.FirstOrDefault().ToLower() != "all") year2Dataset = year2Div2Dataset = year2Dataset.Join(year2division2, y2 => y2.Division2.NullCheck().ToLower().Trim(), d2 => d2.ToLower().Trim(), (y2, d2) => new { y2 }).Select(p => p.y2).Distinct().ToList();
                if (year1division3.FirstOrDefault().ToLower() != "all") year1Dataset = year1Div3Dataset = year1Dataset.Join(year1division3, y1 => y1.Division3.NullCheck().ToLower().Trim(), d3 => d3.ToLower().Trim(), (y1, d3) => new { y1 }).Select(p => p.y1).Distinct().ToList();
                if (year2division3.FirstOrDefault().ToLower() != "all") year2Dataset = year2Div3Dataset = year2Dataset.Join(year2division3, y2 => y2.Division3.NullCheck().ToLower().Trim(), d3 => d3.ToLower().Trim(), (y2, d3) => new { y2 }).Select(p => p.y2).Distinct().ToList();
                if (year1division4.FirstOrDefault().ToLower() != "all") year1Dataset = year1Div4Dataset = year1Dataset.Join(year1division4, y1 => y1.Division4.NullCheck().ToLower().Trim(), d4 => d4.ToLower().Trim(), (y1, d4) => new { y1 }).Select(p => p.y1).Distinct().ToList();
                if (year2division4.FirstOrDefault().ToLower() != "all") year2Dataset = year2Div4Dataset = year2Dataset.Join(year2division4, y2 => y2.Division4.NullCheck().ToLower().Trim(), d4 => d4.ToLower().Trim(), (y2, d4) => new { y2 }).Select(p => p.y2).Distinct().ToList();
                if (year1division5.FirstOrDefault().ToLower() != "all") year1Dataset = year1Div5Dataset = year1Dataset.Join(year1division5, y1 => y1.Division5.NullCheck().ToLower().Trim(), d5 => d5.ToLower().Trim(), (y1, d5) => new { y1 }).Select(p => p.y1).Distinct().ToList();
                if (year2division5.FirstOrDefault().ToLower() != "all") year2Dataset = year2Div5Dataset = year2Dataset.Join(year2division5, y2 => y2.Division5.NullCheck().ToLower().Trim(), d5 => d5.ToLower().Trim(), (y2, d5) => new { y2 }).Select(p => p.y2).Distinct().ToList();
                if (year1division6.FirstOrDefault().ToLower() != "all") year1Dataset = year1Div6Dataset = year1Dataset.Join(year1division6, y1 => y1.Division6.NullCheck().ToLower().Trim(), d6 => d6.ToLower().Trim(), (y1, d6) => new { y1 }).Select(p => p.y1).Distinct().ToList();
                if (year2division6.FirstOrDefault().ToLower() != "all") year2Dataset = year2Div6Dataset = year2Dataset.Join(year2division6, y2 => y2.Division6.NullCheck().ToLower().Trim(), d6 => d6.ToLower().Trim(), (y2, d6) => new { y2 }).Select(p => p.y2).Distinct().ToList();
                if (year1branch.FirstOrDefault().ToLower() != "all") year1Dataset = year1BranchDataset = year1Dataset.Join(year1branch, y1 => y1.Branch.NullCheck().ToLower().Trim(), dbranch => dbranch.ToLower().Trim(), (y1, dbranch) => new { y1 }).Select(p => p.y1).Distinct().ToList();
                if (year2branch.FirstOrDefault().ToLower() != "all") year2Dataset = year2BranchDataset = year2Dataset.Join(year2branch, y2 => y2.Branch.NullCheck().ToLower().Trim(), dbranch => dbranch.ToLower().Trim(), (y2, dbranch) => new { y2 }).Select(p => p.y2).Distinct().ToList();

                compdimensionyear1 = year.GetCompDimensions(year1Dataset, Dimensionslist, questions, africa);
                compdimensionyear2 = year.GetCompDimensions(year2Dataset, Dimensionslist, questions, year2, africa);

                string caption = "2015 n = " + year1Dataset.Count + "; " + (year2 != "None" ? year2 + " n = " + year2Dataset.Count + (year2 == "2010" && cluster == "All" ? " Excl. Imperial Bank" : "") : "");

                if (year2Dataset.Count > 0)
                {
                    foreach (Statement statement in compdimensionyear1.CompStatements.Where(p => p.DimensionName != ""))
                    {
                        double result = 0;
                        bool isSignificant = false;
                        Significance.getSignificance(statement.StatementCount, (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault().StatementCount : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault().StatementCount : 0)), statement.StatementPositiveValue, (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault().StatementPositiveValue : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault().StatementPositiveValue : 0)), out result, out isSignificant);
                        if (statement.DimensionName == "Employment Equity") statementshiftsee.Add(new StatementShift(statement.QuestionIndex, result, isSignificant));
                        statementshifts.Add(new StatementShift(statement.QuestionIndex, result, isSignificant));
                    }
                }

                if (execsum) { cmdInfograph.Visible = cmdExcel.Visible = false; }

                int pageno = 1;

                litReport.Text += PageTemplate.Template("", history.ReportName, "<tr><td style=\"width:100%;height:792px;text-align:left;vertical-align:text-top;background-image:url('" + GetSiteRoot() + "/Images/frontpage-2013.jpg');background-repeat:no-repeat;background-position:center;font-family:Arial;font-weight:bold;font-size:28px;margin:0;padding:0;\"><span style=\"top:75px;left:150px;color:black;position:relative;display:block;\">Nedbank Staff Survey 2015<p style=\"Color:#000000;font-weight:Bold;Font-Size:28px;\">" + (execsum == true ? "Executive Summary" : "Comprehensive Report") + "</p><p style=\"Color:#000000;font-weight:Bold;Font-Size:28px;\">" + reportname + " " + sample + "</p><br /><br /><br /><br /><br /><br /><br /><br /></span><center><span style=\"Color:#000000;Font-Size:16px;font-weight:normal;\">" + (pdf ? "" : "<br/>") + "Confidential<br><br>Contributors: Market and Customer Insights, Group Human Resources,<br>Pure Survey Research (Pty) Ltd, Consulta Research (Pty) Ltd, and Aon Hewitt Ltd.</span></center></span></td></tr>", pdf, pageno++, true);

                litReport.Text += FiltersPage(history, reportname + " " + sample, pdf, pageno++);

                if (Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) <= 2011) litReport.Text += DimensionChart("2015", year2, year.GetCompDimensions(year1Dataset, Dimensionslist, questions, africa, true), compdimensionyear2, year1Dataset.Count, year2Dataset.Count, caption, "Comparative of MEAN scores for " + reportname + " " + sample + " - 12 Dimensions", reportname, ref pageno, true, pdf, year2, true, africa);
                litReport.Text += DimensionChart("2015", year2, compdimensionyear1, compdimensionyear2, year1Dataset.Count, year2Dataset.Count, caption, "Comparative of MEAN scores for " + reportname + " " + sample + " - 14 Dimensions", reportname, ref pageno, true, pdf, year2, true, africa);
                if (cluster != "All") litReport.Text += DimensionChart("Nedbank Group", cluster, year.GetCompDimensions(year1GroupDataset, Dimensionslist, questions, africa), year.GetCompDimensions(year1ClustDataset, Dimensionslist, questions, africa), year1GroupDataset.Count, year1ClustDataset.Count, "Nedbank Group n = " + year1GroupDataset.Count + "; " + cluster + " n = " + year1ClustDataset.Count, cluster + " compared to Nedbank Group Post Sample - 14 Dimensions", reportname + " " + sample, ref pageno, true, pdf, "2015", true, false);
                if (year1division1.FirstOrDefault().ToLower() != "all") litReport.Text += DimensionChart(cluster, "Selected Sample", year.GetCompDimensions(year1ClustDataset, Dimensionslist, questions, africa), compdimensionyear1, year1ClustDataset.Count, year1Dataset.Count, cluster + " n = " + year1ClustDataset.Count + "; Selected Sample n = " + year1Dataset.Count, reportname + " compared to " + cluster + " - 14 Dimensions", reportname + " " + sample + " - 14 Dimensions", ref pageno, true, pdf, "2015", true, false);

                string lowestlevel = "Nedbank";
                if (cluster != "All") lowestlevel = "Cluster";
                if (year1division1.FirstOrDefault().ToLower() != "all") lowestlevel = "Division1";
                if (year1division2.FirstOrDefault().ToLower() != "all") lowestlevel = "Division2";
                if (year1division3.FirstOrDefault().ToLower() != "all") lowestlevel = "Division3";
                if (year1division4.FirstOrDefault().ToLower() != "all") lowestlevel = "Division4";
                if (year1division5.FirstOrDefault().ToLower() != "all") lowestlevel = "Division5";
                if (year1division6.FirstOrDefault().ToLower() != "all") lowestlevel = "Division6";
                if (year1branch.FirstOrDefault().ToLower() != "all") lowestlevel = "Branch";

                if (cluster != "All" && !execsum)
                {
                    if (lowestlevel == "Cluster") litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1ClustDataset, year2ClustDataset, year, "Division1", "Division  1", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);

                    if (lowestlevel == "Division1")
                    {
                        if (year1division1.Count > 1)
                        {
                            header += Block.getBlockHeaderRows("Division 1", year1Div1Dataset.Count, year2Div1Dataset.Count, year.GetCompDimensions(year1Div1Dataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2Div1Dataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Div1Dataset, year2Div1Dataset, year, "Division1", "Division 1", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                        else
                        {
                            header += Block.getBlockHeaderRows(year1division1.FirstOrDefault(), year1Div1Dataset.Count, year2Div1Dataset.Count, year.GetCompDimensions(year1Div1Dataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2Div1Dataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Div1Dataset, year2Div1Dataset, year, "Division2", "Division 1", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                    }

                    if (lowestlevel == "Division2")
                    {
                        if (year1division2.Count > 1)
                        {
                            header += Block.getBlockHeaderRows("Division 2", year1Div2Dataset.Count, year2Div2Dataset.Count, year.GetCompDimensions(year1Div2Dataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2Div2Dataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Div2Dataset, year2Div2Dataset, year, "Division2", "Division 2", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                        else
                        {
                            header += Block.getBlockHeaderRows(year1division2.FirstOrDefault(), year1Div2Dataset.Count, year2Div2Dataset.Count, year.GetCompDimensions(year1Div2Dataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2Div2Dataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Div2Dataset, year2Div2Dataset, year, "Division3", "Division 2", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                    }

                    if (lowestlevel == "Division3")
                    {
                        if (year1division3.Count > 1)
                        {
                            header += Block.getBlockHeaderRows("Division 3", year1Div3Dataset.Count, year2Div3Dataset.Count, year.GetCompDimensions(year1Div3Dataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2Div3Dataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Div3Dataset, year2Div3Dataset, year, "Division3", "Division 3", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                        else
                        {
                            header += Block.getBlockHeaderRows(year1division3.FirstOrDefault(), year1Div3Dataset.Count, year2Div3Dataset.Count, year.GetCompDimensions(year1Div3Dataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2Div3Dataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Div3Dataset, year2Div3Dataset, year, "Division4", "Division 3", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                    }

                    if (lowestlevel == "Division4")
                    {
                        if (year1division4.Count > 1)
                        {
                            header += Block.getBlockHeaderRows("Division 4", year1Div4Dataset.Count, year2Div4Dataset.Count, year.GetCompDimensions(year1Div4Dataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2Div4Dataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Div4Dataset, year2Div4Dataset, year, "Division4", "Division 4", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                        else
                        {
                            header += Block.getBlockHeaderRows(year1division4.FirstOrDefault(), year1Div4Dataset.Count, year2Div4Dataset.Count, year.GetCompDimensions(year1Div4Dataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2Div4Dataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Div4Dataset, year2Div4Dataset, year, "Division5", "Division 4", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                    }

                    if (lowestlevel == "Division5")
                    {
                        if (year1division5.Count > 1)
                        {
                            header += Block.getBlockHeaderRows("Division 5", year1Div5Dataset.Count, year2Div5Dataset.Count, year.GetCompDimensions(year1Div5Dataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2Div5Dataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Div5Dataset, year2Div5Dataset, year, "Division5", "Division 5", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                        else
                        {
                            header += Block.getBlockHeaderRows(year1division5.FirstOrDefault(), year1Div5Dataset.Count, year2Div5Dataset.Count, year.GetCompDimensions(year1Div5Dataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2Div5Dataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Div5Dataset, year2Div5Dataset, year, "Division6", "Division 5", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                    }

                    if (lowestlevel == "Division6")
                    {
                        if (year1division6.Count > 1)
                        {
                            header += Block.getBlockHeaderRows("Division 6", year1Div6Dataset.Count, year2Div6Dataset.Count, year.GetCompDimensions(year1Div6Dataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2Div6Dataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Div6Dataset, year2Div6Dataset, year, "Division6", "Division 6", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                        else
                        {
                            header += Block.getBlockHeaderRows(year1division6.FirstOrDefault(), year1Div6Dataset.Count, year2Div6Dataset.Count, year.GetCompDimensions(year1Div6Dataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2Div6Dataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Div6Dataset, year2Div6Dataset, year, "Branch", "Division 6", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                    }

                    if (lowestlevel == "Branch")
                    {
                        if (year1branch.Count > 1)
                        {
                            header += Block.getBlockHeaderRows("Division 6", year1BranchDataset.Count, year2BranchDataset.Count, year.GetCompDimensions(year1BranchDataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2BranchDataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                            litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1BranchDataset, year2BranchDataset, year, "Branch", "Branch", year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                        }
                        else
                        {
                            header += Block.getBlockHeaderRows(year1branch.FirstOrDefault(), year1BranchDataset.Count, year2BranchDataset.Count, year.GetCompDimensions(year1BranchDataset, Dimensionslist, questions, africa), year.GetCompDimensions(year2BranchDataset, Dimensionslist, questions, year2, africa), year2);
                            headercount++;
                        }
                    }                    

                    string[] demos = demographics.Split(',');
                    foreach (string demo in demos) litReport.Text += Block.GenerateBlockChart(caption, reportname + " " + sample, year1Dataset, year2Dataset, year, demo, demo, year2, header, headercount, ref pageno, africa, Dimensionslist, questions, pdf);
                }

                if (lowestlevel == "Nedbank")
                {
                    litReport.Text += NPS.GenerateNPSChart(reportname, year1NPSDataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Cluster", "Cluster", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                    litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                    litReport.Text += NPS.GenerateNPSChart(reportname, year1NPSDataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Cluster", "Cluster", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                    litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                }
                else
                {
                    if (lowestlevel == "Cluster")
                    {
                        litReport.Text += NPS.GenerateNPSChart(reportname, year1ClustDataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division1", "Division 1", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                        litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                        litReport.Text += NPS.GenerateNPSChart(reportname, year1ClustDataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division1", "Division 1", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                        litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                    }

                    if (lowestlevel == "Division1")
                    {
                        if (year1division1.Count > 1)
                        {
                            npsheader1 += NPS.getNPSHeaderRows("Division 1", year1Div1Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows("Division 1", year1Div1Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div1Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division1", "Division 1", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div1Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division1", "Division 1", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                        else
                        {
                            npsheader1 += NPS.getNPSHeaderRows(year1division1.FirstOrDefault(), year1Div1Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows(year1division1.FirstOrDefault(), year1Div1Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div1Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division2", "Division 1", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div1Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division2", "Division 1", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                    }

                    if (lowestlevel == "Division2")
                    {
                        if (year1division2.Count > 1)
                        {
                            npsheader1 += NPS.getNPSHeaderRows("Division 2", year1Div2Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows("Division 2", year1Div2Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div2Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division2", "Division 2", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div2Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division2", "Division 2", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                        else
                        {
                            npsheader1 += NPS.getNPSHeaderRows(year1division2.FirstOrDefault(), year1Div2Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows(year1division2.FirstOrDefault(), year1Div2Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div2Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division3", "Division 2", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div2Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division3", "Division 2", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                    }

                    if (lowestlevel == "Division3")
                    {
                        if (year1division3.Count > 1)
                        {
                            npsheader1 += NPS.getNPSHeaderRows("Division 3", year1Div3Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows("Division 3", year1Div3Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div3Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division3", "Division 3", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div3Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division3", "Division 3", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                        else
                        {
                            npsheader1 += NPS.getNPSHeaderRows(year1division3.FirstOrDefault(), year1Div3Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows(year1division3.FirstOrDefault(), year1Div3Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div3Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division4", "Division 3", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div3Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division4", "Division 3", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                    }

                    if (lowestlevel == "Division4")
                    {
                        if (year1division4.Count > 1)
                        {
                            npsheader1 += NPS.getNPSHeaderRows("Division 4", year1Div4Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows("Division 4", year1Div4Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div4Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division4", "Division 4", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div4Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division4", "Division 4", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                        else
                        {
                            npsheader1 += NPS.getNPSHeaderRows(year1division4.FirstOrDefault(), year1Div4Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows(year1division4.FirstOrDefault(), year1Div4Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div4Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division5", "Division 4", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div4Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division5", "Division 4", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                    }

                    if (lowestlevel == "Division5")
                    {
                        if (year1division5.Count > 1)
                        {
                            npsheader1 += NPS.getNPSHeaderRows("Division 5", year1Div5Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows("Division 5", year1Div5Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div5Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division5", "Division 5", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div5Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division5", "Division 5", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                        else
                        {
                            npsheader1 += NPS.getNPSHeaderRows(year1division5.FirstOrDefault(), year1Div5Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows(year1division5.FirstOrDefault(), year1Div5Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div5Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division6", "Division 5", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div5Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division6", "Division 5", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                    }

                    if (lowestlevel == "Division6")
                    {
                        if (year1division6.Count > 1)
                        {
                            npsheader1 += NPS.getNPSHeaderRows("Division 6", year1Div6Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows("Division 6", year1Div6Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div6Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division6", "Division 6", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div6Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division6", "Division 6", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                        else
                        {
                            npsheader1 += NPS.getNPSHeaderRows(year1division6.FirstOrDefault(), year1Div6Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows(year1division6.FirstOrDefault(), year1Div6Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div6Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Branch", "Division 6", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1Div6Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Branch", "Division 6", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                    }

                    if (lowestlevel == "Branch")
                    {
                        if (year1branch.Count > 1)
                        {
                            npsheader1 += NPS.getNPSHeaderRows("Branch", year1BranchDataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows("Branch", year1BranchDataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            litReport.Text += NPS.GenerateNPSChart(reportname, year1BranchDataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Branch", "Branch", npsheader1, npsheadercount1, "1", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to bank”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS1Comment).Distinct().ToList()), 1), pdf, pageno++, false);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year1BranchDataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Branch", "Branch", npsheader2, npsheadercount2, "2", year2, ref pageno, execsum, pdf, false);
                            litReport.Text += PageTemplate.Template("Wordcloud* - Likelihood that you would recommend Nedbank as a “great place to work”?", reportname, TagCloudGenerator.GenerateTagCloud(TagCloudGenerator.GenerateList(year1Dataset.Select(p => p.QNPS2Comment).Distinct().ToList()), 2), pdf, pageno++, false);
                        }
                        else
                        {
                            npsheader1 += NPS.getNPSHeaderRows(year1branch.FirstOrDefault(), year1BranchDataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows(year1branch.FirstOrDefault(), year1BranchDataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;
                        }
                    }
                }

                litReport.Text += PageTemplate.Template("", history.ReportName, "<tr><td style=\"width:100%;height:792px;text-align:left;vertical-align:text-top;background-image:url('" + GetSiteRoot() + "/Images/frontpage-2013.jpg');background-repeat:no-repeat;background-position:center;font-family:Arial;font-weight:bold;font-size:28px;margin:0;padding:0;\"><span style=\"top:75px;left:150px;color:black;position:relative;\">Nedbank Staff Survey 2015<br /><span style=\"Color:#000000;font-weight:Bold;Font-Size:28px;\">" + (execsum == true ? "Executive Summary" : "Comprehensive Report") + "</span><br /><br /><span style=\"Color:#000000;font-weight:Bold;Font-Size:28px;\">" + reportname + " " + sample + "</span><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /></span><center><span style=\"Color:#000000;font-weight:Bold;Font-Size:28px;\">" + (pdf ? "" : "<br/>") + "Appendix</span></center></span></td></tr>", pdf, pageno++, true);

                litReport.Text += ScalePage(history, reportname + " " + sample, pdf, pageno++);

                if (!execsum)
                {
                    litReport.Text += AboutPage(history, "About the Nedbank Staff Survey - " + reportname + " " + sample, pdf, pageno++);
                    litReport.Text += MethodologyPage(history, "Methodology - " + reportname + " " + sample, pdf, pageno++);

                    if (demographics != "") litReport.Text += DemographicProfileCharts(year1Dataset, ref pageno, reportname, reportname + " " + sample, demographics, pdf);

                    litReport.Text += TopBottomStatements(year2, compdimensionyear1, compdimensionyear2, statementshifts, caption, reportname + " " + sample, year2Dataset.Count, africa, questions, Dimensionslist, pageno++, pdf);

                    if (year2 != "None" && year2Dataset.Count >= 10) litReport.Text += LeastMostStatements(year2, compdimensionyear1, compdimensionyear2, statementshifts, caption, reportname + " " + sample, africa, questions, Dimensionslist, pageno++, pdf);

                    litReport.Text += StatementCharts(year2, compdimensionyear1, compdimensionyear2, year1Dataset.Count, year2Dataset.Count, statementshifts, caption, reportname + " " + sample, reportname, ref pageno, africa, questions, Dimensionslist, pdf);

                    if (Convert.ToInt32(year2) >= 2012)
                    {
                        npsheader1 = NPS.getNPSHeaderRows("Nedbank Group (Post Sample)", year2NPSDataset.Where(p => p.QNPS1 != null && (p.IsSample ?? false)).Distinct().ToList(), "1");
                        npsheadercount1 = 1;
                        npsheader2 = NPS.getNPSHeaderRows("Nedbank Group (Post Sample)", year2NPSDataset.Where(p => p.QNPS2 != null && (p.IsSample ?? false)).Distinct().ToList(), "2");
                        npsheadercount2 = 1;

                        if (lowestlevel == "Nedbank")
                        {
                            litReport.Text += NPS.GenerateNPSChart(reportname, year2NPSDataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Cluster", "Cluster", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                            litReport.Text += NPS.GenerateNPSChart(reportname, year2NPSDataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Cluster", "Cluster", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                        }
                        else
                        {
                            npsheader1 += NPS.getNPSHeaderRows(cluster, year2ClustDataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                            npsheadercount1++;
                            npsheader2 += NPS.getNPSHeaderRows(cluster, year2ClustDataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                            npsheadercount2++;

                            if (lowestlevel == "Cluster")
                            {
                                litReport.Text += NPS.GenerateNPSChart(reportname, year2ClustDataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division1", "Division 1", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                litReport.Text += NPS.GenerateNPSChart(reportname, year2ClustDataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division1", "Division 1", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                            }

                            if (lowestlevel == "Division1")
                            {
                                if (year2division1.Count > 1)
                                {
                                    npsheader1 += NPS.getNPSHeaderRows("Division 1", year2Div1Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows("Division 1", year2Div1Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div1Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division1", "Division 1", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div1Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division1", "Division 1", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                                else
                                {
                                    npsheader1 += NPS.getNPSHeaderRows(year2division1.FirstOrDefault(), year2Div1Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows(year2division1.FirstOrDefault(), year2Div1Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div1Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division2", "Division 1", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div1Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division2", "Division 1", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                            }

                            if (lowestlevel == "Division2")
                            {
                                if (year2division2.Count > 1)
                                {
                                    npsheader1 += NPS.getNPSHeaderRows("Division 2", year2Div2Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows("Division 2", year2Div2Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div2Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division2", "Division 2", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div2Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division2", "Division 2", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                                else
                                {
                                    npsheader1 += NPS.getNPSHeaderRows(year2division2.FirstOrDefault(), year2Div2Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows(year2division2.FirstOrDefault(), year2Div2Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div2Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division3", "Division 2", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div2Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division3", "Division 2", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                            }

                            if (lowestlevel == "Division3")
                            {
                                if (year2division3.Count > 1)
                                {
                                    npsheader1 += NPS.getNPSHeaderRows("Division 3", year2Div3Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows("Division 3", year2Div3Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div3Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division3", "Division 3", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div3Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division3", "Division 3", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                                else
                                {
                                    npsheader1 += NPS.getNPSHeaderRows(year2division3.FirstOrDefault(), year2Div3Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows(year2division3.FirstOrDefault(), year2Div3Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div3Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division4", "Division 3", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div3Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division4", "Division 3", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                            }

                            if (lowestlevel == "Division4")
                            {
                                if (year2division4.Count > 1)
                                {
                                    npsheader1 += NPS.getNPSHeaderRows("Division 4", year2Div4Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows("Division 4", year2Div4Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div4Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division4", "Division 4", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div4Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division4", "Division 4", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                                else
                                {
                                    npsheader1 += NPS.getNPSHeaderRows(year2division4.FirstOrDefault(), year2Div4Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows(year2division4.FirstOrDefault(), year2Div4Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div4Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division5", "Division 4", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div4Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division5", "Division 4", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                            }

                            if (lowestlevel == "Division5")
                            {
                                if (year2division5.Count > 1)
                                {
                                    npsheader1 += NPS.getNPSHeaderRows("Division 5", year2Div5Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows("Division 5", year2Div5Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div5Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division5", "Division 5", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div5Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division5", "Division 5", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                                else
                                {
                                    npsheader1 += NPS.getNPSHeaderRows(year2division5.FirstOrDefault(), year2Div5Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows(year2division5.FirstOrDefault(), year2Div5Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div5Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division6", "Division 5", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div5Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division6", "Division 5", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                            }

                            if (lowestlevel == "Division6")
                            {
                                if (year2division6.Count > 1)
                                {
                                    npsheader1 += NPS.getNPSHeaderRows("Division 6", year2Div6Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows("Division 6", year2Div6Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div6Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Division6", "Division 6", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div6Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Division6", "Division 6", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                                else
                                {
                                    npsheader1 += NPS.getNPSHeaderRows(year2division6.FirstOrDefault(), year2Div6Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows(year2division6.FirstOrDefault(), year2Div6Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div6Dataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Branch", "Division 6", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2Div6Dataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Branch", "Division 6", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                            }

                            if (lowestlevel == "Branch")
                            {
                                if (year2branch.Count > 1)
                                {
                                    npsheader1 += NPS.getNPSHeaderRows("Branch", year2BranchDataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows("Branch", year2BranchDataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;

                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2BranchDataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "Branch", "Branch", npsheader1, npsheadercount1, "1", year2, ref pageno, true, pdf, true);
                                    litReport.Text += NPS.GenerateNPSChart(reportname, year2BranchDataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "Branch", "Branch", npsheader2, npsheadercount2, "2", year2, ref pageno, true, pdf, true);
                                }
                                else
                                {
                                    npsheader1 += NPS.getNPSHeaderRows(year2branch.FirstOrDefault(), year2BranchDataset.Where(p => p.QNPS1 != null).Distinct().ToList(), "1");
                                    npsheadercount1++;
                                    npsheader2 += NPS.getNPSHeaderRows(year2branch.FirstOrDefault(), year2BranchDataset.Where(p => p.QNPS2 != null).Distinct().ToList(), "2");
                                    npsheadercount2++;
                                }
                            }
                        }
                    }

                    litReport.Text += AllYearsChart(cluster, location, year2, reportname + " " + sample, pageno++, pdf, africa);
                }
            }
        }

        private string FiltersPage(ReportHistory history, string heading, bool pdf, int pageno)
        {
            string html = "<tr><td><table style=\"width:100%;text-align:left;\"><tr><td colspan=\"2\"><h1>Applied Filters</h1></td></tr><tr><td colspan=\"2\">&nbsp;</td></tr>";

            html += "<tr><td style=\"width:100px;\"><b>Cluster : </b></td><td style=\"text-align:left\">" + history.Cluster + "</td></tr>";

            html += "<tr><td colspan=\"2\">&nbsp;</td></tr><tr><td colspan=\"2\"><table style='width:100%;' cellspacing='0' cellpadding='0'>";
            html += "<tr><td style=\"text-align:left;font-size:16px;\" colspan=\"2\"><b>Applied Division Filters for 2015 :</b></td><td style=\"text-align:left;font-size:16px;\" colspan=\"2\"><b>Applied Division Filters for " + history.Year2 + " :</b></td></tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Division 1:</b></td><td style=\"text-align:left\">" + history.Y1Division1.Replace("|", ", ") + "</td><td style=\"text-align:left;width:100px;\"><b>Division 1:</b></td><td style=\"text-align:left\">" + history.Y2Division1.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Division 2:</b></td><td style=\"text-align:left\">" + history.Y1Division2.Replace("|", ", ") + "</td><td style=\"text-align:left;width:100px;\"><b>Division 2:</b></td><td style=\"text-align:left\">" + history.Y2Division2.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Division 3:</b></td><td style=\"text-align:left\">" + history.Y1Division3.Replace("|", ", ") + "</td><td style=\"text-align:left;width:100px;\"><b>Division 3:</b></td><td style=\"text-align:left\">" + history.Y2Division3.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Division 4:</b></td><td style=\"text-align:left\">" + history.Y1Division4.Replace("|", ", ") + "</td><td style=\"text-align:left;width:100px;\"><b>Division 4:</b></td><td style=\"text-align:left\">" + history.Y2Division4.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Division 5:</b></td><td style=\"text-align:left\">" + history.Y1Division5.Replace("|", ", ") + "</td><td style=\"text-align:left;width:100px;\"><b>Division 5:</b></td><td style=\"text-align:left\">" + history.Y2Division5.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Division 6:</b></td><td style=\"text-align:left\">" + history.Y1Division6.Replace("|", ", ") + "</td><td style=\"text-align:left;width:100px;\"><b>Division 6:</b></td><td style=\"text-align:left\">" + history.Y2Division6.Replace("|", ", ") + "</td</tr>";
            html += "<tr><td style=\"text-align:left;width:100px;\"><b>Branch:</b></td><td style=\"text-align:left\">" + history.Y1Branch.Replace("|", ", ") + "</td><td style=\"text-align:left;width:100px;\"><b>Branch:</b></td><td style=\"text-align:left\">" + history.Y2Branch.Replace("|", ", ") + "</td></tr>";
            html += "</table></td></tr><tr><td colspan=\"2\">&nbsp;</td></tr>";

            if (history.ManualBranchCodes != null) html += "<tr><td align=\"left\" colspan=\"2\">" + history.ManualBranchCodes.ToLower().Trim().Split('|').Count() + " Manual Branch Codes applied.</td></tr><tr><td colspan=\"2\">&nbsp;</td></tr>";
            if (history.ManualStaffCodes != null) html += "<tr><td align=\"left\" colspan=\"2\">" + history.ManualStaffCodes.ToLower().Trim().Split('|').Count() + " Manual Staff Codes applied.</td></tr><tr><td colspan=\"2\">&nbsp;</td></tr>";

            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Generational Classification:</b></td><td style=\"text-align:left\">" + history.Classification.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>EE Occupational Level:</b></td><td style=\"text-align:left\">" + history.OccupationalLevel.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Length of service:</b></td><td style=\"text-align:left\">" + history.LOS.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Management Passage:</b></td><td style=\"text-align:left\">" + history.ManagementPassage.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Location:</b></td><td style=\"text-align:left\">" + history.Location.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Gender:</b></td><td style=\"text-align:left\">" + history.Gender.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Race:</b></td><td style=\"text-align:left\">" + history.Race.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Job Family:</b></td><td style=\"text-align:left\">" + history.JobFamily.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Region:</b></td><td style=\"text-align:left\">" + history.Region.Replace("|", ", ") + "</td></tr>";
            html += "<tr><td style=\"text-align:left;width:175px;\"><b>Compare Year:</b></td><td style=\"text-align:left\">2015 vs " + history.Year2 + "</td></tr>";

            html += "</table></td></tr>";

            return PageTemplate.Template(heading, history.ReportName, html, pdf, pageno, false);
        }

        private string ScalePage(ReportHistory history, string caption, bool pdf, int pageno)
        {
            string html = "<tr><td colspan='2'><b>Scale:</b></td></tr>" +
                    "<tr><td>Five point Likert scale utilised</td><td>" +
                    "<table cellpadding='5' cellspacing='0'><tr><td style='border:1px solid black;' align='center'>Strongly<br />Agree</td><td style='border:1px solid black;' align='center'>Agree</td><td style='border:1px solid black;' align='center'>Neither Agree<br />nor Disagree</td><td style='border:1px solid black;' align='center'>Disagree</td><td style='border:1px solid black;' align='center'>Strongly<br />Disagree</td></tr><tr><td colspan='2' style='border:1px solid black;background-color: #009933' align='center'>Strength</td><td style='border:1px solid black;background-color: #999999' align='center'>Neutral</td><td colspan='2' style='border:1px solid black;background-color: #FF0000' align='center'>Improvement required</td></tr></table>" +
                    "</td></tr><tr><td colspan='2'>&nbsp;</td></tr><tr><td colspan='2'>For tracking purposes, a mean (average) has been calculated for all statements, based on the top 2 box results i.e agree and strongly agree.</td></tr><tr><td colspan='2'>&nbsp;</td></tr><tr><td colspan='2'><b>Sample Design:</b><br />Conducted in conjunction with Consulta Research (Pty) Ltd<br />Population surveyed: All Nedbank staff<br />Total respondents: 23294 (74.1% response rate versus 74.1% in 2014, 70.5% in 2013, 69% in 2012 and 69.2% in 2011)<br />Proportional sample for the group comprised of 11786 respondents, stratified by cluster, division, gender and race<br />(95% confidence with 5% error margin, conforms to accepted international norm)</td></tr><tr><td colspan='2'>&nbsp;</td></tr>" +
                    "<tr><td colspan='2' id='tdScaleExtra' runat='server'><table cellpadding='0' cellspacing='0'>" +
                    "<tr><td colspan='2'>&nbsp;</td></tr><tr><td align='left'><b>Acknowledgements:</b><br/><li>Consulta Research (Pty) Ltd:</li><span width='15px'>&nbsp;</span>- Pre and post sampling<br/><span width='15px'>&nbsp;</span>- Text analytics<li>Pure Survey (Pty) Ltd:</li>" +
                    "<span width='15px'>&nbsp;</span>- Improved questionnaire design and reporting<br/><span width='15px'>&nbsp;</span>- Enhancements to analyses as per business requirements" +
                    "<li>Aon Hewitt Ltd:</li><span width='15px'>&nbsp;</span>- Inclusion of staff engagement indices</td></tr>" +
                    "</table></td></tr>";

            return PageTemplate.Template(caption, history.ReportName, html, pdf, pageno, false);
        }

        private string AboutPage(ReportHistory history, string caption, bool pdf, int pageno)
        {
            string html = "<tr><td><table style=\"width:100%;\"><tr><td colspan='3'><b>The Nedbank Staff Survey is a universal climate survey designed to measure and track employee perceptions and organisational performance on the following dimensions:</b></td></tr>" +
                    "<tr><td colspan='3' align='center'>&nbsp;</td></tr>" +
                    "<tr><td>- Strategic Direction</td><td>- Change and Transformation</td><td>- Leadership</td></tr>" +
                    "<tr><td>- Training and Development</td><td>- Management Style</td><td>- Policies and Procedures</td></tr>" +
                    "<tr><td>- Organisational Culture and Values</td><td>- Rewards, Recognition and Performance Management</td><td>- Communication</td></tr>" +
                    "<tr><td>- Diversity</td><td>- Relationship and Trust</td><td>- Ethics</td></tr>" +
                    "<tr><td>- Employment Equity</td><td>- Engagement</td></tr></table></td></tr><tr><td>&nbsp;</td></tr>";

            html += "<tr><td><table style=\"width:100%;\"><tr><td style=\"text-align:center;\"><h2>NSS 2015 Survey Information - " + history.ReportName + "</h2></td></tr><tr><td>&nbsp;</td></tr><tr><td align='left'><b>Fieldwork: 31 August - 18 September 2015</b><br/>" +
                    "<table width='1000px' cellpadding='1' cellspacing='0' style='border-collapse: collapse;font-size:11px;' border='1' bordercolor='#000'>" +
                    "<tr><td style='padding-left:5px;' colspan='6' align='center'><b>Data collection</b></td></tr>" +
                    "<tr><td style='padding-left:5px;'><b>Approach</b></td><td style='padding-left:5px;'><b>Reason</b></td><td style='padding-left:5px;'><b># of potential participants</b></td><td style='padding-left:5px;'><b>Questionnaires submitted</b></td><td style='padding-left:5px;'><b>Questionnaires discarded</b></td><td style='padding-left:5px;'><b>Total usable questionnaire</b></td></tr>" +
                    "<tr><td style='padding-left:5px;'>Email</td><td style='padding-left:5px;'>Complete demographic records on staff database</td><td align='center'>29361</td><td align='center'>20995</td><td align='center'>623</td><td align='center'>20372</td></tr>" +
                    "<tr><td style='padding-left:5px;'>Multiple user link</td><td style='padding-left:5px;'>1. Incomplete demographics on PeopleSoft/People Trax<br/>2. Branch network</td><td align='center'>2073</td><td align='center'>3018</td><td align='center'>96</td><td align='center'>2922</td></tr>" +
                    "<tr><td style='padding-left:5px;'><b>Total number of questionnaires</b></td><td></td><td align='center'><b>31434</b></td><td align='center'><b>24013</b></td><td align='center'><b>719</b></td><td align='center'><b>23294</b></td></tr>" +
                    "<tr><td style='padding-left:5px;'><b>Final group sample</b></td><td colspan='5' style='padding-left:5px;'><b>11786 </b> (sampling conducted in conjunction with Consulta Research (Pty) Ltd)</td></tr>" +
                    "</table><br /><b>Internal Communication:</b><br /><ul class='myClass'><li>7 Email reminders from Nedbank Staff Surveys mailbox.  Communications and promo button were available on MyWorkspace for the duration of the survey, 5 reminders were sent out on MyWorkspace Headline News.  3 Reminders to Group Executives and HR Executives. 6 Reminders to the HR community.</li></ul></td></tr></table></td></tr>";

            return PageTemplate.Template(caption, history.ReportName, html, pdf, pageno, false);
        }

        private string MethodologyPage(ReportHistory history, string caption, bool pdf, int pageno)
        {
            string html = "<tr><td align='left'><table width='100%' cellpadding='0' cellspacing='0' style='border-collapse: collapse;' border='1' bordercolor='#000000'>" +
                    "<tr><td colspan='3' align='center'><b>Group Sample Composition</b></td></tr>" +
                    "<tr><td colspan='3' align='center'>&nbsp;</td></tr>" +
                    "<tr><td style='padding-left:5px;'><b>Cluster</b></td><td style='padding-left:5px;' align='center'><b>Total no. of responses 2015</b></td><td style='padding-left:5px;' align='center'><b>Group Sample 2015</b></td></tr>" +
                    "<tr><td style='padding-left:5px;'>CE OFFICE / COSEC</td><td align='center'>6</td><td align='center'>5</td></tr>" +
                    "<tr><td style='padding-left:5px;'>CIB *</td><td align='center'>2163</td><td align='center'>1414</td></tr>" +
                    "<tr><td style='padding-left:5px;'>ENTERPRISE GOVERNANCE & COMPLIANCE</td><td align='center'>50</td><td align='center'>50</td></tr>" +
                    "<tr><td style='padding-left:5px;'>GMCCA TOTAL</td><td align='center'>49</td><td align='center'>48</td></tr>" +
                    "<tr><td style='padding-left:5px;'>GROUP FINANCE</td><td align='center'>710</td><td align='center'>279</td></tr>" +
                    "<tr><td style='padding-left:5px;'>GROUP HUMAN RESOURCES</td><td align='center'>70</td><td align='center'>68</td></tr>" +
                    "<tr><td style='padding-left:5px;'>GROUP RISK</td><td align='center'>389</td><td align='center'>223</td></tr>" +
                    "<tr><td style='padding-left:5px;'>GROUP STRATEGIC PLANNING & ECONOMICS</td><td align='center'>16</td><td align='center'>16</td></tr>" +
                    "<tr><td style='padding-left:5px;'>GROUP TECHNOLOGY</td><td align='center'>1489</td><td align='center'>744</td></tr>" +
                    "<tr><td style='padding-left:5px;'>NEDBANK WEALTH *</td><td align='center'>1350</td><td align='center'>808</td></tr>" +
                    "<tr><td style='padding-left:5px;'>RBB</td><td align='center'>15558</td><td align='center'>7994</td></tr>" +
                    "<tr><td style='padding-left:5px;'>TOTAL AFRICA **</td><td align='center'>1385</td><td align='center'>78</td></tr>" +
                    "<tr><td style='padding-left:5px;'>TOTAL BALANCE SHEET MANAGEMENT</td><td align='center'>59</td><td align='center'>59</td></tr>" +
                    "<tr><td style='padding-left:5px;'>&nbsp;<td align='center'>&nbsp;</td><td align='center'>&nbsp;</td></tr>" +
                    "<tr><td style='padding-left:5px;'><b>Total</b></td><td align='center'><b>23294</b></td><td align='center'><b>11786</b></td></tr>" +
                    "</table><br/><span style='font-size:10px;font-style:italic;'>* Excludes international staff<br />** Responses excludes 'Africa' submissions. These will be included in the cluster report</span></td></tr>";

            return PageTemplate.Template(caption, history.ReportName, html, pdf, pageno, false);
        }

        private string DimensionChart(string year1, string year2, ComprehensiveDimension compdimensionyear1, ComprehensiveDimension compdimensionyear2, double year1count, double year2count, string caption, string graphname, string reportname, ref int pageno, bool comp, bool pdf, string byear, bool shift, bool africa)
        {
            StringBuilder DimensionChartHTML = new StringBuilder();
            StringBuilder DimensionCategoryXML = new StringBuilder();
            StringBuilder DimensionDataset1XML = new StringBuilder();
            StringBuilder DimensionDataset2XML = new StringBuilder();
            StringBuilder DimensionChartTableHTML = new StringBuilder();

            double result = 0;
            bool isSignificant = false;

            if (year2count == 0) shift = false;

            if (year2 != "None" && year2count >= 10) Significance.getSignificance(year1count, year2count, compdimensionyear1.NedbankOverall.DimensionPositiveValue, compdimensionyear2.NedbankOverall.DimensionPositiveValue, out result, out isSignificant);

            DimensionChartTableHTML.Append("<tr><td style='width:200px;font-weight:bold;padding:4px;'>" + compdimensionyear1.NedbankOverall.DimensionName + "</td><td style='width:50px;font-weight:bold;padding:4px;'>" + compdimensionyear1.NedbankOverall.DimensionPositiveValue.ToString("0.0") + (year2 == "None" || year2count < 10 ? "" : "</td><td style='width:50px;font-weight:bold;padding:4px;'>" + compdimensionyear2.NedbankOverall.DimensionPositiveValue.ToString("0.0")) + "</td>" + (year2 == "None" || year2count < 10 ? "" : shift ? "<td style='width:50px;font-weight:bold;padding:4px;'>" + (year2 != "None" && year2count >= 10 ? result.ToString("0.0") + (isSignificant ? "*" : "") : "&nbsp;") + "</td>" : "") + "</tr>");

            DimensionCategoryXML.Append("<category label='" + HTML.WordWrap(compdimensionyear1.NedbankOverall.DimensionName, 20) + (year2 != "None" && year2count >= 10 ? " (" + result.ToString("0.0") + (isSignificant ? "*" : "") + ")" : "") + "' /> ");

            DimensionDataset1XML.Append("<dataset seriesname='" + year1 + "' color='22513d' anchorSides='6' showValues='1' anchorRadius='2' anchorBorderColor='22513d' anchorBgAlpha='0' showPlotBorder='1' plotBorderThickness='2' plotBorderColor='22513d' plotBorderAlpha='100' drawAnchors='1'>");
            if (year2 != "None" && year2count >= 10) DimensionDataset2XML.Append("<dataset seriesname='" + year2 + "' color='738639' anchorSides='10' anchorBorderColor='738639' anchorBgAlpha='0' anchorRadius='2' showPlotBorder='1' plotBorderThickness='2' plotBorderColor='738639' plotBorderAlpha='100' drawAnchors='1'>");

            DimensionDataset1XML.Append("<set value='" + compdimensionyear1.NedbankOverall.DimensionPositiveValue.ToString("0.0") + "' />");
            if (year2 != "None" && year2count >= 10) DimensionDataset2XML.Append("<set value='" + compdimensionyear2.NedbankOverall.DimensionPositiveValue.ToString("0.0") + "' />");

            foreach (ReportDimension dimension in compdimensionyear1.CompDimensions.Distinct().ToList().OrderBy(p => p.DimensionPositiveValue))
            {
                if (dimension.DimensionPositiveValue > 0 || dimension.DimensionNeutralValue > 0 || dimension.DimensionNegativeValue > 0)
                {
                    if (year2 != "None" && year2count >= 10)
                    {
                        if (!Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) || Convert.ToInt32(byear.ToLower() == "none" ? "0" : byear) >= 2012)
                        {
                            Significance.getSignificance(year1count, year2count, dimension.DimensionPositiveValue, compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue, out result, out isSignificant);
                            DimensionCategoryXML.Append("<category label='" + HTML.WordWrap(dimension.DimensionName.AfricaNewDimension(byear, africa).NewDimension(byear), 20) + " (" + result.ToString("0.0") + (isSignificant ? "*" : "") + ")" + "' showPlotBorder='1'/> ");
                        }
                        else
                            DimensionCategoryXML.Append("<category label='" + HTML.WordWrap(dimension.DimensionName.AfricaNewDimension(byear, africa).NewDimension(byear), 20) + "' showPlotBorder='1'/> ");
                    }
                    else
                        DimensionCategoryXML.Append("<category label='" + HTML.WordWrap(dimension.DimensionName.AfricaNewDimension(byear, africa).NewDimension(byear), 20) + "' showPlotBorder='1'/> ");

                    DimensionDataset1XML.Append("<set value='" + dimension.DimensionPositiveValue.ToString("0.0") + "' />");

                    if (year2 != "None" && year2count >= 10)
                        if (!Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) || Convert.ToInt32(byear.ToLower() == "none" ? "0" : byear) >= 2012)
                            DimensionDataset2XML.Append("<set value='" + compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue.ToString("0.0") + "' />");
                        else
                            DimensionDataset2XML.Append("<set value='0.0' />");
                    else
                        DimensionDataset2XML.Append("<set value='0.0' />");
                }
            }

            bool other = true;

            foreach (ReportDimension dimension in (compdimensionyear1.CompDimensions.OrderByDescending(p => p.DimensionPositiveValue)))
            {
                if (dimension.DimensionPositiveValue > 0 || dimension.DimensionNeutralValue > 0 || dimension.DimensionNegativeValue > 0)
                {
                    if (!Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) || Convert.ToInt32(byear.ToLower() == "none" ? "0" : byear) >= 2012)
                    {
                        if (year2 != "None" && year2count >= 10)
                        {
                            Significance.getSignificance(year1count, year2count, dimension.DimensionPositiveValue, compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue, out result, out isSignificant);
                            DimensionChartTableHTML.Append("<tr" + (other ? " style=\"background:#d4e7bc;\"" : "") + "><td style=\"padding:4px;\">" + dimension.DimensionName.AfricaNewDimension(byear, africa).NewDimension(byear) + "</td><td style='width:50px;padding:4px;'>" + dimension.DimensionPositiveValue.ToString("0.0") + "</td><td style='width:50px;padding:4px;'>" + compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue.ToString("0.0") + "</td>" + (shift ? "<td style='width:50px;padding:4px;'>" + result.ToString("0.0") + (isSignificant ? "*" : "") + "</td>" : "") + "</tr>");
                        }
                        else
                            DimensionChartTableHTML.Append("<tr" + (other ? " style=\"background:#d4e7bc;\"" : "") + "><td style=\"padding:4px;\">" + dimension.DimensionName.AfricaNewDimension(byear, africa).NewDimension(byear) + "</td><td style='width:50px;padding:4px;'>" + dimension.DimensionPositiveValue.ToString("0.0") + (year2 == "None" || year2count < 10 ? "" : "</td><td style='width:50px;padding:4px;'>&nbsp;") + (shift ? "</td><td style='width:50px;padding:4px;'>&nbsp;" : "") + "</td></tr>");
                    }
                    else
                        DimensionChartTableHTML.Append("<tr" + (other ? " style=\"background:#d4e7bc;\"" : "") + "><td style=\"padding:4px;\">" + dimension.DimensionName.AfricaNewDimension(byear, africa).NewDimension(byear) + "</td><td style='width:50px;padding:4px;'>" + dimension.DimensionPositiveValue.ToString("0.0") + (year2 == "None" || year2count < 10 ? "" : "</td><td style='width:50px;padding:4px;'>&nbsp;") + (shift ? "</td><td style='width:50px;padding:4px;'>&nbsp;" : "") + "</td></tr>");
                }

                other = !other;
            }

            DimensionDataset1XML.Append("</dataset>");
            if (year2 != "None" && year2count >= 10) DimensionDataset2XML.Append("</dataset>");

            DimensionChartHTML.Append("<tr><td align='left' valign='top'>");
            DimensionChartHTML.Append(Charts.RadarChart(DimensionCategoryXML.ToString(), DimensionDataset1XML.ToString() + DimensionDataset2XML.ToString(), "Dimension" + pageno, 550, 420));
            DimensionChartHTML.Append("</td><td align='left' valign='top'>");
            DimensionChartHTML.Append(HTML.RadarTable((year2 != "None" && year2count != 0 ? new string[] { year1, year2 } : new string[] { year1 }), DimensionChartTableHTML.ToString(), false, shift));
            DimensionChartHTML.Append("</td></tr>");

            return PageTemplate.Template(graphname, reportname, "<tr><td align='center' colspan='2'><h3>" + caption + "</h3></td></tr><tr><td>&nbsp;</td></tr><tr><td align='center'>" + DimensionChartHTML.ToString() + "</td></tr><tr><td>&nbsp;</td></tr>" + (year2 != "None" && year2count >= 10 ? "<tr><td align='left' style='font-style: italic' colspan='2'>" + Significance.StatSignificantLegend((reportname.Contains("12 Dimensions") ? "" : year1), year2, pdf, comp, africa) + "</td><tr>" : ""), pdf, pageno++, false);
        }

        private string DemographicProfileCharts(List<Year> year1Dataset, ref int pageno, string caption, string reportname, string demographics, bool pdf)
        {
            string[] demos = demographics.Split(',');
            List<DemographicProfile> DemographicProfiles = new List<DemographicProfile>();
            string PageHTML = "";

            for (int i = 0; i < (demos.Count() / 2) + (demos.Count() % 2); i++)
            {
                DemographicProfile demographicprofile = new DemographicProfile();
                demographicprofile.Demographic1Values = new List<ReportDimension>();
                demographicprofile.Demographic2Values = new List<ReportDimension>();

                foreach (string value in year1Dataset.Select(p => p.GetType().GetProperty(demos[i * 2]).GetValue(p, null).NullCheck()).Distinct().ToList())
                    demographicprofile.Demographic1Values.Add(new ReportDimension(value, year1Dataset.Where(p => p.GetType().GetProperty(demos[i * 2]).GetValue(p, null).NullCheck() == value).Count(), 0, 0));

                demographicprofile.Demographic1Values = demographicprofile.Demographic1Values.OrderBy(p => p.DimensionName).ToList();
                demographicprofile.Demographic1 = demos[i * 2];

                if (((i != (demos.Count() / 2) + (demos.Count() % 2) - 1) && (demos.Count() % 2 != 0)) || demos.Count() % 2 == 0)
                {
                    foreach (string value in year1Dataset.Select(p => p.GetType().GetProperty(demos[i * 2 + 1]).GetValue(p, null).NullCheck()).Distinct().ToList())
                        demographicprofile.Demographic2Values.Add(new ReportDimension(value, year1Dataset.Where(p => p.GetType().GetProperty(demos[i * 2 + 1]).GetValue(p, null).NullCheck() == value).Count(), 0, 0));

                    demographicprofile.Demographic2Values = demographicprofile.Demographic2Values.OrderBy(p => p.DimensionName).ToList();
                    demographicprofile.Demographic2 = demos[i * 2 + 1];
                }
                else
                    demographicprofile.Demographic2 = null;

                DemographicProfiles.Add(demographicprofile);
            }

            foreach (DemographicProfile demoprofile in DemographicProfiles)
            {
                StringBuilder Dataset1 = new StringBuilder();
                StringBuilder Dataset2 = new StringBuilder();

                decimal othertotal1 = 0;
                decimal othertotal2 = 0;

                foreach (ReportDimension dimension in demoprofile.Demographic1Values)
                {
                    if (dimension.DimensionPositiveValue / year1Dataset.Count * 100 >= 10)
                    {
                        othertotal1 += (decimal)Math.Round(dimension.DimensionPositiveValue / year1Dataset.Count * 100, 1, MidpointRounding.AwayFromZero);
                        Dataset1.Append("<dataset seriesName='" + dimension.DimensionName.Replace("<", "&lt;").Replace(">", "&gt;") + "' ><set value='" + Convert.ToDecimal(Math.Round(dimension.DimensionPositiveValue / year1Dataset.Count * 100, 1, MidpointRounding.AwayFromZero)).ToString("0.0") + "' /></dataset>");
                    }
                }

                if (othertotal1 > 0) Dataset1.Append("<dataset seriesName='Other'><set value='" + ((decimal)100 - (decimal)othertotal1).ToString("0.0") + "' /></dataset>");

                if (demoprofile.Demographic2Values.Count > 0)
                {
                    foreach (ReportDimension dimension in demoprofile.Demographic2Values)
                    {
                        if (dimension.DimensionPositiveValue / year1Dataset.Count * 100 >= 10)
                        {
                            othertotal2 += (decimal)Math.Round(dimension.DimensionPositiveValue / year1Dataset.Count * 100, 1, MidpointRounding.AwayFromZero);
                            Dataset2.Append("<dataset seriesName='" + dimension.DimensionName.Replace("<", "&lt;").Replace(">", "&gt;") + "' ><set value='" + Convert.ToDecimal(Math.Round(dimension.DimensionPositiveValue / year1Dataset.Count * 100, 1, MidpointRounding.AwayFromZero)).ToString("0.0") + "' /></dataset>");
                        }
                    }

                    if (othertotal2 > 0) Dataset2.Append("<dataset seriesName='Other'><set value='" + ((decimal)100 - (decimal)othertotal2).ToString("0.0") + "' /></dataset>");
                }

                PageHTML += PageTemplate.Template("Demographic Profile - " + reportname, reportname, "<tr><td align='center' colspan='2'><h3>2015 n = " + year1Dataset.Count + "</h3></td></tr><tr><td>&nbsp;</td></tr><tr><td align='center'>" + Charts.StackedBarChart(Dataset1.ToString(), demoprofile.Demographic1, 1024, 200, demoprofile.Demographic1.Replace("Age", "Age Group").Replace("LOS", "Years at Nedbank").Replace("Disabilities", "Disability").Replace("OccupationalLevel", "Occupational Level").Replace("Classification", "Generational Classification").Replace("JobFamily", "Job Family").Replace("StaffCategory", "Staff Category").Replace("ManagementPassage", "Management Passage") + " (%)") + "<br/>" + (demoprofile.Demographic2Values.Count > 0 ? Charts.StackedBarChart(Dataset2.ToString(), demoprofile.Demographic2, 1024, 200, demoprofile.Demographic2.Replace("Age", "Age Group").Replace("LOS", "Years at Nedbank").Replace("Disabilities", "Disability").Replace("OccupationalLevel", "Occupational Level").Replace("Classification", "Generational Classification").Replace("JobFamily", "Job Family").Replace("StaffCategory", "Staff Category").Replace("ManagementPassage", "Management Passage") + " (%)") : "") + "</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td align='left' style='font-style: italic'><b>Definitions</b><table><tr><td>Other: All demographics &lt; 10% grouped together</td></tr></table></td></tr>", pdf, pageno++, false);
            }

            return PageHTML;
        }

        private string TopBottomStatements(string year2, ComprehensiveDimension compdimensionyear1, ComprehensiveDimension compdimensionyear2, List<StatementShift> statementshifts, string caption, string reportname, double year2count, bool africa, List<Question> questions, List<Dimension> dimensions, int pageno, bool pdf)
        {
            StringBuilder DimensionChartHTMLPart1 = new StringBuilder();
            StringBuilder DimensionChartHTMLPart2 = new StringBuilder();

            bool other = true;

            foreach (Statement statement in compdimensionyear1.CompStatements.Where(p => p.DimensionName != "").OrderByDescending(p => p.StatementPositiveValue).Take(5))
            {
                string colour = "";
                string image = "";
                string fontcolor = "";
                List<string> y1qi = new List<string>();
                if (year2 != "None") y1qi = (dimensions.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive).FirstOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(dimensions.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive).FirstOrDefault()) ?? new List<string>()).ToString().Split(',').ToList();

                if (africa && Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) { List<string> Temp = new List<string>(); for (int i = 0; i < y1qi.Count(); i++) { if (Convert.ToBoolean(questions.Where(p => p.QuestionIndex == "QI" + y1qi[i]).Select(p => p.GetType().GetProperty("Survey" + year2 + "Africa").GetValue(p)).FirstOrDefault())) Temp.Add(y1qi[i]); } y1qi = Temp; }

                DimensionChartHTMLPart1.Append("<tr style='height:25px;'>");
                DimensionChartHTMLPart1.Append("<td align='left' style='width:20%;padding:5px;padding-left:15px;font-size:11px;" + (other ? "background:#d4e7bc;" : "") + "'>" + statement.DimensionName.NewDimension(year2) + "</td>");
                DimensionChartHTMLPart1.Append("<td align='left' style='width:51%;padding:5px;font-size:11px;" + (other ? "background:#d4e7bc;" : "") + "'>" + statement.StatementName + (year2 != "None" && year2count >= 10 ? (!y1qi.Contains(statement.QuestionIndex.Replace("Q", "")) && year2 != "None" && y1qi.Count > 0 ? (africa ? "*" : " (New Statement in Dimension)") : "") : "") + "</td>");

                if (year2 != "None" && year2count >= 10)
                    if (!Generic.newDimensions().Contains(statement.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012)
                        Significance.getSignificantSettings(statementshifts.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.Result).FirstOrDefault(), statementshifts.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.isSignificant).FirstOrDefault(), out colour, out image, out fontcolor);

                DimensionChartHTMLPart1.AppendFormat("<td align='center' style='padding:5px;font-size:11px;background-color: {0};color:{1}'>{2}</td>", colour, fontcolor, statement.StatementPositiveValue.ToString("0.0"));
                DimensionChartHTMLPart1.Append("<td align='center' style='padding:5px;font-size:11px'>" + (year2 != "None" && year2count >= 10 ? (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null && (!Generic.newDimensions().Contains(statement.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault().StatementPositiveValue.ToString("0.0") : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault().StatementPositiveValue.ToString("0.0") : "")) : "") + "</td>");
                DimensionChartHTMLPart1.AppendFormat("<td align='center' style='padding:5px;font-size:11px;background-color: {0};color:{1}'>{2}</td>", colour, fontcolor, (year2 != "None" && year2count >= 10 && (!Generic.newDimensions().Contains(statement.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? statementshifts.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault().Result.ToString("0.0") : ""));
                DimensionChartHTMLPart1.Append("<td align='center' style='padding:5px;font-size:11px'>" + image + "</td>");
                DimensionChartHTMLPart1.Append("</tr>");

                other = !other;
            }

            other = true;

            foreach (Statement statement in compdimensionyear1.CompStatements.Where(p => p.DimensionName != "").OrderBy(p => p.StatementPositiveValue).Take(5))
            {
                string colour = "";
                string image = "";
                string fontcolor = "";                
                List<string> y1qi = new List<string>();
                if (year2 != "None") y1qi = (dimensions.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive).FirstOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(dimensions.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive).FirstOrDefault(), null) ?? new List<string>()).ToString().Split(',').ToList();

                if (africa && Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) { List<string> Temp = new List<string>(); for (int i = 0; i < y1qi.Count(); i++) { if (Convert.ToBoolean(questions.Where(p => p.QuestionIndex == "QI" + y1qi[i]).Select(p => p.GetType().GetProperty("Survey" + year2 + "Africa").GetValue(p, null)).FirstOrDefault())) Temp.Add(y1qi[i]); } y1qi = Temp; }

                DimensionChartHTMLPart2.Append("<tr style='height:25px;'>");
                DimensionChartHTMLPart2.Append("<td align='left' style='width:20%;padding:5px;padding-left:15px;font-size:11px;" + (other ? "background:#d4e7bc;" : "") + "'>" + statement.DimensionName.NewDimension(year2) + "</td>");
                DimensionChartHTMLPart2.Append("<td align='left' style='width:51%;padding:5px;font-size:11px;" + (other ? "background:#d4e7bc;" : "") + "'>" + statement.StatementName + (year2 != "None" && year2count >= 10 ? (!y1qi.Contains(statement.QuestionIndex.Replace("Q", "")) && year2 != "None" && y1qi.Count > 0 ? (africa ? "*" : " (New Statement in Dimension)") : "") : "") + "</td>");

                if (year2 != "None" && year2count >= 10)
                    if (!Generic.newDimensions().Contains(statement.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012)
                        Significance.getSignificantSettings(statementshifts.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.Result).FirstOrDefault(), statementshifts.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.isSignificant).FirstOrDefault(), out colour, out image, out fontcolor);

                DimensionChartHTMLPart2.AppendFormat("<td align='center' style='padding:5px;font-size:11px;background-color: {0};color:{1}'>{2}</td>", colour, fontcolor, statement.StatementPositiveValue.ToString("0.0"));
                DimensionChartHTMLPart2.Append("<td align='center' style='padding:5px;font-size:11px'>" + (year2 != "None" && year2count >= 10 ? (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null && (!Generic.newDimensions().Contains(statement.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault().StatementPositiveValue.ToString("0.0") : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault().StatementPositiveValue.ToString("0.0") : "")) : "") + "</td>");
                DimensionChartHTMLPart2.AppendFormat("<td align='center' style='padding:5px;font-size:11px;background-color: {0};color:{1}'>{2}</td>", colour, fontcolor, (year2 != "None" && year2count >= 10 && (!Generic.newDimensions().Contains(statement.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? statementshifts.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault().Result.ToString("0.0") : ""));
                DimensionChartHTMLPart2.Append("<td align='center' style='padding:5px;font-size:11px'>" + image + "</td>");
                DimensionChartHTMLPart2.Append("</tr>");

                other = !other;
            }

            return PageTemplate.Template("Nedbank Staff Survey Dimension - Top and Bottom Statements: " + reportname, reportname, "<tr><td align='center' colspan='6'><h3>" + caption + "</h3></td></tr><tr><td><table style=\"width:100%;border-collapse:collapse;\">" + HTML.StatementTable(new string[] { "2015", year2 }, DimensionChartHTMLPart1.ToString(), DimensionChartHTMLPart2.ToString(), "Top", "Bottom") + "</table></td></tr><tr><td colspan=\"6\">&nbsp;</td></tr>" + HTML.StatementLegend(year2, africa), pdf, pageno, false);
        }

        private string LeastMostStatements(string year2, ComprehensiveDimension compdimensionyear1, ComprehensiveDimension compdimensionyear2, List<StatementShift> statementshifts, string caption, string reportname, bool africa, List<Question> questions, List<Dimension> dimensions, int pageno, bool pdf)
        {
            StringBuilder DimensionChartHTMLPart1 = new StringBuilder();
            StringBuilder DimensionChartHTMLPart2 = new StringBuilder();

            bool other = true;

            foreach (StatementShift statementshift in statementshifts.Where(p => !Generic.ExclQuestions(year2, dimensions).Contains(p.QuestionIndex.Replace("Q", ""))).OrderByDescending(p => p.Result).Take(5))
            {
                string colour = "";
                string image = "";
                string fontcolor = "";
                string dimension = compdimensionyear1.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).Select(p => p.DimensionName).FirstOrDefault();
                List<string> y1qi = new List<string>();
                if (year2 != "None") y1qi = (dimensions.Where(p => p.DimensionText == dimension && p.Comprehensive).SingleOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(dimensions.Where(p => p.DimensionText == dimension && p.Comprehensive).FirstOrDefault()) ?? new List<string>()).ToString().Split(',').ToList();

                if (africa && Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) { List<string> Temp = new List<string>(); for (int i = 0; i < y1qi.Count(); i++) { if (Convert.ToBoolean(questions.Where(p => p.QuestionIndex == "QI" + y1qi[i]).Select(p => p.GetType().GetProperty("Survey" + year2 + "Africa").GetValue(p, null)).FirstOrDefault())) Temp.Add(y1qi[i]); } y1qi = Temp; }

                DimensionChartHTMLPart1.Append("<tr style='height:25px;'>");
                DimensionChartHTMLPart1.Append("<td align='left' style='width:20%;padding:5px;padding-left:15px;font-size:11px;" + (other ? "background:#d4e7bc;" : "") + "'>" + dimension + "</td>");
                DimensionChartHTMLPart1.Append("<td align='left' style='width:51%;paddingt:5px;font-size:11px;" + (other ? "background:#d4e7bc;" : "") + "'>" + compdimensionyear1.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).FirstOrDefault().StatementName + (!y1qi.Contains(statementshift.QuestionIndex.Replace("Q", "")) && year2 != "None" && y1qi.Count > 0 ? (africa ? "*" : " (New Statement in Dimension)") : "") + "</td>");

                if ((compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).Count() > 0 || compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statementshift.QuestionIndex).Count() > 0) && (!Generic.newDimensions().Contains(dimension.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012)) Significance.getSignificantSettings(statementshift.Result, statementshift.isSignificant, out colour, out image, out fontcolor);

                DimensionChartHTMLPart1.AppendFormat("<td align='center' style='padding:5px;font-size:11px;background-color: {0};color:{1}'>{2}</td>", colour, fontcolor, compdimensionyear1.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).FirstOrDefault().StatementPositiveValue.ToString("0.0"));
                DimensionChartHTMLPart1.Append("<td align='center' style='padding:5px;font-size:11px'>" + (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).FirstOrDefault() != null && compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).Count() > 0 && (!Generic.newDimensions().Contains(dimension.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).FirstOrDefault().StatementPositiveValue.ToString("0.0") : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statementshift.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statementshift.QuestionIndex).FirstOrDefault().StatementPositiveValue.ToString("0.0") : "")) + "</td>");
                DimensionChartHTMLPart1.AppendFormat("<td align='center' style='padding:5px;font-size:11px;background-color: {0};color:{1}'>{2}</td>", colour, fontcolor, ((compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).Count() > 0 || compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statementshift.QuestionIndex).Count() > 0) && (!Generic.newDimensions().Contains(dimension.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? statementshift.Result.ToString("0.0") : ""));
                DimensionChartHTMLPart1.Append("<td align='center' style='padding:5px;font-size:11px'>" + image + "</td>");
                DimensionChartHTMLPart1.Append("</tr>");

                other = !other;
            }

            other = true;

            foreach (StatementShift statementshift in statementshifts.Where(p => !Generic.ExclQuestions(year2, dimensions).Contains(p.QuestionIndex.Replace("Q", ""))).OrderBy(p => p.Result).Take(5))
            {
                string colour = "";
                string image = "";
                string fontcolor = "";
                string dimension = compdimensionyear1.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).Select(p => p.DimensionName).FirstOrDefault();
                List<string> y1qi = new List<string>();
                if (year2 != "None") y1qi = (dimensions.Where(p => p.DimensionText == dimension && p.Comprehensive).SingleOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(dimensions.Where(p => p.DimensionText == dimension && p.Comprehensive).FirstOrDefault()) ?? new List<string>()).ToString().Split(',').ToList();

                if (africa && Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) { List<string> Temp = new List<string>(); for (int i = 0; i < y1qi.Count(); i++) { if (Convert.ToBoolean(questions.Where(p => p.QuestionIndex == "QI" + y1qi[i]).Select(p => p.GetType().GetProperty("Survey" + year2 + "Africa").GetValue(p, null)).FirstOrDefault())) Temp.Add(y1qi[i]); } y1qi = Temp; }

                DimensionChartHTMLPart2.Append("<tr style='height:25px;'>");
                DimensionChartHTMLPart2.Append("<td align='left' style='width:20%;padding:5px;padding-left:15px;font-size:11px;" + (other ? "background:#d4e7bc;" : "") + "'>" + dimension + "</td>");
                DimensionChartHTMLPart2.Append("<td align='left' style='width:51%;padding:5px;font-size:11px;" + (other ? "background:#d4e7bc;" : "") + "'>" + compdimensionyear1.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).FirstOrDefault().StatementName + (!y1qi.Contains(statementshift.QuestionIndex.Replace("Q", "")) && year2 != "None" && y1qi.Count > 0 ? (africa ? "*" : " (New Statement in Dimension)") : "") + "</td>");

                if ((compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).Count() > 0 || compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statementshift.QuestionIndex).Count() > 0) && (!Generic.newDimensions().Contains(dimension.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012)) Significance.getSignificantSettings(statementshift.Result, statementshift.isSignificant, out colour, out image, out fontcolor);

                DimensionChartHTMLPart2.AppendFormat("<td align='center' style='padding:5px;font-size:11px;background-color: {0};color:{1}'>{2}</td>", colour, fontcolor, compdimensionyear1.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).FirstOrDefault().StatementPositiveValue.ToString("0.0"));
                DimensionChartHTMLPart2.Append("<td align='center' style='padding:5px;font-size:11px'>" + (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).Count() > 0 ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).SingleOrDefault().StatementPositiveValue.ToString("0.0") : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statementshift.QuestionIndex).FirstOrDefault() != null && (!Generic.newDimensions().Contains(dimension.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statementshift.QuestionIndex).FirstOrDefault().StatementPositiveValue.ToString("0.0") : "")) + "</td>");
                DimensionChartHTMLPart2.AppendFormat("<td align='center' style='padding:5px;font-size:11px;background-color: {0};color:{1}'>{2}</td>", colour, fontcolor, ((compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statementshift.QuestionIndex).Count() > 0 || compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statementshift.QuestionIndex).Count() > 0) && (!Generic.newDimensions().Contains(dimension.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? statementshift.Result.ToString("0.0") : ""));
                DimensionChartHTMLPart2.Append("<td align='center' style='padding:5px;font-size:11px'>" + image + "</td>");
                DimensionChartHTMLPart2.Append("</tr>");

                other = !other;
            }

            return PageTemplate.Template("Nedbank Staff Survey Dimension - Least and Most Improved Statements: " + reportname, reportname, "<tr><td align='center' colspan='6'><h3>" + caption + "</h3></td></tr><tr><td><table style=\"width:100%;border-collapse:collapse;\">" + HTML.StatementTable(new string[] { "2015", year2 }, DimensionChartHTMLPart1.ToString(), DimensionChartHTMLPart2.ToString(), "Most Improved", "Least Improved") + "</table></td></tr><tr><td colspan=\"6\">&nbsp;</td></tr>" + HTML.StatementLegend(year2, africa), pdf, pageno, false);
        }

        private string StatementCharts(string year2, ComprehensiveDimension compdimensionyear1, ComprehensiveDimension compdimensionyear2, double year1count, double year2count, List<StatementShift> statementshifts, string caption, string reportname, string header, ref int pageno, bool africa, List<Question> questions, List<Dimension> DimensionsList, bool pdf)
        {
            string pages = "";

            foreach (ReportDimension dimension in compdimensionyear1.CompDimensions)
            {
                if (dimension.DimensionPositiveValue > 0)
                {
                    compdimensionyear1.CompStatements = compdimensionyear1.CompStatements.OrderByDescending(p => p.StatementPositiveValue).ToList();

                    StringBuilder DimensionCategoryXML = new StringBuilder();
                    StringBuilder DimensionDatasetXML = new StringBuilder();
                    StringBuilder DimensionDataset2XML = new StringBuilder();
                    StringBuilder DimensionDataset3XML = new StringBuilder();

                    double result = 0;
                    bool isSignificant = false;

                    DimensionCategoryXML.Append("<category label='2015' />" + (year2 != "None" && (!Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? "<category label='" + year2 + "' />" : ""));
                    DimensionDatasetXML.Append("<dataset seriesName='Agree' color='009933'>");
                    DimensionDatasetXML.Append("<data value='" + dimension.DimensionPositiveValue.ToString("0.0") + "' />" + (year2 != "None" && (!Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? "<data value='" + compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).Select(p => p.DimensionPositiveValue).FirstOrDefault().ToString("0.0") + "' />" : ""));
                    DimensionDatasetXML.Append("</dataset>");
                    DimensionDatasetXML.Append("<dataset seriesName='Neutral' color='969696'>");
                    DimensionDatasetXML.Append("<data value='" + dimension.DimensionNeutralValue.ToString("0.0") + "' />" + (year2 != "None" && (!Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? "<data value='" + compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).Select(p => p.DimensionNeutralValue).FirstOrDefault().ToString("0.0") + "' />" : ""));
                    DimensionDatasetXML.Append("</dataset>");
                    DimensionDatasetXML.Append("<dataset seriesName='Disagree' color='FF0000'>");
                    DimensionDatasetXML.Append("<data value='" + dimension.DimensionNegativeValue.ToString("0.0") + "' />" + (year2 != "None" && (!Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? "<data value='" + compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).Select(p => p.DimensionNegativeValue).FirstOrDefault().ToString("0.0") + "' />" : ""));
                    DimensionDatasetXML.Append("</dataset>");

                    if (year2 != "None" && year2count >= 10)
                        if ((!Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012))
                            Significance.getSignificance(year1count, year2count, dimension.DimensionPositiveValue, compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).Select(p => p.DimensionPositiveValue).FirstOrDefault(), out result, out isSignificant);

                    string chartmain = Charts.MSStackedBarChart(DimensionCategoryXML.ToString(), DimensionDatasetXML.ToString(), dimension.DimensionName.Replace(" ", "").Replace("&", "").Replace(",", "").Replace("(", "").Replace(")", "") + "Overall", 1024, 75, dimension.DimensionName.AfricaNewDimension(year2, africa) + (!Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012 ? " % (" + result + (isSignificant ? "*" : "") + ")" : ""));

                    string chartrest = "";
                    string legend = "";

                    DimensionCategoryXML = new StringBuilder();
                    DimensionDatasetXML = new StringBuilder();
                    DimensionDataset2XML = new StringBuilder();
                    DimensionDataset3XML = new StringBuilder();
                    string legendbody = "";

                    DimensionDatasetXML.Append("<dataset seriesName='Agree' color='009933'>");
                    DimensionDataset2XML.Append("<dataset seriesName='Neutral' color='969696'>");
                    DimensionDataset3XML.Append("<dataset seriesName='Disagree' color='FF0000'>");

                    foreach (Statement statement in compdimensionyear1.CompStatements.Where(p => p.DimensionName == dimension.DimensionName).Distinct().ToList())
                    {
                        List<string> y1qi = new List<string>();
                        if (year2 != "None") y1qi = (DimensionsList.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive).FirstOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(DimensionsList.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive).FirstOrDefault()) ?? new List<string>()).ToString().Split(',').ToList();

                        if (africa && Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) { List<string> Temp = new List<string>(); for (int i = 0; i < y1qi.Count(); i++) { if (Convert.ToBoolean(questions.Where(p => p.QuestionIndex == "QI" + y1qi[i]).Select(p => p.GetType().GetProperty("Survey" + year2 + "Africa").GetValue(p, null)).SingleOrDefault())) Temp.Add(y1qi[i]); } y1qi = Temp; }

                        if (year2 != "None" && year2count >= 10)
                        {
                            if (!Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012)
                            {
                                DimensionCategoryXML.Append("<category label='" + (statement.StatementName.Length > 30 ? statement.StatementName.Substring(0, 30) + "..." : statement.StatementName).Trim().Replace("'", "`") + " (" + statementshifts.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.Result).FirstOrDefault() + (statementshifts.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.isSignificant).FirstOrDefault() ? "*" : "") + ") 2015'/>" + "<category label='" + year2 + "'/>");
                                legendbody += "<tr><td style='font-size: 11px;' align='left'>• " + statement.StatementName.Replace("'", "`") + (!y1qi.Contains(statement.QuestionIndex.Replace("Q", "")) && year2 != "None" && y1qi.Count > 0 ? (africa ? "***" : " (New Statement in Dimension)") : "") + " (" + statementshifts.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault().Result + (statementshifts.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.isSignificant).FirstOrDefault() ? "*" : "") + ")" + "</td></tr>";
                                DimensionDatasetXML.Append("<data value='" + statement.StatementPositiveValue.ToString("0.0") + "' /><data value='" + (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.StatementPositiveValue).FirstOrDefault().ToString("0.0") : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.StatementPositiveValue).FirstOrDefault().ToString("0.0") : "&nsbp;")) + "' />");
                                DimensionDataset2XML.Append("<data value='" + statement.StatementNeutralValue.ToString("0.0") + "' /><data value='" + (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.StatementNeutralValue).FirstOrDefault().ToString("0.0") : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.StatementNeutralValue).FirstOrDefault().ToString("0.0") : "&nsbp;")) + "' />");
                                DimensionDataset3XML.Append("<data value='" + statement.StatementNegativeValue.ToString("0.0") + "' /><data value='" + (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.StatementNegativeValue).FirstOrDefault().ToString("0.0") : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).FirstOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).Select(p => p.StatementNegativeValue).FirstOrDefault().ToString("0.0") : "&nsbp;")) + "' />");
                            }
                            else
                            {
                                DimensionCategoryXML.Append("<category label='" + (statement.StatementName.Length > 30 ? statement.StatementName.Substring(0, 30) + "..." : statement.StatementName).Trim().Replace("'", "`") + "2015' />");
                                legendbody += "<tr><td style='font-size: 11px;' align='left'>• " + statement.StatementName.Replace("'", "`") + "</td></tr>";
                                DimensionDatasetXML.Append("<data value='" + statement.StatementPositiveValue.ToString("0.0") + "' />");
                                DimensionDataset2XML.Append("<data value='" + statement.StatementNeutralValue.ToString("0.0") + "' />");
                                DimensionDataset3XML.Append("<data value='" + statement.StatementNegativeValue.ToString("0.0") + "' />");
                            }
                        }
                        else
                        {
                            DimensionCategoryXML.Append("<category label='" + (statement.StatementName.Length > 30 ? statement.StatementName.Substring(0, 30) + "..." : statement.StatementName).Trim().Replace("'", "`") + "2015' />");
                            legendbody += "<tr><td style='font-size: 11px;' align='left'>• " + statement.StatementName.Replace("'", "`") + "</td></tr>";
                            DimensionDatasetXML.Append("<data value='" + statement.StatementPositiveValue.ToString("0.0") + "' />");
                            DimensionDataset2XML.Append("<data value='" + statement.StatementNeutralValue.ToString("0.0") + "' />");
                            DimensionDataset3XML.Append("<data value='" + statement.StatementNegativeValue.ToString("0.0") + "' />");
                        }
                    }

                    DimensionDatasetXML.Append("</dataset>");
                    DimensionDataset2XML.Append("</dataset>");
                    DimensionDataset3XML.Append("</dataset>");

                    chartrest = Charts.MSStackedBarChart(DimensionCategoryXML.ToString(), DimensionDatasetXML.ToString() + DimensionDataset2XML.ToString() + DimensionDataset3XML.ToString(), dimension.DimensionName.Replace(" ", "").Replace("&", "").Replace(",", "").Replace("(", "").Replace(")", "") + "All", 1024, 260, "All Statements %");
                    legend = HTML.LegendTemplate(legendbody + (Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) && Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) < 2012 ? "<tr><td><b>No scores available for " + year2 + " as " + dimension.DimensionName + " is a new Dimension in 2012</b></td></tr>" : ""), "All Statements");

                    pages += PageTemplate.Template("Nedbank Staff Survey Dimension - " + reportname, reportname, "<tr><td align='center' style=\"padding-bottom:5px;margin-top:-40px;\"><b>" + caption + "</b></td></tr><tr><td>" + chartmain + chartrest + "</td></tr><tr><td>" + legend + "</td></tr><tr><td>&nbsp;</td></tr><tr><td>" + HTML.LegendTemplate("<tr><td>* Statistically significant change since " + year2 + "</td></tr>" + (africa && (year2 == "None" ? false : Convert.ToInt32(year2) <= 2012) ? "<tr><td>** " + year2 + " Dimension score based on a reduced set of statements. 2014 dimension score based on inclusion of re-instated statements (year on year comparatives at a dimension level to be treated with caution)</td></tr><tr><td>*** Statement omitted in " + year2 + " analysis but re-instated in 2014 (no " + year2 + " comparative score available)</td></tr>" : ""), "Definitions") + "</td></tr>", pdf, pageno++, false);
                }
            }

            return pages;
        }

        private string AllYearsChart(string cluster, List<string> location, string year2, string reportname, int pageno, bool pdf, bool africa)
        {
            StringBuilder DimensionChartHTML = new StringBuilder();

            DimensionChartHTML.Append("<tr><td align='left' valign='top'>");
            DimensionChartHTML.Append(HTML.GetAllYearsGraph(cluster, location, year2, africa));
            DimensionChartHTML.Append("</td><td align='left' valign='top'>");
            DimensionChartHTML.Append(HTML.DimensionsForAllYearsTable(cluster, location, year2, africa));
            DimensionChartHTML.Append("</td></tr>");
            DimensionChartHTML.Append("<tr><td colspan=\"2\">&nbsp;</td></tr>");
            if (year2 != "") DimensionChartHTML.Append(Significance.StatSignificantLegend("", year2, pdf, true, africa)); else DimensionChartHTML.Append(Significance.StatSignificantLegend("", year2, pdf, false, africa));

            return PageTemplate.Template("Comparative of MEAN scores for all Nedbank Staff dimensions", reportname, "<tr><td align='center' colspan='2'><h3>" + HTML.GetAllYearsCaption(cluster, location) + "</h3></td></tr><tr><td colspan=\"2\">&nbsp;</td></tr>" + DimensionChartHTML.ToString(), pdf, pageno, false);
        }

        protected void cmdPDF_Click(object sender, EventArgs e)
        {
            Guid HistoryID = new Guid(RouteData.Values["ReportHistoryID"].ToString());
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == HistoryID).Distinct().SingleOrDefault();

            string filename = "";

            if (RouteData.Values["Report"].ToString() == "exec")
            {
                if (history.PDFExecDate == null) history.PDFExecDate = DateTime.Now;
                history.PDFExecCount = history.PDFExecCount == null ? 1 : history.PDFExecCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PDF";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Executive Summary";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PDFExecFilename == null || history.ManualFileName != null)
                {
                    RenderPDF renderPDF = new RenderPDF();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                    renderPDF.GeneratePDF(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/execpdf/" + history.ReportHistoryID);
                    history.PDFExecFilename = filename;
                    db.SaveChanges();

                    renderPDF = null;
                }
                else
                    filename = history.PDFExecFilename;
            }
            else
            {
                if (history.PDFCompDate == null) history.PDFCompDate = DateTime.Now;
                history.PDFCompCount = history.PDFCompCount == null ? 1 : history.PDFCompCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PDF";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Comprehensive";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PDFCompFilename == null || history.ManualFileName != null)
                {
                    RenderPDF renderPDF = new RenderPDF();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                    renderPDF.GeneratePDF(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/comppdf/" + history.ReportHistoryID);
                    history.PDFCompFilename = filename;
                    db.SaveChanges();

                    renderPDF = null;
                }
                else
                    filename = history.PDFCompFilename;
            }

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=pdf&File=" + filename);

            GC.Collect();

        }

        protected void cmdPPT_Click(object sender, EventArgs e)
        {
            Guid HistoryID = new Guid(RouteData.Values["ReportHistoryID"].ToString());
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == HistoryID).Distinct().SingleOrDefault();

            string filename = "";

            if (RouteData.Values["Report"].ToString() == "exec")
            {
                if (history.PPTExecDate == null) history.PPTExecDate = DateTime.Now;
                history.PPTExecCount = history.PPTExecCount == null ? 1 : history.PPTExecCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PPT";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Executive Summary";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PPTExecFilename == null || history.ManualFileName != null)
                {
                    RenderPPT renderPPT = new RenderPPT();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".ppt";

                    renderPPT.GeneratePPT(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/execppt/" + history.ReportHistoryID);
                    history.PPTExecFilename = filename;
                    db.SaveChanges();

                    renderPPT = null;
                }
                else
                    filename = history.PPTExecFilename;
            }
            else
            {
                if (history.PPTCompDate == null) history.PPTCompDate = DateTime.Now;
                history.PPTCompCount = history.PPTCompCount == null ? 1 : history.PPTCompCount++;

                ReportGeneration gen = new ReportGeneration();
                gen.DateDrawn = DateTime.Now;
                gen.GenerationID = Guid.NewGuid();
                gen.OutputType = "PPT";
                gen.ReportHistoryID = history.ReportHistoryID;
                gen.ReportType = "Comprehensive";
                gen.UserID = UserID;
                db.ReportGenerations.Add(gen);
                db.SaveChanges();

                if (history.PPTCompFilename == null || history.ManualFileName != null)
                {
                    RenderPPT renderPPT = new RenderPPT();
                    filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".ppt";

                    renderPPT.GeneratePPT(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/compppt/" + history.ReportHistoryID);
                    history.PPTCompFilename = filename;
                    db.SaveChanges();

                    renderPPT = null;
                }
                else
                    filename = history.PPTCompFilename;
            }

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=ppt&File=" + filename);

            GC.Collect();
        }

        protected void cmdExcel_Click(object sender, EventArgs e)
        {
            string filename = "";

            Guid HistoryID = new Guid(RouteData.Values["ReportHistoryID"].ToString());
            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == HistoryID).Distinct().SingleOrDefault();

            if (history.ExcelDate == null) history.ExcelDate = DateTime.Now;
            history.ExcelCount = history.ExcelCount == null ? 1 : history.ExcelCount++;

            ReportGeneration gen = new ReportGeneration();
            gen.DateDrawn = DateTime.Now;
            gen.GenerationID = Guid.NewGuid();
            gen.OutputType = "Excel";
            gen.ReportHistoryID = history.ReportHistoryID;
            gen.ReportType = "Comprehensive";
            gen.UserID = UserID;
            db.ReportGenerations.Add(gen);
            db.SaveChanges();

            //if (history.ExcelFilename == null)
            //{
                RenderExcel excel = new RenderExcel();
                filename = excel.Generate(new Guid(RouteData.Values["ReportHistoryID"].ToString()), Server.MapPath("~/Exports"));
                history.ExcelFilename = filename;
                db.SaveChanges();
                excel = null;
            //}
            //else
            //    filename = history.ExcelFilename;

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=excel&File=" + filename);
        }

        protected void cmdInfograph_Click(object sender, EventArgs e)
        {
            string filename = "";

            ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == new Guid(RouteData.Values["ReportHistoryID"].ToString())).Distinct().SingleOrDefault();

            if (history.InfographDate == null) history.InfographDate = DateTime.Now;
            history.InfographCount = history.InfographCount == null ? 1 : history.InfographCount++;

            ReportGeneration gen = new ReportGeneration();
            gen.DateDrawn = DateTime.Now;
            gen.GenerationID = Guid.NewGuid();
            gen.OutputType = "PDF";
            gen.ReportHistoryID = history.ReportHistoryID;
            gen.ReportType = "Infograph 1";
            gen.UserID = UserID;
            db.ReportGenerations.Add(gen);
            db.SaveChanges();

            if (history.InfographFilename == null || history.ManualFileName != null)
            {
                RenderPDF renderPDF = new RenderPDF();
                filename = history.ReportName.Replace(" ", "").Replace("/", "").Replace("&", "").Replace(":", "").Replace(";", "").Replace("-", "") + Guid.NewGuid() + ".pdf";

                renderPDF.GeneratePDF(Server.MapPath("~/exports/") + filename, GetSiteRoot() + "/infograph/" + history.ReportHistoryID, PdfPageSize.A3, PdfPageOrientation.Portrait, 1754);
                history.InfographFilename = filename;
                db.SaveChanges();

                renderPDF = null;
            }
            else
                filename = history.InfographFilename;

            this.iframe.Attributes.Add("src", GetSiteRoot() + "/DownloadFile.aspx?Type=pdf&File=" + filename);
        }

        protected void cmdBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }
    }
}