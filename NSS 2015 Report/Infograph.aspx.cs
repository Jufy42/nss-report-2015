﻿using InfoSoftGlobal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NSS_2015_Report
{
    public partial class Infograph : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Guid HistoryID = new Guid(RouteData.Values["ReportHistoryID"].ToString());
                ReportHistory history = db.ReportHistories.Where(p => p.ReportHistoryID == HistoryID).SingleOrDefault();

                List<Year> year1Dataset = new List<Year>();
                List<Year> year2Dataset = new List<Year>();
                List<Year> year3Dataset = new List<Year>();

                ComprehensiveDimension compdimensionyear1 = new ComprehensiveDimension();
                ComprehensiveDimension compdimensionyear2 = new ComprehensiveDimension();
                ComprehensiveDimension compdimensionyear3 = new ComprehensiveDimension();
                List<StatementShift> statementshifts = new List<StatementShift>();
                List<StatementShift> statementshiftsee = new List<StatementShift>();

                List<Dimension> Dimensionslist = db.Dimensions.Distinct().ToList();
                List<Question> questions = db.Questions.ToList();

                string reportname = history.ReportName;
                string cluster = history.Cluster;
                string year1division1 = history.Y1Division1;
                string year1division2 = history.Y1Division2;
                string year1division3 = history.Y1Division3;
                string year1division4 = history.Y1Division4;
                string year1division5 = history.Y1Division5;
                string year1division6 = history.Y1Division6;
                string year1branch = history.Y1Branch;
                string year2division1 = history.Y2Division1;
                string year2division2 = history.Y2Division2;
                string year2division3 = history.Y2Division3;
                string year2division4 = history.Y2Division4;
                string year2division5 = history.Y2Division5;
                string year2division6 = history.Y2Division6;
                string year2branch = history.Y2Branch;

                string demographics = history.Composition.Replace("|", ", ").Replace(" ", "").Replace("Disability", "Disabilities").Replace("Lengthofservice", "LOS").Replace("EEOccupationalLevel", "OccupationalLevel").Replace("GenerationalClassification", "Classification");
                string sample = cluster == "All" ? "Post Sample" : "";

                string year2 = history.Year2;
                string year3 = (Convert.ToInt32(history.Year2) - 1).ToString();

                year1Dataset = year.YearDataSet();
                year2Dataset = year.YearDataSet(year2);
                year3Dataset = year.YearDataSet(year3);

                if (sample == "Post Sample")
                {
                    year1Dataset = year1Dataset.Where(p => p.IsSample == true).Distinct().ToList();
                    year2Dataset = year2Dataset.Where(p => p.IsSample == true).Distinct().ToList();
                    year3Dataset = year3Dataset.Where(p => p.IsSample == true).Distinct().ToList();
                }

                if (cluster != "All")
                {
                    year1Dataset = year1Dataset.Where(p => p.Cluster.ToLower().Trim() == cluster.ToLower().Trim()).Distinct().ToList();
                    
                    if ((cluster != "RBB" && cluster != "CIB") || (year2division1 == "All"
                        && year2division2 == "All"
                        && year2division3 == "All"
                        && year2division4 == "All"
                        && year2division5 == "All"
                        && year2division6 == "All"
                        && year2branch == "All"))
                    {
                        year2Dataset = year2Dataset.Where(p => p.Cluster.ToLower().Trim() == cluster.ToLower().Trim()).Distinct().ToList();
                        year3Dataset = year3Dataset.Where(p => p.Cluster.ToLower().Trim() == cluster.ToLower().Trim()).Distinct().ToList();
                    }
                }

                if (year1division1 != "All") year1Dataset = year1Dataset.Where(p => year1division1.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division1.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2division1 != "All") { year2Dataset = year2Dataset.Where(p => year2division1.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division1.NullCheck().ToLower().Trim())).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => year2division1.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division1.NullCheck().ToLower().Trim())).Distinct().ToList(); }
                if (year1division2 != "All") year1Dataset = year1Dataset.Where(p => year1division2.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division2.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2division2 != "All") { year2Dataset = year2Dataset.Where(p => year2division2.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division2.NullCheck().ToLower().Trim())).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => year2division2.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division2.NullCheck().ToLower().Trim())).Distinct().ToList(); }
                if (year1division3 != "All") year1Dataset = year1Dataset.Where(p => year1division3.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division3.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2division3 != "All") { year2Dataset = year2Dataset.Where(p => year2division3.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division3.NullCheck().ToLower().Trim())).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => year2division3.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division3.NullCheck().ToLower().Trim())).Distinct().ToList(); }
                if (year1division4 != "All") year1Dataset = year1Dataset.Where(p => year1division4.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division4.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2division4 != "All") { year2Dataset = year2Dataset.Where(p => year2division4.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division4.NullCheck().ToLower().Trim())).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => year2division4.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division4.NullCheck().ToLower().Trim())).Distinct().ToList(); }
                if (year1division5 != "All") year1Dataset = year1Dataset.Where(p => year1division5.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division5.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2division5 != "All") { year2Dataset = year2Dataset.Where(p => year2division5.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division5.NullCheck().ToLower().Trim())).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => year2division5.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division5.NullCheck().ToLower().Trim())).Distinct().ToList(); }
                if (year1division6 != "All") year1Dataset = year1Dataset.Where(p => year1division6.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division6.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2division6 != "All") { year2Dataset = year2Dataset.Where(p => year2division6.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division6.NullCheck().ToLower().Trim())).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => year2division6.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Division6.NullCheck().ToLower().Trim())).Distinct().ToList(); }
                if (year1branch != "All") year1Dataset = year1Dataset.Where(p => year1branch.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList();
                if (year2branch != "All") { year2Dataset = year2Dataset.Where(p => year2branch.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => year2branch.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList(); }

                string classification = history.Classification;
                string occlevel = history.OccupationalLevel;
                string los = history.LOS;
                string passage = history.ManagementPassage;
                string location = history.Location;
                string gender = history.Gender;
                string race = history.Race;
                string jobfamily = history.JobFamily;
                string region = history.Region;

                if (history.ManualBranchCodes != null) { year1Dataset = year1Dataset.ToList().Where(p => history.ManualBranchCodes.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => history.ManualBranchCodes.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Branch.NullCheck().ToLower().Trim())).Distinct().ToList(); }
                if (history.ManualStaffCodes != null) { year1Dataset = year1Dataset.ToList().Where(p => history.ManualStaffCodes.ToLower().Trim().Split('|').AsEnumerable().Contains(p.StaffCode.NullCheck().ToLower().Trim())).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => history.ManualStaffCodes.ToLower().Trim().Split('|').AsEnumerable().Contains(p.StaffCode.NullCheck().ToLower().Trim())).Distinct().ToList(); }

                if (location != "All") { year1Dataset = year1Dataset.Where(p => location.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Location.NullCheck().ToLower().Trim())).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => location.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Location.NullCheck().ToLower().Trim())).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => location.ToLower().Trim().Split('|').AsEnumerable().Contains(p.Location.NullCheck().ToLower().Trim())).Distinct().ToList(); }
                if (classification != "All") { year1Dataset = year1Dataset.Where(p => p.Classification.NullCheck().ToLower().Trim() == classification.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Classification.NullCheck().ToLower().Trim() == classification.ToLower().Trim()).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => p.Classification.NullCheck().ToLower().Trim() == classification.ToLower().Trim()).Distinct().ToList(); }
                if (jobfamily != "All") { year1Dataset = year1Dataset.Where(p => p.JobFamily.Normalize().ToLower().Trim() == jobfamily.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.JobFamily.NullCheck().ToLower().Trim() == jobfamily.ToLower().Trim()).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => p.JobFamily.NullCheck().ToLower().Trim() == jobfamily.ToLower().Trim()).Distinct().ToList(); }
                if (los != "All") { year1Dataset = year1Dataset.Where(p => p.LOS.NullCheck().ToLower().Trim() == los.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.LOS.NullCheck().ToLower().Trim() == los.ToLower().Trim()).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => p.LOS.NullCheck().ToLower().Trim() == los.ToLower().Trim()).Distinct().ToList(); }
                if (region != "All") { year1Dataset = year1Dataset.Where(p => p.Region.NullCheck().ToLower().Trim() == region.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Region.NullCheck().ToLower().Trim() == region.ToLower().Trim()).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => p.Region.NullCheck().ToLower().Trim() == region.ToLower().Trim()).Distinct().ToList(); }
                if (passage != "All") { year1Dataset = year1Dataset.Where(p => p.ManagementPassage.NullCheck().ToLower().Trim() == passage.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.ManagementPassage.NullCheck().ToLower().Trim() == passage.ToLower().Trim()).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => p.ManagementPassage.NullCheck().ToLower().Trim() == passage.ToLower().Trim()).Distinct().ToList(); }
                if (race != "All") { year1Dataset = year1Dataset.Where(p => p.Race.NullCheck().ToLower().Trim() == race.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Race.NullCheck().ToLower().Trim() == race.ToLower().Trim()).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => p.Race.NullCheck().ToLower().Trim() == race.ToLower().Trim()).Distinct().ToList(); }
                if (occlevel != "All") { year1Dataset = year1Dataset.Where(p => p.OccupationalLevel.NullCheck().ToLower().Trim() == occlevel.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.OccupationalLevel.NullCheck().ToLower().Trim() == occlevel.ToLower().Trim()).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => p.OccupationalLevel.NullCheck().ToLower().Trim() == occlevel.ToLower().Trim()).Distinct().ToList(); }
                if (gender != "All") { year1Dataset = year1Dataset.Where(p => p.Gender.NullCheck().ToLower().Trim() == gender.ToLower().Trim()).Distinct().ToList(); year2Dataset = year2Dataset.Where(p => p.Gender.NullCheck().ToLower().Trim() == gender.ToLower().Trim()).Distinct().ToList(); year3Dataset = year3Dataset.Where(p => p.Gender.NullCheck().ToLower().Trim() == gender.ToLower().Trim()).Distinct().ToList(); }

                bool africa = !location.ToLower().Trim().Split('|').AsEnumerable().Contains("all") && !location.ToLower().Trim().Split('|').AsEnumerable().Contains("south africa") && !location.ToLower().Trim().Split('|').AsEnumerable().Contains("uk");

                compdimensionyear1 = year.GetCompDimensions(year1Dataset, Dimensionslist, questions, africa);
                compdimensionyear2 = year.GetCompDimensions(year2Dataset, Dimensionslist, questions, year2, africa);
                compdimensionyear3 = year.GetCompDimensions(year3Dataset, Dimensionslist, questions, year3, africa);

                if (year2Dataset.Count > 0)
                {
                    foreach (Statement statement in compdimensionyear1.CompStatements.Where(p => p.DimensionName != ""))
                    {
                        double result = 0;
                        bool isSignificant = false;
                        Significance.getSignificance(statement.StatementCount, (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementCount : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementCount : 0)), statement.StatementPositiveValue, (compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementPositiveValue : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementPositiveValue : 0)), out result, out isSignificant);
                        if (statement.DimensionName == "Employment Equity (New Dimension in 2012)") statementshiftsee.Add(new StatementShift(statement.QuestionIndex, result, isSignificant)); else statementshifts.Add(new StatementShift(statement.QuestionIndex, result, isSignificant));
                    }
                }

                lblReportName.Text = history.ReportName;
                lbl2012.Text = "2015 n = " + year1Dataset.Count().ToString();
                lbl2011.Text = year2 + " n = " + year2Dataset.Count().ToString();

                lblYear1a.Text = lblYear1b.Text = lblYear1c.Text = lblYear1d.Text = lblYear1e.Text = "2015";
                lblYear2a.Text = lblYear2b.Text = lblYear2c.Text = lblYear2d.Text = lblYear2e.Text = year2;

                overalltrenddiv.Text = overalltrend(year.GetCompDimensions(year1Dataset, Dimensionslist, questions, africa).NedbankOverall.DimensionPositiveValue, year.GetCompDimensions(year2Dataset, Dimensionslist, questions, year2, africa).NedbankOverall.DimensionPositiveValue, year.GetCompDimensions(year3Dataset, Dimensionslist, questions, year3, africa).NedbankOverall.DimensionPositiveValue, year2, year3, year2Dataset.Count);
                dimensionradardiv.Text = DimensionChart(year2, year.GetCompDimensions(year1Dataset, Dimensionslist, questions, africa), compdimensionyear2, year1Dataset.Count, year2Dataset.Count, true, year2);
                HandleDimensions(compdimensionyear1, compdimensionyear2, year2, year2Dataset.Count);
                TopBottomStatements(year2, compdimensionyear1, compdimensionyear2, year2Dataset.Count(), Dimensionslist);
                LeastMostStatements(year2, compdimensionyear1, compdimensionyear2, year2Dataset.Count(), Dimensionslist);
                EngagementTrenddiv.InnerHtml = Engagementtrend(cluster, location, year2, year3);
                EngagementStatements(compdimensionyear1, compdimensionyear2, year2Dataset.Count);
                BottomDemographics(year1Dataset);

                filtersdiv.InnerHtml = "Report Name : " + history.ReportName;
                filtersdiv.InnerHtml += " | Cluster : " + history.Cluster;
                filtersdiv.InnerHtml += " | 2015 Division 1 : " + history.Y1Division1;
                filtersdiv.InnerHtml += " | 2015 Division 2 : " + history.Y1Division2;
                filtersdiv.InnerHtml += " | 2015 Division 3 : " + history.Y1Division3;
                filtersdiv.InnerHtml += " | 2015 Division 4 : " + history.Y1Division4;
                filtersdiv.InnerHtml += " | 2015 Division 5 : " + history.Y1Division5;
                filtersdiv.InnerHtml += " | 2015 Division 6 : " + history.Y1Division6;
                filtersdiv.InnerHtml += " | 2015 Branch : " + (history.Y1Branch.Length > 50 ? "Multiple Values Selected" : history.Y1Branch);
                filtersdiv.InnerHtml += " | " + year2 + " Division 1 : " + history.Y2Division1;
                filtersdiv.InnerHtml += " | " + year2 + " Division 2 : " + history.Y2Division2;
                filtersdiv.InnerHtml += " | " + year2 + " Division 3 : " + history.Y2Division3;
                filtersdiv.InnerHtml += " | " + year2 + " Division 4 : " + (history.Y2Division4.Length > 50 ? "Multiple Values Selected" : history.Y2Division4);
                filtersdiv.InnerHtml += " | " + year2 + " Division 5 : " + history.Y2Division5;
                filtersdiv.InnerHtml += " | " + year2 + " Division 6 : " + history.Y2Division6;
                filtersdiv.InnerHtml += " | " + year2 + " Branch : " + history.Y2Branch;
                filtersdiv.InnerHtml += " | Generational Classification : " + history.Classification;
                filtersdiv.InnerHtml += " | Occupational Level : " + history.OccupationalLevel;
                filtersdiv.InnerHtml += " | Length of Service : " + history.LOS;
                filtersdiv.InnerHtml += " | Management Passage : " + history.ManagementPassage;
                filtersdiv.InnerHtml += " | Gender : " + history.Gender;
                filtersdiv.InnerHtml += " | Race : " + history.Race;
                filtersdiv.InnerHtml += " | Job Family : " + history.JobFamily;
                filtersdiv.InnerHtml += " | Region : " + history.Region;
                filtersdiv.InnerHtml += " | Location : " + history.Location;
            }
        }

        private string overalltrend(double overall2012, double overall2011, double overall2010, string year2, string year3, double year2count)
        {
            lblYear1.Text = "2015 - ";
            lblYear2.Text = year2count < 10 ? "&nbsp;" : (year2 + " - ");
            lblYear3.Text = year2count < 10 ? "&nbsp;" : (year3 + " - ");
            lbl2012Score.Text = overall2012.ToString() + " %";
            lbl2011Score.Text = year2count < 10 ? "" : (overall2011.ToString() + " %");
            lbl2010Score.Text = year2count < 10 ? "" : (overall2010.ToString() + " %");
            return Charts.TrendChart("<point1 Data='" + overall2012 + "' Title='2015'/>" + (year2count < 10 ? "" : "<point2 Data='" + overall2011 + "' Title='" + year2 + "'/><point3 Data='" + overall2010 + "' Title='" + year3 + "'/>"), 600, 180, "overalltrend");
        }

        private string DimensionChart(string year2, ComprehensiveDimension compdimensionyear1, ComprehensiveDimension compdimensionyear2, double year1count, double year2count, bool comp, string byear)
        {
            StringBuilder DimensionChartHTML = new StringBuilder();
            StringBuilder DimensionCategoryXML = new StringBuilder();
            StringBuilder DimensionDataset1XML = new StringBuilder();
            StringBuilder DimensionDataset2XML = new StringBuilder();

            double result = 0;
            bool isSignificant = false;

            if (year2 != "None" && year2count >= 10) Significance.getSignificance(year1count, year2count, compdimensionyear1.NedbankOverall.DimensionPositiveValue, compdimensionyear2.NedbankOverall.DimensionPositiveValue, out result, out isSignificant);

            DimensionCategoryXML.Append("<category label='" + HTML.WordWrap(compdimensionyear1.NedbankOverall.DimensionName, 20) + (year2 != "None" && year2count >= 10 ? " (" + result.ToString("0.0") + (isSignificant ? "*" : "") + ")" : "") + "' /> ");

            DimensionDataset1XML.Append("<dataset seriesname='2015' color='22513d' anchorSides='6' showValues='1' anchorRadius='2' anchorBorderColor='22513d' anchorBgAlpha='0' showPlotBorder='1' plotBorderThickness='2' plotBorderColor='22513d' plotBorderAlpha='100' drawAnchors='1'>");
            if (year2 != "None" && year2count >= 10) DimensionDataset2XML.Append("<dataset seriesname='" + year2 + "' color='738639' anchorSides='10' anchorBorderColor='738639' anchorBgAlpha='0' anchorRadius='2' showPlotBorder='1' plotBorderThickness='2' plotBorderColor='738639' plotBorderAlpha='100' drawAnchors='1'>");

            DimensionDataset1XML.Append("<set value='" + compdimensionyear1.NedbankOverall.DimensionPositiveValue.ToString("0.0") + "' />");
            if (year2 != "None" && year2count >= 10) DimensionDataset2XML.Append("<set value='" + compdimensionyear2.NedbankOverall.DimensionPositiveValue.ToString("0.0") + "' />");

            foreach (ReportDimension dimension in compdimensionyear1.CompDimensions.Distinct().ToList().OrderBy(p => p.DimensionPositiveValue))
            {
                if (dimension.DimensionPositiveValue > 0 || dimension.DimensionNeutralValue > 0 || dimension.DimensionNegativeValue > 0)
                {
                    if (year2 != "None" && year2count >= 10)
                    {
                        if (!Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) || Convert.ToInt32(byear.ToLower() == "none" ? "0" : byear) >= 2012)
                        {
                            Significance.getSignificance(year1count, year2count, dimension.DimensionPositiveValue, compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue, out result, out isSignificant);
                            DimensionCategoryXML.Append("<category label='" + HTML.WordWrap(dimension.DimensionName, 20) + " (" + result.ToString("0.0") + (isSignificant ? "*" : "") + ")" + "' showPlotBorder='1'/> ");
                        }
                        else
                            DimensionCategoryXML.Append("<category label='" + HTML.WordWrap(dimension.DimensionName, 20) + "' showPlotBorder='1'/> ");
                    }
                    else
                        DimensionCategoryXML.Append("<category label='" + HTML.WordWrap(dimension.DimensionName, 20) + "' showPlotBorder='1'/> ");

                    DimensionDataset1XML.Append("<set value='" + dimension.DimensionPositiveValue.ToString("0.0") + "' />");

                    if (year2 != "None" && year2count >= 10)
                        if (!Generic.newDimensions().Contains(dimension.DimensionName.ToLower()) || Convert.ToInt32(byear.ToLower() == "none" ? "0" : byear) >= 2012)
                            DimensionDataset2XML.Append("<set value='" + compdimensionyear2.CompDimensions.Where(p => p.DimensionName == dimension.DimensionName).SingleOrDefault().DimensionPositiveValue.ToString("0.0") + "' />");
                        else
                            DimensionDataset2XML.Append("<set value='0.0' />");
                    else
                        DimensionDataset2XML.Append("<set value='0.0' />");
                }
            }

            DimensionDataset1XML.Append("</dataset>");
            if (year2 != "None" && year2count >= 10) DimensionDataset2XML.Append("</dataset>");

            return Charts.RadarChart(DimensionCategoryXML.ToString(), DimensionDataset1XML.ToString() + DimensionDataset2XML.ToString(), "Dimension", 640, 475);
        }

        private void HandleDimensions(ComprehensiveDimension compdimension1, ComprehensiveDimension compdimension2, string year2, double year2count)
        {
            int count = 1;

            List<StatementShift> dimshift = new List<StatementShift>();
            foreach (ReportDimension dims in compdimension1.CompDimensions)
                if (compdimension2.CompDimensions.Where(p => p.DimensionName == dims.DimensionName).SingleOrDefault() != null)
                    if (compdimension2.CompDimensions.Where(p => p.DimensionName == dims.DimensionName).SingleOrDefault().DimensionPositiveValue > 0)
                        dimshift.Add(new StatementShift(dims.DimensionName, dims.DimensionPositiveValue - compdimension2.CompDimensions.Where(p => p.DimensionName == dims.DimensionName).SingleOrDefault().DimensionPositiveValue, false));

            count = 1;
            foreach (StatementShift shift in dimshift.Distinct().ToList().OrderByDescending(p => p.Result).Take(3))
            {
                Label scorea = Page.FindControl("lblHigh" + count + "a") as Label;
                Label scoreb = Page.FindControl("lblHigh" + count + "b") as Label;
                Label name = Page.FindControl("lblHigh" + count + "Name") as Label;
                scorea.Text = year2count < 10 ? "&nbsp;" : compdimension1.CompDimensions.Where(p => p.DimensionName == shift.QuestionIndex).SingleOrDefault().DimensionPositiveValue.ToString("0.0");
                scoreb.Text = year2count < 10 ? "&nbsp;" : (!Generic.newDimensions().Contains(shift.QuestionIndex.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012 ? compdimension2.CompDimensions.Where(p => p.DimensionName == shift.QuestionIndex).SingleOrDefault().DimensionPositiveValue.ToString("0.0") : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                name.Text = year2count < 10 ? "&nbsp;" : shift.QuestionIndex;
                count++;
            }

            count = 1;
            foreach (StatementShift shift in dimshift.Distinct().ToList().OrderBy(p => p.Result).Take(3))
            {
                Label scorea = Page.FindControl("lblDiff" + count + "a") as Label;
                Label scoreb = Page.FindControl("lblDiff" + count + "b") as Label;
                Label name = Page.FindControl("lblDiff" + count + "Name") as Label;
                scorea.Text = year2count < 10 ? "&nbsp;" : compdimension1.CompDimensions.Where(p => p.DimensionName == shift.QuestionIndex).SingleOrDefault().DimensionPositiveValue.ToString("0.0");
                scoreb.Text = year2count < 10 ? "&nbsp;" : (!Generic.newDimensions().Contains(shift.QuestionIndex.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012 ? compdimension2.CompDimensions.Where(p => p.DimensionName == shift.QuestionIndex).SingleOrDefault().DimensionPositiveValue.ToString("0.0") : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                name.Text = year2count < 10 ? "&nbsp;" : shift.QuestionIndex;
                count++;
            }

            if (dimshift.Count() == 0)
            {
                lblHigh1a.Text = "&nbsp;";
                lblHigh2a.Text = "&nbsp;";
                lblHigh3a.Text = "&nbsp;";

                lblHigh1b.Text = "&nbsp;";
                lblHigh2b.Text = "&nbsp;";
                lblHigh3b.Text = "&nbsp;";

                lblHigh1Name.Text = "&nbsp;";
                lblHigh2Name.Text = "&nbsp;";
                lblHigh3Name.Text = "&nbsp;";

                lblDiff1a.Text = "&nbsp;";
                lblDiff2a.Text = "&nbsp;";
                lblDiff3a.Text = "&nbsp;";

                lblDiff1b.Text = "&nbsp;";
                lblDiff2b.Text = "&nbsp;";
                lblDiff3b.Text = "&nbsp;";

                lblDiff1Name.Text = "&nbsp;";
                lblDiff2Name.Text = "&nbsp;";
                lblDiff3Name.Text = "&nbsp;";
            }
        }

        private void TopBottomStatements(string year2, ComprehensiveDimension compdimensionyear1, ComprehensiveDimension compdimensionyear2, double year2count, List<Dimension> Dimensionslist)
        {
            int count = 1;

            foreach (Statement statement in compdimensionyear1.CompStatements.OrderByDescending(p => p.StatementPositiveValue).Take(3))
            {
                List<string> y1qi = new List<string>();
                if (year2 != "None") y1qi = (Dimensionslist.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive == true).SingleOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(Dimensionslist.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive == true).SingleOrDefault(), null) != null ? Dimensionslist.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive == true).SingleOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(Dimensionslist.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive == true).SingleOrDefault(), null).ToString().Split(',').ToList() : new List<string>());

                Label lblTop = Page.FindControl("lblTop" + count) as Label;
                Label lbl2012 = Page.FindControl("lblTop" + count + "2012") as Label;
                Label lbl2011 = Page.FindControl("lblTop" + count + "2011") as Label;

                lblTop.Text = statement.StatementName + (year2 != "None" && year2count >= 10 ? (!y1qi.Contains(statement.QuestionIndex.Replace("Q", "")) && year2 != "None" && y1qi.Count > 0 ? " (New Statement in Dimension)" : "") : "");

                lbl2012.Text = statement.StatementPositiveValue.ToString("0.0");
                lbl2012.Text = lbl2012.Text == "100.0" ? "100." : lbl2012.Text;
                lbl2011.Text = (year2 != "None" && year2count >= 10 && compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).Count() > 0 && (!Generic.newDimensions().Contains(statement.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementPositiveValue.ToString("0.0") : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).Count() > 0 ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementPositiveValue.ToString("0.0") : ""));
                lbl2011.Text = lbl2011.Text == "100.0" ? "100." : lbl2011.Text;
                count++;
            }
        }

        private void LeastMostStatements(string year2, ComprehensiveDimension compdimensionyear1, ComprehensiveDimension compdimensionyear2, double year2count, List<Dimension> Dimensionslist)
        {
            int count = 1;

            foreach (Statement statement in compdimensionyear1.CompStatements.OrderBy(p => p.StatementPositiveValue).Take(3))
            {
                List<string> y1qi = new List<string>();
                if (year2 != "None") y1qi = (Dimensionslist.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive == true).SingleOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(Dimensionslist.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive == true).SingleOrDefault(), null) != null ? Dimensionslist.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive == true).SingleOrDefault().GetType().GetProperty("DimensionQuestions" + year2).GetValue(Dimensionslist.Where(p => p.DimensionText == statement.DimensionName && p.Comprehensive == true).SingleOrDefault(), null).ToString().Split(',').ToList() : new List<string>());

                Label lblTop = Page.FindControl("lblImproved" + count) as Label;
                Label lbl2012 = Page.FindControl("lblImproved" + count + "2012") as Label;
                Label lbl2011 = Page.FindControl("lblImproved" + count + "2011") as Label;

                lblTop.Text = statement.StatementName + (year2 != "None" && year2count >= 10 ? (!y1qi.Contains(statement.QuestionIndex.Replace("Q", "")) && year2 != "None" && y1qi.Count > 0 ? " (New Statement in Dimension)" : "") : "");

                lbl2012.Text = statement.StatementPositiveValue.ToString("0.0");
                lbl2012.Text = lbl2012.Text == "100.0" ? "100." : lbl2012.Text;
                lbl2011.Text = (year2 != "None" && year2count >= 10 && compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null && (!Generic.newDimensions().Contains(statement.DimensionName.ToLower()) || Convert.ToInt32(year2.ToLower() == "none" ? "0" : year2) >= 2012) ? compdimensionyear2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementPositiveValue.ToString("0.0") : (compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault() != null ? compdimensionyear2.StatementsNotInc.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementPositiveValue.ToString("0.0") : ""));
                lbl2011.Text = lbl2011.Text == "100.0" ? "100." : lbl2011.Text;
                count++;
            }
        }

        private string Engagementtrend(string cluster, string location, string year2, string year3)
        {
            string y2012 = "";
            string y2011 = "";
            string y2010 = "";

            //if (cluster == "NEDBANK BUSINESS BANKING")
            //{
            //    y2012 = "70";
            //    if (year2 == "2013") y2011 = "69"; else if (year2 == "2012") y2011 = "70"; else if (year2 == "2011") y2011 = "62";
            //    if (year3 == "2012") y2010 = "70"; else if (year3 == "2011") y2010 = "62"; else if (year3 == "2010") y2010 = "67";
            //}
            //else if (cluster == "NEDBANK CAPITAL")
            //    if (location == "South Africa" || location == "South Africa|Not Specified" || location == "Not Specified|South Africa")
            //    {
            //        y2012 = "63";
            //        if (year2 == "2013") y2011 = "60"; else if (year2 == "2012") y2011 = "56"; else if (year2 == "2011") y2011 = "54";
            //        if (year3 == "2012") y2010 = "56"; else if (year3 == "2011") y2010 = "54"; else if (year3 == "2010") y2010 = "63";
            //    }
            //    else
            //    {
            //        y2012 = "62";
            //        if (year2 == "2013") y2011 = "61"; else if (year2 == "2012") y2011 = "56"; else if (year2 == "2011") y2011 = "53";
            //        if (year3 == "2012") y2010 = "56"; else if (year3 == "2011") y2010 = "53"; else if (year3 == "2010") y2010 = "61";
            //    }
            //else if (cluster == "TOTAL BALANCE SHEET MANAGEMENT")
            //{
            //    y2012 = "86";
            //    if (year2 == "2013") y2011 = "51"; else if (year2 == "2012") y2011 = "84"; else if (year2 == "2011") y2011 = "63";
            //    if (year3 == "2012") y2010 = "84"; else if (year3 == "2011") y2010 = "63"; else if (year3 == "2010") y2010 = "72";
            //}
            //else if (cluster == "GROUP FINANCE")
            //{
            //    y2012 = "73";
            //    if (year2 == "2013") y2011 = "74"; else if (year2 == "2012") y2011 = "74"; else if (year2 == "2011") y2011 = "70";
            //    if (year3 == "2012") y2010 = "74"; else if (year3 == "2011") y2010 = "70"; else if (year3 == "2010") y2010 = "76";
            //}
            //else if (cluster == "GROUP STRATEGIC PLANNING & ECONOMICS")
            //{
            //    y2012 = "79";
            //    if (year2 == "2013") y2011 = "89"; else if (year2 == "2012") y2011 = "81"; else if (year2 == "2011") y2011 = "63";
            //    if (year3 == "2012") y2010 = "81"; else if (year3 == "2011") y2010 = "63"; else if (year3 == "2010") y2010 = "75";
            //}
            //else if (cluster == "GMCCA TOTAL")
            //{
            //    y2012 = "63";
            //    if (year2 == "2013") y2011 = "67"; else if (year2 == "2012") y2011 = "69"; else if (year2 == "2011") y2011 = "66";
            //    if (year3 == "2012") y2010 = "69"; else if (year3 == "2011") y2010 = "66"; else if (year3 == "2010") y2010 = "62";
            //}
            //else if (cluster == "CE OFFICE / COSEC")
            //{
            //    y2012 = "";
            //    if (year2 == "2013") y2011 = "93"; else if (year2 == "2012") y2011 = "80"; else if (year2 == "2011") y2011 = "83";
            //    if (year3 == "2012") y2010 = "80"; else if (year3 == "2011") y2010 = "83"; else if (year3 == "2010") y2010 = "82";
            //}
            //else if (cluster == "GROUP RISK")
            //{
            //    y2012 = "78";
            //    if (year2 == "2013") y2011 = "72"; else if (year2 == "2012") y2011 = "73"; else if (year2 == "2011") y2011 = "64";
            //    if (year3 == "2012") y2010 = "73"; else if (year3 == "2011") y2010 = "64"; else if (year3 == "2010") y2010 = "68";
            //}
            //else if (cluster == "NEDBANK WEALTH")
            //    if (location == "South Africa" || location == "South Africa|Not Specified" || location == "Not Specified|South Africa")
            //    {
            //        y2012 = "67";
            //        if (year2 == "2013") y2011 = "65"; else if (year2 == "2012") y2011 = "68"; else if (year2 == "2011") y2011 = "61";
            //        if (year3 == "2012") y2010 = "68"; else if (year3 == "2011") y2010 = "61"; else if (year3 == "2010") y2010 = "66";
            //    }
            //    else
            //    {
            //        y2012 = "68";
            //        if (year2 == "2013") y2011 = "67"; else if (year2 == "2012") y2011 = "68"; else if (year2 == "2011") y2011 = "63";
            //        if (year3 == "2012") y2010 = "68"; else if (year3 == "2011") y2010 = "63"; else if (year3 == "2010") y2010 = "65";
            //    }
            //else if (cluster == "NEDBANK CORPORATE")
            //{
            //    y2012 = "74";
            //    if (year2 == "2013") y2011 = "74"; else if (year2 == "2012") y2011 = "70"; else if (year2 == "2011") y2011 = "62";
            //    if (year3 == "2012") y2010 = "70"; else if (year3 == "2011") y2010 = "62"; else if (year3 == "2010") y2010 = "71";
            //}
            //else if (cluster == "NEDBANK RETAIL")
            //{
            //    y2012 = "73";
            //    if (year2 == "2013") y2011 = "72"; else if (year2 == "2012") y2011 = "73"; else if (year2 == "2011") y2011 = "64";
            //    if (year3 == "2012") y2010 = "73"; else if (year3 == "2011") y2010 = "64"; else if (year3 == "2010") y2010 = "70";
            //}
            //else if (cluster == "GROUP HUMAN RESOURCES")
            //{
            //    y2012 = "72";
            //    if (year2 == "2013") y2011 = "69"; else if (year2 == "2012") y2011 = "84"; else if (year2 == "2011") y2011 = "68";
            //    if (year3 == "2012") y2010 = "84"; else if (year3 == "2011") y2010 = "68"; else if (year3 == "2010") y2010 = "78";
            //}
            //else if (cluster == "ENTERPRISE GOVERNANCE & COMPLIANCE")
            //{
            //    y2012 = "83";
            //    if (year2 == "2013") y2011 = "87"; else if (year2 == "2012") y2011 = "80"; else if (year2 == "2011") y2011 = "65";
            //    if (year3 == "2012") y2010 = "80"; else if (year3 == "2011") y2010 = "65"; else if (year3 == "2010") y2010 = "62";
            //}
            //else if (cluster == "GROUP TECHNOLOGY")
            //{
            //    y2012 = "69";
            //    if (year2 == "2013") y2011 = "68"; else if (year2 == "2012") y2011 = "63"; else if (year2 == "2011") y2011 = "56";
            //    if (year3 == "2012") y2010 = "63"; else if (year3 == "2011") y2010 = "56"; else if (year3 == "2010") y2010 = "68";
            //}
            //else if (cluster == "TOTAL AFRICA")
            //{
            //    if (location == "South Africa" || location == "South Africa|Not Specified" || location == "Not Specified|South Africa")
            //    {
            //        y2012 = "74";
            //        if (year2 == "2013") y2011 = "49"; else if (year2 == "2012") y2011 = ""; else if (year2 == "2011") y2011 = "";
            //        if (year3 == "2012") y2010 = ""; else if (year3 == "2011") y2010 = ""; else if (year3 == "2010") y2010 = "";
            //    }
            //    else
            //    {
            //        y2012 = "59";
            //        if (year2 == "2013") y2011 = "49"; else if (year2 == "2012") y2011 = ""; else if (year2 == "2011") y2011 = "";
            //        if (year3 == "2012") y2010 = ""; else if (year3 == "2011") y2010 = ""; else if (year3 == "2010") y2010 = "";
            //    }                
            //}
            //else
            //{
            y2012 = "70";
            if (year2 == "2014") y2011 = "72"; else if (year2 == "2013") y2011 = "72"; else if (year2 == "2012") y2011 = "71"; else if (year2 == "2011") y2011 = "63";
            if (year3 == "2013") y2010 = "72"; if (year3 == "2012") y2010 = "71"; else if (year3 == "2011") y2010 = "63"; else if (year3 == "2010") y2010 = "67";
            //}

            lblEngagement2012.Text = y2012.ToString() + " %";
            lblEngagement2011.Text = (y2011 != "" ? y2011.ToString() + " %" : "-");
            lblEngagement2010.Text = (y2010 != "" ? y2010.ToString() + " %" : "-");
            lblEngYear1.Text = "2015 - ";
            lblEngYear2.Text = (y2011 != "" ? year2 + " - " : "");
            lblEngYear3.Text = (y2010 != "" ? year3 + " - " : "");
            return Charts.TrendChart("<point1 Data='" + y2012 + "' Title='2015'/><point2 Data='" + y2011 + "' Title='" + year2 + "'/><point3 Data='" + y2010 + "' Title='" + year3 + "'/>", 600, 180, "overalltrend");
        }

        private void EngagementStatements(ComprehensiveDimension compdimension1, ComprehensiveDimension compdimension2, double year2count)
        {
            EngagementStatementsdiv.InnerHtml = "<ul class='myClass'>";

            foreach (Statement statement in compdimension1.CompStatements.Where(p => p.DimensionName == "Engagement").Distinct().ToList().OrderBy(p => p.StatementPositiveValue))
                EngagementStatementsdiv.InnerHtml += "<li>" + statement.StatementName + " <span style='color:#c4df9c'> | " + statement.StatementPositiveValue + "%</span>" + " <span style='color:#778d1e'>" + (year2count < 10 ? "&nbsp;" : " | " + compdimension2.CompStatements.Where(p => p.QuestionIndex == statement.QuestionIndex).SingleOrDefault().StatementPositiveValue + "%") + "</span>" + "</li>";

            EngagementStatementsdiv.InnerHtml += "</ul>";
        }

        private void BottomDemographics(List<Year> year1dataset)
        {            
            lblMale.Text = Math.Round((decimal)year1dataset.Where(p => p.Gender == "Male").Count() / (decimal)year1dataset.Count() * 100, 0, MidpointRounding.AwayFromZero).ToString() + "%";
            lblFemale.Text = Math.Round((decimal)year1dataset.Where(p => p.Gender == "Female").Count() / (decimal)year1dataset.Count() * 100, 0, MidpointRounding.AwayFromZero).ToString() + "%";

            RaceChartsDiv.InnerHtml = "<table><tr>";
            RaceChartsDiv.InnerHtml += "<td align='center' width='200px'>" + Charts.RaceChart(Math.Round((double)year1dataset.Where(p => p.Race == "African").Count() / (double)year1dataset.Count() * 100, 0, MidpointRounding.AwayFromZero), 200, 200, "AFRICAN") + "</td>";
            RaceChartsDiv.InnerHtml += "<td align='center' width='200px'>" + Charts.RaceChart(Math.Round((double)year1dataset.Where(p => p.Race == "Coloured").Count() / (double)year1dataset.Count() * 100, 0, MidpointRounding.AwayFromZero), 200, 200, "COLOURED") + "</td>";
            RaceChartsDiv.InnerHtml += "<td align='center' width='200px'>" + Charts.RaceChart(Math.Round((double)year1dataset.Where(p => p.Race == "Indian").Count() / (double)year1dataset.Count() * 100, 0, MidpointRounding.AwayFromZero), 200, 200, "INDIAN") + "</td>";
            RaceChartsDiv.InnerHtml += "<td align='center' width='200px'>" + Charts.RaceChart(Math.Round((double)year1dataset.Where(p => p.Race == "White").Count() / (double)year1dataset.Count() * 100, 0, MidpointRounding.AwayFromZero), 200, 200, "WHITE") + "</td>";
            RaceChartsDiv.InnerHtml += "</tr></table>";

            string points = "";
            string[] pos = new string[] { "Left", "Right" };
            string[] colour = new string[] { "00533E", "738639", "74A18E", "BCE18D", "867A69", "C8F475" };
            int count = 1;
            foreach (string age in year1dataset.OrderBy(p => p.Age).Select(p => p.Age == null ? "Not Specified" : p.Age).Distinct().ToList())
            {
                if (year1dataset.Where(p => p.Age == age).Count() > 0)
                    points += "<graph" + count + " Data='" + Math.Round((decimal)year1dataset.Where(p => p.Age == age).Count() / year1dataset.Count * 100, 0, MidpointRounding.AwayFromZero) + "' Title='" + age + "' TitleColour='#6C6575' BarColour='#" + colour[count - 1] + "' TitlePos='" + pos[count % 2] + "'/>";
                count++;
            }

            AgeChartdiv.InnerHtml = Charts.StackedPieChart(points, 400, 250, "Age");
        }
    }
}